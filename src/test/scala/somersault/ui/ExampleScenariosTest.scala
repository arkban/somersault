package somersault.ui

import java.io.File
import org.apache.commons.io.FileUtils
import org.testng.annotations.Test
import somersault.util.persist.Persister
import somersault.util.{Assert, FilenameUtils2}

class ExampleScenariosTest {

  /**
   * Writes out the example scenarios and reads them back in one by one. The writing and reading successfully is the
   * testing
   */
  @Test def writeAndRead() {
    val path = new File(
      FilenameUtils2.concat(
        FilenameUtils2.tempPath.getPath, this.getClass.getSimpleName, "writeAndRead"))

    FileUtils.forceMkdir(path)

    ExampleScenarios.write(path)

    path.listFiles.filter(_.isFile).foreach(
      f => {
        val scenario = Persister.readScenarioData(f)
        Assert.isTrue(f.getName.contains(scenario.name), f.getName)
      })
  }
}