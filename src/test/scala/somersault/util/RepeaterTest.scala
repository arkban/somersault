package somersault.util

import org.testng.annotations.Test

class RepeaterTest {

  @Test def neverFails() {
    var runCount = 0
    val f = () => runCount += 1
    new Repeater(f, 10).repeat()
    Assert.areEqual(runCount, 1)
  }

  @Test def alwaysFails() {
    val maxAttempts = 10
    val f = () => throw new Exception
    try {
      new Repeater(f, maxAttempts).repeat()
      Assert.fail("Expected exception")
    }
    catch {
      case ex: RepeaterException => Assert.areEqual(ex.attempts, maxAttempts)
    }
  }

  @Test def midFailure() {
    val maxAttempts = 10
    val failAttempt = maxAttempts / 2
    try {
      var runCount = 0
      val f = () => {
        if (runCount <= failAttempt) throw new Exception()
        runCount += 1
      }
      new Repeater(f, maxAttempts).repeat()
      Assert.areEqual(failAttempt, runCount)
    }
    catch {
      case ex: RepeaterException => Assert.areEqual(ex.attempts, 10)
    }
  }
}
