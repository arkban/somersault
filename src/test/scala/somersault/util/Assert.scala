package somersault.util

/**
 * TestNG's asserts are nice but very limited (IMHO). As a fan of MbUnit, with its vast array of Assert options, this
 * class exists to wrap TestNG's Assert class with a nicer interface, akin to MbUnit. At the very least the method
 * names are shorter.
 *
 * The implementation of all methods aims to reduce the call stack as much as possible, so many implementations
 * redirect to the most specific method that actually does real work.
 */
object Assert {

  //-- Array
  def areEqual(actual: Array[Byte], expected: Array[Byte]) {
    org.testng.Assert.assertEquals(actual, expected, null)
  }

  def areEqual(actual: Array[Byte], expected: Array[Byte], message: String) {
    org.testng.Assert.assertEquals(actual, expected, message)
  }

  def notEquals(actual: Array[Byte], expected: Array[Byte], message: String) {
    if (actual.equals(expected))
      Assert.fail(format(" Arrays are not equal", message))
  }

  //-- Boolean

  def isTrue(value: Boolean) { org.testng.Assert.assertTrue(value, null) }

  def isTrue(value: Boolean, message: String) { org.testng.Assert.assertTrue(value, message) }

  def isFalse(value: Boolean) { org.testng.Assert.assertFalse(value, null) }

  def isFalse(value: Boolean, message: String) { org.testng.Assert.assertFalse(value, message) }

  def areEqual(actual: Boolean, expected: Boolean) { org.testng.Assert.assertEquals(actual, expected, null) }

  def areEqual(actual: Boolean, expected: Boolean, message: String) {
    org.testng.Assert.assertEquals(actual, expected, message)
  }

  def notEquals(actual: Boolean, expected: Boolean) { notEquals(actual, expected, null) }

  def notEquals(actual: Boolean, expected: Boolean, message: String) {
    if (actual.equals(expected))
      Assert.fail(formatActualIsExpected(Predef.boolean2Boolean(expected), message))
  }

  //-- Byte

  def areEqual(actual: Byte, expected: Byte) { org.testng.Assert.assertEquals(actual, expected, null) }

  def areEqual(actual: Byte, expected: Byte, message: String) {
    org.testng.Assert.assertEquals(actual, expected, message)
  }

  def areEqual(actual: Byte, expected: Byte, delta: Byte) { areEqual(actual, expected, delta, null) }

  def areEqual(actual: Byte, expected: Byte, delta: Byte, message: String) {
    if (math.abs(actual - expected) > delta)
      Assert.fail(formatActualNotExpected(Predef.byte2Byte(actual), Predef.byte2Byte(expected), message))
  }

  def notEquals(actual: Byte, expected: Byte) { notEquals(actual, expected, 0, null) }

  def notEquals(actual: Byte, expected: Byte, message: String) { notEquals(actual, expected, 0, message) }

  def notEquals(actual: Byte, expected: Byte, delta: Byte) { notEquals(actual, expected, delta, null) }

  def notEquals(actual: Byte, expected: Byte, delta: Byte, message: String) {
    if (math.abs(expected - actual) <= delta)
      Assert.fail(formatActualIsExpected(Predef.byte2Byte(expected), message))
  }

  //-- Double

  def areEqual(actual: Double, expected: Double) { org.testng.Assert.assertEquals(actual, expected, 0d, null) }

  def areEqual(actual: Double, expected: Double, message: String) {
    org.testng.Assert.assertEquals(actual, expected, 0d, message)
  }

  def areEqual(actual: Double, expected: Double, delta: Double) {
    org.testng.Assert.assertEquals(actual, expected, delta, null)
  }

  def areEqual(actual: Double, expected: Double, delta: Double, message: String) {
    org.testng.Assert.assertEquals(actual, expected, delta, message)
  }

  def notEquals(actual: Double, expected: Double) { notEquals(actual, expected, 0d, null) }

  def notEquals(actual: Double, expected: Double, message: String) { notEquals(actual, expected, 0d, message) }

  def notEquals(actual: Double, expected: Double, delta: Double) { notEquals(actual, expected, delta, null) }

  def notEquals(actual: Double, expected: Double, delta: Double, message: String) {
    // handle infinity specially since subtracting to infinite values gives NaN and the the following test fails
    if (java.lang.Double.isInfinite(expected)) {
      if (expected == actual)
        Assert.fail(formatActualIsExpected(Predef.double2Double(expected), message))
    }
    // Because comparison with NaN always returns false
    else if (math.abs(expected - actual) <= delta) {
      Assert.fail(formatActualIsExpected(Predef.double2Double(expected), message))
    }
  }

  //-- Float

  def areEqual(actual: Float, expected: Float) { org.testng.Assert.assertEquals(actual, expected, null) }

  def areEqual(actual: Float, expected: Float, message: String) {
    org.testng.Assert.assertEquals(actual, expected, message)
  }

  def areEqual(actual: Float, expected: Float, delta: Float) { areEqual(actual, expected, delta, null) }

  def areEqual(actual: Float, expected: Float, delta: Float, message: String) {
    if (math.abs(actual - expected) > delta)
      Assert.fail(formatActualNotExpected(Predef.float2Float(actual), Predef.float2Float(expected), message))
  }

  def notEquals(actual: Float, expected: Float) { notEquals(actual, expected, 0L, null) }

  def notEquals(actual: Float, expected: Float, message: String) { notEquals(actual, expected, 0L, message) }

  def notEquals(actual: Float, expected: Float, delta: Float) { notEquals(actual, expected, delta, null) }

  def notEquals(actual: Float, expected: Float, delta: Float, message: String) {
    if (math.abs(expected - actual) <= delta)
      Assert.fail(formatActualIsExpected(Predef.float2Float(expected), message))
  }

  //-- Int

  def areEqual(actual: Int, expected: Int) { org.testng.Assert.assertEquals(actual, expected, null) }

  def areEqual(actual: Int, expected: Int, message: String) {
    org.testng.Assert.assertEquals(actual, expected, message)
  }

  def areEqual(actual: Int, expected: Int, delta: Int) { areEqual(actual, expected, delta, null) }

  def areEqual(actual: Int, expected: Int, delta: Int, message: String) {
    if (math.abs(actual - expected) > delta)
      Assert.fail(formatActualNotExpected(Predef.int2Integer(actual), Predef.int2Integer(expected), message))
  }

  def notEquals(actual: Int, expected: Int) { notEquals(actual, expected, 0, null) }

  def notEquals(actual: Int, expected: Int, message: String) { notEquals(actual, expected, 0, message) }

  def notEquals(actual: Int, expected: Int, delta: Int) { notEquals(actual, expected, delta, null) }

  def notEquals(actual: Int, expected: Int, delta: Int, message: String) {
    if (math.abs(expected - actual) <= delta)
      Assert.fail(formatActualIsExpected(Predef.int2Integer(expected), message))
  }

  //-- Long

  def areEqual(actual: Long, expected: Long) { org.testng.Assert.assertEquals(actual, expected, null) }

  def areEqual(actual: Long, expected: Long, message: String) {
    org.testng.Assert.assertEquals(actual, expected, message)
  }

  def areEqual(actual: Long, expected: Long, delta: Long) { areEqual(actual, expected, delta, null) }

  def areEqual(actual: Long, expected: Long, delta: Long, message: String) {
    if (math.abs(actual - expected) > delta)
      Assert.fail(formatActualNotExpected(Predef.long2Long(actual), Predef.long2Long(expected), message))
  }

  def notEquals(actual: Long, expected: Long) { notEquals(actual, expected, 0L, null) }

  def notEquals(actual: Long, expected: Long, message: String) { notEquals(actual, expected, 0L, message) }

  def notEquals(actual: Long, expected: Long, delta: Long) { notEquals(actual, expected, delta, null) }

  def notEquals(actual: Long, expected: Long, delta: Long, message: String) {
    if (math.abs(expected - actual) <= delta)
      Assert.fail(formatActualIsExpected(Predef.long2Long(expected), message))
  }

  //-- Object

  def areEqual(actual: Object, expected: Object) { org.testng.Assert.assertEquals(actual, expected, null) }

  def areEqual(actual: Object, expected: Object, message: String) {
    org.testng
      .Assert
      .assertEquals(actual, expected, message)
  }

  def notEquals(actual: Object, expected: Object) { notEquals(actual, expected, null) }

  def notEquals(actual: Object, expected: Object, message: String) {
    if (actual.equals(expected))
      Assert.fail(formatActualIsExpected(expected, message))
  }

  //-- Short

  def areEqual(actual: Short, expected: Short) { org.testng.Assert.assertEquals(actual, expected, null) }

  def areEqual(actual: Short, expected: Short, message: String) {
    org.testng.Assert.assertEquals(actual, expected, message)
  }

  def areEqual(actual: Short, expected: Short, delta: Short) { areEqual(actual, expected, delta, null) }

  def areEqual(actual: Short, expected: Short, delta: Short, message: String) {
    if (math.abs(actual - expected) > delta)
      Assert.fail(formatActualNotExpected(Predef.short2Short(actual), Predef.short2Short(expected), message))
  }

  def notEquals(actual: Short, expected: Short) { notEquals(actual, expected, 0.asInstanceOf[Short], null) }

  def notEquals(actual: Short, expected: Short, message: String) { notEquals(actual, expected, 0, message) }

  def notEquals(actual: Short, expected: Short, delta: Short) { notEquals(actual, expected, delta, null) }

  def notEquals(actual: Short, expected: Short, delta: Short, message: String) {
    if (math.abs(expected - actual) <= delta)
      Assert.fail(formatActualIsExpected(Predef.short2Short(expected), message))
  }

  //-- String

  def areEqual(actual: String, expected: String) { org.testng.Assert.assertEquals(actual, expected, null) }

  def areEqual(actual: String, expected: String, message: String) {
    org.testng.Assert.assertEquals(actual, expected, message)
  }

  def notEquals(actual: String, expected: String) { notEquals(actual, expected, null) }

  def notEquals(actual: String, expected: String, message: String) {
    if (actual.equals(expected))
      Assert.fail(formatActualIsExpected(expected, message))
  }

  def same(actual: Any, expected: Any) { org.testng.Assert.assertSame(actual, expected) }

  def same(actual: Any, expected: Any, message: String) { org.testng.Assert.assertSame(actual, expected, message) }

  //-- Fail

  def fail() { org.testng.Assert.fail() }

  def fail(message: String) { org.testng.Assert.fail(message) }

  def fail(message: String, realCause: Throwable) { org.testng.Assert.fail(message, realCause) }

  /**
   * A port of Assert.format()
   */
  private def formatActualNotExpected(actual: Object, expected: Object, message: String): String = {
    val formatted = if (null != message) message + " " else ""
    formatted + "expected:<" + expected + "> but was:<" + actual + ">"
  }

  /**
   * Inversion of Assert.format()
   */
  private def formatActualIsExpected(expected: Object, message: String): String = {
    val formatted = if (null != message) message + " " else ""
    formatted + "did not expect:<" + expected + ">"
  }

  private def format(standardMessage: String, message: String): String = {
    val formatted = if (null != message) message + " " else ""
    formatted + standardMessage
  }
}