package somersault.util

import somersault.util.FilenameUtils2.PathResolutionException
import org.testng.annotations.Test

class FilenameUtils2Test
{
  @Test
  def testGetRelativePathsUnix( )
  {
    Assert.areEqual( "stuff/xyz.dat", FilenameUtils2.getRelativePath( "/var/data/stuff/xyz.dat", "/var/data/", "/"))
    Assert.areEqual( "../../b/c", FilenameUtils2.getRelativePath( "/a/b/c", "/a/x/y/", "/"))
    Assert.areEqual( "../../b/c", FilenameUtils2.getRelativePath( "/m/n/o/a/b/c", "/m/n/o/a/x/y/", "/"))
  }

  @Test
  def testGetRelativePathFileToFile( )
  {
    val target = "C:\\Windows\\Boot\\Fonts\\chs_boot.ttf"
    val base = "C:\\Windows\\Speech\\Common\\sapisvr.exe"

    val relPath = FilenameUtils2.getRelativePath( target, base, "\\")
    Assert.areEqual( "..\\..\\Boot\\Fonts\\chs_boot.ttf", relPath)
  }

  @Test
  def testGetRelativePathDirectoryToFile( )
  {
    val target = "C:\\Windows\\Boot\\Fonts\\chs_boot.ttf"
    val base = "C:\\Windows\\Speech\\Common\\"

    val relPath = FilenameUtils2.getRelativePath( target, base, "\\")
    Assert.areEqual( "..\\..\\Boot\\Fonts\\chs_boot.ttf", relPath)
  }

  @Test
  def testGetRelativePathFileToDirectory( )
  {
    val target = "C:\\Windows\\Boot\\Fonts"
    val base = "C:\\Windows\\Speech\\Common\\foo.txt"

    val relPath = FilenameUtils2.getRelativePath( target, base, "\\")
    Assert.areEqual( "..\\..\\Boot\\Fonts", relPath)
  }

  @Test
  def testGetRelativePathDirectoryToDirectory( )
  {
    val target = "C:\\Windows\\Boot\\"
    val base = "C:\\Windows\\Speech\\Common\\"
    val expected = "..\\..\\Boot"

    val relPath = FilenameUtils2.getRelativePath( target, base, "\\")
    Assert.areEqual( expected, relPath)
  }

  @Test
  def testGetRelativePathDifferentDriveLetters( )
  {
    val target = "D:\\sources\\recovery\\RecEnv.exe"
    val base = "C:\\Java\\workspace\\AcceptanceTests\\Standard test data\\geo\\"

    try
    {
      FilenameUtils2.getRelativePath( target, base, "\\")
      Assert.fail( )
    }
    catch
    {
      case ex: PathResolutionException => // expected exception
    }
  }
}