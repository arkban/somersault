package somersault.util

import java.io.File
import org.apache.commons.io.FileUtils
import org.springframework.dao.DataAccessException
import org.testng.annotations.Test

class MetaDataAccessObjectTest {

  @Test def exists() {
    val dao = new MockDAO("test1")

    Assert.isFalse(dao.exists(dao.dataSource, "hello"))
    dao.set(dao.dataSource, "hello", "world")
    Assert.isTrue(dao.exists(dao.dataSource, "hello"))
  }

  @Test def setAndGet1() {
    val dao = new MockDAO("getAndSet3")

    dao.set(dao.dataSource, "hello", "world")
    Assert.isTrue(dao.exists(dao.dataSource, "hello"))
    Assert.areEqual(dao.get(dao.dataSource, "hello"), "world")
    dao.set(dao.dataSource, "hello", "somersault")
    Assert.isTrue(dao.exists(dao.dataSource, "hello"))
    Assert.areEqual(dao.get(dao.dataSource, "hello"), "somersault")
  }

  @Test def setAndGet2() {
    val dao = new MockDAO("getAndSet2")

    try {
      dao.get(dao.dataSource, "hello")
    }
    catch {
      case ex: DataAccessException =>
    }
  }

  private class MockDAO(testName: String) extends H2DataAccessObject with MetaDataAccessObject {

    val dataSource = {
      val testPath = FilenameUtils2.concat(FilenameUtils2.tempPath.getPath, this.getClass.getSimpleName, testName)
      FileUtils.forceMkdir(new File(testPath))
      createDataSource(new File(FilenameUtils2.concat(testPath, "database")), deleteIfExists = false)
    }
  }

}