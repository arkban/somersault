package somersault.util

import org.testng.annotations.Test
import org.apache.commons.io.FileUtils
import java.io.File

class VersionedDataAccessObjectTest
{
  @Test def readAndWriteVersion1() =
    {
      val dao = new MockVersionedDAO( "readAndWriteVersion1" )

      val version = new Version( 3, 5, 7 )
      dao.writeVersion( dao.dataSource, version )

      Assert.isTrue( version == dao.readVersion( dao.dataSource ) )
    }

  @Test def readAndWriteVersion2() =
    {
      val dao = new MockVersionedDAO( "readAndWriteVersion2" )

      val version1 = new Version( 3, 5, 7 )
      dao.writeVersion( dao.dataSource, version1 )
      val version2 = new Version( 4, 5, 8 )
      dao.writeVersion( dao.dataSource, version2 )

      Assert.isTrue( version2 == dao.readVersion( dao.dataSource ) )
    }

  private class MockVersionedDAO( testName: String ) extends H2DataAccessObject with VersionedDataAccessObject
  {
    val dataSource =
    {
      val testPath = FilenameUtils2.concat( FilenameUtils2.tempPath.getPath, this.getClass.getSimpleName, testName )
      FileUtils.forceMkdir( new File( testPath ) )
      createDataSource( new File( FilenameUtils2.concat( testPath, "database" ) ), false )
    }

    def schemaVersion = new Version( 1, 2, 3 )
  }
}