package somersault.util

import java.io.File
import java.security.MessageDigest
import nu.xom.Element
import org.testng.annotations.Test
import persist.{PersisterSuite, PersisterContext}

class HashInfoTest {

  @Test def checkAlgorithms() {
    check(HashInfo.md5)
    check(HashInfo.sha1)
  }

  @Test def persist() {
    Seq(HashInfo.md5, HashInfo.sha1).foreach(
      hashInfo => {
        val context = new PersisterContext(PersisterSuite.create(), new File("temp"))
        val pw = new HashInfoPersister
        val element = new Element("element")
        pw.write(context, hashInfo, element, dynamic = true)

        Assert.isTrue(pw.canRead(context, element))
        val newHashInfo: HashInfo = pw.read(context, element)
        Assert.areEqual(hashInfo.name, newHashInfo.name)
        Assert.areEqual(hashInfo.size, newHashInfo.size)
        check(newHashInfo)
      })
  }

  private def check(hashInfo: HashInfo) {
    // this should throw if the name is  not right
    val md = MessageDigest.getInstance(hashInfo.name)
    Assert.areEqual(md.getAlgorithm, hashInfo.name) // maybe redundant, but here to eliminate aliasing
    Assert.areEqual(md.getDigestLength, hashInfo.size)
  }
}