package somersault.core.mock

import java.io._
import somersault.core.filesystems._

/**
 * A specialized InputStream whose data is initialized to the data in the provided MockSomerFile.
 */

class MockInputStream( file: MockSomerFile ) extends ByteArrayInputStream( file.data.getBytes )
{
}