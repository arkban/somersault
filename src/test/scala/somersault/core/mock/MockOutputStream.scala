package somersault.core.mock

import java.io._
import somersault.core.filesystems._

/**
 * A specialized OutputStream that writes the data collected to a MockSomerFile on close().
 */

class MockOutputStream(file: MockSomerFile) extends ByteArrayOutputStream {

  override def close() {
    super.close()
    // only overwrite the data if it wasn't already filled. (this can occur by manually setting the data
    // member immediately after construction. that is typically done to ensure that the file is fully formed before
    // being added to the FakeFileSystem via write())
    if (file.data.isEmpty)
      file.data = new String(toByteArray)
  }
}