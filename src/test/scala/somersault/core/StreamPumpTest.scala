package somersault.core

import java.io.{ByteArrayOutputStream, ByteArrayInputStream}
import org.testng.Assert
import org.testng.annotations.Test
import somersault.core.actions.containers.InMemoryActionContainerBuilder

class StreamPumpTest {

  @Test def test1() {
    val data = Array(
      "a",
      "somersault",
      "this has spaces in it",
      "extra-very-big-long-test " * 1000)
    val bufferSizes = Array(10, 1024, 4096, 1, 3, 5, 7, 13)
    val actionContainer = new InMemoryActionContainerBuilder()
    val action = actionContainer.addTransferAction(actionContainer.root, "1")

    bufferSizes.foreach(
      bs => {
        val pump = new StreamPump(bs)

        data.foreach(
          s => {
            val buffer = s.getBytes
            val is = new ByteArrayInputStream(buffer)
            val os = new ByteArrayOutputStream()
            pump.pump(is, os, new ActionStreamPumpData(true, None, action, Nil))

            Assert.assertEquals(buffer, os.toByteArray, "Size: " + bs + ", Data: " + s)
          })
      }

    )
  }

}