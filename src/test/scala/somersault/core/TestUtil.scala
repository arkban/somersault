package somersault.core

import java.io.{File, InputStream, ByteArrayOutputStream}
import java.security.DigestInputStream
import logahawk.SimpleLogger
import org.apache.commons.io.FileUtils
import org.apache.log4j.{Level, ConsoleAppender}
import somersault.util._

/**
 * Utility class for unit tests.
 */
object TestUtil {

  /**
   * A logger suitable for use in unit tests. This will log to standard out.
   */
  val logger = {
    val logger = new SimpleLogger()
    LoggerUtil.addStandardFormatters(logger)

    val rootAppender = new ConsoleAppender(LoggerUtil.rootLog4jLayout)
    rootAppender.setThreshold(Level.INFO)
    LoggerUtil.createLog4jLogger(None, rootAppender, additive = true)

    val somerAppender = new ConsoleAppender(LoggerUtil.somerLog4jLayout)
    somerAppender.setThreshold(Level.DEBUG)
    val log4j = LoggerUtil.createLog4jLogger(Some(LoggerUtil.somerLoggerName), somerAppender, additive = false)

    logger.getListenerContainer.add(new Log4jListener(log4j))
    logger
  }

  /**
   * Reads the contents of the file, and hashes it, returning the raw contents as a string and the Hash. Note this
   * method Asserts that the bytes read from the raw file can be put into a string, so if that fails then your unit
   * test may need to be changed.
   */
  def readContents(hashInfo: HashInfo, input: InputStream): (Hash, String) = {
    val (hash, buffer) = readRawContents(hashInfo, input)
    val str = new String(buffer)
    Assert.areEqual(Hash.toHex(buffer), Hash.toHex(str.getBytes), "this failure means unit test needs to be changed")

    new Tuple2(hash, str)
  }

  /** Reads the contents of the file, and hashes it, returning the Hash and the raw file contents. */
  def readRawContents(hashInfo: HashInfo, input: InputStream): (Hash, Array[Byte]) = {
    val digestIn = new DigestInputStream(input, hashInfo.createDigest())
    val output = new ByteArrayOutputStream()

    new StreamPump().pump(digestIn, output, new StreamPumpData(true, None))

    (new Hash(hashInfo, digestIn.getMessageDigest.digest), output.toByteArray)
  }

  /**
   * Creates and returns a temporary path for this unit test.
   */
  def createTestPath(clazz: Class[_], testName: String): File = {
    val path = new File(FilenameUtils2.concat(FilenameUtils2.tempPath.getPath, clazz.getSimpleName, testName))
    FileUtils.forceMkdir(path)
    path
  }
}