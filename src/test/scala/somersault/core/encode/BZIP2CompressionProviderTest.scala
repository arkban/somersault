package somersault.core.encode

import java.lang.String
import org.testng.annotations.Test
import nu.xom.Element
import somersault.util.Assert
import somersault.core.filters.{BooleanFilterPersister, BooleanFilter, SomerFilter}
import somersault.util.persist.{PersisterContext, PersisterContainer}
import java.io.File

class BZIP2CompressionProviderTest extends CompressionProviderTest
{
  protected def getEncodingProviders( testName: String, filter: SomerFilter) =
    new BZIP2CompressionProvider( filter) :: Nil

  @Test def persist( )
  {
    val provider = new BZIP2CompressionProvider( new BooleanFilter( true), "changed")
    val pwc = new PersisterContainer
    pwc.add( classOf[ BooleanFilter ], new BooleanFilterPersister)
    val context = new PersisterContext( pwc, new File( "temp" ))
    val pw = new BZIP2CompressionProviderPersister

    val element = new Element( "provider")
    pw.write( context, provider, element, true)

    Assert.isTrue( pw.canRead( context, element))
    val newProvider: BZIP2CompressionProvider = pw.read( context, element)
    Assert.areEqual( provider.extension, newProvider.extension)
  }
}