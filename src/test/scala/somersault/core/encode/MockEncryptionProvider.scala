package somersault.core.encode

import somersault.core.filesystems.SomerFile
import java.lang.String
import java.io.{OutputStream, InputStream}

/**
 * An EncodingProvider that mimics NullEncryptioProvider in behavior but pretends to be a real EncryptionProvider.
 */
class MockEncryptionProvider(val key: EncryptionKey) extends EncryptionProvider {

  val name = "Mock Encryption"

  def encode(parentPath: Iterable[String], file: SomerFile) = new MockEncryptionResult(file.name)

  def encodeDir(name: String) = new MockEncryptionResult(name)

  def encodeFile(name: String) = new MockEncryptionResult(name)

  def decode(newName: String, size: Long, input: InputStream) = input
}

class MockEncryptionResult(val newName: String) extends EncryptResult {

  def key = NullEncryptionProvider.key

  def encoded = false

  def encode(output: OutputStream) = output
}
