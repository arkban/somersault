package somersault.core.encode

import org.testng.annotations.Test
import somersault.core.filters.{SomerFilter, BooleanFilter}
import somersault.util.Assert

trait CompressionProviderTest extends EncodingProviderTest[CompressResult] {

  override protected def getEncodingProviders(testName: String) =
    getEncodingProviders(testName, new BooleanFilter(true))

  protected def getEncodingProviders(testName: String, filter: SomerFilter): Iterable[CompressionProvider]

  @Test def noEncoding() {
    getEncodingProviders("noEncoding", new BooleanFilter(false)).foreach(
      provider => {
        val file = createFile("foo")
        val encodeResult = provider.encode(Nil, file)
        Assert.isFalse(encodeResult.encoded)
        Assert.areEqual(encodeResult.newName, file.name)
      })
  }
}