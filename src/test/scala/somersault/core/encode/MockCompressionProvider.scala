package somersault.core.encode

import somersault.core.filesystems.SomerFile
import java.lang.String
import java.io.{OutputStream, InputStream}

/**
 * An EncodingProvider that mimics NullCompressionProvider in behavior but pretends to be a real CompressionProvider.
 */
class MockCompressionProvider( val name: String ) extends CompressionProvider
{
  def encode( parentPath: Iterable[ String ], file: SomerFile ) = new MockCompressResult( file.name )

  def encodeFile( name: String ) = new MockCompressResult( name )

  def encodeDir( name: String ) = new MockCompressResult( name )

  def decode( newName: String, size: Long, input: InputStream ) = input
}

class MockCompressResult( val newName: String ) extends CompressResult
{
  def encoded = false

  def encode( output: OutputStream ) = output
}
