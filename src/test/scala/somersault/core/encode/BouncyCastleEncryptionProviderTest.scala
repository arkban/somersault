package somersault.core.encode

import java.io.{ByteArrayOutputStream, ByteArrayInputStream}
import javax.crypto.Cipher
import org.testng.annotations.Test
import somersault.core.filesystems.SomerFile
import somersault.core.filters.BooleanFilter
import somersault.core.{StreamPumpData, StreamPump}
import somersault.util.Assert

class BouncyCastleEncryptionProviderTest extends EncryptionProviderTest {

  protected def getEncryptionProviders(testName: String, userKey: String) = {
    val keys = (
      BouncyCastleEncryptionKey.createWeakKey(userKey)
        :: BouncyCastleEncryptionKey.createStrongKey(userKey)
        :: Nil)
    keys.map(new BouncyCastleEncryptionProvider(_))
  }

  /**
   * Sanity test of the BouncyCastle in the raw.
   */
  @Test def sanityTest1() {
    val key = BouncyCastleEncryptionKey.createStrongKey("password")

    inputs.foreach(
      origInput => {
        val origInputBytes = origInput.getBytes

        val encCipher = key.generateCipher("salt", Cipher.ENCRYPT_MODE)
        val encBuffer = new Array[Byte](encCipher.getOutputSize(origInputBytes.length))
        var encOutputLen = encCipher.processBytes(origInputBytes, 0, origInputBytes.length, encBuffer, 0)
        encOutputLen += encCipher.doFinal(encBuffer, encOutputLen)

        Assert.notEquals(origInputBytes, encBuffer)

        val decCipher = key.generateCipher("salt", Cipher.DECRYPT_MODE)
        val decBuffer = new Array[Byte](decCipher.getOutputSize(origInputBytes.length))
        var decOutputLen = decCipher.processBytes(encBuffer, 0, encBuffer.length, decBuffer, 0)
        decOutputLen += decCipher.doFinal(decBuffer, decOutputLen)

        val finalOutput = new String(decBuffer, 0, decOutputLen)
        Assert.areEqual(origInput, finalOutput)
      })
  }

  /**
   * Sanity test of the BouncyCastle streams, using multi-byte reading (via StreamPump).
   */
  @Test def sanityTest2() {
    val key = BouncyCastleEncryptionKey.createStrongKey("password")
    val salt = "salt"

    inputs.foreach(
      i => {
        val origInput = i.getBytes

        // encrypt
        val input = new BouncyCastleInputStream(
          new ByteArrayInputStream(origInput), key.generateCipher(salt, Cipher.ENCRYPT_MODE))
        val midOutput = new ByteArrayOutputStream
        new StreamPump().pump(input, midOutput, new StreamPumpData(true, None))

        Assert.notEquals(origInput, midOutput.toByteArray)

        // decrypt
        val midInput = new ByteArrayInputStream(midOutput.toByteArray)
        val finalOutput = new ByteArrayOutputStream
        val encOutput = new BouncyCastleOutputStream(finalOutput, key.generateCipher(salt, Cipher.DECRYPT_MODE))
        new StreamPump().pump(midInput, encOutput, new StreamPumpData(true, None))

        Assert.areEqual(origInput, finalOutput.toByteArray)
      })
  }

  /**
   * Sanity test of the BouncyCastle streams, using multi-byte reading (via StreamPump). The streams are flipped.
   */
  @Test def sanityTest3() {
    val key = BouncyCastleEncryptionKey.createStrongKey("password")
    val salt = "salt"

    inputs.foreach(
      i => {
        val origInput = i.getBytes

        // encrypt
        val input = new ByteArrayInputStream(origInput)
        val midOutput = new ByteArrayOutputStream
        val encMidOutput = new BouncyCastleOutputStream(midOutput, key.generateCipher(salt, Cipher.ENCRYPT_MODE))
        new StreamPump().pump(input, encMidOutput, new StreamPumpData(true, None))

        Assert.notEquals(origInput, midOutput.toByteArray)

        // decrypt
        val midInput = new BouncyCastleInputStream(
          new ByteArrayInputStream(midOutput.toByteArray), key.generateCipher(salt, Cipher.DECRYPT_MODE))
        val finalOutput = new ByteArrayOutputStream
        new StreamPump().pump(midInput, finalOutput, new StreamPumpData(true, None))

        Assert.areEqual(origInput, finalOutput.toByteArray)
      })
  }

  /**
   * Sanity test of the BouncyCastle streams, using single-byte reading.
   */
  @Test def sanityTest4() {
    val key = BouncyCastleEncryptionKey.createStrongKey("password")
    val salt = "salt"

    inputs.foreach(
      i => {
        val origInput = i.getBytes

        // encrypt
        val input = new BouncyCastleInputStream(
          new ByteArrayInputStream(origInput), key.generateCipher(salt, Cipher.ENCRYPT_MODE))
        val midOutput = new ByteArrayOutputStream

        var read = 0
        while (read != -1) {
          read = input.read()
          Assert.isTrue(read == -1 || (0 <= read && read <= 255))
          if (read != -1)
            midOutput.write(read)
        }
        input.close()
        midOutput.close()

        Assert.notEquals(origInput, midOutput.toByteArray)

        // decrypt
        val midInput = new ByteArrayInputStream(midOutput.toByteArray)
        val finalOutput = new ByteArrayOutputStream
        val encOutput = new BouncyCastleOutputStream(finalOutput, key.generateCipher(salt, Cipher.DECRYPT_MODE))

        read = 0
        while (read != -1) {
          read = midInput.read()
          Assert.isTrue(read == -1 || (0 <= read && read <= 255))
          if (read != -1)
            encOutput.write(read)
        }
        midInput.close()
        encOutput.close()

        Assert.areEqual(origInput, finalOutput.toByteArray)
      })
  }

  /**
   * Sanity test of the BouncyCastle streams, using single-byte reading. Streams are flipped.
   */
  @Test def sanityTest5() {
    val key = BouncyCastleEncryptionKey.createStrongKey("password")
    val salt = "salt"

    inputs.foreach(
      i => {
        val origInput = i.getBytes

        // encrypt
        val input = new ByteArrayInputStream(origInput)
        val midOutput = new ByteArrayOutputStream
        val encMidOutput = new BouncyCastleOutputStream(midOutput, key.generateCipher(salt, Cipher.ENCRYPT_MODE))

        var read = 0
        while (read != -1) {
          read = input.read()
          Assert.isTrue(read == -1 || (0 <= read && read <= 255))
          if (read != -1)
            encMidOutput.write(read)
        }
        input.close()
        encMidOutput.close()

        Assert.notEquals(origInput, midOutput.toByteArray)

        // decrypt
        val midInput = new BouncyCastleInputStream(
          new ByteArrayInputStream(midOutput.toByteArray), key.generateCipher(salt, Cipher.DECRYPT_MODE))
        val finalOutput = new ByteArrayOutputStream

        read = 0
        while (read != -1) {
          read = midInput.read()
          Assert.isTrue(read == -1 || (0 <= read && read <= 255))
          if (read != -1)
            finalOutput.write(read)
        }
        midInput.close()
        finalOutput.close()

        Assert.areEqual(origInput, finalOutput.toByteArray)
      })
  }

  /**
   * Sanity test of the BouncyCastle streams and compression.
   */
  @Test def sanityTest6() {
    val compProviders = (
      new GZIPCompressionProvider(new BooleanFilter(true))
        :: new BZIP2CompressionProvider(new BooleanFilter(true))
        :: Nil)
    val key = BouncyCastleEncryptionKey.createStrongKey("password")
    val salt = "salt"

    compProviders.foreach(
      comp => {
        inputs.foreach(
          i => {
            val origInput = i.getBytes

            // encrypt
            val input = new BouncyCastleInputStream(
              new ByteArrayInputStream(origInput), key.generateCipher(salt, Cipher.ENCRYPT_MODE))
            val midOutput = new ByteArrayOutputStream
            new StreamPump().pump(input, midOutput, new StreamPumpData(true, None))

            Assert.notEquals(origInput, midOutput.toByteArray)

            // decrypt
            val midInput = new ByteArrayInputStream(midOutput.toByteArray)
            val finalOutput = new ByteArrayOutputStream
            val encOutput = new BouncyCastleOutputStream(finalOutput, key.generateCipher(salt, Cipher.DECRYPT_MODE))
            new StreamPump().pump(midInput, encOutput, new StreamPumpData(true, None))

            Assert.areEqual(origInput, finalOutput.toByteArray)
          })
      })
  }

  /**
   * Sanity test of the BouncyCastle streams and compression. Streams are flipped.
   */
  @Test def sanityTest7() {
    val compProviders = (
      new GZIPCompressionProvider(new BooleanFilter(true))
        :: new BZIP2CompressionProvider(new BooleanFilter(true))
        :: Nil)
    val key = BouncyCastleEncryptionKey.createStrongKey("password")
    val salt = "salt"

    val file = new SomerFile {
      def name = "name"

      def hash = null

      def attributes = null

      def size = 0L

      def modified = null
    }

    compProviders.foreach(
      comp => {
        inputs.foreach(
          i => {
            val origInput = i.getBytes

            // encrypt
            val input = new ByteArrayInputStream(origInput)
            val midOutput = new ByteArrayOutputStream
            val midEncOutput = new BouncyCastleOutputStream(
              comp.encode(Nil, file).encode(midOutput), key.generateCipher(salt, Cipher.ENCRYPT_MODE))
            new StreamPump().pump(input, midEncOutput, new StreamPumpData(true, None))

            Assert.notEquals(origInput, midOutput.toByteArray)

            // decrypt
            val midInput =
              comp.decode(
                comp.encodeFile(file.name).newName, 0, new BouncyCastleInputStream(
                  new ByteArrayInputStream(midOutput.toByteArray), key.generateCipher(salt, Cipher.DECRYPT_MODE)))
            val finalOutput = new ByteArrayOutputStream
            new StreamPump().pump(midInput, finalOutput, new StreamPumpData(true, None))

            Assert.areEqual(origInput, finalOutput.toByteArray)
          })
      })
  }
}
