package somersault.core.encode

import java.io.{ByteArrayOutputStream, ByteArrayInputStream}
import org.testng.annotations.Test
import scala.collection.immutable.Nil
import somersault.core.TestUtil
import somersault.core.filesystems.SomerFile
import somersault.util.{Assert, Hash, HashInfo}

trait EncodingProviderTest[T <: EncodeResult] {

  protected val inputs = (
    "" ::
      "foo" ::
      "text" ::
      "compAndEnc" ::
      "somersault is a good program" ::
      ("really long text " * 16) ::
      ("absolutely-freaking-monster-text" * 64) ::
      (Predef.seqToCharSequence((0 to 255).map(i => i.asInstanceOf[Char])).toString * 256) ::
      Nil)

  /**
   * Retrieves a EncodingProvider for use in the tests in this class.
   */
  protected def getEncodingProviders(testName: String): Iterable[EncodingProvider[T]]

  @Test def encodeAndDecode() {
    getEncodingProviders("encodeAndDecode").foreach(
      encProvider =>
        inputs.foreach(
          origContents => {
            val file = createFile("foo")

            val encodeResult = encProvider.encode(Nil, file)

            // encode data
            val encContents = new ByteArrayOutputStream
            val encodeStream = encodeResult.encode(encContents)
            encodeStream.write(origContents.getBytes)
            encodeStream.flush()
            encodeStream.close()

            // decode data
            val inStream = new ByteArrayInputStream(encContents.toByteArray)
            val decodeStream = encProvider.decode(file.name, file.size, inStream)
            val (_, decodeBytes) = TestUtil.readRawContents(HashInfo.md5, decodeStream)

            val origBytes = origContents.getBytes
            Assert.areEqual(Hash.toHex(origBytes), Hash.toHex(decodeBytes))
          }))
  }

  protected def createFile(fileName: String): SomerFile =
    new SomerFile {
      def name = fileName

      def hash = null

      def size = 0

      def modified = null

      def attributes = null
    }
}