package somersault.core.encode

import java.io.File
import nu.xom.Element
import org.testng.annotations.Test
import somersault.util.Assert
import somersault.util.persist.{PersisterSuite, PersisterContext}

class BouncyCastleEncryptionKeyTest {

  @Test def persist1() {
    val context = new PersisterContext(PersisterSuite.create(), new File("temp"))
    val key = new BouncyCastleEncryptionKey("one", "two", 5, 10, "three")
    val pw = new BouncyCastleEncryptionKeyPersister

    val element = new Element("key")
    pw.write(context, key, element, dynamic = true)

    Assert.isTrue(pw.canRead(context, element))
    val newKey: BouncyCastleEncryptionKey = pw.read(context, element)
    Assert.areEqual(key.cipherSpec, newKey.cipherSpec)
    Assert.areEqual(key.pbeSpec, newKey.pbeSpec)
    Assert.areEqual(key.userKey, newKey.userKey)
  }
}