package somersault.core.encode

import org.testng.annotations.Test
import somersault.util.Assert

/**
 * Common tests for EncryptionProviders
 */

abstract class EncryptionProviderTest extends EncodingProviderTest[EncryptResult] {

  protected def getEncodingProviders(testName: String) = getEncryptionProviders(testName, "password")

  protected def getEncryptionProviders(testName: String, userKey: String): Iterable[EncryptionProvider]

  @Test def samePasswordDiffName() {
    getEncryptionProviders("samePasswordDiffName", "password").foreach(
      encProvider => {
        val result1 = encProvider.encodeFile("foo")
        val result2 = encProvider.encodeFile("bar")

        Assert.notEquals(result1.newName, result2.newName)
      })
  }

  @Test def sameNameDiffPassword() {
    val prov1 = getEncryptionProviders("sameNameDiffPassword", "foo").toSeq
    val prov2 = getEncryptionProviders("sameNameDiffPassword", "bar").toSeq

    (0 until prov1.size).foreach(
      i => {
        val result1 = prov1(i).encodeFile("foo")
        val result2 = prov2(i).encodeFile("foo")

        Assert.notEquals(result1.newName, result2.newName)
      })
  }
}