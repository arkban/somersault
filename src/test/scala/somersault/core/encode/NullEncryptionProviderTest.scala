package somersault.core.encode

import scala.collection.immutable.Nil

class NullEncryptionProviderTest extends EncodingProviderTest[ EncryptResult ]
{
  protected def getEncodingProviders( testName: String ) = new NullEncryptionProvider :: Nil
}