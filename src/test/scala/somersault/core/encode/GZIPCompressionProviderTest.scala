package somersault.core.encode

import java.lang.String
import org.testng.annotations.Test
import nu.xom.Element
import somersault.util.Assert
import somersault.core.filters.{BooleanFilterPersister, BooleanFilter, SomerFilter}
import somersault.util.persist.{PersisterContext, PersisterContainer}
import java.io.File

class GZIPCompressionProviderTest extends CompressionProviderTest
{
  protected def getEncodingProviders( testName: String, filter: SomerFilter) =
    new GZIPCompressionProvider( filter) :: Nil

  @Test def persist( )
  {
    val provider = new GZIPCompressionProvider( new BooleanFilter( true), "changed")
    val pwc = new PersisterContainer
    pwc.add( classOf[ BooleanFilter ], new BooleanFilterPersister)
    val context = new PersisterContext( pwc, new File( "temp" ))
    val pw = new GZIPCompressionProviderPersister

    val element = new Element( "provider")
    pw.write( context, provider, element, true)

    Assert.isTrue( pw.canRead( context, element))
    val newProvider: GZIPCompressionProvider = pw.read( context, element)
    Assert.areEqual( provider.extension, newProvider.extension)
  }
}