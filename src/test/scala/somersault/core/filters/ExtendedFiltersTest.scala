package somersault.core.filters

import org.testng.annotations.Test
import somersault.core.filesystems.SomerFile
import somersault.util.Assert

class ExtendedFiltersTest {

  /**
   * A smoke test of the StandardCompressionFilter.
   */
  @Test def stdCompfilter() {
    val filter = ExtendedFilters.standardCompressionFilter

    val mkSomerFile = (x: String) => new SomerFile {
      def name = x

      def hash = null

      def size = 0

      def modified = null

      def attributes = null
    }

    Assert.isTrue(filter.accept(Nil, mkSomerFile("hello.txt")))
    Assert.isTrue(filter.accept(Nil, mkSomerFile("hello.doc")))
    Assert.isTrue(filter.accept(Nil, mkSomerFile("hello.bmp")))

    Assert.isFalse(filter.accept(Nil, mkSomerFile("hello.mp2")))
    Assert.isFalse(filter.accept(Nil, mkSomerFile("hello.mp3")))
    Assert.isFalse(filter.accept(Nil, mkSomerFile("hello.mp4")))

    Assert.isFalse(filter.accept(Nil, mkSomerFile("hello.jpeg")))
    Assert.isFalse(filter.accept(Nil, mkSomerFile("hello.jpg")))

    Assert.isFalse(filter.accept(Nil, mkSomerFile("hello.zip")))

    Assert.isFalse(filter.accept(Nil, mkSomerFile("hello.gz")))
    Assert.isFalse(filter.accept(Nil, mkSomerFile("hello.gzi")))
    Assert.isFalse(filter.accept(Nil, mkSomerFile("hello.gzip")))

    Assert.isFalse(filter.accept(Nil, mkSomerFile("hello.rar")))
    Assert.isFalse(filter.accept(Nil, mkSomerFile("hello.r01")))
  }
}