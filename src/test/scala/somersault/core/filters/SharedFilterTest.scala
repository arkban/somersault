package somersault.core.filters

import java.io.File
import nu.xom.Element
import org.testng.annotations.Test
import somersault.core.TestUtil
import somersault.util.persist.{PersisterContext, PersisterSuite, Persister}
import somersault.util.{Assert, FilenameUtils2}
import util.Random

class SharedFilterTest {

  /**
   * Tests various combinations of paths
   */
  @Test def paths() {
    val unitTestName = this.getClass.getSimpleName + ".test"
    val path = TestUtil.createTestPath(classOf[SharedFilterTest], unitTestName)
    // main file is down one dir so we can test shared files up one directory
    val mainFile = new File(FilenameUtils2.concat(path.getCanonicalPath, "shared", "master.xml"))
    mainFile.getParentFile.mkdirs
    val context = new PersisterContext(PersisterSuite.create(), mainFile)
    val readFn = (root: Element) => new SharedFilterPersister().read(context, root).asInstanceOf[SharedFilter]
    val random = new Random()

    val sharedFiles = new File(FilenameUtils2.concat(path.getCanonicalPath, "local.xml")) :: // absolute
      new File(FilenameUtils2.concat(path.getCanonicalPath, "shared", "local.xml")) :: // absolute
      new File(FilenameUtils2.concat(path.getCanonicalPath, "shared", "foo", "local.xml")) :: // absolute
      new File("local.xml") :: // relative
      new File("shared", "local.xml") :: // relative
      new File(FilenameUtils2.concat("shared", "foo"), "local.xml") :: // relative
      new File(FilenameUtils2.concat("foo", "bar"), "local.xml") :: // relative
      Nil

    sharedFiles.foreach(
      sharedFile => {
        val size = random.nextInt(100000)
        val origSharedFilter = new SharedFilter(new SizeFilter(size), sharedFile)

        val writeFn =
          (root: Element) => new SharedFilterPersister().write(context, origSharedFilter, root, dynamic = true)
        Persister.write("shared", writeFn, mainFile)

        val newSharedFilter = Persister.read(mainFile, readFn)

        Assert.isTrue(newSharedFilter.filter.isInstanceOf[SizeFilter])
        Assert.areEqual(newSharedFilter.filter.asInstanceOf[SizeFilter].max, size)
        Assert.areEqual(newSharedFilter.filePath, sharedFile)
      })
  }

  @Test def recursive() {
    val unitTestName = this.getClass.getSimpleName + ".test"
    val path = TestUtil.createTestPath(classOf[SharedFilterTest], unitTestName)
    // main file is down one dir so we can test shared files up one directory
    val mainFile = new File(FilenameUtils2.concat(path.getCanonicalPath, "master.xml"))
    mainFile.getParentFile.mkdirs
    val context = new PersisterContext(PersisterSuite.create(), mainFile)
    val readFn = (root: Element) => new SharedFilterPersister().read(context, root).asInstanceOf[SharedFilter]

    val barFile = new File(FilenameUtils2.concat("..", "bar.xml"))
    val fooFilter = new SharedFilter(new SizeFilter(10000), barFile)

    val fooFile = new File(FilenameUtils2.concat("shared", "nested", "foo.xml"))
    val mainFilter = new SharedFilter(fooFilter, fooFile)

    val writeFn = (root: Element) => new SharedFilterPersister().write(context, mainFilter, root, dynamic = true)
    Persister.write("shared", writeFn, mainFile)

    val newMainFilter = Persister.read(mainFile, readFn)
    Assert.areEqual(newMainFilter.filePath, fooFile)
    Assert.isTrue(new File(path, FilenameUtils2.concat("shared", "nested", "foo.xml")).exists)
    Assert.isTrue(newMainFilter.filter.isInstanceOf[SharedFilter])

    val newFooFilter = newMainFilter.filter.asInstanceOf[SharedFilter]
    Assert.areEqual(newFooFilter.filePath, barFile)
    Assert.isTrue(new File(path, FilenameUtils2.concat("shared", "bar.xml")).exists)
    Assert.isTrue(newFooFilter.filter.isInstanceOf[SizeFilter])
  }
}