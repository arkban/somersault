package somersault.core.filters

import java.io.File
import nu.xom.Element
import org.testng.annotations.Test
import scala.None
import somersault.core.filesystems.{FileAttributes, MockSomerDirectory, MockSomerFile}
import somersault.util.Assert
import somersault.util.persist.{PersisterSuite, PersisterContext}

class SomerFilterTest {

  private val context = new PersisterContext(PersisterSuite.create(), new File("temp"))

  @Test def booleanFilter() {
    Seq(true, false).foreach(
      value => {
        val filter = new BooleanFilter(value)
        Assert.areEqual(filter.accept(Nil, null), value)
      })
  }

  @Test def booleanFilterPersistence() {
    Seq(true, false).foreach(
      value => {
        val filter = new BooleanFilter(value)
        val pw = new BooleanFilterPersister

        val element = new Element("filter")
        pw.write(context, filter, element, dynamic = true)

        Assert.isTrue(pw.canRead(context, element))
        val newFilter: BooleanFilter = pw.read(context, element)
        Assert.areEqual(filter.value, newFilter.value)
      })
  }

  @Test def notFilter() {
    val filter = new NotFilter(new BooleanFilter(true))
    Assert.isFalse(filter.accept(Nil, null))
  }

  @Test def notFilterPersistence() {
    Seq(true, false).foreach(
      value => {
        val filter = new NotFilter(new BooleanFilter(value))
        val pw = new NotFilterPersister

        val element = new Element("filter")
        pw.write(context, filter, element, dynamic = true)

        Assert.isTrue(pw.canRead(context, element))
        val newFilter: NotFilter = pw.read(context, element)
        Assert.areEqual(!value, newFilter.accept(Nil, null))
      })
  }

  @Test def andFilter1() {
    val filter = new BooleanFilter(true)
    val andFilter = new AndFilter(filter, filter, filter)
    Assert.isTrue(andFilter.accept(Nil, null))
  }

  @Test def andFilter2() {
    val andFilter = new AndFilter(new BooleanFilter(true), new BooleanFilter(false), new BooleanFilter(true))
    Assert.isFalse(andFilter.accept(Nil, null))
  }

  @Test def andFilterPersistence() {
    val filter = new AndFilter(new BooleanFilter(true), new BooleanFilter(true), new BooleanFilter(true))

    val pw = new AndFilterPersister

    val element = new Element("filter")
    pw.write(context, filter, element, dynamic = true)

    Assert.isTrue(pw.canRead(context, element))
    val newFilter: AndFilter = pw.read(context, element)
    Assert.isTrue(newFilter.accept(Nil, null))
  }

  @Test def orFilter1() {
    val filter = new OrFilter(new BooleanFilter(false), new BooleanFilter(true), new BooleanFilter(false))
    Assert.isTrue(filter.accept(Nil, null))
  }

  @Test def orFilter2() {
    val filter = new OrFilter(new BooleanFilter(false), new BooleanFilter(false), new BooleanFilter(false))
    Assert.isFalse(filter.accept(Nil, null))
  }

  @Test def orFilterPersistence() {
    val filter = new OrFilter(new BooleanFilter(false), new BooleanFilter(true), new BooleanFilter(false))

    val pw = new OrFilterPersister

    val element = new Element("filter")
    pw.write(context, filter, element, dynamic = true)

    Assert.isTrue(pw.canRead(context, element))
    val newFilter: OrFilter = pw.read(context, element)
    Assert.isTrue(newFilter.accept(Nil, null))
  }

  @Test def nameFilter() {
    val file = new MockSomerFile("/somersault")
    val dir = new MockSomerDirectory(file.fullPath)

    Assert.isTrue((new NameFilter("rsa")).accept(Nil, file))
    Assert.isFalse((new NameFilter("x")).accept(Nil, file))

    Assert.isTrue((new NameFilter("rsa")).accept(Nil, dir))
    Assert.isFalse((new NameFilter("x")).accept(Nil, dir))
  }

  @Test def nameFilterPersistence() {
    val filter = new NameFilter("rsa", "no match")
    val pw = new NameFilterPersister

    val element = new Element("filter")
    pw.write(context, filter, element, dynamic = true)

    Assert.isTrue(pw.canRead(context, element))
    val newFilter: NameFilter = pw.read(context, element)
    newFilter.patterns.foreach(p => Assert.isTrue(filter.patterns.exists(_.toString == p.toString)))
  }

  @Test def sizeFilter1() {
    val f = new MockSomerFile("/somersault")

    Assert.isTrue((new SizeFilter(f.data.size)).accept(Nil, f))
    Assert.isFalse((new SizeFilter(f.data.size - 1)).accept(Nil, f))
    Assert.isFalse((new SizeFilter(-f.data.size, 0)).accept(Nil, f))
    Assert.isTrue((new SizeFilter(f.data.size, f.data.size)).accept(Nil, f))
    Assert.isTrue((new SizeFilter(f.data.size - 1, f.data.size + 1)).accept(Nil, f))
  }

  @Test def sizeFilter2() {
    val d = new MockSomerDirectory("/")

    Assert.isTrue((new SizeFilter(0, 1)).accept(Nil, d))
  }

  @Test def sizeFilterPersistence() {
    val filter = new SizeFilter(100, 200)
    val pw = new SizeFilterPersister

    val element = new Element("filter")
    pw.write(context, filter, element, dynamic = true)

    Assert.isTrue(pw.canRead(context, element))
    val newFilter: SizeFilter = pw.read(context, element)
    Assert.areEqual(filter.min, newFilter.min)
    Assert.areEqual(filter.max, newFilter.max)
  }

  @Test def fileFilter() {
    val f = new MockSomerFile("/somersault")
    val d = new MockSomerDirectory(f.fullPath)
    val filter = new FileFilter

    Assert.isTrue(filter.accept(Nil, f))
    Assert.isFalse(filter.accept(Nil, d))
  }

  @Test def fileFilterPersistence() {
    val f = new MockSomerFile("/somersault")
    val filter = new FileFilter
    val pw = new FileFilterPersister

    val element = new Element("filter")
    pw.write(context, filter, element, dynamic = true)

    Assert.isTrue(pw.canRead(context, element))
    val newFilter: FileFilter = pw.read(context, element)
    Assert.isTrue(newFilter.accept(Nil, f))
  }

  @Test def fileAttributesFilter1() {
    val f = new MockSomerFile("/somersault", attributes = new FileAttributes(true, false, false))

    Assert.isTrue((new FileAttributesFilter(None, None, None)).accept(Nil, f))
    Assert.isTrue((new FileAttributesFilter(Some(true), None, None)).accept(Nil, f))
    Assert.isFalse((new FileAttributesFilter(Some(false), None, None)).accept(Nil, f))
  }

  @Test def fileAttributesFilter2() {
    val f = new MockSomerFile("/somersault", attributes = new FileAttributes(false, true, false))

    Assert.isTrue((new FileAttributesFilter(None, None, None)).accept(Nil, f))
    Assert.isTrue((new FileAttributesFilter(None, Some(true), None)).accept(Nil, f))
    Assert.isFalse((new FileAttributesFilter(None, Some(false), None)).accept(Nil, f))
  }

  @Test def fileAttributesFilter3() {
    val f = new MockSomerFile("/somersault", attributes = new FileAttributes(false, false, true))

    Assert.isTrue((new FileAttributesFilter(None, None, None)).accept(Nil, f))
    Assert.isTrue((new FileAttributesFilter(None, None, Some(true))).accept(Nil, f))
    Assert.isFalse((new FileAttributesFilter(None, None, Some(false))).accept(Nil, f))
  }

  @Test def fileAttributesFilter4() {
    val d = new MockSomerDirectory("/")

    Assert.isTrue((new FileAttributesFilter(None, None, None)).accept(Nil, d))
  }

  @Test def fileAttributesFilterPersistence() {
    val pw = new FileAttributesFilterPersister

    val values = Seq(Some(true), Some(false), None)
    for (e <- values; r <- values; w <- values) {
      val filter = new FileAttributesFilter(e, r, w)

      val element = new Element("filter")
      pw.write(context, filter, element, dynamic = true)

      Assert.isTrue(pw.canRead(context, element))
      val newFilter: FileAttributesFilter = pw.read(context, element)
      Assert.areEqual(filter.executable, newFilter.executable)
      Assert.areEqual(filter.readable, newFilter.readable)
      Assert.areEqual(filter.writable, newFilter.writable)
    }
  }

  @Test def directoryFilter() {
    val f = new MockSomerFile("/somersault")
    val d = new MockSomerDirectory(f.fullPath)
    val filter = new DirectoryFilter

    Assert.isFalse(filter.accept(Nil, f))
    Assert.isTrue(filter.accept(Nil, d))
  }

  @Test def directoryFilterPersistence() {
    val d = new MockSomerDirectory("/somersault")
    val filter = new DirectoryFilter
    val pw = new DirectoryFilterPersister

    val element = new Element("filter")
    pw.write(context, filter, element, dynamic = true)

    Assert.isTrue(pw.canRead(context, element))
    val newFilter: DirectoryFilter = pw.read(context, element)
    Assert.isTrue(newFilter.accept(Nil, d))
  }

  /**
   * Root path
   */
  @Test def pathFilter1() {
    val f = new MockSomerFile("/foo/bar/somersault")
    Seq("/foo/bar", "foo/bar/", "/foo/bar/", "foo/bar")
      .map(v => new PathFilter(v))
      .foreach(
      filter => {
        Assert.isTrue(filter.accept("foo" :: "bar" :: Nil, f))
        Assert.isTrue(filter.accept("pre" :: "foo" :: "bar" :: Nil, f))
        Assert.isTrue(filter.accept("foo" :: "bar" :: "post" :: Nil, f))
        Assert.isFalse(filter.accept("foo" :: Nil, f))
        Assert.isFalse(filter.accept("bar" :: Nil, f))
        Assert.isFalse(filter.accept("foo" :: "mid" :: "bar" :: Nil, f))
      })
  }

  /**
   * Single directory check (with leading and trailing directory separators).
   */
  @Test def pathFilter2() {
    val f = new MockSomerFile("/bar/somersault")
    val filter = new PathFilter("/bar/")

    Assert.isTrue(filter.accept("bar" :: Nil, f))
    Assert.isTrue(filter.accept("foo" :: "bar" :: Nil, f))
    Assert.isTrue(filter.accept("foo" :: "bar" :: "post" :: Nil, f))
    Assert.isFalse(filter.accept("bar_x" :: Nil, f))
    Assert.isFalse(filter.accept("x_bar" :: Nil, f))
  }

  /**
   * Single directory check (without leading and trailing directory separators).
   */
  @Test def pathFilter3() {
    val f = new MockSomerFile("/bar/somersault")
    val filter = new PathFilter("bar")

    Assert.isTrue(filter.accept("bar" :: Nil, f))
    Assert.isTrue(filter.accept("foo" :: "bar" :: Nil, f))
    Assert.isTrue(filter.accept("foo" :: "bar" :: "post" :: Nil, f))
    Assert.isTrue(filter.accept("bar_x" :: Nil, f))
    Assert.isTrue(filter.accept("x_bar" :: Nil, f))
    Assert.isFalse(filter.accept("b_x_ar" :: Nil, f))
  }

  /**
   * Trailing directory check
   */
  @Test def pathFilter4() {
    val d = new MockSomerDirectory("/bar/somersault")
    val filter = new PathFilter("bar/somersault")

    Assert.isTrue(filter.accept("bar" :: Nil, d))
    Assert.isFalse(filter.accept(Nil, d))
    Assert.isFalse(filter.accept("foo" :: Nil, d))
    Assert.isFalse(filter.accept("bar" :: "foo" :: Nil, d))
  }

  @Test def pathFilterPersistence() {
    val filter = new PathFilter(".*rsa.*", "no match")
    val pw = new PathFilterPersister

    val element = new Element("filter")
    pw.write(context, filter, element, dynamic = true)

    Assert.isTrue(pw.canRead(context, element))
    val newFilter: PathFilter = pw.read(context, element)
    newFilter.patterns.foreach(p => Assert.isTrue(filter.patterns.exists(_.toString == p.toString)))
  }

  @Test def depthFilter() {
    val f = new MockSomerFile("somersault")
    val filter = new DepthFilter(3, 4)

    Assert.isFalse(filter.accept(Nil, f))
    Assert.isFalse(filter.accept("foo" :: Nil, f))
    Assert.isTrue(filter.accept("foo" :: "bar" :: Nil, f))
    Assert.isTrue(filter.accept("foo" :: "bar" :: "baz" :: Nil, f))
    Assert.isFalse(filter.accept("foo" :: "bar" :: "baz" :: "bash" :: Nil, f))
  }

  @Test def depthFilterPersistence() {
    val filter = new DepthFilter(1, 10)
    val pw = new DepthFilterPersister

    val element = new Element("filter")
    pw.write(context, filter, element, dynamic = true)

    Assert.isTrue(pw.canRead(context, element))
    val newFilter: DepthFilter = pw.read(context, element)
    Assert.areEqual(filter.min, newFilter.min)
    Assert.areEqual(filter.max, newFilter.max)
  }
}