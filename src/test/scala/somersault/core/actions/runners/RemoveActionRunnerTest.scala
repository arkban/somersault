package somersault.core.actions.runners

import org.joda.time.DateTime
import org.testng.Assert
import org.testng.annotations.Test
import somersault.core.actions.containers.InMemoryActionContainerBuilder
import somersault.core.exceptions.DestDoesNotExistActionRunnerException
import somersault.core.filesystems.{MockSomerFile, FakeFileSystem}
import somersault.core.{ScenarioData, Scenario}

class RemoveActionRunnerTest {

  /**
   * Remove root file
   */
  @Test def remove1() {
    val src = FakeFileSystem.connect("RemoveActionRunnerTest-remove1-Src")
    val dest = FakeFileSystem.connect("RemoveActionRunnerTest-remove1-Dest")
    val f = new MockSomerFile("/1", new DateTime)
    dest.add(f)
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val filesBefore = dest.children(dest.root)
    Assert.assertEquals(filesBefore.size, 1)
    Assert.assertTrue(filesBefore.exists(_ eq f))

    val actions = new InMemoryActionContainerBuilder
    actions.addRemoveAction(actions.root, f.name)
    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.assertEquals(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.assertEquals(filesAfter.size, 0)
  }

  /**
   * Remove nested file
   */
  @Test def remove2() {
    val src = FakeFileSystem.connect("RemoveActionRunnerTest-remove2-Src")
    val s = src.createDir(src.root, "1")
    val dest = FakeFileSystem.connect("RemoveActionRunnerTest-remove2-Dest")
    val d = dest.createDir(dest.root, s.name)
    val f = new MockSomerFile("/1/2", new DateTime)
    dest.add(f)
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val filesBefore = dest.children(d)
    Assert.assertEquals(filesBefore.size, 1)
    Assert.assertTrue(filesBefore.exists(_ eq f))

    val actions = new InMemoryActionContainerBuilder
    val parent1 = actions.addDirectoryAction(actions.root, d.name)
    actions.addRemoveAction(parent1, f.name)
    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.assertEquals(observer.errors.size, 0)

    val filesAfter = dest.children(d)
    Assert.assertEquals(filesAfter.size, 0)
  }

  /**
   * Remove root directory
   */
  @Test def remove3() {
    val src = FakeFileSystem.connect("RemoveActionRunnerTest-remove3-Src")
    val dest = FakeFileSystem.connect("RemoveActionRunnerTest-remove3-Dest")
    val d = dest.createDir(dest.root, "1")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val filesBefore = dest.children(dest.root)
    Assert.assertEquals(filesBefore.size, 1)
    Assert.assertTrue(filesBefore.exists(_ eq d))

    val actions = new InMemoryActionContainerBuilder
    actions.addRemoveAction(actions.root, d.name)
    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.assertEquals(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.assertEquals(filesAfter.size, 0)
  }

  /**
   * Remove nested directory
   */
  @Test def remove4() {
    val src = FakeFileSystem.connect("RemoveActionRunnerTest-remove4-Src")
    val s = src.createDir(src.root, "1")
    val dest = FakeFileSystem.connect("RemoveActionRunnerTest-remove4-Dest")
    val d1 = dest.createDir(dest.root, s.name)
    val d2 = dest.createDir(d1, "2")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val filesBefore = dest.children(d1)
    Assert.assertEquals(filesBefore.size, 1)
    Assert.assertTrue(filesBefore.exists(_ eq d2))

    val actions = new InMemoryActionContainerBuilder
    val parent = actions.addDirectoryAction(actions.root, d1.name)
    actions.addRemoveAction(parent, d2.name)
    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.assertEquals(observer.errors.size, 0)

    val filesAfter = dest.children(d1)
    Assert.assertEquals(filesAfter.size, 0)
  }

  /**
   * Remove non-empty directory
   */
  @Test def remove5() {
    val src = FakeFileSystem.connect("RemoveActionRunnerTest-remove5-Src")
    val dest = FakeFileSystem.connect("RemoveActionRunnerTest-remove5-Dest")
    val d = dest.createDir(dest.root, "1")
    val nestedFile = new MockSomerFile("/1/2", new DateTime)
    dest.add(nestedFile)
    val nestedDir = dest.createDir(d, "3")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val nonEmptyDir = dest.children(d)
    Assert.assertEquals(nonEmptyDir.size, 2)
    Assert.assertTrue(nonEmptyDir.exists(_ eq nestedDir))
    Assert.assertTrue(nonEmptyDir.exists(_ eq nestedFile))

    val filesBefore = dest.children(dest.root)
    Assert.assertEquals(filesBefore.size, 1)
    Assert.assertTrue(filesBefore.exists(_ eq d))

    val actions = new InMemoryActionContainerBuilder
    actions.addRemoveAction(actions.root, d.name)
    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.assertEquals(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.assertEquals(filesAfter.size, 0)
  }

  /**
   * Does not exist at the root
   */
  @Test def doesNotExistException1() {
    val src = FakeFileSystem.connect("RemoveActionRunnerTest-doesNotExistException1-Src")
    val dest = FakeFileSystem.connect("RemoveActionRunnerTest-doesNotExistException1-Dest")
    val f = new MockSomerFile("/1", new DateTime)
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    actions.addRemoveAction(actions.root, f.name)
    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.assertEquals(observer.errors.size, 0)
  }

  /**
   * Does not exist deeper than root
   */
  @Test def doesNotExistException2() {
    val src = FakeFileSystem.connect("RemoveActionRunnerTest-doesNotExistException2-Src")
    val srcDir = src.createDir(src.root, "1")
    val dest = FakeFileSystem.connect("RemoveActionRunnerTest-doesNotExistException2-Dest")
    dest.createDir(dest.root, srcDir.name)
    val f = new MockSomerFile("/1/2", new DateTime)
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    val parent = actions.addDirectoryAction(actions.root, srcDir.name)
    actions.addRemoveAction(parent, f.name)
    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.assertEquals(observer.errors.size, 0)
  }
}