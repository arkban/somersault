package somersault.core.actions.runners

import org.testng.annotations.Test
import scala.Nil
import somersault.core.actions.containers.InMemoryActionContainerBuilder
import somersault.core.exceptions.{DestDoesNotExistActionRunnerException, SourceDoesNotExistActionRunnerException}
import somersault.core.filesystems._
import somersault.core.{ScenarioData, Scenario}
import somersault.util.Assert

class TransferActionRunnerTest {

  /**
   * Basic file transfer.
   */
  @Test def transfer1() {
    val src = FakeFileSystem.connect("TransferActionRunnerTest-transfer1-Src")
    val dest = FakeFileSystem.connect("TransferActionRunnerTest-transfer1-Dest")
    val srcFile = new MockSomerFile("/1")
    src.add(srcFile)
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val filesBefore = dest.children(dest.root)
    Assert.areEqual(filesBefore.size, 0)

    val actions = new InMemoryActionContainerBuilder
    actions.addTransferAction(actions.root, srcFile.name)
    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.areEqual(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.areEqual(filesAfter.size, 1)
    val destFile = filesAfter.toList(0).asInstanceOf[MockSomerFile]
    Assert.areEqual(srcFile.name, destFile.name)
    Assert.areEqual(srcFile.data, destFile.data)
  }

  /**
   * Overwrite of existing file.
   */
  @Test def transfer2() {
    val src = FakeFileSystem.connect("TransferActionRunnerTest-transfer2-Src")
    val dest = FakeFileSystem.connect("TransferActionRunnerTest-transfer2-Dest")
    val srcFile = new MockSomerFile("/1")
    val destFileOrig = new MockSomerFile("/1")
    src.add(srcFile)
    dest.add(destFileOrig)
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val filesBefore = dest.children(dest.root)
    Assert.areEqual(filesBefore.size, 1)
    Assert.isTrue(filesBefore.exists(_ eq destFileOrig))

    val actions = new InMemoryActionContainerBuilder
    actions.addTransferAction(actions.root, srcFile.name)
    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.areEqual(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.areEqual(filesAfter.size, 1)
    val destFile = filesAfter.toList(0).asInstanceOf[MockSomerFile]
    Assert.areEqual(srcFile.name, destFile.name)
    Assert.areEqual(srcFile.data, destFile.data)
  }

  /**
   * Transfer a large file (which requires multiple chunks being transferred).
   */
  @Test def transfer3() {
    val src = FakeFileSystem.connect("TransferActionRunnerTest-transfer3-Src")
    val dest = FakeFileSystem.connect("TransferActionRunnerTest-transfer3-Dest")
    val srcFile = new MockSomerFile("/1", data = "somersault" * 1000)
    src.add(srcFile)
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val filesBefore = dest.children(dest.root)
    Assert.areEqual(filesBefore.size, 0)

    val actions = new InMemoryActionContainerBuilder
    actions.addTransferAction(actions.root, srcFile.name)
    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.areEqual(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.areEqual(filesAfter.size, 1)
    val destFile = filesAfter.toList(0).asInstanceOf[MockSomerFile]
    Assert.areEqual(srcFile.name, destFile.name)
    Assert.areEqual(srcFile.data, destFile.data)
  }

  /**
   * Transfer nested file
   */
  @Test def transfer4() {
    val src = FakeFileSystem.connect("TransferActionRunnerTest-transfer4-Src")
    val dest = FakeFileSystem.connect("TransferActionRunnerTest-transfer4-Dest")
    val srcFile = new MockSomerFile("/1/2")
    val srcDir = src.createDir(src.root, "1")
    val destDir = dest.createDir(dest.root, "1")
    src.add(srcFile)
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val filesBefore = dest.children(destDir)
    Assert.areEqual(filesBefore.size, 0)

    val actions = new InMemoryActionContainerBuilder
    val parent = actions.addDirectoryAction(actions.root, srcDir.name)
    actions.addTransferAction(parent, srcFile.name)
    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.areEqual(observer.errors.size, 0)

    val filesAfter = dest.children(destDir)
    Assert.areEqual(filesAfter.size, 1)
    val destFile = filesAfter.toList(0).asInstanceOf[MockSomerFile]
    Assert.areEqual(srcFile.name, destFile.name)
    Assert.areEqual(srcFile.data, destFile.data)
  }

  /**
   * Source file does not exist.
   */
  @Test def doesNotExistException() {
    val src = FakeFileSystem.connect("TransferActionRunnerTest-doesNotExistException-Src")
    val dest = FakeFileSystem.connect("TransferActionRunnerTest-doesNotExistException-Dest")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    actions.addTransferAction(actions.root, "1")
    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.areEqual(observer.errors.size, 1)
    Assert.isTrue(observer.errors.exists(_.isInstanceOf[SourceDoesNotExistActionRunnerException]))

    val ex = observer.errors
      .find(_.isInstanceOf[SourceDoesNotExistActionRunnerException])
      .get.asInstanceOf[SourceDoesNotExistActionRunnerException]
    Assert.areEqual(ex.action.name, "1")
  }

  /**
   * Parent dest directory does not exist
   */
  @Test def destDirDoesNotExistException() {
    val src = FakeFileSystem.connect("TransferActionRunnerTest-destDirDoesNotExistException-Src")
    val srcDir = src.createDir(src.root, "1")
    val srcFile = new MockSomerFile("/1/2", data = "")
    src.add(srcFile)
    val dest = FakeFileSystem.connect("TransferActionRunnerTest-destDirDoesNotExistException-Dest")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    val parent = actions.addDirectoryAction(actions.root, srcDir.name)
    actions.addTransferAction(parent, "2")
    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.areEqual(observer.errors.size, 1)
    Assert.isTrue(observer.errors.exists(_.isInstanceOf[DestDoesNotExistActionRunnerException]))

    val ex = observer.errors
      .find(_.isInstanceOf[DestDoesNotExistActionRunnerException])
      .get.asInstanceOf[DestDoesNotExistActionRunnerException]
    Assert.areEqual(ex.action.name, "1")
  }

  /**
   * Verifies that once a TransferAction is finished it closes the streams.
   */
  @Test def closeStreams() {

    val src = FakeFileSystem.connect("TransferActionRunnerTest-closeStreams-Src")
    val dest = FakeFileSystem.connect("TransferActionRunnerTest-closeStreams-Dest")
    val srcFile = new MockSomerFile("/1")
    src.add(srcFile)

    val wrappedSrc = new StreamClosedCheckingFileSystem(src)
    val wrappedDest = new StreamClosedCheckingFileSystem(dest)

    val observer = new MockActionRunnerObserver
    val fsData = new FileSystemRunnerData(
      new Scenario(new ScenarioData("name", "desc", wrappedSrc.data, wrappedDest.data), wrappedSrc, wrappedDest),
      observer)
    val data = new DirectoryRunnerData(fsData, Nil, wrappedSrc.root, wrappedDest.root)
    val transfer = new TransferActionRunner(data)

    val actions = new InMemoryActionContainerBuilder
    val action = actions.addTransferAction(actions.root, srcFile.name)
    transfer.run(action)

    Assert.isTrue(wrappedSrc.isInputFromReadClosed)
    Assert.isTrue(wrappedDest.isOutputFromWriteClosed)
  }
}