package somersault.core.actions.runners

import org.testng.Assert
import org.testng.annotations.Test
import somersault.core.actions.containers.InMemoryActionContainerBuilder
import somersault.core.filesystems.{SomerFile, MockSomerFile, SomerDirectory, FakeFileSystem}
import somersault.core.{ScenarioData, Scenario}

class SomerActionRunnerTest {

  /**
   * CreateDirAction before RemoveAction of same name.
   * - Check that the directory is created.
   */
  @Test def invalidActionOrder1() {
    val src = FakeFileSystem.connect("SomerActionRunnerTest-invalidActionOrder1-Src")
    val dest = FakeFileSystem.connect("SomerActionRunnerTest-invalidActionOrder1-Dest")
    val destDir = dest.createDir(dest.root, "1")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    actions.addCreateDirAction(actions.root, destDir.name)
    actions.addRemoveAction(actions.root, destDir.name)

    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.assertEquals(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.assertEquals(filesAfter.size, 1)
    Assert.assertTrue(filesAfter.forall(_.isInstanceOf[SomerDirectory]))
    Assert.assertTrue(filesAfter.exists(_.name == destDir.name))
  }

  /**
   * TransferAction before RemoveAction of same name.
   * - Check that the is transferred.
   */
  @Test def invalidActionOrder2() {
    val src = FakeFileSystem.connect("SomerActionRunnerTest-invalidActionOrder2-Src")
    val srcFile = new MockSomerFile("/1")
    src.add(srcFile)
    val dest = FakeFileSystem.connect("SomerActionRunnerTest-invalidActionOrder2-Dest")
    val destFile = new MockSomerFile(srcFile.fullPath)
    dest.add(destFile)
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    actions.addTransferAction(actions.root, srcFile.name)
    actions.addRemoveAction(actions.root, destFile.name)

    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.assertEquals(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.assertEquals(filesAfter.size, 1)
    val newDestFile = filesAfter.find(_.isInstanceOf[SomerFile])
    Assert.assertTrue(newDestFile.isDefined)
    Assert.assertTrue(newDestFile.get.name == srcFile.name)
    Assert.assertTrue(newDestFile.get.asInstanceOf[MockSomerFile].data == srcFile.data)
  }

  /**
   * TransferAction before CreateDirAction of same name.
   * - Check that an exception is thrown
   */
  @Test def invalidActionOrder3() {
    val src = FakeFileSystem.connect("SomerActionRunnerTest-invalidActionOrder3-Src")
    val srcFile = new MockSomerFile("/1")
    src.add(srcFile)
    val dest = FakeFileSystem.connect("SomerActionRunnerTest-invalidActionOrder3-Dest")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    actions.addTransferAction(actions.root, srcFile.name)
    actions.addDirectoryAction(actions.root, srcFile.name)

    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.assertEquals(observer.errors.size, 1)

    val filesAfter = dest.children(dest.root)
    Assert.assertEquals(filesAfter.size, 1)
    Assert.assertTrue(filesAfter.forall(_.isInstanceOf[SomerFile]))
    Assert.assertTrue(filesAfter.exists(_.name == srcFile.name))
  }
}