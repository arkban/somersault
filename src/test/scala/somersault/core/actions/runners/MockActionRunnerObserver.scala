package somersault.core.actions.runners

import java.lang.Exception
import logahawk.Severity
import scala.collection.mutable.ListBuffer
import somersault.core.actions.{DirectoryAction, TransferAction, CreateDirAction, RemoveAction}
import somersault.core.filesystems.SomerFile

/**
 * This does no logging but does save all errors it finds.
 */
class MockActionRunnerObserver extends ActionRunnerObserver
{
  val errors = new ListBuffer[ Exception ]

  def start( actionCount: Long ) = {}

  def end() = {}

  def error( severity: Severity, ex: Exception ) = errors += ex

  def recurse( path: List[ String ], action: DirectoryAction ) = {}

  def transfer( path: List[ String ], action: TransferAction, file: SomerFile ) = {}

  def createDir( path: List[ String ], action: CreateDirAction ) = {}

  def remove( path: List[ String ], action: RemoveAction ) = {}
}