package somersault.core.actions.runners

import org.testng.Assert
import org.testng.annotations.Test
import somersault.core.actions.containers.InMemoryActionContainerBuilder
import somersault.core.exceptions.DestDoesNotExistActionRunnerException
import somersault.core.filesystems.{SomerDirectory, FakeFileSystem}
import somersault.core.{ScenarioData, Scenario}

class CreateDirActionRunnerTest {

  /**
   * Create directories at the root.
   */
  @Test def createDir1() {
    val src = FakeFileSystem.connect("CreateDirActionRunnerTest-createDir1-Src")
    val dest = FakeFileSystem.connect("CreateDirActionRunnerTest-createDir1-Dest")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val filesBefore = dest.children(dest.root)
    Assert.assertEquals(filesBefore.size, 0)

    val actions = new InMemoryActionContainerBuilder
    actions.addCreateDirAction(actions.root, "1")
    actions.addCreateDirAction(actions.root, "2")

    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.assertEquals(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.assertEquals(filesAfter.size, 2)

    val d1 = filesAfter.find(_.name == "1").get.asInstanceOf[SomerDirectory]
    val d2 = filesAfter.find(_.name == "2").get.asInstanceOf[SomerDirectory]

    Assert.assertEquals(dest.children(d1).size, 0)
    Assert.assertEquals(dest.children(d2).size, 0)
  }

  /**
   * Create a a single path of directories 3 levels deep.
   */
  @Test def createDir2() {
    val src = FakeFileSystem.connect("CreateDirActionRunnerTest-createDir2-Src")
    val srcD1 = src.createDir(src.root, "1")
    val srcD2 = src.createDir(srcD1, "2")
    src.createDir(srcD2, "3")
    val dest = FakeFileSystem.connect("CreateDirActionRunnerTest-createDir2-Dest")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val filesBeforeLevel1 = dest.children(dest.root)
    Assert.assertEquals(filesBeforeLevel1.size, 0)
    val actions1 = new InMemoryActionContainerBuilder
    actions1.addCreateDirAction(actions1.root, "1")
    val observer1 = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer1)).run(actions1)
    Assert.assertEquals(observer1.errors.size, 0)
    val filesAfterLevel1 = dest.children(dest.root)
    Assert.assertEquals(filesAfterLevel1.size, 1)
    val d1 = filesAfterLevel1.find(_.name == "1").get.asInstanceOf[SomerDirectory]

    val filesBeforeLevel2 = dest.children(d1)
    Assert.assertEquals(filesBeforeLevel2.size, 0)

    val actions2 = new InMemoryActionContainerBuilder
    val parent21 = actions2.addDirectoryAction(actions2.root, d1.name)
    actions2.addCreateDirAction(parent21, "2")
    val observer2 = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer2)).run(actions2)
    Assert.assertEquals(observer2.errors.size, 0)
    val filesAfterLevel2 = dest.children(d1)
    Assert.assertEquals(filesAfterLevel2.size, 1)
    val d2 = filesAfterLevel2.find(_.name == "2").get.asInstanceOf[SomerDirectory]

    val filesBeforeLevel3 = dest.children(d2)
    Assert.assertEquals(filesBeforeLevel3.size, 0)
    val actions3 = new InMemoryActionContainerBuilder
    val parent31 = actions3.addDirectoryAction(actions3.root, d1.name)
    val parent32 = actions3.addDirectoryAction(parent31, d2.name)
    actions3.addCreateDirAction(parent32, "3")
    val observer3 = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer3)).run(actions3)
    Assert.assertEquals(observer3.errors.size, 0)
    val filesAfterLevel3 = dest.children(d2)
    Assert.assertEquals(filesAfterLevel3.size, 1)
    Assert.assertTrue(filesAfterLevel3.exists(_.name == "3"))
  }

  @Test def parentDoesNotExistException() {
    val src = FakeFileSystem.connect("parentDoesNotExistException-Src")
    val d1 = src.createDir(src.root, "1")
    src.createDir(src.root, "2")
    val dest = FakeFileSystem.connect("parentDoesNotExistException-Dest")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    val parent1 = actions.addDirectoryAction(actions.root, "1")
    actions.addCreateDirAction(parent1, "2")
    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)

    Assert.assertEquals(observer.errors.size, 1)
    Assert.assertTrue(observer.errors.exists(_.isInstanceOf[DestDoesNotExistActionRunnerException]))

    val ex = observer
      .errors
      .find(_.isInstanceOf[DestDoesNotExistActionRunnerException])
      .get
      .asInstanceOf[DestDoesNotExistActionRunnerException]
    Assert.assertEquals(ex.action.name, d1.name)
  }

  /**
   * Duplicate root directory
   */
  @Test def dirAlreadyExistsException1() {
    val src = FakeFileSystem.connect("CreateDirActionRunnerTest-dirAlreadyExistsException1-Src")
    val dest = FakeFileSystem.connect("CreateDirActionRunnerTest-dirAlreadyExistsException1-Dest")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    actions.addCreateDirAction(actions.root, "1")

    val observer1 = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer1)).run(actions)
    Assert.assertEquals(observer1.errors.size, 0)

    val observer2 = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer2)).run(actions)
    Assert.assertEquals(observer2.errors.size, 0)
  }

  /**
   * Duplicate nested directory
   */
  @Test def dirAlreadyExistsException2() {
    val src = FakeFileSystem.connect("CreateDirActionRunnerTest-dirAlreadyExistsException2-Src")
    val s = src.createDir(src.root, "1")
    val dest = FakeFileSystem.connect("CreateDirActionRunnerTest-dirAlreadyExistsException2-Dest")
    dest.createDir(dest.root, s.name)
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    val parent1 = actions.addDirectoryAction(actions.root, s.name)
    actions.addCreateDirAction(parent1, "2")

    val observer1 = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer1)).run(actions)
    Assert.assertEquals(observer1.errors.size, 0)

    val observer2 = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer2)).run(actions)
    Assert.assertEquals(observer2.errors.size, 0)
  }
}