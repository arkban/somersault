package somersault.core.actions.containers

import org.testng.annotations.Test
import org.testng.Assert
import somersault.core.actions.{DirectoryAction, RemoveAction, TransferAction, CreateDirAction}

/**
 * This class contains a suite of tests for ActionContainers. This can be thought of an acceptance test for
 * ActionContainerBuilder implementations. Implementations are free to create their own tests in addition to the ones here.
 */
abstract class ActionContainerBuilderTest
{
  /**
   * Creates an ActionContainerBuilder. This will be called once per test.
   *
   * @param testName The name of the test being run. This will be unique for each test run.
   */
  protected def createActionContainer( testName: String): ActionContainerBuilder

  @Test def noActions( )
  {
    val actionContainer = this.createActionContainer( "noActions")
    Assert.assertEquals( actionContainer.children( actionContainer.root).size, 0)
  }

  @Test def createCreateDirAction1( )
  {
    val actionContainer = this.createActionContainer( "addCreateDirAction1")
    val c = actionContainer.addCreateDirAction( actionContainer.root, "1")
    Assert.assertTrue( c.isInstanceOf[ CreateDirAction ])
    Assert.assertEquals( c.name, "1")
    val actions = actionContainer.children( actionContainer.root)
    Assert.assertEquals( actions.size, 1)
    Assert.assertTrue( actions.find( x => x.name == c.name && x.getClass == c.getClass).isDefined)
  }

  @Test def createCreateDirAction2( )
  {
    val actionContainer = this.createActionContainer( "addCreateDirAction2")
    actionContainer.addCreateDirAction( actionContainer.root, "1")
    try
    {
      actionContainer.addCreateDirAction( actionContainer.root, "1")
      Assert.fail( "Expected IllegalArgumentException")
    }
    catch
    {
      case ex: IllegalArgumentException =>
      {}
    }
  }

  @Test def createTransferAction1( )
  {
    val actionContainer = this.createActionContainer( "addTransferAction1")
    val c = actionContainer.addTransferAction( actionContainer.root, "1")
    Assert.assertTrue( c.isInstanceOf[ TransferAction ])
    Assert.assertEquals( c.name, "1")
    val actions = actionContainer.children( actionContainer.root)
    Assert.assertEquals( actions.size, 1)
    Assert.assertTrue( actions.find( x => x.name == c.name && x.getClass == c.getClass).isDefined)
  }

  @Test def createTransferAction2( )
  {
    val actionContainer = this.createActionContainer( "createTransferAction2")
    actionContainer.addTransferAction( actionContainer.root, "1")
    try
    {
      actionContainer.addTransferAction( actionContainer.root, "1")
      Assert.fail( "Expected IllegalArgumentException")
    }
    catch
    {
      case ex: IllegalArgumentException =>
      {}
    }
  }

  @Test def createRemoveAction1( )
  {
    val actionContainer = this.createActionContainer( "addRemoveAction1")
    val c = actionContainer.addRemoveAction( actionContainer.root, "1")
    Assert.assertTrue( c.isInstanceOf[ RemoveAction ])
    Assert.assertEquals( c.name, "1")
    val actions = actionContainer.children( actionContainer.root)
    Assert.assertEquals( actions.size, 1)
    Assert.assertTrue( actions.find( x => x.name == c.name && x.getClass == c.getClass).isDefined)
  }

  @Test def createRemoveAction2( )
  {
    val actionContainer = this.createActionContainer( "createRemoveAction2")
    actionContainer.addRemoveAction( actionContainer.root, "1")
    try
    {
      actionContainer.addRemoveAction( actionContainer.root, "1")
      Assert.fail( "Expected IllegalArgumentException")
    }
    catch
    {
      case ex: IllegalArgumentException =>
      {}
    }
  }

  @Test def createDirectoryAction1( )
  {
    val actionContainer = this.createActionContainer( "addDirectoryAction1")
    val c = actionContainer.addDirectoryAction( actionContainer.root, "1")
    Assert.assertTrue( c.isInstanceOf[ DirectoryAction ])
    Assert.assertEquals( c.name, "1")
    val actions = actionContainer.children( actionContainer.root)
    Assert.assertEquals( actions.size, 1)
    Assert.assertTrue( actions.find( x => x.name == c.name && x.getClass == c.getClass).isDefined)
  }

  @Test def createDirectoryAction2( )
  {
    val actionContainer = this.createActionContainer( "createDirectoryAction2")
    actionContainer.addDirectoryAction( actionContainer.root, "1")
    try
    {
      actionContainer.addDirectoryAction( actionContainer.root, "1")
      Assert.fail( "Expected IllegalArgumentException")
    }
    catch
    {
      case ex: IllegalArgumentException =>
      {}
    }
  }

  /**
   * Getting the children of a newly added DirectoryAction should return zero children.
   */
  @Test def children1( )
  {
    val actionContainer = this.createActionContainer( "children1")
    val c = actionContainer.addDirectoryAction( actionContainer.root, "1")
    Assert.assertEquals( actionContainer.children( c).size, 0)
  }

  /**
   * Get should return empty DirectoryActions if they were previously added.
   */
  @Test def children2( )
  {
    val actionContainer = this.createActionContainer( "children2")
    val parent = actionContainer.addDirectoryAction( actionContainer.root, "1")
    val child = actionContainer.addDirectoryAction( parent, "2")
    val parentActions = actionContainer.children( parent)
    Assert.assertEquals( parentActions.size, 1)
    Assert.assertTrue( parentActions.find( x => x.name == child.name && x.getClass == child.getClass).isDefined)
    Assert.assertEquals( actionContainer.children( child).size, 0)
  }

  /**
   * Add children to non-root DirectoryAction
   */
  @Test def children3( )
  {
    val actionContainer = this.createActionContainer( "children3")
    val parent = actionContainer.addDirectoryAction( actionContainer.root, "1")
    val rootActions = actionContainer.children( actionContainer.root)
    Assert.assertEquals( rootActions.size, 1)
    Assert.assertTrue( rootActions.exists( _.name == parent.name))
    Assert.assertTrue( rootActions.exists( _.getClass == parent.getClass))
    Assert.assertEquals( actionContainer.children( parent).size, 0)

    val child = actionContainer.addDirectoryAction( parent, "2")
    val parentActions = actionContainer.children( parent)
    Assert.assertEquals( parentActions.size, 1)
    Assert.assertTrue( parentActions.find( x => x.name == child.name && x.getClass == child.getClass).isDefined)
  }

  /**
   * Multiple children added to non-root DirectoryAction
   * - Check multiple adds appends correctly
   */
  @Test def children4( )
  {
    val actionContainer = this.createActionContainer( "children4")
    val parent = actionContainer.addDirectoryAction( actionContainer.root, "1")
    val rootActions = actionContainer.children( actionContainer.root)
    Assert.assertEquals( rootActions.size, 1)
    Assert.assertTrue( rootActions.exists( _.name == parent.name))
    Assert.assertTrue( rootActions.exists( _.getClass == parent.getClass))
    Assert.assertEquals( actionContainer.children( parent).size, 0)

    val child1 = actionContainer.addDirectoryAction( parent, "1")
    val parentActions1 = actionContainer.children( parent)
    Assert.assertEquals( parentActions1.size, 1)
    Assert.assertTrue( parentActions1.find( x => x.name == child1.name && x.getClass == child1.getClass).isDefined)

    val child2 = actionContainer.addDirectoryAction( parent, "2")
    val parentActions2 = actionContainer.children( parent)
    Assert.assertEquals( parentActions2.size, 2)
    Assert.assertTrue( parentActions2.find( x => x.name == child1.name && x.getClass == child1.getClass).isDefined)
    Assert.assertTrue( parentActions2.find( x => x.name == child2.name && x.getClass == child2.getClass).isDefined)
  }

  /**
   * Remove root actions
   */
  @Test def remove1( )
  {
    val actionContainer = this.createActionContainer( "remove1")

    val c1 = actionContainer.addCreateDirAction( actionContainer.root, "1")
    val c2 = actionContainer.addTransferAction( actionContainer.root, "2")
    val c3 = actionContainer.addRemoveAction( actionContainer.root, "3")
    val c4 = actionContainer.addDirectoryAction( actionContainer.root, "4")

    actionContainer.remove( c1)
    actionContainer.remove( c2)
    actionContainer.remove( c3)
    actionContainer.remove( c4)

    val actions = actionContainer.children( actionContainer.root)
    Assert.assertEquals( actions.size, 0)
  }

  /**
   * Remove nested actions
   */
  @Test def remove2( )
  {
    val actionContainer = this.createActionContainer( "remove2")

    val parent = actionContainer.addDirectoryAction( actionContainer.root, "parent")
    val c1 = actionContainer.addCreateDirAction( parent, "1")
    val c2 = actionContainer.addTransferAction( parent, "2")
    val c3 = actionContainer.addRemoveAction( parent, "3")
    val c4 = actionContainer.addDirectoryAction( parent, "4")

    actionContainer.remove( parent)

    val actions = actionContainer.children( actionContainer.root)
    Assert.assertEquals( actions.size, 0)
  }

  /**
   * Remove actions more than once.
   */
  @Test def remove3( )
  {
    val actionContainer = this.createActionContainer( "remove3")
    val c = actionContainer.addCreateDirAction( actionContainer.root, "1")
    actionContainer.remove( c)

    try
    {
      actionContainer.remove( c)
      Assert.fail( "Expected IllegalArgumentException")
    }
    catch
    {
      case ex: IllegalArgumentException =>
    }
  }
}