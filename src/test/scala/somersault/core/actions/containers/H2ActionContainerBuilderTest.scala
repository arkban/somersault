package somersault.core.actions.containers

import java.io.File
import org.apache.commons.io.FileUtils
import org.testng.Assert
import org.testng.annotations.Test
import somersault.core.TestUtil
import somersault.util.{FilenameUtils2, H2DataAccessObject, VersionedDataAccessObject, Version, VersionMismatchException}

class H2ActionContainerBuilderTest extends ActionContainerBuilderTest {

  private val actionContainerFileName = "h2actioncontainer"

  protected def createTestPath(testName: String): String = {
    val testPath = FilenameUtils2.concat(FilenameUtils2.tempPath.getPath, this.getClass.getSimpleName, testName)
    FileUtils.forceMkdir(new File(testPath))
    testPath
  }

  protected def createActionContainer(testName: String) =
    new H2ActionContainerBuilder(
      new H2ActionContainerBuilderData(
        TestUtil.logger, new File(FilenameUtils2.concat(createTestPath(testName), actionContainerFileName))))

  /**
   * Test that index checks its versions. We create an index, modify its schemaVersion, then re-open that index, and the
   * re-opening should fail on the schemaVersion check.
   */
  @Test def errorOnDiffVersion() {
    val testName = "errorOnDiffVersion"
    val data = new H2ActionContainerBuilderData(
      TestUtil.logger, new File(FilenameUtils2.concat(createTestPath(testName), actionContainerFileName)))

    // initialized new index and change schemaVersion number
    val index = new H2ActionContainerBuilder(data)
    val rawIndexDao = new H2DataAccessObject with VersionedDataAccessObject {
      def dataSource = createDataSource(data.file, deleteIfExists = false)

      val schemaVersion = new Version(1, 2, 3)
    }
    val currVersion = rawIndexDao.readVersion(rawIndexDao.dataSource)
    val newVersion = new Version(currVersion.major + 1, currVersion.minor + 1, currVersion.release + 1)
    rawIndexDao.writeVersion(rawIndexDao.dataSource, newVersion)

    // re-open index, expecting error on different schemaVersion that expected
    try {
      new H2ActionContainerBuilder(data)
      Assert.fail("expected VersionMismatchException")
    }
    catch {
      case ex: VersionMismatchException =>
    }
  }
}