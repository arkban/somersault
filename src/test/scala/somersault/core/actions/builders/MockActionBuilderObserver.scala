package somersault.core.actions.builders

import java.lang.{Exception, String}
import logahawk.Severity
import scala.collection.mutable
import somersault.core.comparators.SomerFileComparator
import somersault.core.filters.SomerFilter

/**
 * This does no logging but does save all errors it finds.
 */
class MockActionBuilderObserver extends ActionBuilderObserver {

  val errors = new mutable.ListBuffer[Exception]

  def start() {}

  def end() {}

  def recurse(path: List[String]) {}

  def filterAccept(filter: SomerFilter, path: List[String]) {}

  def filterReject(filter: SomerFilter, path: List[String]) {}

  def comparatorAccept(comparator: SomerFileComparator, path: List[String]) {}

  def comparatorReject(comparator: SomerFileComparator, path: List[String]) {}

  def error(severity: Severity, ex: Exception) { errors += ex }
}