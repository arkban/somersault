package somersault.core.actions.builders.backup

import org.testng.annotations.Test
import somersault.core.actions.CreateDirAction
import somersault.core.actions.DirectoryAction
import somersault.core.actions.RemoveAction
import somersault.core.actions.TransferAction
import somersault.core.actions.builders.MockActionBuilderObserver
import somersault.core.actions.containers.InMemoryActionContainerBuilder
import somersault.core.actions.runners.{MockActionRunnerObserver, ActionRunnerController, FileSystemRunnerData}
import somersault.core.filesystems._
import somersault.core.{ScenarioData, Scenario}
import somersault.util.{Assert, FilenameUtils2}

class MiscTest {

  /**
   * Both file systems are empty.
   * - Check that no actions are created.
   */
  @Test def empty() {
    val src = FakeFileSystem.connect("MiscTest-empty-Src")
    val dest = FakeFileSystem.connect("MiscTest-empty-Dest")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    Assert.areEqual(actions.children(actions.root).size, 0)

    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.areEqual(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.areEqual(filesAfter.size, 0)
  }

  /**
   * Source contains a directory at the root, dest contains a different directory at root.
   * - Check that dest directory is removed.
   * - Check that src directory is created in dest.
   */
  @Test def createAndRemoveDir() {
    val src = FakeFileSystem.connect("MiscTest-createAndRemoveDir-Src")
    val d1 = src.createDir(src.root, "1")
    val dest = FakeFileSystem.connect("MiscTest-createAndRemoveDir-Dest")
    val d2 = dest.createDir(dest.root, "2")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    Assert.areEqual(actions.children(actions.root).size, 3)
    Assert.isTrue(
      actions.children(actions.root).exists(
        x => x.isInstanceOf[CreateDirAction] && x.asInstanceOf[CreateDirAction].name == d1.name))
    Assert.isTrue(
      actions.children(actions.root).exists(
        x => x.isInstanceOf[DirectoryAction] && x.asInstanceOf[DirectoryAction].name == d1.name))
    Assert.isTrue(
      actions.children(actions.root).exists(
        x => x.isInstanceOf[RemoveAction] && x.asInstanceOf[RemoveAction].name == d2.name))

    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.areEqual(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.areEqual(filesAfter.size, 1)
    Assert.isTrue(filesAfter.forall(_.isInstanceOf[SomerDirectory]))
    Assert.isTrue(filesAfter.exists(_.name == d1.name))
  }

  /**
   * Transfer nested files that do not exist in the dest. The directory does not exist in the dest.
   */
  @Test def createDirAndTransfer() {
    val src = FakeFileSystem.connect("MiscTest-createDirAndTransfer-Src")
    src.createDir(src.root, "1")
    val dest = FakeFileSystem.connect("MiscTest-createDirAndTransfer-Dest")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val srcF1 = new MockSomerFile("/1/A")
    val srcF2 = new MockSomerFile("/1/B")
    src.add(srcF1, srcF2)

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    Assert.areEqual(actions.children(actions.root).size, 2)
    Assert.isTrue(actions.children(actions.root).exists(_.isInstanceOf[CreateDirAction]))
    Assert.isTrue(actions.children(actions.root).exists(_.isInstanceOf[DirectoryAction]))

    val dirAction = actions
      .children(actions.root)
      .find(_.isInstanceOf[DirectoryAction])
      .get
      .asInstanceOf[DirectoryAction]
    Assert.areEqual(actions.children(dirAction).size, 2)
    Assert.isTrue(actions.children(dirAction).forall(_.isInstanceOf[TransferAction]))
    Assert.isTrue(actions.children(dirAction).exists(_.asInstanceOf[TransferAction].name == srcF1.name))
    Assert.isTrue(actions.children(dirAction).exists(_.asInstanceOf[TransferAction].name == srcF2.name))

    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.areEqual(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.areEqual(filesAfter.size, 1)
    Assert.isTrue(filesAfter.forall(_.isInstanceOf[SomerDirectory]))

    val subFilesAfter = dest.children(
      filesAfter.find(_.isInstanceOf[SomerDirectory]).get.asInstanceOf[SomerDirectory])
    Assert.areEqual(subFilesAfter.size, 2)
    Assert.isTrue(subFilesAfter.forall(_.isInstanceOf[SomerFile]))
    Assert.isTrue(subFilesAfter.exists(_.name == srcF1.name))
    Assert.isTrue(subFilesAfter.exists(_.name == srcF2.name))
  }

  /**
   * Overwrite a directory in the dest with a file in the source.
   */
  @Test def removeDirAddFile1() {
    val src = FakeFileSystem.connect("MiscTest-removeDirAddFile1-Src")
    val dest = FakeFileSystem.connect("MiscTest-removeDirAddFile1-Dest")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val srcF1 = new MockSomerFile("/1")
    val srcF2 = new MockSomerFile("/2")
    src.add(srcF1, srcF2)

    dest.createDir(dest.root, srcF1.name)
    dest.createDir(dest.root, srcF2.name)

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    Assert.areEqual(actions.children(actions.root).size, 4)

    val removeActions = actions.children(actions.root).filter(_.isInstanceOf[RemoveAction]).toList
    Assert.areEqual(removeActions.size, 2)
    Assert.isTrue(removeActions.exists(_.asInstanceOf[RemoveAction].name == srcF1.name))
    Assert.isTrue(removeActions.exists(_.asInstanceOf[RemoveAction].name == srcF2.name))

    val transferActions = actions.children(actions.root).filter(_.isInstanceOf[TransferAction]).toList
    Assert.areEqual(transferActions.size, 2)
    Assert.isTrue(transferActions.exists(_.asInstanceOf[TransferAction].name == srcF1.name))
    Assert.isTrue(transferActions.exists(_.asInstanceOf[TransferAction].name == srcF2.name))

    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.areEqual(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.areEqual(filesAfter.size, 2)
    Assert.isTrue(filesAfter.forall(_.isInstanceOf[SomerFile]))
    Assert.isTrue(filesAfter.exists(_.name == srcF1.name))
    Assert.isTrue(filesAfter.exists(_.name == srcF2.name))
  }

  /**
   * Overwrite a directory in the dest with a file in the source. Restore is enabled.
   * - Check that an error is created.
   * - Check that no actions are created.
   */
  @Test def removeDirAddFile2() {
    val src = FakeFileSystem.connect("MiscTest-removeDirAddFile2-Src")
    val dest = FakeFileSystem.connect("MiscTest-removeDirAddFile2-Dest")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val srcF1 = new MockSomerFile("/1")
    src.add(srcF1)

    dest.createDir(dest.root, srcF1.name)

    val actions = new InMemoryActionContainerBuilder
    val observer = new MockActionBuilderObserver
    new Backup().build(new BackupData(scenario, actions, observer, true))
    Assert.areEqual(observer.errors.size, 1)
    Assert.areEqual(actions.children(actions.root).size, 0)
  }

  /**
   * Overwrite a file in the dest with a directory in the source.
   */
  @Test def removeFileAddDir1() {
    val src = FakeFileSystem.connect("MiscTest-removeFileAddDir1-Src")
    val dest = FakeFileSystem.connect("MiscTest-removeFileAddDir1-Dest")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val srcD1 = src.createDir(src.root, "1")
    val srcD2 = src.createDir(src.root, "2")

    val destF1 = new MockSomerFile(FilenameUtils2.dirSeparator + srcD1.name)
    val destF2 = new MockSomerFile(FilenameUtils2.dirSeparator + srcD2.name)
    dest.add(destF1, destF2)

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    Assert.areEqual(actions.children(actions.root).size, 6)

    val removeActions = actions.children(actions.root).filter(_.isInstanceOf[RemoveAction]).toList
    Assert.areEqual(removeActions.size, 2)
    Assert.isTrue(removeActions.exists(_.asInstanceOf[RemoveAction].name == srcD1.name))
    Assert.isTrue(removeActions.exists(_.asInstanceOf[RemoveAction].name == srcD1.name))

    val createDirActions = actions.children(actions.root).filter(_.isInstanceOf[CreateDirAction]).toList
    Assert.areEqual(createDirActions.size, 2)
    Assert.isTrue(createDirActions.exists(_.asInstanceOf[CreateDirAction].name == srcD1.name))
    Assert.isTrue(createDirActions.exists(_.asInstanceOf[CreateDirAction].name == srcD2.name))

    val dirActions = actions.children(actions.root).filter(_.isInstanceOf[DirectoryAction]).toList
    Assert.areEqual(dirActions.size, 2)
    Assert.isTrue(dirActions.exists(_.asInstanceOf[DirectoryAction].name == srcD1.name))
    Assert.isTrue(dirActions.exists(_.asInstanceOf[DirectoryAction].name == srcD2.name))

    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.areEqual(observer.errors.size, 0, observer.errors.toString())

    val filesAfter = dest.children(dest.root)
    Assert.areEqual(filesAfter.size, 2)
    Assert.isTrue(filesAfter.forall(_.isInstanceOf[SomerDirectory]))
    Assert.isTrue(filesAfter.exists(_.name == srcD1.name))
    Assert.isTrue(filesAfter.exists(_.name == srcD2.name))
  }

  /**
   * Overwrite a file in the dest with a directory in the source.
   */
  @Test def removeFileAddDir2() {
    val src = FakeFileSystem.connect("MiscTest-removeFileAddDir2-Src")
    val dest = FakeFileSystem.connect("MiscTest-removeFileAddDir2-Dest")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val srcD1 = src.createDir(src.root, "1")

    val destF1 = new MockSomerFile(FilenameUtils2.dirSeparator + srcD1.name)
    dest.add(destF1)

    val actions = new InMemoryActionContainerBuilder
    val observer = new MockActionBuilderObserver
    new Backup().build(new BackupData(scenario, actions, observer, true))
    Assert.areEqual(observer.errors.size, 1)
    Assert.areEqual(actions.children(actions.root).size, 0)
  }

  /**
   * Ensure that lock (represented as a file) in the source is NOT transferred.
   */
  @Test def lockSrc() {
    val src = FakeFileSystem.connect("MiscTest-lock1-Src")
    val dest = FakeFileSystem.connect("MiscTest-lock1-Dest")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    src.lock(LockingFileSystemMode.Read)

    Assert.isTrue(src.hasLock(LockingFileSystemMode.Read))

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    Assert.areEqual(actions.children(actions.root).size, 0)
  }

  /**
   * Ensure that lock (represented as a file) in the dest is NOT removed.
   */
  @Test def lockDest() {
    val src = FakeFileSystem.connect("MiscTest-lock1-Src")
    val dest = FakeFileSystem.connect("MiscTest-lock1-Dest")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    dest.lock(LockingFileSystemMode.Write)

    Assert.isTrue(dest.hasLock(LockingFileSystemMode.Write))

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    actions.children(actions.root)
    Assert.areEqual(actions.children(actions.root).size, 0)
  }
}