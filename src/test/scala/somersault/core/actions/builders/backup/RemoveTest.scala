package somersault.core.actions.builders.backup

import org.testng.Assert
import org.testng.annotations.Test
import somersault.core.actions.builders.MockActionBuilderObserver
import somersault.core.actions.containers.InMemoryActionContainerBuilder
import somersault.core.actions.runners.{MockActionRunnerObserver, FileSystemRunnerData, ActionRunnerController}
import somersault.core.actions.{DirectoryAction, RemoveAction}
import somersault.core.filesystems.{MockSomerFile, FakeFileSystem}
import somersault.core.{ScenarioData, Scenario}

class RemoveTest {

  /**
   * Source file system is empty, dest contains only files at root
   * - Check that root files are removed.
   */
  @Test def remove1() {
    val src = FakeFileSystem.connect("RemoveTest-remove1-Src")
    val dest = FakeFileSystem.connect("RemoveTest-remove1-Dest")
    dest.add(new MockSomerFile("/1"), new MockSomerFile("/2"))
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    Assert.assertEquals(actions.children(actions.root).size, 2)
    Assert.assertTrue(actions.children(actions.root).forall(_.isInstanceOf[RemoveAction]))

    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.assertEquals(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.assertEquals(filesAfter.size, 0)
  }

  /**
   * Source file system is empty, dest contains only directories at root.
   * - Check that root directories are removed.
   */
  @Test def remove2() {
    val src = FakeFileSystem.connect("RemoveTest-remove2-Src")
    val dest = FakeFileSystem.connect("RemoveTest-remove2-Dest")
    dest.createDir(dest.root, "1")
    dest.createDir(dest.root, "2")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    Assert.assertEquals(actions.children(actions.root).size, 2)
    Assert.assertTrue(actions.children(actions.root).forall(_.isInstanceOf[RemoveAction]))

    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.assertEquals(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.assertEquals(filesAfter.size, 0)
  }

  /**
   * Source file system is empty, dest contains a tree of nested directories.
   * - Check that only root directories are scheduled to be removed (nested directories should not be explicitly listed).
   */
  @Test def remove3() {
    val src = FakeFileSystem.connect("RemoveTest-remove3-Src")
    val dest = FakeFileSystem.connect("RemoveTest-remove3-Dest")
    val d1 = dest.createDir(dest.root, "1")
    dest.createDir(d1, "A")
    dest.createDir(d1, "B")
    val d2 = dest.createDir(dest.root, "2")
    dest.createDir(d2, "C")
    dest.createDir(d2, "D")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    Assert.assertEquals(actions.children(actions.root).size, 2)
    Assert.assertTrue(actions.children(actions.root).forall(_.isInstanceOf[RemoveAction]))

    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.assertEquals(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.assertEquals(filesAfter.size, 0)
  }

  /**
   * Source file system is empty, dest contains root directories and nested files.
   * - Check that only root directories are scheduled to be removed (nested files should not be explicitly listed).
   */
  @Test def remove4() {
    val src = FakeFileSystem.connect("RemoveTest-remove4-Src")
    val dest = FakeFileSystem.connect("RemoveTest-remove4-Dest")
    dest.createDir(dest.root, "1")
    dest.add(new MockSomerFile("/1/A"), new MockSomerFile("/1/B"))
    dest.createDir(dest.root, "2")
    dest.add(new MockSomerFile("/2/C"), new MockSomerFile("/2/D"))
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    Assert.assertEquals(actions.children(actions.root).size, 2)
    Assert.assertTrue(actions.children(actions.root).forall(_.isInstanceOf[RemoveAction]))

    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.assertEquals(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.assertEquals(filesAfter.size, 0)
  }

  /**
   * Source file system is non-empty, dest contains a tree of nested directories.
   * - Check that root directories are NOT scheduled to be removed.
   * - Check that nested directories are scheduled to be removed.
   */
  @Test def remove5() {
    val src = FakeFileSystem.connect("RemoveTest-remove5-Src")
    src.createDir(src.root, "1")
    src.createDir(src.root, "2")
    val dest = FakeFileSystem.connect("RemoveTest-remove5-Dest")
    val d1 = dest.createDir(dest.root, "1")
    dest.createDir(d1, "A")
    dest.createDir(d1, "B")
    val d2 = dest.createDir(dest.root, "2")
    dest.createDir(d2, "C")
    dest.createDir(d2, "D")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    Assert.assertEquals(actions.children(actions.root).size, 2)
    Assert.assertTrue(actions.children(actions.root).forall(_.isInstanceOf[DirectoryAction]))

    val dirAction1 = actions.children(actions.root)
      .find(_.asInstanceOf[DirectoryAction].name == "1").get.asInstanceOf[DirectoryAction]
    Assert.assertTrue(actions.children(dirAction1).exists(_.asInstanceOf[RemoveAction].name == "A"))
    Assert.assertTrue(actions.children(dirAction1).exists(_.asInstanceOf[RemoveAction].name == "B"))

    val dirAction2 = actions.children(actions.root)
      .find(_.asInstanceOf[DirectoryAction].name == "2").get.asInstanceOf[DirectoryAction]
    Assert.assertTrue(actions.children(dirAction2).exists(_.asInstanceOf[RemoveAction].name == "C"))
    Assert.assertTrue(actions.children(dirAction2).exists(_.asInstanceOf[RemoveAction].name == "D"))

    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.assertEquals(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.assertEquals(filesAfter.size, 2)
  }

  /**
   * Source file system is non-empty, dest contains a tree of nested directories.
   * - Check that root directories are NOT scheduled to be removed.
   * - Check that nested files are scheduled to be removed.
   */
  @Test def remove6() {
    val src = FakeFileSystem.connect("RemoveTest-remove6-Src")
    src.createDir(src.root, "1")
    src.createDir(src.root, "2")
    val dest = FakeFileSystem.connect("RemoveTest-remove6-Dest")
    dest.createDir(dest.root, "1")
    dest.add(new MockSomerFile("/1/A"), new MockSomerFile("/1/B"))
    dest.createDir(dest.root, "2")
    dest.add(new MockSomerFile("/2/C"), new MockSomerFile("/2/D"))
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    Assert.assertEquals(actions.children(actions.root).size, 2)
    Assert.assertTrue(actions.children(actions.root).forall(_.isInstanceOf[DirectoryAction]))

    val dirAction1 = actions.children(actions.root)
      .find(_.asInstanceOf[DirectoryAction].name == "1").get.asInstanceOf[DirectoryAction]
    Assert.assertTrue(actions.children(dirAction1).exists(_.asInstanceOf[RemoveAction].name == "A"))
    Assert.assertTrue(actions.children(dirAction1).exists(_.asInstanceOf[RemoveAction].name == "B"))

    val dirAction2 = actions.children(actions.root)
      .find(_.asInstanceOf[DirectoryAction].name == "2").get.asInstanceOf[DirectoryAction]
    Assert.assertTrue(actions.children(dirAction2).exists(_.asInstanceOf[RemoveAction].name == "C"))
    Assert.assertTrue(actions.children(dirAction2).exists(_.asInstanceOf[RemoveAction].name == "D"))

    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.assertEquals(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.assertEquals(filesAfter.size, 2)
  }

  /**
   * Source file system is empty, dest contains root directories and nested files. Restore is enabled.
   * - Check that no RemoveActions are created.
   */
  @Test def remove7() {
    val src = FakeFileSystem.connect("RemoveTest-remove4-Src")
    val dest = FakeFileSystem.connect("RemoveTest-remove4-Dest")
    dest.createDir(dest.root, "1")
    dest.createDir(dest.root, "2")
    dest.add(new MockSomerFile("/A"))
    dest.add(new MockSomerFile("/B"))
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, true))

    Assert.assertEquals(actions.children(actions.root).size, 0)
  }
}