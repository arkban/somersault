package somersault.core.actions.builders.backup

import org.testng.Assert
import org.testng.annotations.Test
import somersault.core.actions.builders.MockActionBuilderObserver
import somersault.core.actions.containers.InMemoryActionContainerBuilder
import somersault.core.actions.{RemoveAction, DirectoryAction, CreateDirAction, TransferAction}
import somersault.core.filesystems.{MockSomerFile, FakeFileSystem}
import somersault.core.filters.{BooleanFilter, NameFilter}
import somersault.core.{ScenarioData, Scenario}

class FilterTest {

  /**
   * Transfer only files at the root that meet the filter criteria.
   */
  @Test def filter1() {
    val src = FakeFileSystem.connect("FilterTest-filter1-Src")
    val dest = FakeFileSystem.connect("FilterTest-filter1-Dest")
    val srcF1 = new MockSomerFile("/1")
    val srcF2 = new MockSomerFile("/2")
    src.add(srcF1, srcF2)
    val scenario = new Scenario(
      new ScenarioData("name", "dest", src.data, dest.data, filter = new NameFilter(".*" + srcF1.name + ".*")),
      src,
      dest)

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    Assert.assertEquals(actions.children(actions.root).size, 1)
    Assert.assertTrue(actions.children(actions.root).forall(_.isInstanceOf[TransferAction]))
    Assert.assertTrue(actions.children(actions.root).forall(_.asInstanceOf[TransferAction].name == srcF1.name))
  }

  /**
   * Transfer only nested files that meet the filter criteria.
   */
  @Test def filter2() {
    val src = FakeFileSystem.connect("FilterTest-filter2-Src")
    src.createDir(src.root, "1")
    val dest = FakeFileSystem.connect("FilterTest-filter2-Dest")
    val scenario = new Scenario(
      new ScenarioData("name", "dest", src.data, dest.data, filter = new NameFilter(".*\\d.*")),
      src,
      dest)

    val srcF1 = new MockSomerFile("/1/2")
    val srcF2 = new MockSomerFile("/1/B")
    src.add(srcF1, srcF2)

    // must match directory and files
    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    Assert.assertEquals(actions.children(actions.root).size, 2)
    Assert.assertTrue(actions.children(actions.root).exists(_.isInstanceOf[CreateDirAction]))
    Assert.assertTrue(actions.children(actions.root).exists(_.isInstanceOf[DirectoryAction]))

    val dirAction = actions
      .children(actions.root)
      .find(_.isInstanceOf[DirectoryAction])
      .get
      .asInstanceOf[DirectoryAction]
    Assert.assertEquals(actions.children(dirAction).size, 1)
    Assert.assertTrue(actions.children(dirAction).exists(_.isInstanceOf[TransferAction]))
    Assert.assertTrue(actions.children(dirAction).forall(_.asInstanceOf[TransferAction].name == srcF1.name))
  }

  /**
   * Same file exists in both source and dest, but filter causes the dest file to be removed.
   */
  @Test def filter3() {
    val src = FakeFileSystem.connect("FilterTest-filter3-Src")
    val dest = FakeFileSystem.connect("FilterTest-filter3-Dest")
    val scenario = new Scenario(
      new ScenarioData("name", "dest", src.data, dest.data, filter = new BooleanFilter(false)),
      src,
      dest)

    val file = new MockSomerFile("/1")
    src.add(file)
    dest.add(file)

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    Assert.assertEquals(actions.children(actions.root).size, 1)
    Assert.assertTrue(actions.children(actions.root).forall(_.isInstanceOf[RemoveAction]))
  }
}