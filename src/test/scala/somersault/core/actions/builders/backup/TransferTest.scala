package somersault.core.actions.builders.backup

import org.joda.time.DateTime
import org.testng.Assert
import org.testng.annotations.Test
import somersault.core.actions.builders.MockActionBuilderObserver
import somersault.core.actions.containers.InMemoryActionContainerBuilder
import somersault.core.actions.{DirectoryAction, TransferAction}
import somersault.core.comparators.{HashComparator, SizeComparator, ModifyDateComparator}
import somersault.core.filesystems.{MockSomerFile, FakeFileSystem}
import somersault.core.{ScenarioData, Scenario}

class TransferTest {

  /**
   * Transfer files at the root that do not exist in the dest.
   */
  @Test def transfer1() {
    val src = FakeFileSystem.connect("TransferTest-transfer1-Src")
    val dest = FakeFileSystem.connect("TransferTest-transfer1-Dest")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val srcF1 = new MockSomerFile("/1")
    val srcF2 = new MockSomerFile("/2")
    src.add(srcF1, srcF2)

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    Assert.assertEquals(actions.children(actions.root).size, 2)
    Assert.assertTrue(actions.children(actions.root).forall(_.isInstanceOf[TransferAction]))
    Assert.assertTrue(actions.children(actions.root).exists(_.asInstanceOf[TransferAction].name == srcF1.name))
    Assert.assertTrue(actions.children(actions.root).exists(_.asInstanceOf[TransferAction].name == srcF2.name))
  }

  /**
   * Transfer nested files that do not exist in the dest. The directory exists in the dest.
   */
  @Test def transfer2() {
    val src = FakeFileSystem.connect("TransferTest-transfer2-Src")
    val srcDir = src.createDir(src.root, "1")
    val dest = FakeFileSystem.connect("TransferTest-transfer2-Dest")
    dest.createDir(src.root, srcDir.name)
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val srcF1 = new MockSomerFile("/1/A")
    val srcF2 = new MockSomerFile("/1/B")
    src.add(srcF1, srcF2)

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    Assert.assertEquals(actions.children(actions.root).size, 1)
    Assert.assertTrue(actions.children(actions.root).exists(_.isInstanceOf[DirectoryAction]))

    val actionsRoot = actions
      .children(actions.root)
      .find(_.isInstanceOf[DirectoryAction])
      .get
      .asInstanceOf[DirectoryAction]
    Assert.assertEquals(actions.children(actionsRoot).size, 2)
    Assert.assertTrue(actions.children(actionsRoot).forall(_.isInstanceOf[TransferAction]))
    Assert.assertTrue(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcF1.name))
    Assert.assertTrue(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcF2.name))
  }

  /**
   * Overwrite root files that exist in the dest. The files are different based on date.
   */
  @Test def overwriteByDate1() {
    val src = FakeFileSystem.connect("overwriteByDate1-Src")
    val dest = FakeFileSystem.connect("overwriteByDate1-Dest")
    val scenario = new Scenario(
      new ScenarioData("name", "dest", src.data, dest.data, comparator = new ModifyDateComparator(0)), src, dest)

    val content = "hello world"
    val date = new DateTime(10000000L)

    val srcControl = new MockSomerFile("/control", date, content)
    val srcF1 = new MockSomerFile("/1", date, content)
    val srcF2 = new MockSomerFile("/2", date, content)
    src.add(srcControl, srcF1, srcF2)

    val destControl = new MockSomerFile(srcControl.fullPath, date, srcControl.data)
    val destF1 = new MockSomerFile(srcF1.fullPath, new DateTime(date.getMillis - 1), srcF1.data)
    val destF2 = new MockSomerFile(srcF2.fullPath, new DateTime(date.getMillis + 1), srcF2.data)
    dest.add(destControl, destF1, destF2)

    val actions = new InMemoryActionContainerBuilder
    val observer = new MockActionBuilderObserver
    new Backup().build(new BackupData(scenario, actions, observer, false))
    Assert.assertEquals(observer.errors.size, 0)

    val actionsRoot = actions.root
    Assert.assertEquals(actions.children(actions.root).size, 2)
    Assert.assertTrue(actions.children(actionsRoot).forall(_.isInstanceOf[TransferAction]))
    Assert.assertFalse(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcControl.name))
    Assert.assertTrue(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcF1.name))
    Assert.assertTrue(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcF2.name))
  }

  /**
   * Overwrite nested files that exist in the dest. The files are different based on date.
   */
  @Test def overwriteByDate2() {
    val src = FakeFileSystem.connect("overwriteByDate2-Src")
    val srcDir = src.createDir(src.root, "1")
    val dest = FakeFileSystem.connect("overwriteByDate2-Dest")
    dest.createDir(src.root, srcDir.name)
    val scenario = new Scenario(
      new ScenarioData("name", "dest", src.data, dest.data, comparator = new ModifyDateComparator(0)), src, dest)

    val content = "hello world"
    val date = new DateTime(10000000L)

    val srcControl = new MockSomerFile("/1/control", date, content)
    val srcF1 = new MockSomerFile("/1/1", date, content)
    val srcF2 = new MockSomerFile("/1/2", date, content)
    src.add(srcControl, srcF1, srcF2)

    val destControl = new MockSomerFile(srcControl.fullPath, date, srcControl.data)
    val destF1 = new MockSomerFile(srcF1.fullPath, new DateTime(date.getMillis - 1), srcF1.data)
    val destF2 = new MockSomerFile(srcF2.fullPath, new DateTime(date.getMillis + 1), srcF2.data)
    dest.add(destControl, destF1, destF2)

    val actions = new InMemoryActionContainerBuilder
    val observer = new MockActionBuilderObserver
    new Backup().build(new BackupData(scenario, actions, observer, false))
    Assert.assertEquals(observer.errors.size, 0)

    Assert.assertEquals(actions.children(actions.root).size, 1)
    Assert.assertTrue(actions.children(actions.root).exists(_.isInstanceOf[DirectoryAction]))

    val actionsRoot = actions
      .children(actions.root)
      .find(_.isInstanceOf[DirectoryAction])
      .get
      .asInstanceOf[DirectoryAction]
    Assert.assertEquals(actions.children(actionsRoot).size, 2)
    Assert.assertTrue(actions.children(actionsRoot).forall(_.isInstanceOf[TransferAction]))
    Assert.assertFalse(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcControl.name))
    Assert.assertTrue(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcF1.name))
    Assert.assertTrue(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcF2.name))
  }

  /**
   * Overwrite root files that exist in the dest. The files are different based on content, and the check is on size.
   */
  @Test def overwriteBySize1() {
    val src = FakeFileSystem.connect("overwriteBySize1-Src")
    val dest = FakeFileSystem.connect("overwriteBySize1-Dest")
    val scenario = new Scenario(
      new ScenarioData("name", "dest", src.data, dest.data, comparator = new SizeComparator), src, dest)

    val content = "hello world"
    val date = new DateTime(10000000L)

    val srcControl = new MockSomerFile("/control", date, content)
    val srcF1 = new MockSomerFile("/1", date, content)
    val srcF2 = new MockSomerFile("/2", date, content)
    src.add(srcControl, srcF1, srcF2)

    val destControl = new MockSomerFile(srcControl.fullPath, date, srcControl.data)
    val destF1 = new MockSomerFile(srcF1.fullPath, date, srcF1.data + "1")
    val destF2 = new MockSomerFile(srcF2.fullPath, date, srcF2.data.substring(1))
    dest.add(destControl, destF1, destF2)

    val actions = new InMemoryActionContainerBuilder
    val observer = new MockActionBuilderObserver
    new Backup().build(new BackupData(scenario, actions, observer, false))
    Assert.assertEquals(observer.errors.size, 0)

    val actionsRoot = actions.root
    Assert.assertEquals(actions.children(actions.root).size, 2)
    Assert.assertTrue(actions.children(actionsRoot).forall(_.isInstanceOf[TransferAction]))
    Assert.assertFalse(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcControl.name))
    Assert.assertTrue(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcF1.name))
    Assert.assertTrue(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcF2.name))
  }

  /**
   * Overwrite nested files that exist in the dest. The files are different based on content, and the check is on size.
   */
  @Test def overwriteBySize2() {
    val src = FakeFileSystem.connect("overwriteBySize2-Src")
    val srcDir = src.createDir(src.root, "1")
    val dest = FakeFileSystem.connect("overwriteBySize2-Dest")
    dest.createDir(src.root, srcDir.name)
    val scenario = new Scenario(
      new ScenarioData("name", "dest", src.data, dest.data, comparator = new SizeComparator), src, dest)

    val content = "hello world"
    val date = new DateTime(10000000L)

    val srcControl = new MockSomerFile("/1/control", date, content)
    val srcF1 = new MockSomerFile("/1/1", date, content)
    val srcF2 = new MockSomerFile("/1/2", date, content)
    src.add(srcControl, srcF1, srcF2)

    val destControl = new MockSomerFile(srcControl.fullPath, date, srcControl.data)
    val destF1 = new MockSomerFile(srcF1.fullPath, date, srcF1.data + "1")
    val destF2 = new MockSomerFile(srcF2.fullPath, date, srcF2.data.substring(1))
    dest.add(destControl, destF1, destF2)

    val actions = new InMemoryActionContainerBuilder
    val observer = new MockActionBuilderObserver
    new Backup().build(new BackupData(scenario, actions, observer, false))
    Assert.assertEquals(observer.errors.size, 0)

    Assert.assertEquals(actions.children(actions.root).size, 1)
    Assert.assertTrue(actions.children(actions.root).exists(_.isInstanceOf[DirectoryAction]))

    val actionsRoot = actions
      .children(actions.root)
      .find(_.isInstanceOf[DirectoryAction])
      .get
      .asInstanceOf[DirectoryAction]
    Assert.assertEquals(actions.children(actionsRoot).size, 2)
    Assert.assertTrue(actions.children(actionsRoot).forall(_.isInstanceOf[TransferAction]))
    Assert.assertFalse(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcControl.name))
    Assert.assertTrue(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcF1.name))
    Assert.assertTrue(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcF2.name))
  }

  /**
   * Overwrite root files that exist in the dest. The files are different based on content, and the check is on hash.
   */
  @Test def overwriteByHash1() {
    val src = FakeFileSystem.connect("overwriteByHash1-Src")
    val dest = FakeFileSystem.connect("overwriteByHash1-Dest")
    val scenario = new Scenario(
      new ScenarioData("name", "dest", src.data, dest.data, comparator = new HashComparator), src, dest)

    val content = "hello world"
    val date = new DateTime(10000000L)

    val srcControl = new MockSomerFile("/control", date, content)
    val srcF1 = new MockSomerFile("/1", date, content)
    val srcF2 = new MockSomerFile("/2", date, content)
    src.add(srcControl, srcF1, srcF2)

    val destControl = new MockSomerFile(srcControl.fullPath, date, srcControl.data)
    val destF1 = new MockSomerFile(srcF1.fullPath, date, srcF1.data + "1")
    val destF2 = new MockSomerFile(srcF2.fullPath, date, srcF2.data.substring(1))
    dest.add(destControl, destF1, destF2)

    val actions = new InMemoryActionContainerBuilder
    val observer = new MockActionBuilderObserver
    new Backup().build(new BackupData(scenario, actions, observer, false))
    Assert.assertEquals(observer.errors.size, 0)

    val actionsRoot = actions.root
    Assert.assertEquals(actions.children(actions.root).size, 2)
    Assert.assertTrue(actions.children(actionsRoot).forall(_.isInstanceOf[TransferAction]))
    Assert.assertFalse(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcControl.name))
    Assert.assertTrue(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcF1.name))
    Assert.assertTrue(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcF2.name))
  }

  /**
   * Overwrite nested files that exist in the dest. The files are different based on content, and the check is on hash.
   */
  @Test def overwriteByHash2() {
    val src = FakeFileSystem.connect("overwriteByHash2-Src")
    val srcDir = src.createDir(src.root, "1")
    val dest = FakeFileSystem.connect("overwriteByHash2-Dest")
    dest.createDir(src.root, srcDir.name)
    val scenario = new Scenario(
      new ScenarioData("name", "dest", src.data, dest.data, comparator = new HashComparator), src, dest)

    val content = "hello world"
    val date = new DateTime(10000000L)

    val srcControl = new MockSomerFile("/1/control", date, content)
    val srcF1 = new MockSomerFile("/1/1", date, content)
    val srcF2 = new MockSomerFile("/1/2", date, content)
    src.add(srcControl, srcF1, srcF2)

    val destControl = new MockSomerFile(srcControl.fullPath, date, srcControl.data)
    val destF1 = new MockSomerFile(srcF1.fullPath, date, srcF1.data + "1")
    val destF2 = new MockSomerFile(srcF2.fullPath, date, srcF2.data.substring(1))
    dest.add(destControl, destF1, destF2)

    val actions = new InMemoryActionContainerBuilder
    val observer = new MockActionBuilderObserver
    new Backup().build(new BackupData(scenario, actions, observer, false))
    Assert.assertEquals(observer.errors.size, 0)

    Assert.assertEquals(actions.children(actions.root).size, 1)
    Assert.assertTrue(actions.children(actions.root).exists(_.isInstanceOf[DirectoryAction]))

    val actionsRoot = actions
      .children(actions.root)
      .find(_.isInstanceOf[DirectoryAction])
      .get
      .asInstanceOf[DirectoryAction]
    Assert.assertEquals(actions.children(actionsRoot).size, 2)
    Assert.assertTrue(actions.children(actionsRoot).forall(_.isInstanceOf[TransferAction]))
    Assert.assertFalse(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcControl.name))
    Assert.assertTrue(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcF1.name))
    Assert.assertTrue(actions.children(actionsRoot).exists(_.asInstanceOf[TransferAction].name == srcF2.name))
  }
}