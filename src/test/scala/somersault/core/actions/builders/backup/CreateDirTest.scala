package somersault.core.actions.builders.backup

import org.testng.Assert
import org.testng.annotations.Test
import scala.collection.mutable
import somersault.core.actions.builders.MockActionBuilderObserver
import somersault.core.actions.containers.InMemoryActionContainerBuilder
import somersault.core.actions.runners.{MockActionRunnerObserver, FileSystemRunnerData, ActionRunnerController}
import somersault.core.actions.{DirectoryAction, CreateDirAction}
import somersault.core.filesystems.{SomerDirectory, FakeFileSystem}
import somersault.core.{ScenarioData, Scenario}

class CreateDirTest {

  /**
   * Source contains directories at the root, dest is empty.
   * - Check that directories are created.
   */
  @Test def createDir1() {
    val src = FakeFileSystem.connect("CreateDirTest-createDir1-Src")
    val dest = FakeFileSystem.connect("CreateDirTest-createDir1-Dest")
    val d1 = src.createDir(src.root, "1")
    val d2 = src.createDir(src.root, "2")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    Assert.assertEquals(actions.children(actions.root).size, 4)

    val createActions = actions.children(actions.root).filter(_.isInstanceOf[CreateDirAction])
    Assert.assertTrue(createActions.exists(_.asInstanceOf[CreateDirAction].name == d1.name))
    Assert.assertTrue(createActions.exists(_.asInstanceOf[CreateDirAction].name == d2.name))
    val dirActions = actions.children(actions.root).filter(_.isInstanceOf[DirectoryAction])
    Assert.assertTrue(dirActions.exists(_.asInstanceOf[DirectoryAction].name == d1.name))
    Assert.assertTrue(dirActions.exists(_.asInstanceOf[DirectoryAction].name == d2.name))

    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.assertEquals(observer.errors.size, 0)

    val filesAfter = dest.children(dest.root)
    Assert.assertEquals(filesAfter.size, 2)
    Assert.assertTrue(filesAfter.exists(_.name == d1.name))
    Assert.assertTrue(filesAfter.exists(_.name == d2.name))
  }

  /**
   * Source contains multiple levels of nested directories at the root, dest is empty.
   * - Check that directories are created.
   */
  @Test def createDir2() {
    val src = FakeFileSystem.connect("CreateDirTest-createDir2-Src")
    val dest = FakeFileSystem.connect("CreateDirTest-createDir2-Dest")
    val scenario = new Scenario(new ScenarioData("name", "dest", src.data, dest.data), src, dest)

    val entries = List(
      "1" :: "A" :: "E" :: "10" :: Nil,
      "1" :: "A" :: "E" :: "11" :: Nil,
      "1" :: "A" :: "F" :: "12" :: Nil,
      "1" :: "A" :: "F" :: "13" :: Nil,

      "1" :: "B" :: "G" :: "14" :: Nil,
      "1" :: "B" :: "G" :: "15" :: Nil,
      "1" :: "B" :: "H" :: "16" :: Nil,
      "1" :: "B" :: "H" :: "17" :: Nil,

      "2" :: "C" :: "I" :: "18" :: Nil,
      "2" :: "C" :: "I" :: "19" :: Nil,
      "2" :: "C" :: "J" :: "1A" :: Nil,
      "2" :: "C" :: "J" :: "1B" :: Nil,

      "2" :: "D" :: "K" :: "1C" :: Nil,
      "2" :: "D" :: "K" :: "1D" :: Nil,
      "2" :: "D" :: "L" :: "1E" :: Nil,
      "2" :: "D" :: "L" :: "1F" :: Nil)

    // create directories
    var created = new mutable.HashSet[String]
    entries.foreach(
      entry => {
        var parentDir = src.root
        entry.foreach(
          e => {
            if (created.contains(e)) {
              parentDir = src.children(parentDir).find(_.name == e).get.asInstanceOf[SomerDirectory]
            }
            else {
              created += e
              parentDir = src.createDir(parentDir, e)
            }
          })
      })

    val actions = new InMemoryActionContainerBuilder
    new Backup().build(new BackupData(scenario, actions, new MockActionBuilderObserver, false))

    // check actions
    entries.foreach(
      entry => {
        var currDirAction = actions.root
        entry.foreach(
          e => {
            Assert.assertTrue(
              actions.children(currDirAction).exists(
                x => x.isInstanceOf[CreateDirAction] && x.asInstanceOf[CreateDirAction].name == e))

            // recurse into DirectoryActions  (if not last item in list)
            if (entry.indexOf(e) != entry.size - 1) {
              Assert.assertTrue(
                actions.children(currDirAction).exists(
                  x => x.isInstanceOf[DirectoryAction] && x.asInstanceOf[DirectoryAction].name == e))
              currDirAction = actions.children(currDirAction).find(
                x => x.isInstanceOf[DirectoryAction] && x.asInstanceOf[DirectoryAction].name == e)
                .get.asInstanceOf[DirectoryAction]
            }
          })
      })

    val observer = new MockActionRunnerObserver
    new ActionRunnerController(new FileSystemRunnerData(scenario, observer)).run(actions)
    Assert.assertEquals(observer.errors.size, 0)

    // check results
    entries.foreach(
      entry => {
        var currDir = dest.root
        entry.foreach(
          e => {
            val dirs = dest.children(currDir)
            Assert.assertTrue(dirs.exists(_.name == e))

            // recurse into DirectoryActions  (if not last item in list)
            if (entry.indexOf(e) != entry.size - 1)
              currDir = dirs.find(_.name == e).get.asInstanceOf[SomerDirectory]
          })
      })
  }
}