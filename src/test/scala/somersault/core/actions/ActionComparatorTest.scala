package somersault.core.actions

import org.testng.Assert
import org.testng.annotations.Test
import somersault.core.actions.containers.InMemoryActionContainerBuilder

class ActionComparatorTest {

  @Test def equals1() {
    val actions1 = new InMemoryActionContainerBuilder
    val actions2 = new InMemoryActionContainerBuilder
    val comp = new ActionComparator

    val a = actions1.addRemoveAction(actions1.root, "1")
    val b = actions2.addRemoveAction(actions2.root, "1")

    Assert.assertTrue(comp.compare(a, b) == 0)
  }

  @Test def equals2() {
    val actions1 = new InMemoryActionContainerBuilder
    val actions2 = new InMemoryActionContainerBuilder
    val comp = new ActionComparator

    val a = actions1.addCreateDirAction(actions1.root, "1")
    val b = actions2.addCreateDirAction(actions2.root, "1")

    Assert.assertTrue(comp.compare(a, b) == 0)
  }

  @Test def equals3() {
    val actions1 = new InMemoryActionContainerBuilder
    val actions2 = new InMemoryActionContainerBuilder
    val comp = new ActionComparator

    val a = actions1.addTransferAction(actions1.root, "1")
    val b = actions2.addTransferAction(actions2.root, "1")

    Assert.assertTrue(comp.compare(a, b) == 0)
  }

  @Test def equals4() {
    val actions1 = new InMemoryActionContainerBuilder
    val actions2 = new InMemoryActionContainerBuilder
    val comp = new ActionComparator

    val a = actions1.addDirectoryAction(actions1.root, "1")
    val b = actions2.addDirectoryAction(actions2.root, "1")

    Assert.assertTrue(comp.compare(a, b) == 0)
  }

  @Test def order1() {
    val actions = new InMemoryActionContainerBuilder
    val comp = new ActionComparator

    val a = actions.addRemoveAction(actions.root, "1")
    val b = actions.addTransferAction(actions.root, "1")
    val c = actions.addCreateDirAction(actions.root, "1")
    val d = actions.addDirectoryAction(actions.root, "1")

    Assert.assertTrue(comp.compare(a, b) < 0)
    Assert.assertTrue(comp.compare(a, c) < 0)
    Assert.assertTrue(comp.compare(a, d) < 0)
  }

  @Test def order2() {
    val actions = new InMemoryActionContainerBuilder
    val comp = new ActionComparator

    val a = actions.addRemoveAction(actions.root, "1")
    val b = actions.addTransferAction(actions.root, "1")
    val c = actions.addCreateDirAction(actions.root, "1")
    val d = actions.addDirectoryAction(actions.root, "1")

    Assert.assertTrue(comp.compare(b, a) > 0)
    Assert.assertTrue(comp.compare(b, c) < 0)
    Assert.assertTrue(comp.compare(b, d) < 0)
  }

  @Test def order3() {
    val actions = new InMemoryActionContainerBuilder
    val comp = new ActionComparator

    val a = actions.addRemoveAction(actions.root, "1")
    val b = actions.addTransferAction(actions.root, "1")
    val c = actions.addCreateDirAction(actions.root, "1")
    val d = actions.addDirectoryAction(actions.root, "1")

    Assert.assertTrue(comp.compare(c, a) > 0)
    Assert.assertTrue(comp.compare(c, b) > 0)
    Assert.assertTrue(comp.compare(c, d) < 0)
  }

  @Test def order4() {
    val actions = new InMemoryActionContainerBuilder
    val comp = new ActionComparator

    val a = actions.addRemoveAction(actions.root, "1")
    val b = actions.addTransferAction(actions.root, "1")
    val c = actions.addCreateDirAction(actions.root, "1")
    val d = actions.addDirectoryAction(actions.root, "1")

    Assert.assertTrue(comp.compare(d, a) > 0)
    Assert.assertTrue(comp.compare(d, b) > 0)
    Assert.assertTrue(comp.compare(d, c) > 0)
  }
}