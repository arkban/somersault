package somersault.core.comparators

import java.io.File
import nu.xom.Element
import org.testng.annotations.Test
import somersault.util.Assert
import somersault.util.persist.{PersisterContext, PersisterContainer}

class MultiSomerFileComparatorTest {

  @Test def persist() {
    val container = new PersisterContainer
    container.add(classOf[BooleanComparator], new BooleanComparatorPersister)
    container.add(classOf[HashComparator], new HashComparatorPersister)
    container.add(classOf[SizeComparator], new SizeComparatorPersister)
    container.add(classOf[ModifyDateComparator], new ModifyDateComparatorPersister)
    val context = new PersisterContext(container, new File("temp"))

    val comp = new MultiSomerFileComparator(
      new BooleanComparator(true), new HashComparator, new SizeComparator, new ModifyDateComparator)
    val pw = new MultiSomerFileComparatorPersister
    val element = new Element("comp")
    pw.write(context, comp, element, dynamic = true)

    Assert.isTrue(pw.canRead(context, element))
    val newComp: MultiSomerFileComparator = pw.read(context, element)
    val comparators = newComp.comparators.toSeq
    Assert.isTrue(comparators(0).isInstanceOf[BooleanComparator])
    Assert.isTrue(comparators(1).isInstanceOf[HashComparator])
    Assert.isTrue(comparators(2).isInstanceOf[SizeComparator])
    Assert.isTrue(comparators(3).isInstanceOf[ModifyDateComparator])
  }
}