package somersault.core.comparators

import java.io.File
import nu.xom.Element
import org.joda.time.DateTime
import org.testng.annotations.Test
import somersault.util.Assert
import somersault.util.persist.{PersisterSuite, PersisterContext}

class ModifyDateComparatorTest {

  /**
   * Test with no granularity.
   */
  @Test def comp1() {
    val comp = new ModifyDateComparator(0)
    val epochAlpha = new DateTime(1000000)
    val epochBeta = new DateTime(epochAlpha.getMillis + 1)

    Assert.isTrue(comp.compare(epochAlpha, epochAlpha) == 0)
    Assert.isTrue(comp.compare(epochAlpha, epochBeta) < 0)
    Assert.isTrue(comp.compare(epochBeta, epochAlpha) > 0)
  }

  /**
   * Test with granularity EQUAL to the differences between all values
   */
  @Test def comp2() {
    val comp = new ModifyDateComparator(1)
    val epochAlpha = new DateTime(1000000)
    val epochBeta = new DateTime(epochAlpha.getMillis + 1)

    Assert.isTrue(comp.compare(epochAlpha, epochAlpha) == 0)
    Assert.isTrue(comp.compare(epochAlpha, epochBeta) == 0)
    Assert.isTrue(comp.compare(epochBeta, epochAlpha) == 0)
  }

  /**
   * Test with granularity LARGER than the differences between all values
   */
  @Test def comp3() {
    val comp = new ModifyDateComparator(2)
    val epochAlpha = new DateTime(1000000)
    val epochBeta = new DateTime(epochAlpha.getMillis + 1)

    Assert.isTrue(comp.compare(epochAlpha, epochAlpha) == 0)
    Assert.isTrue(comp.compare(epochAlpha, epochBeta) == 0)
    Assert.isTrue(comp.compare(epochBeta, epochAlpha) == 0)
  }

  @Test def persist() {
    val context = new PersisterContext(PersisterSuite.create(), new File("temp"))
    val comp = new ModifyDateComparator(1234)
    val pw = new ModifyDateComparatorPersister

    val element = new Element("comp")
    pw.write(context, comp, element, dynamic = true)

    Assert.isTrue(pw.canRead(context, element))
    val newComp: ModifyDateComparator = pw.read(context, element)
    Assert.areEqual(newComp.granularityMs, comp.granularityMs)
  }
}