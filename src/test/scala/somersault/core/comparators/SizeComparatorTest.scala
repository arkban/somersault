package somersault.core.comparators

import java.io.File
import nu.xom.Element
import org.testng.annotations.Test
import somersault.util.Assert
import somersault.util.persist.{PersisterSuite, PersisterContext}

class SizeComparatorTest {

  @Test def persist() {
    val context = new PersisterContext(PersisterSuite.create(), new File("temp"))
    val comp = new SizeComparator
    val pw = new SizeComparatorPersister

    val element = new Element("comp")
    pw.write(context, comp, element, dynamic = true)

    Assert.isTrue(pw.canRead(context, element))
    pw.read(context, element) // verify no exception
  }
}