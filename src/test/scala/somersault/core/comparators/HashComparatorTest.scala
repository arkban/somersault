package somersault.core.comparators

import java.io.File
import nu.xom.Element
import org.testng.annotations.Test
import somersault.util.persist.{PersisterSuite, PersisterContext}
import somersault.util.{Assert, HashInfo, Hash}

class HashComparatorTest {

  /**
   * Test HashInfo against name.
   */
  @Test def hashInfo1() {
    val comp = new HashComparator
    val alpha = new HashInfo("alpha", 10)
    val beta = new HashInfo("beta", 10)

    Assert.isTrue(comp.compare(alpha, alpha) == 0)
    Assert.isTrue(comp.compare(alpha, beta) < 0)
    Assert.isTrue(comp.compare(beta, alpha) > 0)
  }

  /**
   * Test HashInfo against size.
   */
  @Test def hashInfo2() {
    val comp = new HashComparator
    val alpha = new HashInfo("hashInfo", 10)
    val beta = new HashInfo(alpha.name, 20)

    Assert.isTrue(comp.compare(alpha, alpha) == 0)
    Assert.isTrue(comp.compare(alpha, beta) < 0)
    Assert.isTrue(comp.compare(beta, alpha) > 0)
  }

  @Test def hash1() {
    val comp = new HashComparator()
    val alphaData = Array[Byte](25)
    val betaData = Array[Byte](alphaData(0))

    val alpha = new Hash(HashInfo.md5, alphaData)
    val beta = new Hash(HashInfo.md5, betaData)

    Assert.isTrue(comp.compare(alpha, alpha) == 0)
    Assert.isTrue(comp.compare(alpha, beta) == 0)
  }

  /**
   * Test Hash
   */
  @Test def hash2() {
    val comp = new HashComparator()
    val alphaData = Array[Byte](25)
    val betaData = Array[Byte](52)

    val alpha = new Hash(HashInfo.md5, alphaData)
    val beta = new Hash(HashInfo.md5, betaData)

    Assert.isTrue(comp.compare(alpha, alpha) == 0)
    Assert.isTrue(comp.compare(alpha, beta) != 0) // test not equal, not order
    Assert.isTrue(comp.compare(beta, alpha) != 0) // test not equal, not order
  }

  @Test def persist() {
    val context = new PersisterContext(PersisterSuite.create(), new File("temp"))
    val comp = new HashComparator
    val pw = new HashComparatorPersister

    val element = new Element("comp")
    pw.write(context, comp, element, dynamic = true)

    Assert.isTrue(pw.canRead(context, element))
    pw.read(context, element) // verify no exception
  }
}