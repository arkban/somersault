package somersault.core.comparators

import java.io.File
import nu.xom.Element
import org.testng.annotations.Test
import somersault.util.Assert
import somersault.util.persist.{PersisterSuite, PersisterContext}

class BooleanComparatorTest {

  @Test def persist() {
    val context = new PersisterContext(PersisterSuite.create(), new File("temp"))
    Seq(true, false).foreach(
      value => {
        val comp = new BooleanComparator(value)
        val pw = new BooleanComparatorPersister

        val element = new Element("comp")
        pw.write(context, comp, element, dynamic = true)

        Assert.isTrue(pw.canRead(context, element))
        val newComp: BooleanComparator = pw.read(context, element)
        Assert.areEqual(newComp.value, value)
        Assert.areEqual(newComp.value, comp.value)
      })
  }
}