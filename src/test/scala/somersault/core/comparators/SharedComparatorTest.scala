package somersault.core.comparators

import java.io.File
import nu.xom.Element
import org.testng.annotations.Test
import somersault.core.TestUtil
import somersault.util.persist.{PersisterSuite, PersisterContext, Persister}
import somersault.util.{Assert, FilenameUtils2}

class SharedComparatorTest {

  /**
   * This tests with an absolute path because its the unit test framework uses random test directories.
   */
  @Test def test() {
    val unitTestName = this.getClass.getSimpleName + ".test"
    val path = TestUtil.createTestPath(classOf[SharedComparatorTest], unitTestName)
    // main file is down one dir so we can test shared files up one directory
    val mainFile = new File(FilenameUtils2.concat(path.getCanonicalPath, "shared", "master.xml"))
    mainFile.getParentFile.mkdirs
    val context = new PersisterContext(PersisterSuite.create(), mainFile)

    val sharedFiles = new File(FilenameUtils2.concat(path.getCanonicalPath, "local.xml")) :: // absolute
      new File(FilenameUtils2.concat(path.getCanonicalPath, "shared", "local.xml")) :: // absolute
      new File(FilenameUtils2.concat(path.getCanonicalPath, "shared", "foo", "local.xml")) :: // absolute
      new File("local.xml") :: // relative
      new File("shared", "local.xml") :: // relative
      new File(FilenameUtils2.concat("shared", "foo"), "local.xml") :: // relative
      new File(FilenameUtils2.concat("foo", "bar"), "local.xml") :: // relative
      Nil
    sharedFiles.foreach(
      sharedFile => {
        val origSharedComparator = new SharedComparator(new SizeComparator, sharedFile)

        val writeFn = (root: Element) => new SharedComparatorPersister().write(
          context, origSharedComparator, root, dynamic = true)
        Persister.write("shared", writeFn, mainFile)

        val readFn = (root: Element) => new SharedComparatorPersister()
          .read(context, root)
          .asInstanceOf[SharedComparator]
        val newSharedComparator = Persister.read(mainFile, readFn)

        Assert.isTrue(newSharedComparator.comparator.isInstanceOf[SizeComparator])
        Assert.areEqual(newSharedComparator.filePath, sharedFile)
      })
  }

  @Test def recursive() {
    val unitTestName = this.getClass.getSimpleName + ".test"
    val path = TestUtil.createTestPath(classOf[SharedComparatorTest], unitTestName)
    // main file is down one dir so we can test shared files up one directory
    val mainFile = new File(FilenameUtils2.concat(path.getCanonicalPath, "master.xml"))
    mainFile.getParentFile.mkdirs
    val context = new PersisterContext(PersisterSuite.create(), mainFile)
    val readFn = (root: Element) => new SharedComparatorPersister()
      .read(context, root)
      .asInstanceOf[SharedComparator]

    val barFile = new File(FilenameUtils2.concat("..", "bar.xml"))
    val fooComparator = new SharedComparator(new SizeComparator(), barFile)

    val fooFile = new File(FilenameUtils2.concat("shared", "nested", "foo.xml"))
    val mainComparator = new SharedComparator(fooComparator, fooFile)

    val writeFn = (root: Element) => new SharedComparatorPersister()
      .write(context, mainComparator, root, dynamic = true)
    Persister.write("shared", writeFn, mainFile)

    val newMainComparator = Persister.read(mainFile, readFn)
    Assert.areEqual(newMainComparator.filePath, fooFile)
    Assert.isTrue(new File(path, FilenameUtils2.concat("shared", "nested", "foo.xml")).exists)
    Assert.isTrue(newMainComparator.comparator.isInstanceOf[SharedComparator])

    val newFooComparator = newMainComparator.comparator.asInstanceOf[SharedComparator]
    Assert.areEqual(newFooComparator.filePath, barFile)
    Assert.isTrue(new File(path, FilenameUtils2.concat("shared", "bar.xml")).exists)
    Assert.isTrue(newFooComparator.comparator.isInstanceOf[SizeComparator])
  }
}