package somersault.core.filesystems

import java.util.concurrent.atomic.AtomicBoolean
import org.joda.time.Duration
import org.testng.annotations.Test
import somersault.util.{Assert, Runner}

/**
 * The tests in this class can take some significant time, as locks are acquired and released based on random
 * wait times that are on the order of 1 to 5 seconds.
 */
class LockingFileSystemTest {

  @Test
  def unlockWithoutLock() {
    val internals = new LockingFileSystemTest.Internals()
    val fs = FakeFileSystem.connect("unlockWithoutLock", internals)

    Assert.isFalse(fs.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs.hasLock(LockingFileSystemMode.Write))
    fs.unlock()
    Assert.isFalse(fs.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs.hasLock(LockingFileSystemMode.Write))
  }

  @Test
  def readNoContention() {
    val internals = new LockingFileSystemTest.Internals()
    val fs = FakeFileSystem.connect("readNoContention", internals)

    Assert.isTrue(fs.lock(LockingFileSystemMode.Read))
    Assert.isTrue(fs.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs.hasLock(LockingFileSystemMode.Write))
    // repeat lock (should not add new locks)
    Assert.isTrue(fs.lock(LockingFileSystemMode.Read))
    Assert.isTrue(fs.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs.hasLock(LockingFileSystemMode.Write))
  }

  @Test
  def writeNoContention() {
    val internals = new LockingFileSystemTest.Internals()
    val fs = FakeFileSystem.connect("writeNoContention", internals)

    Assert.isTrue(fs.lock(LockingFileSystemMode.Write))
    Assert.isFalse(fs.hasLock(LockingFileSystemMode.Read))
    Assert.isTrue(fs.hasLock(LockingFileSystemMode.Write))
    // repeat lock (should not add new locks)
    Assert.isTrue(fs.lock(LockingFileSystemMode.Write))
    Assert.isFalse(fs.hasLock(LockingFileSystemMode.Read))
    Assert.isTrue(fs.hasLock(LockingFileSystemMode.Write))
  }

  /** Testing mode change from read to write */
  @Test
  def readToWrite() {
    val internals = new LockingFileSystemTest.Internals()
    val fs = FakeFileSystem.connect("readVsWrite", internals)

    Assert.isTrue(fs.lock(LockingFileSystemMode.Read))
    Assert.isTrue(fs.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs.hasLock(LockingFileSystemMode.Write))

    Assert.isFalse(fs.lock(LockingFileSystemMode.Write))
    Assert.isTrue(fs.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs.hasLock(LockingFileSystemMode.Write))

    fs.unlock()
    Assert.isTrue(fs.lock(LockingFileSystemMode.Write))
    Assert.isFalse(fs.hasLock(LockingFileSystemMode.Read))
    Assert.isTrue(fs.hasLock(LockingFileSystemMode.Write))
  }

  /** Testing mode change from write to read */
  @Test
  def writeToRead() {
    val internals = new LockingFileSystemTest.Internals()
    val fs = FakeFileSystem.connect("writeVsRead", internals)

    Assert.isTrue(fs.lock(LockingFileSystemMode.Write))
    Assert.isFalse(fs.hasLock(LockingFileSystemMode.Read))
    Assert.isTrue(fs.hasLock(LockingFileSystemMode.Write))

    Assert.isFalse(fs.lock(LockingFileSystemMode.Read))
    Assert.isFalse(fs.hasLock(LockingFileSystemMode.Read))
    Assert.isTrue(fs.hasLock(LockingFileSystemMode.Write))

    fs.unlock()
    Assert.isTrue(fs.lock(LockingFileSystemMode.Read))
    Assert.isTrue(fs.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs.hasLock(LockingFileSystemMode.Write))
  }

  @Test
  def multipleReads() {
    val internals = new LockingFileSystemTest.Internals()
    val fs1 = FakeFileSystem.connect("multipleReads", internals)
    val fs2 = FakeFileSystem.connect("multipleReads", internals)

    Assert.isTrue(fs1.lock(LockingFileSystemMode.Read))
    Assert.isTrue(fs2.lock(LockingFileSystemMode.Read))
    Assert.isTrue(fs1.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs1.hasLock(LockingFileSystemMode.Write))
    Assert.isTrue(fs2.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs2.hasLock(LockingFileSystemMode.Write))

    // repeat lock (should not add new locks)
    Assert.isTrue(fs1.lock(LockingFileSystemMode.Read))
    Assert.isTrue(fs2.lock(LockingFileSystemMode.Read))
    Assert.isTrue(fs1.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs1.hasLock(LockingFileSystemMode.Write))
    Assert.isTrue(fs2.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs2.hasLock(LockingFileSystemMode.Write))

    fs1.unlock()
    fs2.unlock()
    Assert.isFalse(fs1.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs1.hasLock(LockingFileSystemMode.Write))
    Assert.isFalse(fs2.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs2.hasLock(LockingFileSystemMode.Write))
  }

  @Test
  def multipleWrites() {
    val internals = new LockingFileSystemTest.Internals()
    val fs1 = FakeFileSystem.connect("multipleWrites", internals)
    val fs2 = FakeFileSystem.connect("multipleWrites", internals)

    Assert.isTrue(fs1.lock(LockingFileSystemMode.Write))
    Assert.isFalse(fs2.lock(LockingFileSystemMode.Write))
    Assert.isFalse(fs1.hasLock(LockingFileSystemMode.Read))
    Assert.isTrue(fs1.hasLock(LockingFileSystemMode.Write))
    Assert.isFalse(fs2.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs2.hasLock(LockingFileSystemMode.Write))

    // repeat lock (should not add new locks)
    Assert.isTrue(fs1.lock(LockingFileSystemMode.Write))
    Assert.isFalse(fs2.lock(LockingFileSystemMode.Write))
    Assert.isFalse(fs1.hasLock(LockingFileSystemMode.Read))
    Assert.isTrue(fs1.hasLock(LockingFileSystemMode.Write))
    Assert.isFalse(fs2.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs2.hasLock(LockingFileSystemMode.Write))

    fs1.unlock()
    fs2.unlock()
    Assert.isFalse(fs1.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs1.hasLock(LockingFileSystemMode.Write))
    Assert.isFalse(fs2.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs2.hasLock(LockingFileSystemMode.Write))
  }

  /** One FS has read, the other wants write */
  @Test
  def writeAfterRead() {
    val internals = new LockingFileSystemTest.Internals()
    val fsRead = FakeFileSystem.connect("writeAfterRead", internals)
    val fsWrite = FakeFileSystem.connect("writeAfterRead", internals)

    Assert.isTrue(fsRead.lock(LockingFileSystemMode.Read))
    Assert.isFalse(fsWrite.lock(LockingFileSystemMode.Write))
    Assert.isTrue(fsRead.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fsRead.hasLock(LockingFileSystemMode.Write))
    Assert.isFalse(fsWrite.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fsWrite.hasLock(LockingFileSystemMode.Write))

    // repeat lock (should not add new locks)
    Assert.isTrue(fsRead.lock(LockingFileSystemMode.Read))
    Assert.isFalse(fsWrite.lock(LockingFileSystemMode.Write))
    Assert.isTrue(fsRead.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fsRead.hasLock(LockingFileSystemMode.Write))
    Assert.isFalse(fsWrite.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fsWrite.hasLock(LockingFileSystemMode.Write))

    // switch who has the lock
    fsRead.unlock()
    Assert.isTrue(fsWrite.lock(LockingFileSystemMode.Write))
    Assert.isFalse(fsRead.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fsRead.hasLock(LockingFileSystemMode.Write))
    Assert.isFalse(fsWrite.hasLock(LockingFileSystemMode.Read))
    Assert.isTrue(fsWrite.hasLock(LockingFileSystemMode.Write))
  }

  /** One FS has write, the other wants read */
  @Test
  def readAfterWrite() {
    val internals = new LockingFileSystemTest.Internals()
    val fsRead = FakeFileSystem.connect("readAfterWrite", internals)
    val fsWrite = FakeFileSystem.connect("readAfterWrite", internals)

    Assert.isTrue(fsWrite.lock(LockingFileSystemMode.Write))
    Assert.isFalse(fsRead.lock(LockingFileSystemMode.Read))
    Assert.isFalse(fsRead.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fsRead.hasLock(LockingFileSystemMode.Write))
    Assert.isFalse(fsWrite.hasLock(LockingFileSystemMode.Read))
    Assert.isTrue(fsWrite.hasLock(LockingFileSystemMode.Write))

    // repeat lock (should not add new locks)
    Assert.isTrue(fsWrite.lock(LockingFileSystemMode.Write))
    Assert.isFalse(fsRead.lock(LockingFileSystemMode.Read))
    Assert.isFalse(fsRead.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fsRead.hasLock(LockingFileSystemMode.Write))
    Assert.isFalse(fsWrite.hasLock(LockingFileSystemMode.Read))
    Assert.isTrue(fsWrite.hasLock(LockingFileSystemMode.Write))

    // switch who has the lock
    fsWrite.unlock()
    Assert.isTrue(fsRead.lock(LockingFileSystemMode.Read))
    Assert.isTrue(fsRead.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fsRead.hasLock(LockingFileSystemMode.Write))
    Assert.isFalse(fsWrite.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fsWrite.hasLock(LockingFileSystemMode.Write))
  }

  @Test
  def removeAllLocks() {
    val internals = new LockingFileSystemTest.Internals()
    val fs1 = FakeFileSystem.connect("removeAllLocks", internals)
    val fs2 = FakeFileSystem.connect("removeAllLocks", internals)

    Assert.isTrue(fs1.lock(LockingFileSystemMode.Read))
    Assert.isTrue(fs2.lock(LockingFileSystemMode.Read))
    Assert.isTrue(fs1.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs1.hasLock(LockingFileSystemMode.Write))
    Assert.isTrue(fs2.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs2.hasLock(LockingFileSystemMode.Write))

    fs1.removeAllLocks()
    Assert.isFalse(fs1.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs1.hasLock(LockingFileSystemMode.Write))
    Assert.isFalse(fs2.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs2.hasLock(LockingFileSystemMode.Write))
  }

  @Test
  def readLockContention() {
    // repeated for better coverage since this test involves threads
    (0 until 10).foreach(
      i => {
        val internals = new LockingFileSystemTest.Internals()
        val fs1 = FakeFileSystem.connect("readLockContention", internals)
        val fs2 = FakeFileSystem.connect("readLockContention", internals)

        // lock such that they two compete
        val flag1 = new AtomicBoolean(false)
        val flag2 = new AtomicBoolean(false)
        val t1 = new Thread(new Runner(() => flag1.set(fs1.lock(LockingFileSystemMode.Read))))
        val t2 = new Thread(new Runner(() => flag2.set(fs2.lock(LockingFileSystemMode.Read))))

        // switch which thread starts first
        if (i % 2 == 0) {
          t1.start()
          t2.start()
        }
        else {
          t2.start()
          t1.start()
        }
        t1.join()
        t2.join()

        Assert.isTrue(flag1.get)
        Assert.isTrue(flag2.get)
        Assert.isTrue(fs1.hasLock(LockingFileSystemMode.Read))
        Assert.isFalse(fs1.hasLock(LockingFileSystemMode.Write))
        Assert.isTrue(fs2.hasLock(LockingFileSystemMode.Read))
        Assert.isFalse(fs2.hasLock(LockingFileSystemMode.Write))
      })
  }

  @Test
  def readWriteLockContention() {
    // repeated for better coverage since this test involves threads
    (0 until 10).foreach(
      i => {
        val internals = new LockingFileSystemTest.Internals()
        val fsRead = FakeFileSystem.connect("writeLockContention", internals)
        val fsWrite = FakeFileSystem.connect("writeLockContention", internals)

        // lock such that they two compete
        val flag1 = new AtomicBoolean(false)
        val flag2 = new AtomicBoolean(false)
        val t1 = new Thread(new Runner(() => flag1.set(fsRead.lock(LockingFileSystemMode.Read))))
        val t2 = new Thread(new Runner(() => flag2.set(fsWrite.lock(LockingFileSystemMode.Write))))

        // switch which thread starts first
        if (i % 2 == 0) {
          t1.start()
          t2.start()
        }
        else {
          t2.start()
          t1.start()
        }
        t1.join()
        t2.join()

        val xor = (x: Boolean, y: Boolean) => x || y && !(x && y)
        Assert.isTrue(xor(flag1.get, flag2.get))
        Assert.isTrue(fsRead.hasLock(LockingFileSystemMode.Read) || fsWrite.hasLock(LockingFileSystemMode.Write))
        Assert.isFalse(fsRead.hasLock(LockingFileSystemMode.Write))
        Assert.isFalse(fsWrite.hasLock(LockingFileSystemMode.Read))
      })
  }

  @Test
  def writeLockContention() {
    // repeated for better coverage since this test involves threads
    (0 until 10).foreach(
      i => {
        val internals = new LockingFileSystemTest.Internals()
        val fs1 = FakeFileSystem.connect("writeLockContention", internals)
        val fs2 = FakeFileSystem.connect("writeLockContention", internals)

        // lock such that they two compete
        val flag1 = new AtomicBoolean(false)
        val flag2 = new AtomicBoolean(false)
        val t1 = new Thread(new Runner(() => flag1.set(fs1.lock(LockingFileSystemMode.Write))))
        val t2 = new Thread(new Runner(() => flag2.set(fs2.lock(LockingFileSystemMode.Write))))

        // switch which thread starts first
        if (i % 2 == 0) {
          t1.start()
          t2.start()
        }
        else {
          t2.start()
          t1.start()
        }
        t1.join()
        t2.join()

        val xor = (x: Boolean, y: Boolean) => x || y && !(x && y)
        Assert.isTrue(xor(flag1.get, flag2.get))
        Assert.isTrue(fs1.hasLock(LockingFileSystemMode.Write) || fs2.hasLock(LockingFileSystemMode.Write))
        Assert.isFalse(fs1.hasLock(LockingFileSystemMode.Read))
        Assert.isFalse(fs2.hasLock(LockingFileSystemMode.Read))
      })
  }

  /**
   * One FS has a stale write, the other wants write. There is only one test for writes because writes are exclusive
   * and reads are inclusive.
   */
  @Test
  def writeAfterStaleWrite() {
    val lockTimeout = 10
    val internals = new LockingFileSystemTest.Internals() {
      override val lockingFileSystemOptions = new LockingFileSystem.Options(
        backOffMsMax = 1, lockTimeout = new Duration(lockTimeout))
    }
    val fs1 = FakeFileSystem.connect("writeAfterStaleRead", internals)
    val fs2 = FakeFileSystem.connect("writeAfterStaleRead", internals)

    Assert.isTrue(fs1.lock(LockingFileSystemMode.Write))
    Assert.isFalse(fs1.hasLock(LockingFileSystemMode.Read))
    Assert.isTrue(fs1.hasLock(LockingFileSystemMode.Write))
    Assert.isFalse(fs2.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs2.hasLock(LockingFileSystemMode.Write))

    Thread.sleep(lockTimeout * 2) // twice as long should be good enough for staleness

    // re-lock after the lock timeout expires
    Assert.isTrue(fs2.lock(LockingFileSystemMode.Write))
    Assert.isFalse(fs1.hasLock(LockingFileSystemMode.Read))
    Assert.isFalse(fs1.hasLock(LockingFileSystemMode.Write))
    Assert.isFalse(fs2.hasLock(LockingFileSystemMode.Read))
    Assert.isTrue(fs2.hasLock(LockingFileSystemMode.Write))
  }
}

object LockingFileSystemTest {

  class Internals extends MockFileSystemInternals {

    override val lockingFileSystemOptions = new LockingFileSystem.Options(backOffMsMax = 1)
  }

}
