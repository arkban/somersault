package somersault.core.filesystems.index

import java.lang.String
import somersault.util.HashInfo
import somersault.core.TestUtil
import somersault.core.encode.{NullCompressionProvider, NullEncryptionProvider, EncryptionProvider, CompressionProvider}

class FakeIndexTest extends IndexTest
{
  protected def createIndex( testName: String, data: IndexData ) = new FakeIndex

  protected def createData(
          testName: String,
          hashInfo: HashInfo,
          compProvider: CompressionProvider,
          encProvider: EncryptionProvider ): IndexData =
    {
      return new IndexData
      {
        val hashInfo = HashInfo.md5

        val logger = TestUtil.logger

        val encryptionProvider = new NullEncryptionProvider

        val compressionProvider = new NullCompressionProvider

        def persist() = throw new UnsupportedOperationException
      }
    }
}