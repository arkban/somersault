package somersault.core.filesystems.index

import somersault.util.{Assert, HashMismatchException}
import org.testng.annotations.Test
import somersault.core.TestUtil
import somersault.core.filesystems.FileSystemTest

/**
 * Unit tests for Indexed FileSystems unit tests where there is also a non-Indexed FileSystem. There are certain tests
 * that can only be run when you can completely bypass the Index.
 */
trait IndexedAndNonIndexedFileSystemTests extends IndexedFileSystemTest
{
  /**
   * Creates a new root file, changes the stored file, and verifies that on read an exception is thrown indicating
   * that the result file has a different hash.
   */
  @Test def verifyRead1( )
  {
    val unitTestName = "verifyRead1"
    val fs = createFileSystem( unitTestName, "", logger)

    try
    {
      val fooContentsOrig1 = "verifyRead" * 5 + "1"
      val fooFile = FileSystemTest.writeFile( fs, fs.root, "foo", fooContentsOrig1)

      // change last character by a single bit and keep length
      val fooContentsOrig2 = fooContentsOrig1.substring( 0, fooContentsOrig1.length - 1) + "2"

      Assert.areEqual( fooContentsOrig1.length, fooContentsOrig2.length)
      writeRawContents( unitTestName, fs.data, fooFile.name :: Nil, fooFile, fooContentsOrig2)

      // this will thread the stream and should throw when the hash does not match
      TestUtil.readRawContents( fs.data.hashInfo, fs.read( fooFile))

      Assert.fail( "Expected HashMismatchException")
    }
    catch
    {
      case ex: HashMismatchException =>
    }
    finally fs.disconnect( )
  }
}