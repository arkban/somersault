package somersault.core.filesystems.index

import org.joda.time.DateTime
import scala.collection.mutable
import somersault.core.exceptions.{AlreadyExistsException, PathDoesNotExistException}
import somersault.core.filesystems.FileAttributes
import somersault.util.Hash

/**
 * A fake index that exists only in memory. This implementation is designed to work as much like a real Index
 * as possible, which may involve some less-than-ideal code to mimic such behavior.
 */
class FakeIndex extends Index {

  /**
   * A map from IDs to MockIndexArtifact. This map contains only active (not deleted) MockIndexArtifacts. This exists
   * to keep track of which MockIndexArtifacts are actually active -- removing an artifact from the Index does not
   * remove the caller's reference to that artifact. All inputs should be checked for existence in this set before
   * acting on them.
   */
  private val fs = new mutable.HashMap[Long, MockIndexArtifact]

  private val _root = {
    val tempRoot = new MockIndexDirectory(FakeIndex.nextId(), None, "", "")
    fs += tempRoot.id -> tempRoot
    tempRoot
  }

  override def root: IndexDirectory = _root

  override def children(parent: IndexDirectory): Iterable[IndexArtifact] =
    parent match {
      case mockParent: MockIndexDirectory if !fs.contains(mockParent.id) =>
        throw new PathDoesNotExistException(getPath(mockParent))
      case mockParent: MockIndexDirectory => mockParent.children
      case _ => throw new IllegalArgumentException("invalid type")
    }

  override def find(parent: IndexDirectory, name: String): Option[IndexArtifact] =
    parent match {
      case mockParent: MockIndexDirectory if !fs.contains(mockParent.id) =>
        throw new PathDoesNotExistException(getPath(mockParent))
      case mockParent: MockIndexDirectory => mockParent.children.find(_.origName == name)
      case _ => throw new IllegalArgumentException("invalid type")
    }

  def remove(artifact: IndexArtifact) {
    if (artifact eq root)
      throw new IllegalArgumentException("cannot delete root")

    artifact match {
      case a: MockIndexArtifact => {
        // remove recursively
        a match {
          case d: MockIndexDirectory => d.children.foreach(remove)
          case _ =>
        }

        // remove from parent
        fs.get(a.parentId.get) match {
          case Some(parent: MockIndexDirectory) =>
            fs -= a.id
            parent.children -= a
          case None => throw new PathDoesNotExistException(a.origName :: Nil, null)
          case x => throw new Exception("Unexpected response: " + x)
        }
      }
      case _ => throw new IllegalArgumentException("invalid type")
    }
  }

  def createDir(parent: IndexDirectory, pt: IndexDirectoryPrototype): IndexDirectory =
    parent match {
      case mockParent: MockIndexDirectory => {
        if (!fs.contains(mockParent.id))
          throw new PathDoesNotExistException(getPath(mockParent) ::: pt.origName :: Nil)
        if (mockParent.children.exists(_.origName == pt.origName))
          throw new AlreadyExistsException(getPath(mockParent) ::: pt.origName :: Nil)

        val newDir = new MockIndexDirectory(FakeIndex.nextId(), new Some(mockParent.id), pt)
        mockParent.children += newDir
        fs += (newDir.id -> newDir)
        newDir
      }
      case _ => throw new IllegalArgumentException("invalid type")
    }

  def writeFile(parent: IndexDirectory, pt: IndexFilePrototype): IndexFile =
    parent match {
      case mockParent: MockIndexDirectory => {
        if (!fs.contains(mockParent.id))
          throw new PathDoesNotExistException(getPath(mockParent) ::: pt.origName :: Nil)

        // remove file if it exists
        mockParent.children.find(_.origName == pt.origName) match {
          case Some(x) => mockParent.children -= x
          case None =>
        }

        val newFile = new MockIndexFile(FakeIndex.nextId(), mockParent.id, pt)
        mockParent.children += newFile
        fs += (newFile.id -> newFile)
        newFile
      }
      case _ => throw new IllegalArgumentException("invalid type")
    }

  /**
   * Returns a path, starting with the provided parent ID.
   */
  private def getPath(dir: MockIndexDirectory): List[String] =
    dir.parentId match {
      case Some(parentId) =>
        fs.get(parentId) match {
          case Some(parent: MockIndexDirectory) => getPath(parent) ::: dir.origName :: Nil
          case Some(f: MockIndexFile) => throw new IllegalArgumentException("file cannot be parent")
          case None => dir.origName :: Nil // the parent is missing, so we can display only this name
          case x => throw new Exception("Unexpected response: " + x)
        }
      case None => Nil // this is the root
    }

  private abstract class MockIndexArtifact(
    val id: Long, val parentId: Option[Long], val origName: String, val storedName: String)
    extends IndexArtifact

  private class MockIndexDirectory(id: Long, parentId: Option[Long], origName: String, storedName: String)
    extends MockIndexArtifact(id, parentId, origName, storedName) with IndexDirectory {

    val children = new mutable.HashSet[MockIndexArtifact]

    def this(id: Long, parentId: Option[Long], pt: IndexDirectoryPrototype) =
      this(id, parentId, pt.origName, pt.storedName)
  }

  private class MockIndexFile(
    id: Long,
    parentId: Long,
    origName: String,
    storedName: String,
    val size: Long,
    val modified: DateTime,
    val attributes: FileAttributes,
    override val encrypted: Boolean,
    override val compressed: Boolean,
    val hash: Hash)
    extends MockIndexArtifact(id, new Some(parentId), origName, storedName) with IndexFile {

    def this(id: Long, parentId: Long, pt: IndexFilePrototype) =
      this(
        id,
        parentId,
        pt.origName,
        pt.storedName,
        pt.size,
        pt.modified,
        pt.attributes,
        pt.encrypted,
        pt.compressed,
        pt.hash)
  }

}

object FakeIndex {

  private[this] var currId: Long = 0

  private def nextId() = {
    val temp = currId
    currId += 1
    temp
  }
}