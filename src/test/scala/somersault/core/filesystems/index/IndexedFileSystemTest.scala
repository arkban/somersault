package somersault.core.filesystems.index

import logahawk.Logger
import org.apache.commons.lang.IllegalClassException
import org.testng.annotations.Test
import somersault.core.encode._
import somersault.core.filesystems.{SomerArtifact, SomerFile, SomerDirectory, FileSystemData, FileSystem, FileSystemTest}
import somersault.core.filters.{BooleanFilter, NotFilter}
import somersault.util.Assert

/**
 * Extends FileSystemTest with unit tests for indexed FileSystems.
 */
trait IndexedFileSystemTest extends FileSystemTest {

  /**
   * Implements createFileSystem() with a NullEncodingProvider.
   */
  override protected def createFileSystem(testName: String, nameSuffix: String, logger: Logger): FileSystem =
    createFileSystem(testName, nameSuffix, logger, new NullCompressionProvider, new NullEncryptionProvider)

  protected def createFileSystem(
    testName: String,
    nameSuffix: String,
    logger: Logger,
    compProvider: CompressionProvider,
    encProvider: EncryptionProvider): FileSystem

  /**
   * Creates an IndexedFileSystem, finds the file specified by the given path, and retrieves the file without
   * performing the decoding.
   */
  protected def findRawFile(
    testName: String, data: FileSystemData, filePath: Iterable[String]): (FileSystem, SomerDirectory)

  /**
   * Writes the OutputStream (without any encoding) to the provided file path.
   *
   * @param file The SomerFile from the source FileSystem, matching the call FileSystem.write(parent, file).
   */
  protected def writeRawContents(
    testName: String, data: FileSystemData, filePath: Iterable[String], file: SomerFile, contents: String) =
    data match {
      case indexedData: FileSystemData => {
        val (nonIndexedFs, parent) = findRawFile(testName, indexedData, filePath)
        val fileName = filePath.toList.takeRight(1)(0)
        FileSystemTest.writeFile(nonIndexedFs, parent, fileName, contents)
      }
      case _ => throw new IllegalClassException(classOf[FileSystemData], data)
    }

  /**
   * Creates a new root file that is compressed and verifies its contents.
   */
  @Test def compress1() {
    val unitTestName = this.getClass.getSimpleName + ".compress1"
    val compProvider = new GZIPCompressionProvider(new BooleanFilter(true))
    val fs = createFileSystem(unitTestName, "", logger, compProvider, new NullEncryptionProvider)

    try {
      val fooContentsOrig = "compress" * 100
      val fooFile = FileSystemTest.writeFile(fs, fs.root, "foo", fooContentsOrig)

      // verifies FileSystem decoding of previously written file
      FileSystemTest.compareContents(fs, fooFile, fooContentsOrig)

      verifyEncodedName(fs, fooFile, compProvider)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Creates a new non-root file that is compressed and verifies its contents.
   */
  @Test def compress2() {
    val unitTestName = this.getClass.getSimpleName + ".compress2"
    val compProvider = new GZIPCompressionProvider(new BooleanFilter(true))
    val fs = createFileSystem(unitTestName, "", logger, compProvider, new NullEncryptionProvider)

    try {
      val barContentsOrig = "compress" * 100
      val fooDir = fs.createDir(fs.root, "foo")
      val barFile = FileSystemTest.writeFile(fs, fooDir, "bar", barContentsOrig)

      // verifies FileSystem decoding of previously written file
      FileSystemTest.compareContents(fs, barFile, barContentsOrig)

      verifyEncodedName(fs, barFile, compProvider)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Creates a new root file that is compressed and verifies contents using a different compression provider filter
   * for reading then writing.
   */
  @Test def compress3() {
    val unitTestName = this.getClass.getSimpleName + ".compress3"
    val fooFilename = "foo"
    val fooContentsOrig = "compress" * 100

    val fs1 = createFileSystem(
      unitTestName, "", logger, new GZIPCompressionProvider(new BooleanFilter(true)), new NullEncryptionProvider)
    try {
      FileSystemTest.writeFile(fs1, fs1.root, fooFilename, fooContentsOrig)
    }
    finally fs1.disconnect()
    verifyFileSystem(unitTestName, fs1)

    val fs2 = createFileSystem(
      unitTestName,
      "",
      logger,
      new GZIPCompressionProvider(new NotFilter(new BooleanFilter(true))),
      new NullEncryptionProvider)
    try {
      val fooFile = fs2.children(fs2.root).find(_.name == fooFilename).get.asInstanceOf[SomerFile]
      FileSystemTest.compareContents(fs2, fooFile, fooContentsOrig)
    }
    finally fs2.disconnect()
    verifyFileSystem(unitTestName, fs2)
  }

  /**
   * Creates a new root file that is compressed and verifies contents using a different compression provider filter
   * for reading then writing.
   */
  @Test def compress4() {
    val unitTestName = this.getClass.getSimpleName + ".compress4"
    val fooDirName = "foo"
    val barFileName = "bar"
    val barContentsOrig = "compress" * 100

    val fs1 = createFileSystem(
      unitTestName, "", logger, new GZIPCompressionProvider(new BooleanFilter(true)), new NullEncryptionProvider)
    try {
      val fooDir = fs1.createDir(fs1.root, fooDirName)
      FileSystemTest.writeFile(fs1, fooDir, barFileName, barContentsOrig)
    }
    finally fs1.disconnect()
    verifyFileSystem(unitTestName, fs1)

    val fs2 = createFileSystem(
      unitTestName, "", logger, new GZIPCompressionProvider(new BooleanFilter(false)), new NullEncryptionProvider)
    try {
      val fooDir = fs2.children(fs2.root).find(_.name == fooDirName).get.asInstanceOf[SomerDirectory]
      val barFile = fs2.children(fooDir).find(_.name == barFileName).get.asInstanceOf[SomerFile]

      FileSystemTest.compareContents(fs2, barFile, barContentsOrig)
    }
    finally fs2.disconnect()
    verifyFileSystem(unitTestName, fs2)
  }

  /**
   * Creates a new root file that is encrypted and verifies its contents.
   */
  @Test def encrypt1() {
    val unitTestName = this.getClass.getSimpleName + ".encrypt1"
    val encProvider = new BouncyCastleEncryptionProvider(BouncyCastleEncryptionKey.createStrongKey("password"))
    val fs = createFileSystem(unitTestName, "", logger, new NullCompressionProvider, encProvider)

    try {
      val fooContentsOrig = "encrypt" * 100
      val fooFile = FileSystemTest.writeFile(fs, fs.root, "foo", fooContentsOrig)

      // verifies FileSystem decoding of previously written file
      FileSystemTest.compareContents(fs, fooFile, fooContentsOrig)

      verifyEncodedName(fs, fooFile, encProvider)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Creates a new non-root file that is encrypted and verifies its contents.
   */
  @Test def encrypt2() {
    val unitTestName = this.getClass.getSimpleName + ".encrypt2"
    val encProvider = new BouncyCastleEncryptionProvider(BouncyCastleEncryptionKey.createStrongKey("password"))
    val fs = createFileSystem(unitTestName, "", logger, new NullCompressionProvider, encProvider)

    try {
      val barContentsOrig = "encrypt" * 100
      val fooDir = fs.createDir(fs.root, "foo")
      val barFile = FileSystemTest.writeFile(fs, fooDir, "bar", barContentsOrig)

      // verifies FileSystem decoding of previously written file
      FileSystemTest.compareContents(fs, barFile, barContentsOrig)

      verifyEncodedName(fs, barFile, encProvider)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Creates a new root file that is encrypted and verifies contents using the same encryption provider filter
   * for reading and writing.
   */
  @Test def encrypt3() {
    val unitTestName = this.getClass.getSimpleName + ".encrypt3"
    val fooFilename = "foo"
    val fooContentsOrig = "encrypt" * 100
    val encProvider = new BouncyCastleEncryptionProvider(BouncyCastleEncryptionKey.createStrongKey("password"))

    val fs1 = createFileSystem(unitTestName, "", logger, new NullCompressionProvider, encProvider)
    try {
      FileSystemTest.writeFile(fs1, fs1.root, fooFilename, fooContentsOrig)
    }
    finally fs1.disconnect()
    verifyFileSystem(unitTestName, fs1)

    val fs2 = createFileSystem(unitTestName, "", logger, new NullCompressionProvider, encProvider)
    try {
      val fooFile = fs2.children(fs2.root).find(_.name == fooFilename).get.asInstanceOf[SomerFile]
      FileSystemTest.compareContents(fs2, fooFile, fooContentsOrig)
    }
    finally fs2.disconnect()
    verifyFileSystem(unitTestName, fs2)
  }

  /**
   * Creates a new root file that is encrypted and verifies contents using the same encryption provider filter
   * for reading and writing.
   */
  @Test def encrypt4() {
    val unitTestName = this.getClass.getSimpleName + ".encrypt4"
    val fooDirName = "foo"
    val barFileName = "bar"
    val barContentsOrig = "encrypt" * 100
    val encProvider = new BouncyCastleEncryptionProvider(BouncyCastleEncryptionKey.createStrongKey("password"))

    val fs1 = createFileSystem(unitTestName, "", logger, new NullCompressionProvider, encProvider)
    try {
      val fooDir = fs1.createDir(fs1.root, fooDirName)
      FileSystemTest.writeFile(fs1, fooDir, barFileName, barContentsOrig)
    }
    finally fs1.disconnect()
    verifyFileSystem(unitTestName, fs1)

    val fs2 = createFileSystem(unitTestName, "", logger, new NullCompressionProvider, encProvider)
    try {
      val fooDir = fs2.children(fs2.root).find(_.name == fooDirName).get.asInstanceOf[SomerDirectory]
      val barFile = fs2.children(fooDir).find(_.name == barFileName).get.asInstanceOf[SomerFile]

      FileSystemTest.compareContents(fs2, barFile, barContentsOrig)
    }
    finally fs2.disconnect()
    verifyFileSystem(unitTestName, fs2)
  }

  /**
   * Creates a new root directory verifies its name is encrypted.
   */
  @Test def encrypt5() {
    val unitTestName = this.getClass.getSimpleName + ".encrypt5"
    val encProvider = new BouncyCastleEncryptionProvider(BouncyCastleEncryptionKey.createStrongKey("password"))
    val fs = createFileSystem(unitTestName, "", logger, new NullCompressionProvider, encProvider)

    try {
      val foo = fs.createDir(fs.root, "foo")

      val list = fs.children(fs.root)
      Assert.areEqual(list.size, 1)
      Assert.isTrue(list.exists(x => x.isInstanceOf[SomerDirectory] && x.name == foo.name))

      verifyEncodedName(fs, foo, encProvider)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Creates a new non-root directory verifies its name is encrypted.
   */
  @Test def encrypt6() {
    val unitTestName = this.getClass.getSimpleName + ".encrypt6"
    val encProvider = new BouncyCastleEncryptionProvider(BouncyCastleEncryptionKey.createStrongKey("password"))
    val fs = createFileSystem(unitTestName, "", logger, new NullCompressionProvider, encProvider)

    try {
      val foo = fs.createDir(fs.root, "foo")
      val bar = fs.createDir(foo, "bar")

      val listRoot = fs.children(fs.root)
      Assert.areEqual(listRoot.size, 1)
      Assert.isTrue(listRoot.exists(x => x.isInstanceOf[SomerDirectory] && x.name == foo.name))
      verifyEncodedName(fs, foo, encProvider)

      val listFoo = fs.children(foo)
      Assert.areEqual(listFoo.size, 1)
      Assert.isTrue(listFoo.exists(x => x.isInstanceOf[SomerDirectory] && x.name == bar.name))
      verifyEncodedName(fs, bar, encProvider)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Creates a new root file that is compressed and encrypted and verifies contents using a different
   * compression and encryption provider filter for reading and writing.
   */
  @Test def compAndEnc1() {
    val unitTestName = this.getClass.getSimpleName + ".compAndEnc1"
    val fooFilename = "foo"
    val fooContentsOrig = "compAndEnc" * 100
    val encProvider = new BouncyCastleEncryptionProvider(BouncyCastleEncryptionKey.createStrongKey("password"))

    val fs1 = createFileSystem(
      unitTestName, "", logger, new GZIPCompressionProvider(new BooleanFilter(true)), encProvider)
    try {
      FileSystemTest.writeFile(fs1, fs1.root, fooFilename, fooContentsOrig)
    }
    finally fs1.disconnect()
    verifyFileSystem(unitTestName, fs1)

    val fs2 = createFileSystem(
      unitTestName, "", logger, new GZIPCompressionProvider(new BooleanFilter(false)), encProvider)
    try {
      val fooFile = fs2.children(fs2.root).find(_.name == fooFilename).get.asInstanceOf[SomerFile]
      FileSystemTest.compareContents(fs2, fooFile, fooContentsOrig)
    }
    finally fs2.disconnect()
    verifyFileSystem(unitTestName, fs2)
  }

  /**
   * Creates a non-root file that is compressed and encrypted and verifies contents using a different
   * compression and encryption provider filter for reading and writing.
   */
  @Test def compAndEnc2() {
    val unitTestName = this.getClass.getSimpleName + ".compAndEnc2"
    val fooDirName = "foo"
    val barFileName = "bar"
    val barContentsOrig = "compAndEnc" * 100
    val encProvider = new BouncyCastleEncryptionProvider(BouncyCastleEncryptionKey.createStrongKey("password"))

    val fs1 = createFileSystem(
      unitTestName, "", logger, new GZIPCompressionProvider(new BooleanFilter(true)), encProvider)
    try {
      val fooDir = fs1.createDir(fs1.root, fooDirName)
      FileSystemTest.writeFile(fs1, fooDir, barFileName, barContentsOrig)
    }
    finally fs1.disconnect()
    verifyFileSystem(unitTestName, fs1)

    val fs2 = createFileSystem(
      unitTestName, "", logger, new GZIPCompressionProvider(new BooleanFilter(false)), encProvider)
    try {
      val fooDir = fs2.children(fs2.root).find(_.name == fooDirName).get.asInstanceOf[SomerDirectory]
      val barFile = fs2.children(fooDir).find(_.name == barFileName).get.asInstanceOf[SomerFile]

      FileSystemTest.compareContents(fs2, barFile, barContentsOrig)
    }
    finally fs2.disconnect()
    verifyFileSystem(unitTestName, fs2)
  }

  /**
   * Verifies the name of the artifact is different when encoded
   */
  protected def verifyEncodedName[T <: EncodeResult](
    fs: FileSystem, a: SomerArtifact, encProvider: EncodingProvider[T]) {
    val encResult = a match {
      case f: SomerFile => encProvider.encodeFile(f.name)
      case d: SomerDirectory => encProvider.encodeDir(d.name)
      case _ => throw new Exception("not file or dir?!?")
    }
    val index = fs.getIndexArtifact(a).get
    Assert.isTrue(encResult.encoded)
    Assert.areEqual(a.name, index.origName)
    Assert.notEquals(a.name, index.storedName)
  }

  protected def verifyIndexName(fs: FileSystem, dir: SomerDirectory) {
    val index = fs.getIndexArtifact(dir).get
    Assert.areEqual(dir.name, index.origName)
    Assert.notEquals(dir.name, index.storedName)
  }
}