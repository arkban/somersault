package somersault.core.filesystems.index

import java.io.{InputStream, File}
import java.lang.String
import org.apache.commons.io.{FilenameUtils, FileUtils}
import org.joda.time.DateTime
import org.testng.annotations.Test
import somersault.core.TestUtil
import somersault.core.encode.{GZIPCompressionProvider, CompressionProviderMismatchException, EncryptionProvider, CompressionProvider, NullEncryptionProvider, BouncyCastleEncryptionProvider, NullCompressionProvider, BouncyCastleEncryptionKey, EncryptionProviderMismatchException}
import somersault.core.filesystems.{SomerFile, FileAttributes}
import somersault.core.filters.BooleanFilter
import somersault.util.{HashInfoMismatchException, VersionMismatchException, Version, VersionedDataAccessObject, H2DataAccessObject, Hash, Assert, HashInfo, FilenameUtils2}

class H2IndexTest extends IndexTest {

  private val indexFileName = "h2index"

  private def createTestPath(testName: String) = {
    val testPath = FilenameUtils2.concat(FilenameUtils2.tempPath.getPath, this.getClass.getSimpleName, testName)
    FileUtils.forceMkdir(new File(testPath))
    testPath
  }

  private def createTestIndexFile(testName: String) =
    new File(FilenameUtils.concat(createTestPath(testName), indexFileName))

  protected def createIndex(testName: String, data: IndexData) =
    data match {
      case d: H2IndexData => new H2Index(d)
      case _ => throw new IllegalArgumentException("expected H2IndexData")
    }

  protected def createData(
    testName: String,
    hashInfo: HashInfo,
    compProvider: CompressionProvider,
    encProvider: EncryptionProvider): IndexData =
    new H2IndexData(hashInfo, compProvider, encProvider, TestUtil.logger, createTestIndexFile(testName))

  /**
   * Re-opening the index should retain all existing data.
   */
  @Test def recreateIndex1() {
    val testName = "recreateIndex1"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index1 = createIndex(testName, data)

    index1.createDir(index1.root, new IndexDirectoryPrototype("foo", "foo2"))
    index1.writeFile(
      index1.root, new IndexFilePrototype(
        "barFile", "barFile2", 0, new DateTime, new FileAttributes(false, false, false), false, false,
        new Hash(data.hashInfo, new Array[Byte](0))))
    Assert.areEqual(index1.children(index1.root).size, 2)

    val index2 = createIndex(testName, data)
    Assert.areEqual(index2.children(index2.root).size, 2)
  }

  /**
   * Test that index checks its versions. We create an index, modify, then re-open that index, and the re-opening
   * should fail on the check.
   */
  @Test def errorOnDiffVersion() {
    val testName = "errorOnDiffVersion"
    val testIndexFile = new File(FilenameUtils2.concat(createTestPath(testName), indexFileName))
    val data = new H2IndexData(
      HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider, TestUtil.logger, testIndexFile)

    // initialized new index and change schemaVersion number
    new H2Index(data)
    val rawIndexDao = new H2DataAccessObject with VersionedDataAccessObject {
      def dataSource = createDataSource(testIndexFile, deleteIfExists = false)

      val schemaVersion = new Version(1, 2, 3)
    }
    val currVersion = rawIndexDao.readVersion(rawIndexDao.dataSource)
    val newVersion = new Version(currVersion.major + 1, currVersion.minor + 1, currVersion.release + 1)
    rawIndexDao.writeVersion(rawIndexDao.dataSource, newVersion)

    // re-open index, expecting error on different schemaVersion that expected
    try {
      new H2Index(data)
      Assert.fail("expected VersionMismatchException ")
    }
    catch {
      case ex: VersionMismatchException =>
    }
  }

  /**
   * Test that index checks its HashInfo. We create an index, modify, then re-open that index, and the re-opening
   * should fail on the check.
   */
  @Test def errorOnDiffHashInfo() {
    val testName = "errorOnDiffHashInfo"

    val data1 = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    createIndex(testName, data1)

    try {
      val data2 = createData(testName, HashInfo.sha1, data1.compressionProvider(), data1.encryptionProvider())
      createIndex(testName, data2)
      Assert.fail("expected HashInfoMismatchException")
    }
    catch {
      case ex: HashInfoMismatchException =>
        Assert.areEqual(ex.actual, HashInfo.sha1)
        Assert.areEqual(ex.expected, HashInfo.md5)
    }
  }

  /**
   * Test that index checks its CompressionProvider is compared.
   * - Check that compression is used and then not used.
   */
  @Test def errorOnDiffCompProvider1() {
    val testName = "errorOnDiffCompProvider1"

    val data1 = createData(
      testName, HashInfo.md5, new GZIPCompressionProvider(new BooleanFilter(true)), new NullEncryptionProvider)
    createIndex(testName, data1)

    try {
      val data2 = createData(testName, data1.hashInfo, new NullCompressionProvider, data1.encryptionProvider())
      createIndex(testName, data2)
      Assert.fail("expected CompressionProviderMismatchException")
    }
    catch {
      case ex: CompressionProviderMismatchException =>
        Assert.areEqual(ex.expectedName, data1.compressionProvider().name)
        Assert.notEquals(ex.actualName, data1.compressionProvider().name)
    }
  }

  /**
   * Test that index checks its CompressionProvider is compared.
   * - Check that compression is used then a different compression is used.
   */
  @Test def errorOnDiffCompProvider2() {
    val testName = "errorOnDiffCompProvider2"

    val data1 = createData(
      testName, HashInfo.md5, new GZIPCompressionProvider(new BooleanFilter(true)), new NullEncryptionProvider)
    createIndex(testName, data1)

    try {
      val compProvider2 = new CompressionProvider {
        def decode(newName: String, size: Long, input: InputStream) = null

        def encodeDir(name: String) = null

        def encodeFile(name: String) = null

        def encode(parentPath: Iterable[String], file: SomerFile) = null

        def name = data1.compressionProvider().name + "2"
      }

      val data2 = createData(testName, data1.hashInfo, compProvider2, data1.encryptionProvider())
      createIndex(testName, data2)
      Assert.fail("expected CompressionProviderMismatchException")
    }
    catch {
      case ex: CompressionProviderMismatchException =>
        Assert.areEqual(ex.expectedName, data1.compressionProvider().name)
        Assert.notEquals(ex.actualName, data1.compressionProvider().name)
    }
  }

  /**
   * Test that index checks its EncryptionProvider is compared.
   * - Check that encryption is used and then not used.
   */
  @Test def errorOnDiffEncProvider1() {
    val testName = "errorOnDiffEncProvider1"

    val key = BouncyCastleEncryptionKey.createStrongKey("password")
    val encProvider = new BouncyCastleEncryptionProvider(key)
    val data1 = createData(testName, HashInfo.md5, new NullCompressionProvider, encProvider)
    createIndex(testName, data1)

    try {
      val data2 = createData(testName, data1.hashInfo, data1.compressionProvider(), new NullEncryptionProvider)
      createIndex(testName, data2)
      Assert.fail("expected CompressionProviderMismatchException")
    }
    catch {
      case ex: EncryptionProviderMismatchException =>
    }
  }

  /**
   * Test that index checks its EncryptionProvider is compared.
   * - Check that a different key is used.
   */
  @Test def errorOnDiffEncProvider2() {
    val testName = "errorOnDiffEncProvider2"

    val key1 = BouncyCastleEncryptionKey.createStrongKey("password")
    val encProvider1 = new BouncyCastleEncryptionProvider(key1)
    val data1 = createData(testName, HashInfo.md5, new NullCompressionProvider, encProvider1)
    createIndex(testName, data1)

    try {
      val key2 = new BouncyCastleEncryptionKey(
        key1.cipherSpec, key1.pbeSpec, key1.keyLength, key1.saltLength, key1.userKey + "2")
      val encProvider2 = new BouncyCastleEncryptionProvider(key2)
      val data2 = createData(testName, data1.hashInfo, data1.compressionProvider(), encProvider2)
      createIndex(testName, data2)
      Assert.fail("expected CompressionProviderMismatchException")
    }
    catch {
      case ex: EncryptionProviderMismatchException =>
    }
  }
}