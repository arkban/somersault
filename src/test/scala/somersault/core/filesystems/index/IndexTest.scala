package somersault.core.filesystems.index

import org.joda.time.DateTime
import org.testng.annotations.Test
import scala.collection.immutable.Nil
import somersault.core.encode.{NullCompressionProvider, NullEncryptionProvider, EncryptionProvider, CompressionProvider}
import somersault.core.exceptions.{AlreadyExistsException, PathDoesNotExistException}
import somersault.core.filesystems.FileAttributes
import somersault.util.{Assert, HashInfo, Hash}

abstract class IndexTest {

  protected def createIndex(testName: String, data: IndexData): Index

  protected def createData(
    testName: String,
    hashInfo: HashInfo,
    compProvider: CompressionProvider,
    encProvider: EncryptionProvider): IndexData

  /**
   * Get children from the root.
   * - No files or directories exist.
   */
  @Test def root() {
    val testName = "root"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val fs = createIndex(testName, data)

    val list = fs.children(fs.root)
    Assert.areEqual(list.size, 0)
  }

  /**
   * Creates two directories at the root. The directories are empty.
   */
  @Test def createDir1() {
    val testName = "createDir1"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index = createIndex(testName, data)

    val foo = index.createDir(index.root, new IndexDirectoryPrototype("foo", "foo2"))
    val list1 = index.children(index.root)
    Assert.areEqual(list1.size, 1)
    Assert.isTrue(list1.forall(_.isInstanceOf[IndexDirectory]))
    Assert.isTrue(list1.exists(x => x.origName == foo.origName && x.storedName == foo.storedName))

    val bar = index.createDir(index.root, new IndexDirectoryPrototype("bar", "bar2"))
    val list2 = index.children(index.root)
    Assert.areEqual(list2.size, 2)
    Assert.isTrue(list2.forall(_.isInstanceOf[IndexDirectory]))
    Assert.isTrue(list2.exists(x => x.origName == foo.origName && x.storedName == foo.storedName))
    Assert.isTrue(list2.exists(x => x.origName == bar.origName && x.storedName == bar.storedName))
  }

  /**
   * Creates nested directories. The nested directories are empty.
   */
  @Test def createDir2() {
    val testName = "createDir2"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index = createIndex("createDir2", data)
    val alpha = index.createDir(index.root, new IndexDirectoryPrototype("alpha", "alpha2"))
    val beta = index.createDir(index.root, new IndexDirectoryPrototype("beta", "beta2"))

    val alphaFoo = index.createDir(alpha, new IndexDirectoryPrototype("foo", "foo2"))
    val alphaBar = index.createDir(alpha, new IndexDirectoryPrototype("bar", "bar2"))
    val alphaList = index.children(alpha)
    Assert.areEqual(alphaList.size, 2)
    Assert.isTrue(alphaList.forall(_.isInstanceOf[IndexDirectory]))
    Assert.isTrue(alphaList.exists(x => x.origName == alphaFoo.origName && x.storedName == alphaFoo.storedName))
    Assert.isTrue(alphaList.exists(x => x.origName == alphaBar.origName && x.storedName == alphaBar.storedName))

    val betaFoo = index.createDir(beta, new IndexDirectoryPrototype("foo", "foo2"))
    val betaBar = index.createDir(beta, new IndexDirectoryPrototype("bar", "bar2"))
    val betaList = index.children(beta)
    Assert.areEqual(betaList.size, 2)
    Assert.isTrue(betaList.forall(_.isInstanceOf[IndexDirectory]))
    Assert.isTrue(betaList.exists(x => x.origName == betaFoo.origName && x.storedName == betaFoo.storedName))
    Assert.isTrue(betaList.exists(x => x.origName == betaBar.origName && x.storedName == betaBar.storedName))
  }

  /**
   * Creates same root directory multiple times.
   */
  @Test def createDir3() {
    val testName = "createDir3"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index = createIndex(testName, data)
    val foo = new IndexDirectoryPrototype("foo", "foo2")
    index.createDir(index.root, foo)
    try {
      index.createDir(index.root, foo)
      Assert.fail("Expected AlreadyExistsException")
    }
    catch {
      case ex: AlreadyExistsException =>
        Assert.areEqual(ex.path.size, 1, ex.path.toString())
        Assert.areEqual(ex.path(0), foo.origName, ex.path.toString())
    }
  }

  /**
   * Creates same nested directory multiple times
   */
  @Test def createDir4() {
    val testName = "createDir4"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index = createIndex(testName, data)
    val foo = index.createDir(index.root, new IndexDirectoryPrototype("foo", "foo2"))
    val bar = new IndexDirectoryPrototype("bar", "bar2")
    index.createDir(foo, bar)
    try {
      index.createDir(foo, bar)
      Assert.fail("Expected AlreadyExistsException")
    }
    catch {
      case ex: AlreadyExistsException =>
        Assert.areEqual(ex.path.size, 2, ex.path.toString())
        Assert.areEqual(ex.path(0), foo.origName, ex.path.toString())
        Assert.areEqual(ex.path(1), bar.origName, ex.path.toString())
    }
  }

  /**
   * Creates root directory over existing file.
   */
  @Test def createDir5() {
    val testName = "createDir5"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index = createIndex(testName, data)
    val foo = index.writeFile(
      index.root, new IndexFilePrototype(
        "foo", "foo2", 0, new DateTime, new FileAttributes(false, false, false), false, false,
        new Hash(data.hashInfo, new Array[Byte](0))))
    try {
      index.createDir(index.root, new IndexDirectoryPrototype(foo.origName, foo.storedName))
      Assert.fail("Expected AlreadyExistsException")
    }
    catch {
      case ex: AlreadyExistsException =>
        Assert.areEqual(ex.path.size, 1, ex.path.toString())
        Assert.areEqual(ex.path(0), foo.origName, ex.path.toString())
    }
  }

  /**
   * Creates nested directory over existing file.
   */
  @Test def createDir6() {
    val testName = "createDir6"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index = createIndex(testName, data)
    val foo = index.createDir(index.root, new IndexDirectoryPrototype("foo", "foo2"))
    val bar = index.writeFile(
      foo, new IndexFilePrototype(
        "bar", "bar2", 0, new DateTime, new FileAttributes(false, false, false), false, false,
        new Hash(data.hashInfo, new Array[Byte](0))))

    try {
      index.createDir(foo, new IndexDirectoryPrototype(bar.origName, bar.storedName))
      Assert.fail("Expected AlreadyExistsException")
    }
    catch {
      case ex: AlreadyExistsException =>
        Assert.areEqual(ex.path.size, 2, ex.path.toString())
        Assert.areEqual(ex.path(0), foo.origName, ex.path.toString())
        Assert.areEqual(ex.path(1), bar.origName, ex.path.toString())
    }
  }

  /**
   * Remove two directories at the root. The directories are empty.
   */
  @Test def remove1() {
    val testName = "remove1"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index = createIndex(testName, data)
    val foo = index.createDir(index.root, new IndexDirectoryPrototype("foo", "foo2"))
    val bar = index.createDir(index.root, new IndexDirectoryPrototype("bar", "bar2"))

    index.remove(foo)
    val list1 = index.children(index.root)
    Assert.areEqual(list1.size, 1)
    Assert.isTrue(list1.forall(_.isInstanceOf[IndexDirectory]))
    Assert.isTrue(list1.exists(x => x.origName == bar.origName && x.storedName == bar.storedName))

    index.remove(bar)
    val list2 = index.children(index.root)
    Assert.areEqual(list2.size, 0)
  }

  /**
   * Remove nested directories. The nested directories are empty.
   */
  @Test def remove2() {
    val testName = "remove2"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index = createIndex(testName, data)
    val alpha = index.createDir(index.root, new IndexDirectoryPrototype("alpha", "alpha2"))
    val beta = index.createDir(index.root, new IndexDirectoryPrototype("beta", "beta2"))
    val alphaFoo = index.createDir(alpha, new IndexDirectoryPrototype("foo", "foo2"))
    val alphaBar = index.createDir(alpha, new IndexDirectoryPrototype("bar", "bar2"))
    val betaFoo = index.createDir(beta, new IndexDirectoryPrototype("foo", "foo2"))
    val betaBar = index.createDir(beta, new IndexDirectoryPrototype("bar", "bar2"))

    // remove individually
    index.remove(alphaFoo)
    val alphaList1 = index.children(alpha)
    Assert.areEqual(alphaList1.size, 1)
    Assert.isTrue(alphaList1.forall(_.isInstanceOf[IndexDirectory]))
    Assert.isTrue(alphaList1.exists(x => x.origName == alphaBar.origName && x.storedName == alphaBar.storedName))

    index.remove(alphaBar)
    val alphaList2 = index.children(alpha)
    Assert.areEqual(alphaList2.size, 0)

    // remove together
    index.remove(betaFoo)
    index.remove(betaBar)
    val betaList = index.children(beta)
    Assert.areEqual(betaList.size, 0)

    // remove root
    index.remove(alpha)
    index.remove(beta)
    val rootList = index.children(index.root)
    Assert.areEqual(rootList.size, 0)
  }

  /**
   * Remove non-empty root directories.
   */
  @Test def remove3() {
    val testName = "remove3"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index = createIndex(testName, data)
    val alpha = index.createDir(index.root, new IndexDirectoryPrototype("alpha", "alpha2"))
    val beta = index.createDir(index.root, new IndexDirectoryPrototype("beta", "beta2"))
    index.createDir(alpha, new IndexDirectoryPrototype("foo", "foo2"))
    index.createDir(alpha, new IndexDirectoryPrototype("bar", "bar2"))
    index.createDir(beta, new IndexDirectoryPrototype("foo", "foo2"))
    index.createDir(beta, new IndexDirectoryPrototype("bar", "bar2"))

    index.remove(alpha)
    val rootList1 = index.children(index.root)
    Assert.areEqual(rootList1.size, 1)
    Assert.isTrue(rootList1.forall(_.isInstanceOf[IndexDirectory]))
    Assert.isTrue(rootList1.exists(x => x.origName == beta.origName && x.storedName == beta.storedName))

    index.remove(beta)
    val rootList2 = index.children(index.root)
    Assert.areEqual(rootList2.size, 0)
  }

  /**
   * Remove nested directories. The nested directories are empty.
   */
  @Test def remove4() {
    val testName = "remove4"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index = createIndex(testName, data)
    val alpha = index.createDir(index.root, new IndexDirectoryPrototype("alpha", "alpha2"))
    val beta = index.createDir(index.root, new IndexDirectoryPrototype("beta", "beta2"))
    val alphaFoo = index.createDir(alpha, new IndexDirectoryPrototype("foo", "foo2"))
    index.createDir(alphaFoo, new IndexDirectoryPrototype("bar", "bar2"))
    val betaFoo = index.createDir(beta, new IndexDirectoryPrototype("foo", "foo2"))
    index.createDir(betaFoo, new IndexDirectoryPrototype("bar", "bar2"))

    index.remove(alphaFoo)
    val alphaList = index.children(alpha)
    Assert.areEqual(alphaList.size, 0)

    index.remove(betaFoo)
    val betaList = index.children(beta)
    Assert.areEqual(betaList.size, 0)

    // remove root
    index.remove(alpha)
    index.remove(beta)
    val rootList = index.children(index.root)
    Assert.areEqual(rootList.size, 0)
  }

  /**
   * Removes a file at the root.
   */
  @Test def remove5() {
    val testName = "remove5"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index = createIndex(testName, data)
    val foo = index.writeFile(
      index.root, new IndexFilePrototype(
        "foo", "foo2", 0, new DateTime, new FileAttributes(false, false, false), false, false,
        new Hash(data.hashInfo, new Array[Byte](0))))

    val rootList1 = index.children(index.root)
    Assert.areEqual(rootList1.size, 1)
    val fooFile = rootList1.find(
      x => x.isInstanceOf[IndexFile] && x.origName == foo.origName && x.storedName == foo.storedName)
      .get
      .asInstanceOf[IndexFile]

    index.remove(fooFile)
    val rootList2 = index.children(index.root)
    Assert.areEqual(rootList2.size, 0)
  }

  /**
   * Removes a nested file.
   */
  @Test def remove6() {
    val testName = "remove6"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index = createIndex(testName, data)

    val foo = index.createDir(index.root, new IndexDirectoryPrototype("foo", "foo2"))
    val bar = index.writeFile(
      foo, new IndexFilePrototype(
        "bar", "bar2", 0, new DateTime, new FileAttributes(false, false, false), false, false,
        new Hash(data.hashInfo, new Array[Byte](0))))

    val rootList1 = index.children(foo)
    Assert.areEqual(rootList1.size, 1)
    val barFile = rootList1.find(
      x => x.isInstanceOf[IndexFile] && x.origName == bar.origName && x.storedName == bar.storedName)
      .get
      .asInstanceOf[IndexFile]

    index.remove(barFile)
    val rootList2 = index.children(foo)
    Assert.areEqual(rootList2.size, 0)
  }

  /**
   * Removes a root directory containing a directory and file.
   */
  @Test def remove7() {
    val testName = "remove7"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index = createIndex(testName, data)

    val foo = index.createDir(index.root, new IndexDirectoryPrototype("foo", "foo2"))
    index.createDir(foo, new IndexDirectoryPrototype("barDir", "barDir2"))
    index.writeFile(
      foo, new IndexFilePrototype(
        "barFile", "barFile2", 0, new DateTime, new FileAttributes(false, false, false), false, false,
        new Hash(data.hashInfo, new Array[Byte](0))))

    val rootList1 = index.children(foo)
    Assert.areEqual(rootList1.size, 2)

    index.remove(foo)
    val rootList2 = index.children(index.root)
    Assert.areEqual(rootList2.size, 0)
  }

  /**
   * Remove a directory at the root, and get an exception if trying to get its children after removal.
   */
  @Test def remove8() {
    val testName = "remove8"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index = createIndex(testName, data)
    val foo = index.createDir(index.root, new IndexDirectoryPrototype("foo", "foo2"))
    val bar = index.createDir(foo, new IndexDirectoryPrototype("bar", "bar2"))

    index.remove(foo)
    val list1 = index.children(index.root)
    Assert.areEqual(list1.size, 0)

    try {
      index.children(foo)
      Assert.fail("Expected PathDoesNotExistException")
    }
    catch {
      case ex: PathDoesNotExistException =>
        Assert.areEqual(ex.path.size, 1, ex.path.toString())
        Assert.areEqual(ex.path(0), foo.origName, ex.path.toString())
    }

    try {
      index.children(bar)
      Assert.fail("Expected PathDoesNotExistException")
    }
    catch {
      case ex: PathDoesNotExistException =>
        Assert.areEqual(ex.path.size, 1, ex.path.toString())
        Assert.areEqual(ex.path(0), bar.origName, ex.path.toString())
    }
  }

  /**
   * Remove root should fail.
   */
  @Test def remove9() {
    val testName = "remove9"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index = createIndex(testName, data)
    try {
      index.remove(index.root)
      Assert.fail("Expected IllegalArgumentException")
    }
    catch {
      case ex: IllegalArgumentException =>
    }
  }

  /**
   * Creates a new root file.
   */
  @Test def writeFile1() {
    val testName = "writeFile1"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index = createIndex(testName, data)

    getComparisonPrototypes(data.hashInfo).foreach(
      pt => {
        val foo = index.writeFile(index.root, pt)
        index.children(index.root).find(_.origName == pt.origName) match {
          case Some(file: IndexFile) => compareFiles(file, foo)
          case _ => Assert.fail(pt.origName)
        }
      })
  }

  /**
   * Creates a new non-root file.
   */
  @Test def writeFile2() {
    val testName = "writeFile2"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index = createIndex(testName, data)
    val bar = index.createDir(index.root, new IndexDirectoryPrototype("bar", "bar2"))

    getComparisonPrototypes(data.hashInfo).foreach(
      pt => {
        val foo = index.writeFile(bar, pt)
        index.children(bar).find(_.origName == pt.origName) match {
          case Some(file: IndexFile) => compareFiles(file, foo)
          case _ => Assert.fail(pt.origName)
        }
      })
  }

  /**
   * Verifies overwriting a root file with the same data correctly changes data.
   */
  @Test def writeFile3() {
    val testName = "writeFile3"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index = createIndex(testName, data)

    val hashData1 = new Array[Byte](1)
    val hashData2 = new Array[Byte](1)
    hashData2(0) = (hashData1(0) + 1).asInstanceOf[Byte]

    val foo1 = index.writeFile(
      index.root, new IndexFilePrototype(
        "foo", "foo2", 0, new DateTime, new FileAttributes(false, false, false), false, false,
        new Hash(data.hashInfo, hashData1)))
    val list1 = index.children(index.root).toList
    Assert.areEqual(list1.size, 1)
    val file1 = list1(0).asInstanceOf[IndexFile]
    compareFiles(file1, foo1)

    val foo2 = index.writeFile(
      index.root, new IndexFilePrototype(
        foo1.origName, foo1.storedName + "1", 1, new DateTime(foo1.modified.getMillis + 10000), new FileAttributes(
          true, true, true), true, true, new Hash(data.hashInfo, hashData2)))
    val list2 = index.children(index.root).toList
    Assert.areEqual(list2.size, 1)
    val file2 = list2(0).asInstanceOf[IndexFile]
    compareFiles(file2, foo2)

    Assert.areEqual(foo1.origName, foo2.origName)
    Assert.notEquals(foo1.storedName, foo2.storedName)
    Assert.notEquals(foo1.size, foo2.size)
    Assert.notEquals(foo1.modified.getMillis, foo2.modified.getMillis, 1000L)
    Assert.notEquals(foo1.attributes, foo2.attributes)
    Assert.notEquals(foo1.compressed, foo2.compressed)
    Assert.notEquals(foo1.encrypted, foo2.encrypted)
    Assert.notEquals(foo1.hash, foo2.hash)
  }

  /**
   * Verifies overwriting a non-root file with the same data correctly changes data.
   */
  @Test def writeFile4() {
    val testName = "writeFile4"
    val data = createData(testName, HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider)
    val index = createIndex(testName, data)

    val hashData1 = new Array[Byte](1)
    val hashData2 = new Array[Byte](1)
    hashData2(0) = (hashData1(0) + 1).asInstanceOf[Byte]
    val foo = index.createDir(index.root, new IndexDirectoryPrototype("bar", "bar2"))

    val bar1 = index.writeFile(
      foo, new IndexFilePrototype(
        "bar", "bar2", 0, new DateTime, new FileAttributes(false, false, false), false, false,
        new Hash(data.hashInfo, hashData1)))
    val list1 = index.children(foo).toList
    Assert.areEqual(list1.size, 1)
    val file1 = list1(0).asInstanceOf[IndexFile]
    compareFiles(file1, bar1)

    val bar2 = index.writeFile(
      foo, new IndexFilePrototype(
        bar1.origName, bar1.storedName + "1", 1, new DateTime(bar1.modified.getMillis + 10000), new FileAttributes(
          true, true, true), true, true, new Hash(data.hashInfo, hashData2)))
    val list2 = index.children(foo).toList
    Assert.areEqual(list2.size, 1)
    val file2 = list2(0).asInstanceOf[IndexFile]
    compareFiles(file2, bar2)

    Assert.areEqual(bar1.origName, bar2.origName)
    Assert.notEquals(bar1.storedName, bar2.storedName)
    Assert.notEquals(bar1.size, bar2.size)
    Assert.notEquals(bar1.modified.getMillis, bar2.modified.getMillis, 1000L)
    Assert.notEquals(bar1.attributes, bar2.attributes)
    Assert.notEquals(bar1.compressed, bar2.compressed)
    Assert.notEquals(bar1.encrypted, bar2.encrypted)
    Assert.notEquals(bar1.hash, bar2.hash)
  }

  private def getComparisonPrototypes(hashInfo: HashInfo): Iterable[IndexFilePrototype] = {
    val random = new java.util.Random()
    val hashBytes = new Array[Byte](16)
    random.nextBytes(hashBytes)

    new IndexFilePrototype(
      "foo_name", "foo_diffName", 0, new DateTime, new FileAttributes(false, false, false), false, false,
      new Hash(hashInfo, new Array[Byte](0))) ::
      new IndexFilePrototype(
        "foo_size", "foo", 1234567, new DateTime, new FileAttributes(false, false, false), false, false,
        new Hash(hashInfo, new Array[Byte](0))) ::
      new IndexFilePrototype(
        "foo_date", "foo", 0, new DateTime(1956, 9, 14, 10, 20, 30, 400), new FileAttributes(
          true, false, false), false, false, new Hash(hashInfo, new Array[Byte](0))) ::
      new IndexFilePrototype(
        "foo_exec", "foo", 0, new DateTime, new FileAttributes(true, false, false), false, false,
        new Hash(hashInfo, new Array[Byte](0))) ::
      new IndexFilePrototype(
        "foo_read", "foo", 0, new DateTime, new FileAttributes(false, true, false), false, false,
        new Hash(hashInfo, new Array[Byte](0))) ::
      new IndexFilePrototype(
        "foo_write",
        "foo",
        0,
        new DateTime,
        new FileAttributes(false, false, true),
        false,
        false,
        new Hash(hashInfo, new Array[Byte](0))) ::
      new IndexFilePrototype(
        "foo_comp", "foo", 0, new DateTime, new FileAttributes(false, false, true), true, false,
        new Hash(hashInfo, new Array[Byte](0))) ::
      new IndexFilePrototype(
        "foo_enc", "foo", 0, new DateTime, new FileAttributes(false, false, true), false, true,
        new Hash(hashInfo, new Array[Byte](0))) ::
      new IndexFilePrototype(
        "foo_hash", "foo", 0, new DateTime, new FileAttributes(false, false, true), false, false,
        new Hash(hashInfo, hashBytes)) ::
      Nil
  }

  private def compareFiles(actual: IndexFile, expected: IndexFile) {
    Assert.areEqual(actual.origName, expected.origName, expected.origName)
    Assert.areEqual(actual.storedName, expected.storedName, expected.origName)
    Assert.areEqual(actual.size, expected.size, expected.origName)
    Assert.areEqual(actual.modified.getMillis, expected.modified.getMillis, 1000L, expected.origName)
    Assert.areEqual(actual.attributes, expected.attributes, expected.origName)
    Assert.areEqual(actual.compressed, expected.compressed, expected.origName)
    Assert.areEqual(actual.encrypted, expected.encrypted, expected.origName)
    Assert.areEqual(actual.hash, expected.hash, expected.origName)
  }
}