package somersault.core.filesystems.amazonS3

import collection.mutable
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.{S3ObjectSummary, Bucket}
import java.io.{FileReader, File, ByteArrayInputStream}
import java.util.Properties
import logahawk.Logger
import org.apache.commons.lang.IllegalClassException
import org.testng.annotations.Test
import scala.Some
import scala.collection.JavaConversions._
import scala.collection.convert.Wrappers
import somersault.core.TestUtil
import somersault.core.encode._
import somersault.core.filesystems._
import somersault.core.filesystems.index.{IndexSupport, IndexFile, IndexedFileSystemTest}
import somersault.util.persist.PersisterContainer
import somersault.util.{FilenameUtils2, Assert, HashInfo}

/**
 * These tests can take sometime because they depend on Amazon S3, a remote resource.
 */
class AmazonS3FileSystemTest extends IndexedFileSystemTest {

  private val bucketNamePrefix = "unittest"

  override protected def createFileSystem(
    testName: String,
    nameSuffix: String,
    logger: Logger,
    compProvider: CompressionProvider,
    encProvider: EncryptionProvider) = {
    val bucketName = getBucketName(testName)
    val unitTestProperties = loadProperties()
    val credentials = new AmazonS3FileSystem.Credentials(
      new BasicAWSCredentials(
        unitTestProperties.getProperty("accessKey"), unitTestProperties.getProperty("secretKey")))

    val runtimeData = FileSystemTest.createRuntimeData(
      TestUtil.createTestPath(classOf[AmazonS3FileSystemTest], testName))
    val fs = AmazonS3FileSystem.connect(
      new AmazonS3FileSystemData(
        testName + nameSuffix,
        HashInfo.md5,
        compProvider,
        encProvider,
        LockRefreshSupport.defaults,
        IndexSupport.defaults,
        credentials,
        bucketName), runtimeData)
    val service = fs.asInstanceOf[AmazonS3FileSystem].service.client

    // delete unit test buckets with a different unique stamp (to keep storage costs down and to avoid clutter while
    // debugging).
    val oldBuckets = new Wrappers.JCollectionWrapper[Bucket](service.listBuckets).filter(
      b => b.getName.startsWith(bucketNamePrefix) && b.getName != bucketName)

    if (oldBuckets.size > 50) throw new Exception("more than 50 buckets, I think we broke something!")
    oldBuckets.foreach(
      b => {
        try {
          logger.debug("Deleting old unit test Bucket: " + b.getName)

          val objects = new Wrappers.JCollectionWrapper[S3ObjectSummary](
            service.listObjects(b.getName).getObjectSummaries)
          objects.foreach(o => service.deleteObject(b.getName, o.getKey))
          service.deleteBucket(b.getName)
        }
        catch {
          case ex: Exception => logger
            .error("Failed to list or delete bucket, ignoring for now: " + b.getName, ex.getMessage)
        }
      })

    fs
  }

  override protected def verifyFileSystem(testName: String, fs: FileSystem) {
    val newFs = AmazonS3FileSystem.connect(fs.data, fs.runtimeData)
    try {
      val indexKey = AmazonS3FileSystem.createIndexKey(newFs.data)

      // get all keys in FileSystem
      val keys = new JListWrapper(
        newFs.service.client.listObjects(newFs.data.bucketName).getObjectSummaries).map(o => o.getKey).filterNot(
        _ == indexKey) // we don't count the index as a real file
      // collect the storedNames of all indexFiles in the index
      val storedNames = getAllStoredNames(newFs, newFs.root, Nil)

      Assert.areEqual(keys.size, storedNames.size)
      val onlyInIndex = keys.filterNot(storedNames.contains)
      Assert.areEqual(onlyInIndex.size, 0)
      val onlyInFileSystem = storedNames.filterNot(keys.contains)
      Assert.areEqual(onlyInFileSystem.size, 0)
    }
    finally newFs.disconnect()
  }

  private def getAllStoredNames(
    fs: FileSystem, parent: SomerDirectory, parentPath: List[String]): Seq[String] = {
    val names = new mutable.ListBuffer[String]
    fs.children(parent).foreach {
      case f: SomerFile => names += fs.getIndexArtifact(f).get.asInstanceOf[IndexFile].storedName
      case d: SomerDirectory => names ++= getAllStoredNames(fs, d, parentPath ::: d.name :: Nil)
    }
    names
  }

  protected def createFileSystemDataPersister(testName: String, logger: Logger): FileSystemDataPersister = {
    val pwc = new PersisterContainer
    pwc.add(classOf[NullCompressionProvider], new NullCompressionProviderPersister())
    pwc.add(classOf[NullEncryptionProvider], new NullEncryptionProviderPersister())
    new AmazonS3FileSystemDataPersister
  }

  protected def findRawFile(
    testName: String, data: FileSystemData, filePath: Iterable[String]): (FileSystem, SomerDirectory) =
    data match {
      case amazonData: AmazonS3FileSystemData => {
        val runtimeData = FileSystemTest.createRuntimeData(
          TestUtil.createTestPath(classOf[AmazonS3FileSystemTest], testName))
        val directFs = AmazonS3FileSystem.connect(
          new AmazonS3FileSystemData(
            testName,
            amazonData.hashInfo,
            new MockCompressionProvider(amazonData.compressionProvider.name),
            new MockEncryptionProvider(amazonData.encryptionProvider.key),
            LockRefreshSupport.defaults,
            IndexSupport.defaults,
            amazonData.credentials,
            amazonData.bucketName), runtimeData)

        // walk through parent directories
        var curr = directFs.root
        if (filePath.size >= 2) {
          filePath.take(filePath.size - 1).foreach(
            path =>
              directFs.children(curr).find(_.name == path) match {
                case Some(d: SomerDirectory) => curr = d
                case Some(f: SomerFile) => throw new Exception("Unexpected file in path at " + path)
                case None => throw new Exception("Bad path at " + path)
                case x => throw new Exception("Unexpected response: " + x)
              })
        }

        (directFs.asInstanceOf[FileSystem], curr)
      }
      case _ => throw new IllegalClassException(classOf[AmazonS3FileSystemData], data)
    }

  /**
   * Check that without encryption the path is recognizable in Amazon and the file content is not encrypted.
   */
  @Test def noEncryptVerify() {
    val unitTestName = "noEncVerification" // shortened for bucket name
    val fs = createFileSystem(unitTestName, "", logger).asInstanceOf[AmazonS3FileSystem]

    val origContent = "somersault unit testing\nmultiple lines of data\nwish us luck!\n" * 100
    val dirNames = "feline" :: "canine" :: "avian" :: Nil
    val fileNames = "cat" :: "dog" :: "bird" :: Nil

    try {
      dirNames.foreach(
        d => {
          val dir = fs.createDir(fs.root, d)
          fileNames.foreach(FileSystemTest.writeFile(fs, dir, _, origContent))
        })
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)

    val indexKey = AmazonS3FileSystem.createIndexKey(fs.data)
    val keys = new JListWrapper(
      fs.service.client.listObjects(fs.data.bucketName).getObjectSummaries).map(o => o.getKey).filterNot(
      _ == indexKey) // we don't count the index as a real file

    keys.foreach(
      k => {
        Assert.isTrue(containsAny(k)(dirNames))
        Assert.isTrue(containsAny(k)(fileNames))

        val (_, readContent) = TestUtil.readContents(fs.data.hashInfo, fs.service.readObject(k))
        Assert.areEqual(origContent, readContent)
      })
  }

  /**
   * Check that with encryption the path is NOT recognizable in Amazon and the file content is really encrypted.
   *
   * This method is the same as noEncryptVerify() but with the assert flags flipped.
   */
  @Test def encryptVerify() {
    val unitTestName = "encVerification" // shortened for bucket name
    val encProvider = new BouncyCastleEncryptionProvider(BouncyCastleEncryptionKey.createStrongKey("password"))
    val fs = createFileSystem(unitTestName, "", logger, new NullCompressionProvider, encProvider)
      .asInstanceOf[AmazonS3FileSystem]

    val origContent = "somersault unit testing\nmultiple lines of data\nwish us luck!\n" * 100
    val dirNames = "feline" :: "canine" :: "avian" :: Nil
    val fileNames = "cat" :: "dog" :: "bird" :: Nil

    try {
      dirNames.foreach(
        d => {
          val dir = fs.createDir(fs.root, d)
          fileNames.foreach(FileSystemTest.writeFile(fs, dir, _, origContent))
        })
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)

    val indexKey = AmazonS3FileSystem.createIndexKey(fs.data)
    val keys = new JListWrapper(fs.service.client.listObjects(fs.data.bucketName).getObjectSummaries)
      .map(o => o.getKey)
      .filterNot(_ == indexKey) // we don't count the index as a real file

    keys.foreach(
      k => {
        Assert.isFalse(containsAny(dirNames)(k))
        Assert.isFalse(containsAny(fileNames)(k))

        val (_, readContentBytes) = TestUtil.readRawContents(fs.data.hashInfo, fs.service.readObject(k))
        val readContent = new String(readContentBytes)
        Assert.notEquals(origContent, readContent)
      })
  }

  /**
   * Index believes it contains a directory tree but the real FileSystem is empty.
   */
  @Test def reconcile1() {
    val unitTestName = this.getClass.getSimpleName + ".reconcile1"

    val fs1 = createFileSystem(unitTestName, "", logger).asInstanceOf[AmazonS3FileSystem]
    try {
      val dirA = fs1.createDir(fs1.root, "dirA")
      fs1.createDir(fs1.root, "dirB")
      FileSystemTest.writeFile(fs1, fs1.root, "fileC", "contents")
      FileSystemTest.writeFile(fs1, dirA, "fileAA", "contents")
      fs1.createDir(dirA, "dirAB")

      Assert.notEquals(fs1.service.client.listObjects(fs1.data.bucketName).getObjectSummaries.size, 0)
      Assert.notEquals(fs1.children(fs1.root).size, 0)

      // delete all objects from fileSystem
      fs1.service.getKeys(1024).getObjectSummaries.foreach(
        obj => fs1.service.client.deleteObject(fs1.data.bucketName, obj.getKey))

      val objects = fs1.service.client.listObjects(fs1.data.bucketName).getObjectSummaries
      Assert.areEqual(objects.size, 0)
      Assert.notEquals(fs1.children(fs1.root).size, 0)
    }
    finally fs1.disconnect()

    val fs2 = createFileSystem(unitTestName, "", logger)
    try {
      val reconcileObs = new MockReconcileObserver(logger)
      fs2.reconcile(reconcileObs)
      Assert.areEqual(reconcileObs.errors.size, 0)
    }
    finally fs2.disconnect()

    val fs3 = createFileSystem(unitTestName, "", logger).asInstanceOf[AmazonS3FileSystem]
    try {
      val objects2 = fs3.service.client.listObjects(fs3.data.bucketName).getObjectSummaries
      Assert.areEqual(objects2.size, 1)
      Assert.areEqual(objects2.get(0).getKey, AmazonS3FileSystem.indexFilename)

      // check that the tree contains no files
      def checkTree(parent: SomerDirectory) {
        fs3.children(parent).foreach {
          case d: SomerDirectory => checkTree(d)
          case f: SomerFile => Assert.fail("found file " + f.name)
        }
      }
      checkTree(fs3.root)
    }
    finally fs3.disconnect()

    verifyFileSystem(unitTestName, fs3)
  }

  /**
   * Index believes it is empty but the real FileSystem contains a directory tree.
   */
  @Test def reconcile2() {
    val unitTestName = this.getClass.getSimpleName + ".reconcile2"

    val fs1 = createFileSystem(unitTestName, "", logger).asInstanceOf[AmazonS3FileSystem]
    try {
      val dirA = fs1.createDir(fs1.root, "dirA")
      fs1.createDir(fs1.root, "dirB")
      FileSystemTest.writeFile(fs1, fs1.root, "fileC", "contents")
      FileSystemTest.writeFile(fs1, dirA, "fileAA", "contents")
      fs1.createDir(dirA, "dirAB")

      Assert.notEquals(fs1.service.getKeys(1024).getObjectSummaries.size, 0)
      Assert.notEquals(fs1.children(fs1.root).size, 0)

      // delete all files from index
      fs1.indexSupport.index.children(fs1.indexSupport.index.root).foreach(
        fs1.indexSupport.index.remove)

      Assert.notEquals(fs1.service.getKeys(1024).getObjectSummaries.size, 0)
      Assert.areEqual(fs1.children(fs1.root).size, 0)
    }
    finally fs1.disconnect()

    val fs2 = createFileSystem(unitTestName, "", logger)
    try {
      val reconcileObs = new MockReconcileObserver(logger)
      fs2.reconcile(reconcileObs)
      Assert.areEqual(reconcileObs.errors.size, 0)
    }
    finally fs2.disconnect()

    val fs3 = createFileSystem(unitTestName, "", logger).asInstanceOf[AmazonS3FileSystem]
    try {
      val objects = fs3.service.client.listObjects(fs3.data.bucketName).getObjectSummaries
      Assert.areEqual(objects.size, 1)
      Assert.areEqual(objects.get(0).getKey, AmazonS3FileSystem.indexFilename)
      Assert.areEqual(fs3.children(fs3.root).size, 0)
    }
    finally fs3.disconnect()

    verifyFileSystem(unitTestName, fs3)
  }

  /**
   * Reconcile an empty file system (with no Index)
   */
  @Test def reconcile3() {
    val unitTestName = this.getClass.getSimpleName + ".reconcile3"

    val fs = createFileSystem(unitTestName, "", logger)
    try {
      val reconcileObs = new MockReconcileObserver(logger)
      fs.reconcile(reconcileObs)
      Assert.areEqual(reconcileObs.errors.size, 0)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Reconcile an non-empty file system (with no Index)
   */
  @Test def reconcile4() {
    val unitTestName = this.getClass.getSimpleName + ".reconcile4"

    val fs = createFileSystem(unitTestName, "", logger).asInstanceOf[AmazonS3FileSystem]
    try {
      val inputData = ("testKeyData_" * 10).getBytes
      val buffer = new ByteArrayInputStream(inputData)
      fs.service.writeObject("testKey", inputData.length, buffer)

      val reconcileObs = new MockReconcileObserver(logger)
      fs.reconcile(reconcileObs)
      Assert.areEqual(reconcileObs.errors.size, 0)

      val objects = fs.service.client.listObjects(fs.data.bucketName).getObjectSummaries
      Assert.areEqual(objects.size, 0)
      Assert.areEqual(fs.children(fs.root).size, 0)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * FileSystem created with a file that is not in index. Index is removed manually. Reconcile should succeed
   * and install a new blank index.
   */
  @Test def reconcile5() {
    val unitTestName = this.getClass.getSimpleName + ".reconcile3"

    val fs1 = createFileSystem(unitTestName, "", logger).asInstanceOf[AmazonS3FileSystem]
    try {
      val inputData = ("testKeyData_" * 10).getBytes
      val buffer = new ByteArrayInputStream(inputData)
      fs1.service.writeObject("testKey", inputData.length, buffer)
    }
    finally fs1.disconnect()

    // remove index manually
    val service = new AmazonS3FileSystem.Service(
      new AmazonS3Client(fs1.data.credentials.credentials), fs1.data.bucketName)
    service.removeObject(AmazonS3FileSystem.createIndexKey(fs1.data))

    val fs2 = createFileSystem(unitTestName, "", logger).asInstanceOf[AmazonS3FileSystem]
    try {
      val reconcileObs = new MockReconcileObserver(logger)
      fs2.reconcile(reconcileObs)
      Assert.areEqual(reconcileObs.errors.size, 0)

      val objects = fs2.service.client.listObjects(fs2.data.bucketName).getObjectSummaries
      Assert.areEqual(objects.size, 0)
      Assert.areEqual(fs2.children(fs2.root).size, 0)
    }
    finally fs2.disconnect()

    verifyFileSystem(unitTestName, fs2)
  }

  /**
   * Returns the bucket name used for this unit test. This method is idempotent.
   */
  private def getBucketName(testName: String): String = {
    // we must remove underscores from the bucketName because they are illegal
    val uniqueStamp = FilenameUtils2.uniqueStamp.replaceAll("_", "")
    bucketNamePrefix + "." + uniqueStamp + "." + testName.toLowerCase
  }

  /**
   * Gets properties for this unit test. The properties should be used to store things that should not be checked in
   * (like passwords to Amazon S3).
   */
  private def loadProperties(): Properties = {
    val propFile = new File(FilenameUtils2.concat("config", "AmazonS3FileSystemTest.properties"))
    if (!propFile.exists)
      throw new IllegalStateException("properties file \"" + propFile.getAbsolutePath + "\" does not exist")

    val unitTestProperties = new Properties
    val propInput = new FileReader(propFile)
    unitTestProperties.load(propInput)
    propInput.close()
    unitTestProperties
  }

  /**
   * Returns true if any of the provided values in the list are in the provided string.
   */
  private def containsAny(list: List[String])(x: String): Boolean = list.exists(x.indexOf(_) > -1)

  private def containsAny(x: String)(list: List[String]): Boolean = list.exists(x.indexOf(_) > -1)
}