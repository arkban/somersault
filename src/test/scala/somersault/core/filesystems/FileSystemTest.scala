package somersault.core.filesystems

import java.io.{File, FileNotFoundException, PrintWriter, ByteArrayInputStream}
import logahawk.Logger
import nu.xom.Element
import org.joda.time.DateTime
import org.testng.annotations.Test
import somersault.core.TestUtil
import somersault.core.exceptions.{PathDoesNotExistException, AlreadyExistsException}
import somersault.util.persist.{PersisterSuite, PersisterContext}
import somersault.util.{NullObservableInputStreamListener, NullObservableOutputStreamListener, Hash, Assert}

/**
 * This class contains a suite of tests for {@link FileSystem}s. This can be thought of an acceptance test for
 * FileSystems implementations. Implementations are free to create their own tests in addition to the ones here.
 */
trait FileSystemTest {

  protected val logger = TestUtil.logger

  /**
   * Creates the FileSystem that will be used for all unit tests. The first thing that will be called is connect().
   *
   * @param testName The name of the unit tests. This can be used for making the directory that the unit tests may
   *                 use for temporary data
   * @param nameSuffix Suffix to add the { @link FileSystem}'s name. This can be used to distinguish multiple
   *                   { @link FileSystem}s in the same unit test.
   * @param logger Logger is passed in here mostly because it allows us to force creation of the Logger before anything
   *               else happens in the test.
   */
  protected def createFileSystem(testName: String, nameSuffix: String, logger: Logger): FileSystem

  /**
   * Creates a FileSystemDataPersister to verify persisting a FileSystemData.
   */
  protected def createFileSystemDataPersister(testName: String, logger: Logger): FileSystemDataPersister

  /**
   * This will be called at the end of the FileSystem to allow the implementation to compare the Index (if one is used)
   * against the actual FileSystem contents. This should do nothing for FileSystems that do not use Indexes, and may
   * do nothing if a FileSystem requires an Index.
   *
   * If possible, one recommended implementation is to create an instance of the FileSystem with and without an Index,
   * and then use a ActionBuilder (such as Backup) to assert that no SomerActions are created when comparing
   * the with and without FileSystem implementation.
   */
  protected def verifyFileSystem(testName: String, fs: FileSystem)

  /**
   * Get children from the root.
   * - No files or directories exist.
   */
  @Test def root() {
    val unitTestName = this.getClass.getSimpleName + ".root"
    val fs = createFileSystem(unitTestName, "", logger)

    try {
      val list = fs.children(fs.root)
      Assert.isTrue(list.isEmpty)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Creates two directories at the root. The directories are empty.
   */
  @Test def createDir1() {
    val unitTestName = this.getClass.getSimpleName + ".createDir1"
    val fs = createFileSystem(unitTestName, "", logger)

    try {
      val foo = fs.createDir(fs.root, "foo")
      val list1 = fs.children(fs.root)
      Assert.areEqual(list1.size, 1)
      Assert.isTrue(list1.forall(_.isInstanceOf[SomerDirectory]))
      Assert.isTrue(list1.exists(_.name == foo.name))

      val bar = fs.createDir(fs.root, "bar")
      val list2 = fs.children(fs.root)
      Assert.areEqual(list2.size, 2)
      Assert.isTrue(list2.forall(_.isInstanceOf[SomerDirectory]))
      Assert.isTrue(list2.exists(_.name == foo.name))
      Assert.isTrue(list2.exists(_.name == bar.name))
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Creates nested directories. The nested directories are empty.
   */
  @Test def createDir2() {
    val unitTestName = this.getClass.getSimpleName + ".createDir2"
    val fs = createFileSystem(unitTestName, "", logger)

    try {
      val alpha = fs.createDir(fs.root, "alpha")
      val beta = fs.createDir(fs.root, "beta")

      val alphaFoo = fs.createDir(alpha, "foo")
      val alphaBar = fs.createDir(alpha, "bar")
      val alphaList = fs.children(alpha)
      Assert.areEqual(alphaList.size, 2)
      Assert.isTrue(alphaList.forall(_.isInstanceOf[SomerDirectory]))
      Assert.isTrue(alphaList.exists(_.name == alphaFoo.name))
      Assert.isTrue(alphaList.exists(_.name == alphaBar.name))

      val betaFoo = fs.createDir(beta, "foo")
      val betaBar = fs.createDir(beta, "bar")
      val betaList = fs.children(beta)
      Assert.areEqual(betaList.size, 2)
      Assert.isTrue(betaList.forall(_.isInstanceOf[SomerDirectory]))
      Assert.isTrue(betaList.exists(_.name == betaFoo.name))
      Assert.isTrue(betaList.exists(_.name == betaBar.name))
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Creates same root directory multiple times.
   */
  @Test def createDir3() {
    val unitTestName = this.getClass.getSimpleName + ".createDir3"
    val fs = createFileSystem(unitTestName, "", logger)

    val fooFilename = "foo"
    try {
      val foo = fs.createDir(fs.root, fooFilename)

      fs.createDir(fs.root, foo.name)
      Assert.fail("Expected AlreadyExistsException ")
    }
    catch {
      case ex: AlreadyExistsException =>
        Assert.areEqual(ex.path.size, 1, ex.path.toString())
        Assert.areEqual(ex.path(0), fooFilename, ex.path.toString())
    }
    finally {
      fs.disconnect()
    }

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Creates same nested directory multiple times.
   */
  @Test def createDir4() {
    val unitTestName = this.getClass.getSimpleName + ".createDir4"
    val fs = createFileSystem(unitTestName, "", logger)

    val fooFilename = "foo"
    val barFilename = "bar"
    try {
      val foo = fs.createDir(fs.root, fooFilename)
      val bar = fs.createDir(foo, barFilename)

      fs.createDir(foo, bar.name)
      Assert.fail("Expected AlreadyExistsException ")
    }
    catch {
      case ex: AlreadyExistsException =>
        Assert.areEqual(ex.path.size, 2, ex.path.toString())
        Assert.areEqual(ex.path(0), fooFilename, ex.path.toString())
        Assert.areEqual(ex.path(1), barFilename, ex.path.toString())
    }
    finally {
      fs.disconnect()
    }

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Creates root directory over existing file.
   */
  @Test def createDir5() {
    val unitTestName = this.getClass.getSimpleName + ".createDir5"
    val fs = createFileSystem(unitTestName, "", logger)

    val fooFilename = "foo"
    try {
      FileSystemTest.writeFile(fs, fs.root, fooFilename, "contents" * 100)

      fs.createDir(fs.root, fooFilename)
      Assert.fail("Expected AlreadyExistsException")
    }
    catch {
      case ex: AlreadyExistsException =>
        Assert.areEqual(ex.path.size, 1, ex.path.toString())
        Assert.areEqual(ex.path(0), fooFilename, ex.path.toString())
    }
    finally {
      fs.disconnect()
    }

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Creates nested directory over existing file.
   */
  @Test def createDir6() {
    val unitTestName = this.getClass.getSimpleName + ".createDir6"
    val fs = createFileSystem(unitTestName, "", logger)

    val fooFilename = "foo"
    val barFilename = "bar"
    try {
      val foo = fs.createDir(fs.root, fooFilename)

      FileSystemTest.writeFile(fs, foo, barFilename, "contents" * 100)

      fs.createDir(foo, barFilename)
      Assert.fail("Expected AlreadyExistsException")
    }
    catch {
      case ex: AlreadyExistsException =>
        Assert.areEqual(ex.path.size, 2, ex.path.toString())
        Assert.areEqual(ex.path(0), fooFilename, ex.path.toString())
        Assert.areEqual(ex.path(1), barFilename, ex.path.toString())
    }
    finally {
      fs.disconnect()
    }

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Remove two directories at the root. The directories are empty.
   */
  @Test def remove1() {
    val unitTestName = this.getClass.getSimpleName + ".remove1"
    val fs = createFileSystem(unitTestName, "", logger)

    try {
      val foo = fs.createDir(fs.root, "foo")
      val bar = fs.createDir(fs.root, "bar")

      fs.remove(foo)
      val list1 = fs.children(fs.root)
      Assert.areEqual(list1.size, 1)
      Assert.isTrue(list1.forall(_.isInstanceOf[SomerDirectory]))
      Assert.isTrue(list1.exists(_.name == bar.name))

      fs.remove(bar)
      val list2 = fs.children(fs.root)
      Assert.isTrue(list2.isEmpty)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Remove nested directories. The nested directories are empty.
   */
  @Test def remove2() {
    val unitTestName = this.getClass.getSimpleName + ".remove2"
    val fs = createFileSystem(unitTestName, "", logger)

    try {
      val alpha = fs.createDir(fs.root, "alpha")
      val beta = fs.createDir(fs.root, "beta")
      val alphaFoo = fs.createDir(alpha, "foo")
      val alphaBar = fs.createDir(alpha, "bar")
      val betaFoo = fs.createDir(beta, "foo")
      val betaBar = fs.createDir(beta, "bar")

      // remove individually
      fs.remove(alphaFoo)
      val alphaList1 = fs.children(alpha)
      Assert.areEqual(alphaList1.size, 1)
      Assert.isTrue(alphaList1.forall(_.isInstanceOf[SomerDirectory]))
      Assert.isTrue(alphaList1.exists(_.name == alphaBar.name))
      fs.remove(alphaBar)
      val alphaList2 = fs.children(alpha)
      Assert.isTrue(alphaList2.isEmpty)

      // remove together
      fs.remove(betaFoo)
      fs.remove(betaBar)
      val betaList = fs.children(beta)
      Assert.isTrue(betaList.isEmpty)

      // remove root
      fs.remove(alpha)
      fs.remove(beta)
      val rootList = fs.children(fs.root)
      Assert.isTrue(rootList.isEmpty)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Remove non-empty root directories.
   */
  @Test def remove3() {
    val unitTestName = this.getClass.getSimpleName + ".remove3"
    val fs = createFileSystem(unitTestName, "", logger)

    try {
      val alpha = fs.createDir(fs.root, "alpha")
      val beta = fs.createDir(fs.root, "beta")
      fs.createDir(alpha, "foo")
      fs.createDir(alpha, "bar")
      fs.createDir(beta, "foo")
      fs.createDir(beta, "bar")

      fs.remove(alpha)
      val rootList1 = fs.children(fs.root)
      Assert.areEqual(rootList1.size, 1)
      Assert.isTrue(rootList1.forall(_.isInstanceOf[SomerDirectory]))
      Assert.isTrue(rootList1.exists(_.name == beta.name))

      fs.remove(beta)
      val rootList2 = fs.children(fs.root)
      Assert.isTrue(rootList2.isEmpty)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Remove nested directories. The nested directories are empty.
   */
  @Test def remove4() {
    val unitTestName = this.getClass.getSimpleName + ".remove4"
    val fs = createFileSystem(unitTestName, "", logger)

    try {
      val alpha = fs.createDir(fs.root, "alpha")
      val beta = fs.createDir(fs.root, "beta")
      val alphaFoo = fs.createDir(alpha, "foo")
      fs.createDir(alphaFoo, "bar")
      val betaFoo = fs.createDir(beta, "foo")
      fs.createDir(betaFoo, "bar")

      fs.remove(alphaFoo)
      val alphaList = fs.children(alpha)
      Assert.equals(alphaList.isEmpty)

      fs.remove(betaFoo)
      val betaList = fs.children(beta)
      Assert.equals(betaList.isEmpty)

      // remove root
      fs.remove(alpha)
      fs.remove(beta)
      val rootList = fs.children(fs.root)
      Assert.equals(rootList.isEmpty)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Removes a file at the root.
   */
  @Test def remove5() {
    val unitTestName = this.getClass.getSimpleName + ".remove5"
    val fs = createFileSystem(unitTestName, "", logger)

    try {
      val fooContentsOrig = "somersault unit testing\nmultiple lines of data\nwish us luck!\n" * 100
      val foo = FileSystemTest.writeFile(fs, fs.root, "foo", fooContentsOrig)

      fs.remove(foo)
      val rootList2 = fs.children(fs.root)
      Assert.equals(rootList2.isEmpty)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Removes a nested file.
   */
  @Test def remove6() {
    val unitTestName = this.getClass.getSimpleName + ".remove6"
    val fs = createFileSystem(unitTestName, "", logger)

    try {
      val barContentsOrig = "somersault unit testing\nmultiple lines of data\nwish us luck!\n" * 100
      val foo = fs.createDir(fs.root, "foo")
      val bar = FileSystemTest.writeFile(fs, foo, "bar", barContentsOrig)

      fs.remove(bar)
      val rootList2 = fs.children(foo)
      Assert.equals(rootList2.isEmpty)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Removes a root directory containing a file.
   */
  @Test def remove7() {
    val unitTestName = this.getClass.getSimpleName + ".remove7"
    val fs = createFileSystem(unitTestName, "", logger)

    try {
      val barContentsOrig = "somersault unit testing\nmultiple lines of data\nwish us luck!\n" * 100
      val foo = fs.createDir(fs.root, "foo")
      FileSystemTest.writeFile(fs, foo, "bar", barContentsOrig)

      fs.remove(foo)
      val rootList2 = fs.children(fs.root)
      Assert.equals(rootList2.isEmpty)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Remove a directory at the root, and get an exception if trying to get its children after removal.
   */
  @Test def remove8() {
    val unitTestName = this.getClass.getSimpleName + ".remove8"
    val fs = createFileSystem(unitTestName, "", logger)

    val fooFilename = "foo"
    val barFilename = "bar"
    try {
      val foo = fs.createDir(fs.root, fooFilename)
      val bar = fs.createDir(foo, barFilename)

      fs.remove(foo)
      val list1 = fs.children(fs.root)
      Assert.equals(list1.isEmpty)

      try {
        fs.children(foo)
        Assert.fail("Expected PathDoesNotExistException")
      }
      catch {
        case ex: PathDoesNotExistException =>
          // error should contain the name, but we can't expect it to know what its path previously was
          Assert.isTrue(ex.path.contains(fooFilename), ex.path.toString())
      }

      try {
        fs.children(bar)
        Assert.fail("Expected PathDoesNotExistException")
      }
      catch {
        case ex: PathDoesNotExistException =>
          // error should contain the name, but we can't expect it to know what its path previously was
          Assert.isTrue(ex.path.contains(barFilename), ex.path.toString())
      }
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Create root directory with child files and directories. Remove the root directory. Re-add the same directory name.
   * - Check that the re-added directory is empty.
   */
  @Test def remove9() {
    val unitTestName = this.getClass.getSimpleName + ".remove9"
    val fs = createFileSystem(unitTestName, "", logger)

    try {
      val alpha = fs.createDir(fs.root, "alpha")
      fs.createDir(alpha, "foo")
      FileSystemTest.writeFile(fs, alpha, "bar", "contents" * 100)

      Assert.areEqual(fs.children(alpha).size, 2)

      fs.remove(alpha)

      val alpha2 = fs.createDir(fs.root, alpha.name)

      Assert.equals(fs.children(alpha2).isEmpty)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Creates a new root file and verifies its contents.
   */
  @Test def transfer1() {
    val unitTestName = this.getClass.getSimpleName + ".transfer1"
    val fs = createFileSystem(unitTestName, "", logger)

    try {
      val fooContentsOrig = "somersault unit testing\nmultiple lines of data\nwish us luck!\n"
      val fooFile = FileSystemTest.writeFile(fs, fs.root, "foo", fooContentsOrig)

      FileSystemTest.compareContents(fs, fooFile, fooContentsOrig)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Creates a new non-root file and verifies its contents.
   */
  @Test def transfer2() {
    val unitTestName = this.getClass.getSimpleName + ".transfer2"
    val fs = createFileSystem(unitTestName, "", logger)

    try {
      val barContentsOrig = "somersault unit testing\nmultiple lines of data\nwish us luck!\n" * 100
      val foo = fs.createDir(fs.root, "foo")
      val barFile = FileSystemTest.writeFile(fs, foo, "bar", barContentsOrig)

      FileSystemTest.compareContents(fs, barFile, barContentsOrig)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Open for reading a root file that was previously removed.
   */
  @Test def transfer3() {
    val unitTestName = this.getClass.getSimpleName + ".transfer3"
    val fs = createFileSystem(unitTestName, "", logger)

    val fooFilename = "foo"
    try {
      val fooFile = FileSystemTest.writeFile(fs, fs.root, fooFilename, "contents" * 100)
      fs.remove(fooFile)

      fs.read(fooFile)
      Assert.fail("Expected PathDoesNotExistException")
    }
    catch {
      case ex: PathDoesNotExistException =>
        // error should contain the name, but we can't expect it to know what its path previously was
        Assert.isTrue(ex.path.contains(fooFilename), ex.path.toString())
    }
    finally {
      fs.disconnect()
    }

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Open for reading a non-root file that was previously removed.
   */
  @Test def transfer4() {
    val unitTestName = this.getClass.getSimpleName + ".transfer4"
    val fs = createFileSystem(unitTestName, "", logger)

    val barFilename = "bar"
    try {
      val foo = fs.createDir(fs.root, "foo")
      val bar = FileSystemTest.writeFile(fs, foo, barFilename, "contents" * 100)
      fs.remove(bar)

      fs.read(bar)
      Assert.fail("Expected PathDoesNotExistException")
    }
    catch {
      case ex: PathDoesNotExistException =>
        // error should contain the name, but we can't expect it to know what its path previously was
        Assert.isTrue(ex.path.contains(barFilename))
    }
    finally {
      fs.disconnect()
    }

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Open a root directory for writing.
   */
  @Test def transfer5() {
    val unitTestName = this.getClass.getSimpleName + ".transfer5"
    val fs = createFileSystem(unitTestName, "", logger)

    val fooFilename = "foo"
    try {
      val fooDir = fs.createDir(fs.root, fooFilename)

      FileSystemTest.writeFile(fs, fs.root, fooDir.name, "contents" * 100)
      Assert.fail("Expected AlreadyExistsException")
    }
    catch {
      case ex: AlreadyExistsException =>
        Assert.areEqual(ex.path.size, 1)
        Assert.areEqual(ex.path(0), fooFilename)
    }
    finally {
      fs.disconnect()
    }

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Open a nested directory for writing.
   */
  @Test def transfer6() {
    val unitTestName = this.getClass.getSimpleName + ".transfer6"
    val fs = createFileSystem(unitTestName, "", logger)

    val fooFilename = "foo"
    val barFilename = "bar"
    try {
      val foo = fs.createDir(fs.root, fooFilename)
      val bar = fs.createDir(foo, barFilename)

      FileSystemTest.writeFile(fs, foo, bar.name, "contents" * 100)
      Assert.fail("Expected AlreadyExistsException")
    }
    catch {
      case ex: AlreadyExistsException =>
        Assert.areEqual(ex.path.size, 2, ex.path.toString())
        Assert.areEqual(ex.path(0), fooFilename, ex.path.toString())
        Assert.areEqual(ex.path(1), barFilename, ex.path.toString())
    }
    finally {
      fs.disconnect()
    }

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Verifies overwriting a root file completely removes any existing content
   */
  @Test def transfer7() {
    val unitTestName = this.getClass.getSimpleName + ".transfer7"
    val fs = createFileSystem(unitTestName, "", logger)

    try {
      val fooContentsOrig1 = "somersault unit testing\nmultiple lines of data\nwish us luck!\n" * 100
      val fooFile1 = FileSystemTest.writeFile(fs, fs.root, "foo", fooContentsOrig1)
      FileSystemTest.compareContents(fs, fooFile1, fooContentsOrig1)

      // less data than the original to verify that the file was truncated
      val fooContentsOrig2 = fooContentsOrig1.substring(fooContentsOrig1.length / 2)
      val fooFile2 = FileSystemTest.writeFile(fs, fs.root, fooFile1.name, fooContentsOrig2)
      FileSystemTest.compareContents(fs, fooFile2, fooContentsOrig2)
    }
    finally {
      fs.disconnect()
    }

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Verifies overwriting a non-root file completely removes any existing content
   */
  @Test def transfer8() {
    val unitTestName = this.getClass.getSimpleName + ".transfer8"
    val fs = createFileSystem(unitTestName, "", logger)

    try {
      val barContentsOrig1 = "somersault unit testing\nmultiple lines of data\nwish us luck!\n" * 100
      val foo = fs.createDir(fs.root, "foo")

      val barFile1 = FileSystemTest.writeFile(fs, foo, "bar", barContentsOrig1)
      FileSystemTest.compareContents(fs, barFile1, barContentsOrig1)

      // less data than the original to verify that the file was truncated
      val barContentsOrig2 = barContentsOrig1.substring(barContentsOrig1.length / 2)
      val barFile2 = FileSystemTest.writeFile(fs, foo, barFile1.name, barContentsOrig2)
      FileSystemTest.compareContents(fs, barFile2, barContentsOrig2)
    }
    finally {
      fs.disconnect()
    }

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Creates a new non-root file with very long file names, intending to check that long file names in nested
   * directories do not cause adverse problems. The entire path is long, but not insanely long, as most FileSystems
   * have reasonable limits, and most file systems don't have pointless-ly long paths.
   *
   * Note The entire path length is less than 255 characters because there are some FileSystems (such as Windows,
   * and AmazonS3FileSystem) that have a 255 character limit on the entire path.
   */
  @Test def transfer9() {
    val unitTestName = this.getClass.getSimpleName + ".transfer9"
    val fs = createFileSystem(unitTestName, "", logger)

    val path = "foo" * 10 :: "bar" * 10 :: "somer" * 10 :: "sault" * 10 :: Nil
    val fileName = "cat" * 10

    try {
      val curr = path.foldLeft(fs.root) {case (c, p) => fs.createDir(c, p)}
      val fileContentsOrig = "somersault unit testing\nmultiple lines of data\nwish us luck!\n" * 100
      val file = FileSystemTest.writeFile(fs, curr, fileName, fileContentsOrig)

      FileSystemTest.compareContents(fs, file, fileContentsOrig)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }

  /**
   * Test connecting, disconnect()ing, and re-connecting to an empty file system works as expected.
   */
  @Test def connect1() {
    val unitTestName = this.getClass.getSimpleName + ".connect1"
    val fs1 = createFileSystem(unitTestName, "", logger)

    try {
      val list = fs1.children(fs1.root)
      Assert.isTrue(list.isEmpty)
    }
    finally fs1.disconnect()
    verifyFileSystem(unitTestName, fs1)

    val fs2 = createFileSystem(unitTestName, "", logger)
    try {
      val list = fs2.children(fs2.root)
      Assert.isTrue(list.isEmpty)
    }
    finally fs2.disconnect()
    verifyFileSystem(unitTestName, fs2)
  }

  /**
   * Test connecting, disconnect()ing, and re-connecting to a full file system works as expected.
   */
  @Test def connect2() {
    val unitTestName = this.getClass.getSimpleName + ".connect2"
    val fs1 = createFileSystem(unitTestName, "", logger)

    val dirs = List(
      "foo" :: "cat" :: Nil, "foo" :: "dog" :: Nil, "bar" :: "cat" :: Nil, "bar" :: "dog" :: Nil)
    val files = List(
      "file" :: Nil,
      "foo" :: "file" :: Nil,
      "foo" :: "dog" :: "file" :: Nil,
      "bar" :: "file" :: Nil,
      "bar" :: "dog" :: "file" :: Nil)
    val fileContentPrefix = "contents for file "

    try {
      // create dirs
      dirs.foreach(
        dirEntry => {
          var parent = fs1.root
          dirEntry.foreach(
            d => {
              fs1.children(parent).find(_.name == d) match {
                case Some(foundDir: SomerDirectory) => parent = foundDir
                case None => parent = fs1.createDir(parent, d)
                case _ => Assert.fail("found other at " + d + " instead of directory")
              }
            })
        })

      // create files (assumes that the dirs exist and are correct)
      files.foreach(
        fileEntry => {
          var parent = fs1.root
          fileEntry.foreach(
            f => {
              fs1.children(parent).find(_.name == f) match {
                case Some(foundDir: SomerDirectory) => parent = foundDir
                case None => FileSystemTest.writeFile(fs1, parent, f, fileContentPrefix + f)
                case _ => Assert.fail("found other at " + f + " instead of directory")
              }
            })
        })
    }
    finally fs1.disconnect()
    verifyFileSystem(unitTestName, fs1)

    val fs2 = createFileSystem(unitTestName, "", logger)
    try {
      // verify dirs
      dirs.foreach(
        dirEntry => {
          var parent = fs2.root
          dirEntry.foreach(
            d => {
              val results = fs2.children(parent)
              results.find(_.name == d) match {
                case Some(foundDir: SomerDirectory) => parent = foundDir
                case None => Assert.fail("cannot find directory " + d)
                case _ => Assert.fail("found other at " + d + " instead of directory")
              }
            })
        })

      // verify files (assumes that the dirs exist and are correct)
      files.foreach(
        fileEntry => {
          var parent = fs2.root
          fileEntry.foreach(
            f => {
              fs2.children(parent).find(_.name == f) match {
                case Some(foundDir: SomerDirectory) =>
                  Assert.notEquals(fileEntry.last, f) // dir is not last
                  parent = foundDir
                case Some(foundFile: SomerFile) =>
                  Assert.isTrue(fileEntry.last == f) // file is last
                  FileSystemTest.compareContents(fs2, foundFile, fileContentPrefix + f)
                case None => FileSystemTest.writeFile(fs2, parent, f, "contents for file " + f)
                case _ => Assert.fail("found other at " + f + " instead of directory")
              }
            })
        })
    }
    finally fs1.disconnect()
    verifyFileSystem(unitTestName, fs2)
  }

  @Test def persist() {
    if (this.isInstanceOf[FakeFileSystemTest]) return

    val unitTestName = this.getClass.getSimpleName + ".persist"
    val context = new PersisterContext(PersisterSuite.create(), new File("temp"))
    val fs = createFileSystem(unitTestName, "", logger)
    val pw = createFileSystemDataPersister(unitTestName, logger)
    val element = new Element("fs")
    pw.write(context, fs.data, element, dynamic = true)

    Assert.isTrue(pw.canRead(context, element))
    val newData: FileSystemData = pw.read(context, element)
    Assert.areEqual(fs.data.name, newData.name)
    Assert.areEqual(fs.data.hashInfo, newData.hashInfo)
  }

  /**
   * Two FileSystems try to read the same physical location. Both are allowed.
   */
  @Test
  def lock1() {
    val unitTestName = this.getClass.getSimpleName + ".lock1"
    val fs1 = createFileSystem(unitTestName, "_1", logger)
    val fs2 = createFileSystem(unitTestName, "_2", logger)

    try {
      Assert.isTrue(fs1.lock(LockingFileSystemMode.Read))
      Assert.isTrue(fs2.lock(LockingFileSystemMode.Read))
    }
    finally {
      fs1.unlock()
      fs2.unlock()

      fs1.disconnect()
      fs2.disconnect()
    }
  }

  /**
   * Two FileSystems try to read and write the same physical location. The read occurs first, and only it is allowed.
   */
  @Test
  def lock2() {
    val unitTestName = this.getClass.getSimpleName + ".lock2"
    val fsRead = createFileSystem(unitTestName, "_Read", logger)
    val fsWrite = createFileSystem(unitTestName, "_Write", logger)

    try {
      Assert.isTrue(fsRead.lock(LockingFileSystemMode.Read))
      Assert.isFalse(fsWrite.lock(LockingFileSystemMode.Write))
    }
    finally {
      fsRead.unlock()
      fsRead.disconnect()
    }
  }

  /**
   * Two FileSystems try to read and write the same physical location. The write occurs first, and only it is allowed.
   */
  @Test
  def lock3() {
    val unitTestName = this.getClass.getSimpleName + ".lock3"
    val fsRead = createFileSystem(unitTestName, "_Read", logger)
    val fsWrite = createFileSystem(unitTestName, "_Write", logger)

    try {
      Assert.isTrue(fsWrite.lock(LockingFileSystemMode.Write))
      Assert.isFalse(fsRead.lock(LockingFileSystemMode.Read))
    }
    finally {
      fsWrite.unlock()
      fsWrite.disconnect()
    }
  }

  /**
   * Two FileSystems try to write the same physical location. The one that locks first is allowed.
   */
  @Test
  def lock4() {
    val unitTestName = this.getClass.getSimpleName + ".lock4"
    val fs1 = createFileSystem(unitTestName, "_1", logger)
    val fs2 = createFileSystem(unitTestName, "_2", logger)

    try {
      Assert.isTrue(fs1.lock(LockingFileSystemMode.Write))
      Assert.isFalse(fs2.lock(LockingFileSystemMode.Write))
    }
    finally {
      fs1.unlock()
      fs1.disconnect()
    }
  }

  /**
   * The first FileSystem has a read lock, the second wants a write lock. The first locker is stale, and must be
   * forcibly removed. The the second FileSystem can acquire the lock.
   */
  @Test
  def removeAllLocks1() {
    val unitTestName = this.getClass.getSimpleName + ".removeAllLocks1"
    val fs1 = createFileSystem(unitTestName, "", logger)
    val fs2 = createFileSystem(unitTestName, "", logger)

    try {
      Assert.isTrue(fs1.lock(LockingFileSystemMode.Read))
      Assert.isFalse(fs2.lock(LockingFileSystemMode.Write))

      fs2.removeAllLocks()

      Assert.isTrue(fs2.lock(LockingFileSystemMode.Write))
    }
    finally {
      fs1.unlock()
      fs2.unlock()

      fs1.disconnect()
      fs2.disconnect()
    }
  }

  /**
   * The first FileSystem has a write lock, the second wants a write lock. The first locker is stale, and must be
   * forcibly removed. The the second FileSystem can acquire the lock.
   */
  @Test
  def removeAllLocks2() {
    val unitTestName = this.getClass.getSimpleName + ".lock1"
    val fs1 = createFileSystem(unitTestName, "_1", logger)
    val fs2 = createFileSystem(unitTestName, "_2", logger)

    try {
      Assert.isTrue(fs1.lock(LockingFileSystemMode.Write))
      Assert.isFalse(fs2.lock(LockingFileSystemMode.Write))

      fs2.removeAllLocks()

      Assert.isTrue(fs2.lock(LockingFileSystemMode.Write))
    }
    finally {
      fs1.unlock()
      fs2.unlock()

      fs1.disconnect()
      fs2.disconnect()
    }
  }

  /**
   * Creates nested directories and verify existence with both children() and find()
   *
   * This was originally based on createDir2.
   */
  @Test def childrenVsFind1() {
    val unitTestName = this.getClass.getSimpleName + ".childrenVsFind1"
    val fs = createFileSystem(unitTestName, "", logger)

    try {
      val alpha = fs.createDir(fs.root, "alpha")
      val beta = fs.createDir(fs.root, "beta")
      val alphaFoo = fs.createDir(alpha, "foo")
      val alphaBar = fs.createDir(alpha, "bar")
      val betaFoo = fs.createDir(beta, "foo")
      val betaBar = fs.createDir(beta, "bar")

      // check with children()

      val alphaList = fs.children(alpha)
      Assert.areEqual(alphaList.size, 2)
      Assert.isTrue(alphaList.forall(_.isInstanceOf[SomerDirectory]))
      Assert.isTrue(alphaList.exists(_.name == alphaFoo.name))
      Assert.isTrue(alphaList.exists(_.name == alphaBar.name))

      val betaList = fs.children(beta)
      Assert.areEqual(betaList.size, 2)
      Assert.isTrue(betaList.forall(_.isInstanceOf[SomerDirectory]))
      Assert.isTrue(betaList.exists(_.name == betaFoo.name))
      Assert.isTrue(betaList.exists(_.name == betaBar.name))

      // check with find()

      fs.find(fs.root, "alpha") match {
        case Some(a: SomerDirectory) =>
          Assert.isTrue(fs.find(alpha, "foo").isDefined)
          Assert.isTrue(fs.find(alpha, "bar").isDefined)

          // test with directory found using find()
          Assert.isTrue(fs.find(a, "foo").isDefined)
          Assert.isTrue(fs.find(a, "bar").isDefined)

        case None => Assert.fail("failed to find 'alpha'")
        case x => Assert.fail("found something other than 'alpha':" + x)
      }
      fs.find(fs.root, "beta") match {
        case Some(b: SomerDirectory) =>
          Assert.isTrue(fs.find(alpha, "foo").isDefined)
          Assert.isTrue(fs.find(alpha, "bar").isDefined)

          // test with directory found using find()
          Assert.isTrue(fs.find(b, "foo").isDefined)
          Assert.isTrue(fs.find(b, "bar").isDefined)

        case None => Assert.fail("failed to find 'beta'")
        case x => Assert.fail("found something other than 'beta':" + x)
      }
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }
}

object FileSystemTest {

  /**
   * Writes the file to the FileSystem, finds it in the FileSystem, and returns it. This will throw if writing or
   * searching fails.
   */
  def writeFile(fs: FileSystem, parent: SomerDirectory, fileName: String, contents: String): SomerFile = {
    // mock file to meet FileSystem.write()'s argument needs while keeping this method signature easy to use
    val sourceFile = new SomerFile {
      val name = fileName

      val size = contents.length.toLong

      val modified = new DateTime

      val attributes = new FileAttributes(false, false, false)

      val hash = Hash.create(fs.data.hashInfo, new ByteArrayInputStream(contents.getBytes))
    }

    val compResult = fs.data.compressionProvider.encode(Nil, sourceFile)
    val encResult = fs.data.encryptionProvider.encode(Nil, sourceFile)
    val output = fs.write(parent, sourceFile, compResult, encResult)
    val writer = new PrintWriter(output)
    writer.print(contents)
    writer.close()

    val list = fs.children(parent)
    list.find(_.name == fileName) match {
      case Some(f: SomerFile) => f
      case Some(d: SomerDirectory) => throw new Exception("directory found where file expected: " + d.name)
      case None => throw new FileNotFoundException(fileName)
      case x => throw new Exception("Unexpected response: " + x)
    }
  }

  /**
   * Reads file's contents and compares it to the string contents that was used to create the file via FileSystemTest.writeFile()
   */
  def compareContents(fs: FileSystem, file: SomerFile, origContents: String) {
    val (hashFromFile, contentFromFile) = TestUtil.readRawContents(fs.data.hashInfo, fs.read(file))
    val origContentBytes = origContents.getBytes
    val hashFromOrigContents = Hash.create(fs.data.hashInfo, new ByteArrayInputStream(origContentBytes))

    Assert.areEqual(origContentBytes, contentFromFile)
    Assert.areEqual(file.size, origContents.length)

    Assert.areEqual(hashFromOrigContents, hashFromFile)
    Assert.areEqual(file.hash, hashFromOrigContents)
    Assert.areEqual(file.hash, hashFromFile)
  }

  def createRuntimeData(tempPath: File): RuntimeFileSystemData = {
    val observer = new FileSystemObserverContainer(
      TestUtil.logger, new NullObservableInputStreamListener, new NullObservableOutputStreamListener)
    new RuntimeFileSystemData(observer, tempPath)
  }
}
