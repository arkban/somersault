package somersault.core.filesystems

import org.testng._
import org.testng.annotations._
import org.joda.time.DateTime

class MockSomerArtifactTest
{
  @Test def nameFiles() =
  {
    val f1 = new MockSomerFile( "/file", new DateTime )
    Assert.assertEquals( f1.name, "file" )
    val p1 = f1.path.toList
    Assert.assertEquals( p1.length, 1 )
    Assert.assertEquals( p1( 0 ), "file" )

    val f2 = new MockSomerFile( "/foo/file", new DateTime )
    Assert.assertEquals( f2.name, "file" )
    val p2 = f2.path.toList
    Assert.assertEquals( p2.length, 2 )
    Assert.assertEquals( p2( 0 ), "foo" )
    Assert.assertEquals( p2( 1 ), "file" )

    val f3 = new MockSomerFile( "/foo/bar/file", new DateTime )
    Assert.assertEquals( f3.name, "file" )
    val p3 = f3.path.toList
    Assert.assertEquals( p3.length, 3 )
    Assert.assertEquals( p3( 0 ), "foo" )
    Assert.assertEquals( p3( 1 ), "bar" )
    Assert.assertEquals( p3( 2 ), "file" )
  }

  @Test def nameDirs() =
  {
    val d1 = new MockSomerDirectory( "/dir" )
    Assert.assertEquals( d1.name, "dir" )
    val p1 = d1.path.toList
    Assert.assertEquals( p1.length, 1 )
    Assert.assertEquals( p1( 0 ), "dir" )

    val d2 = new MockSomerDirectory( "/foo/dir" )
    Assert.assertEquals( d2.name, "dir" )
    val p2 = d2.path.toList
    Assert.assertEquals( p2.length, 2 )
    Assert.assertEquals( p2( 0 ), "foo" )
    Assert.assertEquals( p2( 1 ), "dir" )

    val d3 = new MockSomerDirectory( "/foo/bar/dir" )
    Assert.assertEquals( d3.name, "dir" )
    val p3 = d3.path.toList
    Assert.assertEquals( p3.length, 3 )
    Assert.assertEquals( p3( 0 ), "foo" )
    Assert.assertEquals( p3( 1 ), "bar" )
    Assert.assertEquals( p3( 2 ), "dir" )
  }

  @Test def name2() =
  {
    val fs = FakeFileSystem.connect( "name2" )
    Assert.assertEquals( fs.root.name, "/" )
  }

  @Test def split()
  {
    val (p1, n1) = MockSomerArtifact.split( "/foo" )
    Assert.assertEquals( p1, "/" )
    Assert.assertEquals( n1, "foo" )

    val (p2, n2) = MockSomerArtifact.split( "/foo/bar" )
    Assert.assertEquals( p2, "/foo" )
    Assert.assertEquals( n2, "bar" )

    val (p3, n3) = MockSomerArtifact.split( "/foo/bar/munch" )
    Assert.assertEquals( p3, "/foo/bar" )
    Assert.assertEquals( n3, "munch" )
  }

  @Test def splitPath()
  {
    val p1 = MockSomerArtifact.splitPath( "/foo" )
    Assert.assertEquals( p1.size, 1 )
    Assert.assertEquals( p1( 0 ), "foo" )

    val p2 = MockSomerArtifact.splitPath( "/foo/bar" )
    Assert.assertEquals( p2.size, 2 )
    Assert.assertEquals( p2( 0 ), "foo" )
    Assert.assertEquals( p2( 1 ), "bar" )

    val p3 = MockSomerArtifact.splitPath( "/foo/bar/munch" )
    Assert.assertEquals( p3.size, 3 )
    Assert.assertEquals( p3( 0 ), "foo" )
    Assert.assertEquals( p3( 1 ), "bar" )
    Assert.assertEquals( p3( 2 ), "munch" )
  }
}