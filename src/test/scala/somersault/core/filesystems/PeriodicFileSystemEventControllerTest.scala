package somersault.core.filesystems

import org.testng.annotations.Test
import somersault.util.Assert

class PeriodicFileSystemEventControllerTest {

  @Test def onlyActionCountThresholdExceeded() {
    val f = new MockSomerFile("/root", data = "0123456789")
    val periodic = new MockPeriodicFileSystemEventController(
      new PeriodicFileSystemEventControllerData(10, 1000000, 1, 3, 0, 10000000))
    periodic.update(f)
    Assert.areEqual(0, periodic.fireCount)
    periodic.update(f)
    Assert.areEqual(0, periodic.fireCount)
    periodic.update(f)
    Assert.areEqual(1, periodic.fireCount)
    periodic.update(f)
    Assert.areEqual(1, periodic.fireCount)
  }

  @Test def onlyByteThresholdExceeded() {
    val f = new MockSomerFile("/root", data = "0")
    val periodic = new MockPeriodicFileSystemEventController(
      new PeriodicFileSystemEventControllerData(0, 1, 3, 100, 0, 10000000))
    periodic.update(f)
    Assert.areEqual(0, periodic.fireCount)
    periodic.update(f)
    Assert.areEqual(0, periodic.fireCount)
    periodic.update(f)
    Assert.areEqual(1, periodic.fireCount)
    periodic.update(f)
    Assert.areEqual(1, periodic.fireCount)
  }

  @Test def onlyTimeThresholdExceeded() {
    val f = new MockSomerFile("/root", data = "0123456789")
    val periodic = new MockPeriodicFileSystemEventController(
      new PeriodicFileSystemEventControllerData(10, 11000000, 3, 100, 10, 50))
    periodic.update(f)
    Assert.areEqual(0, periodic.fireCount)
    periodic.update(f)
    Assert.areEqual(0, periodic.fireCount)
    Thread.sleep(100)
    periodic.update(f)
    Assert.areEqual(1, periodic.fireCount)
    periodic.update(f)
    Assert.areEqual(1, periodic.fireCount)
  }

  @Test def allThresholdsExceeded() {
    val f = new MockSomerFile("/root", data = "0123456789")
    val is = new MockPeriodicFileSystemEventController(
      new PeriodicFileSystemEventControllerData(0, 1, 0, 1, 0, 1))
    is.update(f)
    Assert.areEqual(1, is.fireCount)
    is.update(f)
    Assert.areEqual(2, is.fireCount)
    is.update(f)
    Assert.areEqual(3, is.fireCount)
  }

  @Test def reset() {
    val f = new MockSomerFile("/root", data = "0123456789")
    val periodic = new MockPeriodicFileSystemEventController(
      new PeriodicFileSystemEventControllerData(5, 15, 1, 2, 0, 10))

    periodic.update(f)
    Assert.areEqual(0, periodic.fireCount)
    periodic.update(f)
    Assert.areEqual(1, periodic.fireCount)

    periodic.reset()

    periodic.update(f)
    Assert.areEqual(1, periodic.fireCount)
    periodic.update(f)
    Assert.areEqual(2, periodic.fireCount)
  }

  @Test def flushOnDisconnect() {
    val f = new MockSomerFile("/root", data = "0123456789")
    val periodic = new MockPeriodicFileSystemEventController(
      new PeriodicFileSystemEventControllerData(100000, 1000000, 10, 100, 10000000, 10000000))

    periodic.update(f)
    Assert.areEqual(0, periodic.fireCount)

    periodic.fire()
    Assert.areEqual(1, periodic.fireCount)
  }

  private class MockPeriodicFileSystemEventController(val data: PeriodicFileSystemEventControllerData)
    extends PeriodicFileSystemEventController {

    var fireCount = 0

    def fire() { fireCount += 1 }
  }

}

class PeriodicFileSystemEventControllerDataTest {

  /**
   * Test valid ranges
   */
  @Test def inputs() {

    // sanity check -- no errors because both valid ranges
    new PeriodicFileSystemEventControllerData(0, 1, 0, 1, 0, 1)

    val invalidValues =
    // non-zero ranges
      (0, 1, 0, 0, 0, 1) ::(0, 0, 0, 1, 0, 1) ::(0, 1, 0, 1, 0, 0) ::
        // negative values
        (-1, 1, 0, 1, 0, 1) ::(0, -1, 0, 1, 0, 1) ::
        (0, 1, -1, 1, 0, 1) ::(0, 1, 0, -1, 0, 1) ::
        (0, 1, 0, 1, -1, 1) ::(0, 1, 0, 0, 0, -1) ::
        // negative ranges
        (3, 2, 0, 1, 0, 1) ::(0, 1, 3, 2, 0, 1) ::(0, 1, 0, 1, 3, 2) ::
        Nil

    invalidValues.foreach {
      case (bytesMin, bytesMax, actionsMin, actionsMax, msMin, msMax) =>
        try {
          new PeriodicFileSystemEventControllerData(bytesMin, bytesMax, actionsMin, actionsMax, msMin, msMax)
          Assert.fail(
            "should throw IllegalArgumentException: " +
              (bytesMin, bytesMax, actionsMin, actionsMax, msMin, msMax).toString)
        }
        catch {
          case ex: IllegalArgumentException =>
        }
    }
  }
}