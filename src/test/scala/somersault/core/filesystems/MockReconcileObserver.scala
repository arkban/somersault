package somersault.core.filesystems

import logahawk.{Logger, Severity}
import scala.collection.mutable

class MockReconcileObserver(logger: Logger) extends ReconcileObserver {

  val errors = new mutable.ListBuffer[Exception]

  def start() { logger.info("Starting reconcilation...") }

  def end() { logger.info("End reconcilation") }

  def removeFromFileSystem(path: String) { logger.info("Remove from FileSystem: " + path) }

  def removeFromIndex(path: String) { logger.info("Remove from Index: " + path) }

  def error(severity: Severity, ex: Exception) { errors += ex }
}