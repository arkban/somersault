package somersault.core.filesystems

import index.{H2IndexData, H2Index}
import java.io.File
import org.apache.commons.io.{FileUtils, FilenameUtils}
import org.testng.annotations.Test
import somersault.core.TestUtil
import somersault.core.encode.{NullEncryptionProvider, NullCompressionProvider}
import somersault.util.{FilenameUtils2, HashInfo, Assert}

/**
 * Tests a DryRunFileSystem wrapping a FakeFileSystem.
 */
class DryRunFileSystemTest {

  /**
   * Get children from the root.
   * - No files or directories exist.
   */
  @Test def root() {
    val unitTestName = this.getClass.getName + ".root"
    val innerFs = FakeFileSystem.connect(unitTestName)
    val dryRunFs = new DryRunFileSystem(innerFs, createIndex(unitTestName))

    try {
      Assert.areEqual(dryRunFs.children(dryRunFs.root).size, 0)
    }
    finally dryRunFs.disconnect()
  }

  /**
   * One directory exists in the underlying FileSystem, one directory is created in the DryRunFileSystem.
   */
  @Test def createDir1() {
    val unitTestName = this.getClass.getName + ".createDir1"
    val innerFs = FakeFileSystem.connect(unitTestName)
    val dryRunFs = new DryRunFileSystem(innerFs, createIndex(unitTestName))

    try {
      val foo = innerFs.createDir(innerFs.root, "foo")
      val bar = dryRunFs.createDir(dryRunFs.root, "bar")

      val innerList = innerFs.children(innerFs.root)
      Assert.areEqual(innerList.size, 1)
      Assert.isTrue(innerList.forall(_.isInstanceOf[SomerDirectory]))
      Assert.isTrue(innerList.exists(_.name == foo.name))

      val dryRunList = dryRunFs.children(dryRunFs.root)
      Assert.areEqual(dryRunList.size, 2)
      Assert.isTrue(dryRunList.forall(_.isInstanceOf[SomerDirectory]))
      Assert.isTrue(dryRunList.exists(_.name == foo.name))
      Assert.isTrue(dryRunList.exists(_.name == bar.name))
    }
    finally dryRunFs.disconnect()
  }

  /**
   * Create a directory tree in the DryRunFileSystem
   */
  @Test def createDir2() {
    val unitTestName = this.getClass.getName + ".createDir2"
    val innerFs = FakeFileSystem.connect(unitTestName)
    val dryRunFs = new DryRunFileSystem(innerFs, createIndex(unitTestName))

    try {
      val foo = dryRunFs.createDir(dryRunFs.root, "foo")
      val bar = dryRunFs.createDir(foo, "bar")
      val baz = dryRunFs.createDir(foo, "baz")

      Assert.areEqual(innerFs.children(innerFs.root).size, 0)

      val rootList = dryRunFs.children(dryRunFs.root)
      Assert.areEqual(rootList.size, 1)
      Assert.isTrue(rootList.forall(_.isInstanceOf[SomerDirectory]))

      val fooList = dryRunFs.children(foo)
      Assert.areEqual(fooList.size, 2)
      Assert.isTrue(fooList.forall(_.isInstanceOf[SomerDirectory]))
      Assert.isTrue(fooList.exists(_.name == bar.name))
      Assert.isTrue(fooList.exists(_.name == baz.name))
    }
    finally dryRunFs.disconnect()
  }

  /**
   * One directory exists in the underlying FileSystem, that directory is removed in the DryRunFileSystem.
   */
  @Test def remove1() {
    val unitTestName = this.getClass.getName + ".remove1"
    val innerFs = FakeFileSystem.connect(unitTestName)
    val dryRunFs = new DryRunFileSystem(innerFs, createIndex(unitTestName))

    try {
      val fooInner = innerFs.createDir(innerFs.root, "foo")
      val fooDryRun = dryRunFs.children(dryRunFs.root).find(_.name == fooInner.name).get
      dryRunFs.remove(fooDryRun)

      val innerList = innerFs.children(innerFs.root)
      Assert.areEqual(innerList.size, 1)
      Assert.isTrue(innerList.forall(_.isInstanceOf[SomerDirectory]))
      Assert.isTrue(innerList.exists(_.name == fooInner.name))

      val dryRunList = dryRunFs.children(dryRunFs.root)
      Assert.areEqual(dryRunList.size, 0)
    }
    finally dryRunFs.disconnect()
  }

  /**
   * One file exists in the underlying FileSystem, that directory is removed in the DryRunFileSystem.
   */
  @Test def remove2() {
    val unitTestName = this.getClass.getName + ".remove2"
    val innerFs = FakeFileSystem.connect(unitTestName)
    val dryRunFs = new DryRunFileSystem(innerFs, createIndex(unitTestName))

    try {
      val fooInner = FileSystemTest.writeFile(innerFs, innerFs.root, "foo", "contents")
      val fooDryRun = dryRunFs.children(dryRunFs.root).find(_.name == fooInner.name).get
      dryRunFs.remove(fooDryRun)

      val innerList = innerFs.children(innerFs.root)
      Assert.areEqual(innerList.size, 1)
      Assert.isTrue(innerList.forall(_.isInstanceOf[SomerFile]))
      Assert.isTrue(innerList.exists(_.name == fooInner.name))

      val dryRunList = dryRunFs.children(dryRunFs.root)
      Assert.areEqual(dryRunList.size, 0)
    }
    finally dryRunFs.disconnect()
  }

  /**
   * Removes a root directory in the DryRunFileSystem containing a sub-directory and file.
   */
  @Test def remove3() {
    val unitTestName = this.getClass.getName + ".remove3"
    val innerFs = FakeFileSystem.connect(unitTestName)
    val dryRunFs = new DryRunFileSystem(innerFs, createIndex(unitTestName))

    try {
      val foo = dryRunFs.createDir(dryRunFs.root, "foo")
      dryRunFs.createDir(foo, "bar")
      FileSystemTest.writeFile(dryRunFs, foo, "baz", "contents1")
      dryRunFs.remove(foo)

      Assert.areEqual(innerFs.children(innerFs.root).size, 0)
      Assert.areEqual(dryRunFs.children(dryRunFs.root).size, 0)
    }
    finally dryRunFs.disconnect()
  }

  /**
   * Writes a root file to the DryRunFileSystem.
   */
  @Test def transfer1() {
    val unitTestName = this.getClass.getName + ".transfer1"
    val innerFs = FakeFileSystem.connect(unitTestName)
    val dryRunFs = new DryRunFileSystem(innerFs, createIndex(unitTestName))

    try {
      val foo = FileSystemTest.writeFile(dryRunFs, dryRunFs.root, "foo", "contents1")

      Assert.areEqual(innerFs.children(innerFs.root).size, 0)

      val dryRunList = dryRunFs.children(dryRunFs.root)
      Assert.areEqual(dryRunList.size, 1)
      Assert.isTrue(dryRunList.forall(_.isInstanceOf[SomerFile]))
      Assert.isTrue(dryRunList.exists(_.name == foo.name))
    }
    finally dryRunFs.disconnect()
  }

  /**
   * Writes a non-root file to the DryRunFileSystem where the parent directory exists in the inner FileSystem.
   */
  @Test def transfer2() {
    val unitTestName = this.getClass.getName + ".transfer2"
    val innerFs = FakeFileSystem.connect(unitTestName)
    val dryRunFs = new DryRunFileSystem(innerFs, createIndex(unitTestName))

    try {
      val innerFoo = innerFs.createDir(innerFs.root, "foo")
      val dryRunFoo = dryRunFs.createDir(dryRunFs.root, innerFoo.name)
      val bar = FileSystemTest.writeFile(dryRunFs, dryRunFoo, "bar", "contents1")

      Assert.areEqual(innerFs.children(innerFoo).size, 0)

      val dryRunList = dryRunFs.children(dryRunFoo)
      Assert.areEqual(dryRunList.size, 1)
      Assert.isTrue(dryRunList.forall(_.isInstanceOf[SomerFile]))
      Assert.isTrue(dryRunList.exists(_.name == bar.name))
    }
    finally dryRunFs.disconnect()
  }

  /**
   * Writes a non-root file to the DryRunFileSystem where the parent directory does not exist in the inner FileSystem.
   */
  @Test def transfer3() {
    val unitTestName = this.getClass.getName + ".transfer3"
    val innerFs = FakeFileSystem.connect(unitTestName)
    val dryRunFs = new DryRunFileSystem(innerFs, createIndex(unitTestName))

    try {
      val dryRunFoo = dryRunFs.createDir(dryRunFs.root, "foo")
      val bar = FileSystemTest.writeFile(dryRunFs, dryRunFoo, "bar", "contents1")

      Assert.areEqual(innerFs.children(innerFs.root).size, 0)

      val dryRunList = dryRunFs.children(dryRunFoo)
      Assert.areEqual(dryRunList.size, 1)
      Assert.isTrue(dryRunList.forall(_.isInstanceOf[SomerFile]))
      Assert.isTrue(dryRunList.exists(_.name == bar.name))
    }
    finally dryRunFs.disconnect()
  }

  private def createIndex(testName: String): H2Index = {
    val testPath = FilenameUtils2.concat(FilenameUtils2.tempPath.getPath, this.getClass.getSimpleName, testName)
    FileUtils.forceMkdir(new File(testPath))
    val indexPath = FilenameUtils.concat(testPath, DryRunFileSystem.indexFilenamePrefix)
    new H2Index(
      new H2IndexData(
        HashInfo.md5, new NullCompressionProvider, new NullEncryptionProvider, TestUtil.logger, new File(indexPath)))
  }
}