package somersault.core.filesystems

import java.lang.String
import logahawk.Logger

class FakeFileSystemTest extends FileSystemTest {

  protected def createFileSystem(testName: String, nameSuffix: String, logger: Logger): FileSystem =
    FakeFileSystem.connect(testName, nameSuffix)

  protected def createFileSystemDataPersister(testName: _root_.scala.Predef.String, logger: Logger) = null

  protected def verifyFileSystem(testName: String, fs: FileSystem) {}
}