package somersault.core.filesystems.local

import java.io.File
import java.lang.String
import logahawk.Logger
import org.testng.annotations.Test
import somersault.core.TestUtil
import somersault.core.filesystems.{FileSystemDataPersister, FileSystem, FileSystemTest}
import somersault.util.{Assert, HashInfo}

class LocalDirectFileSystemTest extends FileSystemTest {

  protected def createFileSystem(testName: String, nameSuffix: String, logger: Logger): FileSystem = {
    val testPath = TestUtil.createTestPath(classOf[LocalIndexedFileSystemTest], testName)
    val runtimeData = FileSystemTest.createRuntimeData(testPath)
    LocalDirectFileSystem.connect(
      new LocalDirectFileSystemData(testName + nameSuffix, HashInfo.md5, testPath), runtimeData)
  }

  protected def verifyFileSystem(testName: String, fs: FileSystem) {}

  def createFileSystemDataPersister(testName: String, logger: Logger): FileSystemDataPersister =
    new LocalDirectFileSystemDataPersister

  @Test def symlinksIgnored() {
    val unitTestName = this.getClass.getSimpleName + ".symlinksIgnored"
    val testPath = TestUtil.createTestPath(classOf[LocalIndexedFileSystemTest], unitTestName)
    val fs = createFileSystem(unitTestName, "", logger)

    val symlink = new File(testPath, "symlink")
    // symlinking to our parent dir is weird but avoids any issues of deleting something we don't want to delete
    // when we delete the unit test output
    val process = Runtime.getRuntime.exec(Array("ln", "-s", testPath.getAbsolutePath, symlink.getAbsolutePath))
    val exitCode = process.waitFor()
    Assert.areEqual(exitCode, 0)

    val children = fs.children(fs.root)
    Assert.isTrue(children.isEmpty)
  }

  @Test def namedPipesIgnored() {
    val unitTestName = this.getClass.getSimpleName + ".namedPipesIgnored"
    val testPath = TestUtil.createTestPath(classOf[LocalIndexedFileSystemTest], unitTestName)
    val fs = createFileSystem(unitTestName, "", logger)

    val pipe = new File(testPath, "pipe")
    val process = Runtime.getRuntime.exec(Array("mkfifo", pipe.getAbsolutePath))
    val exitCode = process.waitFor()
    Assert.areEqual(exitCode, 0)

    val children = fs.children(fs.root)
    Assert.isTrue(children.isEmpty)
  }
}