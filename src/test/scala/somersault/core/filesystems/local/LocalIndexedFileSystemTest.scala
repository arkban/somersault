package somersault.core.filesystems.local

import java.lang.String
import logahawk.Logger
import org.apache.commons.io.FileUtils
import org.testng.annotations.Test
import scala.Some
import somersault.core.TestUtil
import somersault.core.encode.{NullEncryptionProviderPersister, NullCompressionProviderPersister, NullEncryptionProvider, NullCompressionProvider, MockEncryptionProvider, MockCompressionProvider, EncryptionProvider, CompressionProvider}
import somersault.core.filesystems._
import somersault.core.filesystems.index.{IndexSupport, IndexDirectory, Index, IndexedAndNonIndexedFileSystemTests}
import somersault.util.persist.PersisterContainer
import somersault.util.{Assert, HashInfo}

class LocalIndexedFileSystemTest extends IndexedAndNonIndexedFileSystemTests {

  override protected def createFileSystem(
    testName: String,
    nameSuffix: String,
    logger: Logger,
    compProvider: CompressionProvider,
    encProvider: EncryptionProvider): FileSystem = {
    val testPath = TestUtil.createTestPath(classOf[LocalIndexedFileSystemTest], testName)
    val runtimeData = FileSystemTest.createRuntimeData(testPath)
    LocalIndexedFileSystem.connect(
      new LocalIndexedFileSystemData(
        testName + nameSuffix,
        HashInfo.md5,
        compProvider,
        encProvider,
        LockRefreshSupport.defaults,
        IndexSupport.defaults,
        testPath), runtimeData)
  }

  protected def createFileSystemDataPersister(testName: String, logger: Logger): FileSystemDataPersister = {
    val pwc = new PersisterContainer
    pwc.add(classOf[NullCompressionProvider], new NullCompressionProviderPersister())
    pwc.add(classOf[NullEncryptionProvider], new NullEncryptionProviderPersister())
    new LocalIndexedFileSystemDataPersister
  }

  override protected def verifyFileSystem(testName: String, indexedFs: FileSystem) {
    val newIndexedFs = LocalIndexedFileSystem.connect(indexedFs.data, indexedFs.runtimeData)
    try {
      val fsReal = {
        val fsRealData = new LocalDirectFileSystemData(
          "verify", indexedFs.data.hashInfo, indexedFs.data.asInstanceOf[LocalFileSystemData].rootFile)
        LocalDirectFileSystem.connect(fsRealData, indexedFs.runtimeData)
      }
      try {
        val index = newIndexedFs.asInstanceOf[LocalIndexedFileSystem].index

        verifyFileSystemRecursive(fsReal, fsReal.root, index, index.root)
      }
      finally fsReal.disconnect()
    }
    finally indexedFs.disconnect()
  }

  private def verifyFileSystemRecursive(
    fsReal: FileSystem, parentReal: SomerDirectory, index: Index, parentIndexed: IndexDirectory) {
    val childrenReal = fsReal.children(parentReal).filterNot(
      _.name.startsWith(
        LocalIndexedFileSystem.indexFilenamePrefix))
    val childrenIndexed = index.children(parentIndexed)

    val childrenRealNames = childrenReal.map(_.name).toSeq
    val childrenIndexedNames = childrenIndexed.map(_.storedName).toSeq
    Assert.areEqual(childrenRealNames.size, childrenIndexedNames.size)
    val onlyInIndex = childrenRealNames.filterNot(childrenIndexedNames.contains(_))
    Assert.areEqual(onlyInIndex.size, 0)
    val onlyInReal = childrenIndexedNames.filterNot(childrenRealNames.contains(_))
    Assert.areEqual(onlyInReal.size, 0)

    childrenReal.foreach {
      case dirReal: SomerDirectory =>
        childrenIndexed.find(_.storedName == dirReal.name) match {
          case Some(dirIndexed: IndexDirectory) => verifyFileSystemRecursive(fsReal, dirReal, index, dirIndexed)
          case Some(x) =>
            System.out.println()
            throw new Exception("Directory not found in Index: " + dirReal.name)
          case None =>
            System.out.println()
            throw new Exception("Directory not found in Index: " + dirReal.name)
          case _ =>
            System.out.println()
            throw new Exception("Directory not found in Index: " + dirReal.name)
        }
      case _ =>
    }
  }

  protected def findRawFile(
    testName: String, data: FileSystemData, filePath: Iterable[String]): (FileSystem, SomerDirectory) = {
    val testPath = TestUtil.createTestPath(classOf[LocalIndexedFileSystemTest], testName)
    val runtimeData = FileSystemTest.createRuntimeData(testPath)
    val directFs = LocalIndexedFileSystem.connect(
      new LocalIndexedFileSystemData(
        testName,
        data.hashInfo,
        new MockCompressionProvider(data.compressionProvider.name),
        new MockEncryptionProvider(data.encryptionProvider.key),
        LockRefreshSupport.defaults,
        IndexSupport.defaults,
        testPath), runtimeData)

    // walk through parent directories
    var curr = directFs.root
    if (filePath.size >= 2) {
      filePath.toList.take(filePath.size - 1).foreach(
        path => directFs.children(curr).find(_.name == path) match {
          case Some(d: SomerDirectory) => curr = d
          case Some(f: SomerFile) => throw new Exception("Unexpected file in path at " + path)
          case None => throw new Exception("Bad path at " + path)
          case x => throw new Exception("Unexpected response: " + x)
        })
    }

    (directFs.asInstanceOf[FileSystem], curr)
  }

  /**
   * Index believes it contains a directory tree but the real FileSystem is empty.
   */
  @Test def reconcile1() {
    val unitTestName = this.getClass.getSimpleName + ".reconcile1"

    val fs1 = createFileSystem(unitTestName, "", logger).asInstanceOf[LocalIndexedFileSystem]
    try {
      val dirA = fs1.createDir(fs1.root, "dirA")
      fs1.createDir(fs1.root, "dirB")
      FileSystemTest.writeFile(fs1, fs1.root, "fileC", "contents")
      FileSystemTest.writeFile(fs1, dirA, "fileAA", "contents")
      fs1.createDir(dirA, "dirAB")

      Assert.notEquals(fs1.data.rootFile.listFiles.size, 0)
      Assert.notEquals(fs1.children(fs1.root).size, 0)

      // delete all files from fileSystem
      fs1.data.rootFile.listFiles
        .filter(!_.getName.startsWith(LocalIndexedFileSystem.indexFilenamePrefix))
        .foreach(f => FileUtils.deleteQuietly(f))
    }
    finally fs1.disconnect()

    val fs2 = createFileSystem(unitTestName, "", logger).asInstanceOf[LocalIndexedFileSystem]
    try {
      //      val files1 = fs2.data.rootFile.listFiles
      //        .filterNot(_.getName.startsWith(LocalIndexedFileSystem.indexFilenamePrefix))
      Assert.notEquals(fs2.children(fs2.root).size, 0)

      val reconcileObs = new MockReconcileObserver(logger)
      fs2.reconcile(reconcileObs)
      Assert.areEqual(reconcileObs.errors.size, 0)

      val files2 = fs2.data.rootFile.listFiles
        .filterNot(_.getName.startsWith(LocalIndexedFileSystem.indexFilenamePrefix))
      Assert.areEqual(files2.size, 0)
      Assert.areEqual(fs2.children(fs2.root).size, 0)

      // check that the tree contains no files
      def checkTree(parent: SomerDirectory) {
        fs2.children(parent).foreach {
          case d: SomerDirectory => checkTree(d)
          case f: SomerFile => Assert.fail("found file " + f.name)
        }
      }
      checkTree(fs2.root)
    }
    finally fs2.disconnect()

    verifyFileSystem(unitTestName, fs2)
  }

  /**
   * Index believes it is empty but the real FileSystem contains a directory tree.
   */
  @Test def reconcile2() {
    val unitTestName = this.getClass.getSimpleName + ".reconcile2"

    val fs1 = createFileSystem(unitTestName, "", logger).asInstanceOf[LocalIndexedFileSystem]
    try {
      val dirA = fs1.createDir(fs1.root, "dirA")
      fs1.createDir(fs1.root, "dirB")
      FileSystemTest.writeFile(fs1, fs1.root, "fileC", "contents")
      FileSystemTest.writeFile(fs1, dirA, "fileAA", "contents")
      fs1.createDir(dirA, "dirAB")

      Assert.notEquals(fs1.data.rootFile.listFiles.size, 0)
      Assert.notEquals(fs1.children(fs1.root).size, 0)

      // delete all files from index
      fs1.index.children(fs1.index.root).foreach(fs1.index.remove)
    }
    finally fs1.disconnect()

    val fs2 = createFileSystem(unitTestName, "", logger).asInstanceOf[LocalIndexedFileSystem]
    try {
      Assert.notEquals(fs2.data.rootFile.listFiles.size, 0)
      Assert.areEqual(fs2.children(fs2.root).size, 0)

      val reconcileObs = new MockReconcileObserver(logger)
      fs2.reconcile(reconcileObs)
      Assert.areEqual(reconcileObs.errors.size, 0)

      val files2 = fs2.data.rootFile.listFiles.filterNot(
        _.getName.startsWith(
          LocalIndexedFileSystem.indexFilenamePrefix))
      Assert.areEqual(files2.size, 0)
      Assert.areEqual(fs2.children(fs2.root).size, 0)
    }
    finally fs2.disconnect()

    verifyFileSystem(unitTestName, fs2)
  }

  /**
   * Reconcile an empty file system (no Index)
   */
  @Test def reconcile3() {
    val unitTestName = this.getClass.getSimpleName + ".reconcile3"

    val fs = createFileSystem(unitTestName, "", logger)
    try {
      val reconcileObs = new MockReconcileObserver(logger)
      fs.reconcile(reconcileObs)
      Assert.areEqual(reconcileObs.errors.size, 0)
    }
    finally fs.disconnect()

    verifyFileSystem(unitTestName, fs)
  }
}
