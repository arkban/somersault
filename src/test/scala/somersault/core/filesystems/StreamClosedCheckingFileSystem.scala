package somersault.core.filesystems

import org.joda.time.DateTime
import somersault.core.encode.{EncryptResult, CompressResult}
import somersault.util.{ObservableInputStreamListener, ObservableInputStream, ObservableOutputStreamListener, ObservableOutputStream}

/**
 * Allows checking if the input and output streams from reading/writing are closed after transfer. This only works
 * for a single read and a single write.
 *
 * This class was originally written for {@link TransferActionRunnerTest}. It exists here due to the access modifer
 * constraints imposed by {@link LockingFileSystem}.
 */
class StreamClosedCheckingFileSystem(fs: FileSystem) extends ProxyFileSystem(fs) {

  var isInputFromReadClosed = false

  var isOutputFromWriteClosed = false

  override def getIndexArtifact(a: SomerArtifact) = None

  override def disconnect() {}

  override def write(
    parent: SomerDirectory,
    file: SomerFile,
    compResult: CompressResult,
    encResult: EncryptResult) = {
    val output = new ObservableOutputStream(fs.write(parent, file, compResult, encResult), Some(file.size))
    output.addListener(
      new ObservableOutputStreamListener {
        override def close(output: ObservableOutputStream) { isOutputFromWriteClosed = true }
      })
    output
  }

  override def read(file: SomerFile) = {
    val input = new ObservableInputStream(fs.read(file), Some(file.size))
    input.addListener(
      new ObservableInputStreamListener {
        override def close(input: ObservableInputStream) { isInputFromReadClosed = true }
      })
    input
  }

  override protected[filesystems] def putLock(lockName: String, timestamp: DateTime) {}

  override protected[filesystems] def removeLock(lockName: String) {}

  override protected[filesystems] def findExistingLocks() = Map.empty
}
