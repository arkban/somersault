package somersault.core.filesystems

import java.io.ByteArrayInputStream
import java.util.Random
import org.joda.time.DateTime
import somersault.util.{FilenameUtils2, Hash}

/**
 * Mock implementations of the SomerArtifact hierarchy.
 */

abstract class MockSomerArtifact(val fullPath: String) extends SomerArtifact {

  def path =
    if (fullPath == FilenameUtils2.dirSeparator) Nil
    else fullPath.substring(1).split("\\" + FilenameUtils2.dirSeparator).toList

  def name: String =
    if (fullPath == FilenameUtils2.dirSeparator) fullPath
    else MockSomerArtifact.split(fullPath)._2
}

case class MockSomerDirectory(override val fullPath: String)
  extends MockSomerArtifact(fullPath) with SomerDirectory

/**
 * @param data Mimics the contents of the file represented by this class.
 */
class MockSomerFile(
  override val fullPath: String,
  val modified: DateTime = new DateTime,
  var data: String = MockSomerArtifact.makeFakeData(),
  val attributes: FileAttributes = new FileAttributes(false, false, false))
  extends MockSomerArtifact(fullPath) with SomerFile {

  def size = data.length

  lazy val hash: Hash = Hash.create(FakeFileSystem.hashInfo, new ByteArrayInputStream(data.getBytes))
}

object MockSomerArtifact {

  /**
   * Standard directory separator character.
   */
  def split(fullPath: String): Tuple2[String, String] = {
    val lastSep = fullPath.lastIndexOf(FilenameUtils2.dirSeparator) + FilenameUtils2.dirSeparator.length
    var parent = fullPath.substring(0, lastSep - 1)
    val name = fullPath.substring(lastSep)

    // handle files at the root
    if (parent == "")
      parent = FilenameUtils2.dirSeparator

    (parent, name)
  }

  def makePath(parent: MockSomerDirectory, name: String): String =
    if (parent.fullPath.endsWith(FilenameUtils2.dirSeparator)) parent.fullPath + name
    else parent.fullPath + FilenameUtils2.dirSeparator + name

  /**
   * Creates a String with random data, at least 10 characters long and all capital letters
   */
  def makeFakeData(): String = {
    val random = new Random()
    val buffer = new Array[Char](random.nextInt(10) + 10)
    (0 until buffer.length).foreach( i =>       buffer(i) = (65 + random.nextInt(26)).asInstanceOf[Char])
    new String(buffer)
  }

  def splitPath(fullPath: String): List[String] =
    fullPath.substring(1).split(FilenameUtils2.dirSeparator).toList
}