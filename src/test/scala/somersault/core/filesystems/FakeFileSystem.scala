package somersault.core.filesystems

import collection.mutable
import java.io.InputStream
import net.jcip.annotations.NotThreadSafe
import org.joda.time.DateTime
import somersault.core.TestUtil
import somersault.core.encode._
import somersault.core.exceptions.{AlreadyExistsException, PathDoesNotExistException}
import somersault.core.mock.{MockOutputStream, MockInputStream}
import somersault.util.{FilenameUtils2, NullObservableOutputStreamListener, NullObservableInputStreamListener, HashInfo}

/**
 * A fake file system that exists only in memory. This implementation is designed to work as much like a normal
 * {@link FileSystem}s possible, which may involve some less-than-ideal code to mimic such behavior. The reason such
 * an implementation exists is to make it easier to construct the various unit tests in {@link FileSystemTest}, to
 * get the behavior right before trying them on the more practical and usually more complex {@link FileSystem}
 * implementations. This class is also especially helpful when testing the various wrapper classes like
 * {@link ConnectCheckingFileSystem}.
 */
class FakeFileSystem(
  val data: FileSystemData, val runtimeData: RuntimeFileSystemData, val internals: MockFileSystemInternals)
  extends FileSystem {

  def getIndexArtifact(a: SomerArtifact) = None

  def disconnect() {}

  def root: SomerDirectory = internals.root

  def children(parent: SomerDirectory) = parent match {
    case mockParent: MockSomerArtifact =>
      internals.fs.get(mockParent.fullPath) match {
        case Some(s) => s.toList
        case None => throw new PathDoesNotExistException(MockSomerArtifact.splitPath(mockParent.fullPath))
      }
    case x => throw new IllegalArgumentException("invalid type: " + x)
  }

  def find(parent: SomerDirectory, name: String) = parent match {
    case mockParent: MockSomerArtifact =>
      internals.fs.get(mockParent.fullPath) match {
        case Some(s) => s.find(_.name == name)
        case None => throw new PathDoesNotExistException(MockSomerArtifact.splitPath(mockParent.fullPath))
      }
    case x => throw new IllegalArgumentException("invalid type: " + x)
  }

  def read(f: SomerFile): InputStream = f match {
    case mockFile: MockSomerFile =>
      val (parentPath, _) = MockSomerArtifact.split(mockFile.fullPath)
      internals.fs.get(parentPath) match {
        case Some(artifacts) =>
          artifacts.find(_.name == f.name) match {
            case Some(found: MockSomerFile) => new MockInputStream(found)
            case None => throw new PathDoesNotExistException(MockSomerArtifact.splitPath(mockFile.fullPath))
            case x => throw new Exception("Unexpected response: " + x)
          }
        case None => throw new PathDoesNotExistException(MockSomerArtifact.splitPath(parentPath))
      }
    case x => throw new IllegalArgumentException("invalid type: " + x)
  }

  def write(parent: SomerDirectory, file: SomerFile, compResult: CompressResult, encResult: EncryptResult) =
    parent match {
      case mockParent: MockSomerDirectory =>
        internals.fs.get(mockParent.fullPath) match {
          // found parent
          case Some(artifacts) => {
            // remove the file (if it exists)
            artifacts.find(_.name == file.name) match {
              case Some(f: MockSomerFile) => remove(f)
              case Some(d: MockSomerDirectory) =>
                throw new AlreadyExistsException(MockSomerArtifact.splitPath(d.fullPath))
              case None =>
              case x => throw new Exception("Unexpected response: " + x)
            }

            // we may have done a remove above, so we need to re-get the artifacts list because remove needs to do
            // a convoluted clone-and-remove, which makes the original artifacts list obsolete.
            val newArtifacts = internals.fs.get(mockParent.fullPath).get

            // we clone the file because we do not require a MockSomerFile as input
            val f: MockSomerFile = {
              val path = MockSomerArtifact.makePath(mockParent, file.name)
              val data = file match {
                case mockFile: MockSomerFile => mockFile.data
                case _ => ""
              }
              new MockSomerFile(path, modified = file.modified, data = data, attributes = file.attributes)
            }

            newArtifacts += f
            new MockOutputStream(f)
          }
          case None => throw new PathDoesNotExistException(MockSomerArtifact.splitPath(mockParent.fullPath))
        }
      case x => throw new IllegalArgumentException("invalid type: " + x)
    }

  def createDir(parent: SomerDirectory, name: String): SomerDirectory = parent match {
    case mockParent: MockSomerDirectory =>
      internals.fs.get(mockParent.fullPath) match {
        case Some(artifacts) =>
          artifacts.find(_.name == name) match {
            case Some(a: MockSomerArtifact) => throw new AlreadyExistsException(
              MockSomerArtifact.splitPath(a.fullPath))
            case None =>
              // add directory to parent
              val d = new MockSomerDirectory(MockSomerArtifact.makePath(mockParent, name))
              artifacts += d

              // add directory as new key
              internals.fs += (d.fullPath -> new mutable.HashSet[SomerArtifact])

              d
            case x => throw new Exception("Unexpected response: " + x)
          }
        case None => throw new PathDoesNotExistException(MockSomerArtifact.splitPath(mockParent.fullPath))
      }
    case x => throw new IllegalArgumentException("invalid type: " + x)
  }

  def remove(f: SomerArtifact) {
    f match {
      case mockArtifact: MockSomerArtifact =>
        val (parentPath, _) = MockSomerArtifact.split(mockArtifact.fullPath)
        internals.fs.get(parentPath) match {
          // found parent
          case Some(artifacts) =>
            artifacts.find(_.name == f.name) match {
              // found file
              case Some(found: MockSomerArtifact) =>
                internals.fs += (parentPath -> (artifacts - found))

                // recursively remove children and remove parent as a key
                found match {
                  case foundDir: MockSomerDirectory =>
                    children(foundDir).foreach(remove)
                    internals.fs -= found.fullPath
                  case _ =>
                }
              case None => throw new PathDoesNotExistException(MockSomerArtifact.splitPath(mockArtifact.fullPath))
              case x => throw new Exception("Unexpected response: " + x)
            }
          case None => throw new PathDoesNotExistException(MockSomerArtifact.splitPath(parentPath))
        }
      case x => throw new IllegalArgumentException("invalid type: " + x)
    }
  }

  /**
   * Helper method to allow creating files outside of the standard FileSystem methods
   */
  def add(files: SomerFile*) {
    files.foreach {
      case f: MockSomerFile =>
        val (parentPath, _) = MockSomerArtifact.split(f.fullPath)
        internals.fs.get(parentPath) match {
          // found parent
          case Some(artifacts) => artifacts.find(_.name == f.name) match {
            case Some(found) => throw new Exception("File already exists")
            case None => artifacts += f
          }
          case None => throw new Exception("Parent does not exist")
        }
      case x => throw new IllegalArgumentException("invalid type: " + x)
    }
  }

  def reconcile(obs: ReconcileObserver) {}

  protected[filesystems] def putLock(lockName: String, timestamp: DateTime) {
    val lockFile = new MockSomerFile(MockSomerArtifact.makePath(internals.root, lockName))
    lockFile.data = timestamp.toString(LockingFileSystem.lockTimestampFormatter)
    write(root, lockFile, new NullCompressResult(lockName), new NullEncryptResult(lockName)).close()
  }

  protected[filesystems] def removeLock(lockName: String) {
    children(root).find(_.name == lockName) match {
      case Some(lockFile: MockSomerFile) => remove(lockFile)
      case None =>
      case x => throw new Exception("Unexpected response: " + x)
    }
  }

  protected[filesystems] def findExistingLocks() = {
    val pattern = generateLockNameRegex()
    children(root)
      .collect {case f: MockSomerFile => f}
      .filter(f => pattern.matcher(f.name).matches)
      .map(f => f.name -> LockingFileSystem.lockTimestampFormatter.parseDateTime(f.data))
      .toMap
  }

  /**
   * Intentionally made public for use in unit tests (specifically {@link LockingFileSystemTest}.
   */
  override def hasLock(mode: LockingFileSystemMode.Mode) = super.hasLock(mode)

  override protected val options = internals.lockingFileSystemOptions
}

/**
 * The internal data of a {@link FakeFileSystem}.
 *
 * This can be shared between multiple {@link FakeFileSystem}s that represent the same underlying file system.
 *
 * This stores the locks as files because that is the most common way different {@link FileSystem} implementations
 * will store them.
 */
@NotThreadSafe
class MockFileSystemInternals {

  /**
   * Stores all the SomerArtifact relationships in a map from parent path to child.
   */
  val fs = new mutable.HashMap[String, mutable.HashSet[SomerArtifact]]

  val root = {
    val tempRoot = new MockSomerDirectory(FilenameUtils2.dirSeparator)
    fs += (tempRoot.name -> new mutable.HashSet[SomerArtifact])
    tempRoot
  }

  /** Exposed here to allow overriding to change from the defaults. */
  val lockingFileSystemOptions = new LockingFileSystem.Options
}

object FakeFileSystem {

  val hashInfo = HashInfo.md5

  /**
   * {@link MockFileSystemInternal}s created are stored here so they can be re-used. This is important if a
   * {@link FakeFileSystem} needs to represent real persistence -- that is the data stored still exists even if
   * disconnect() is called. (For one this allows unit testing to work as one would expect.)
   */
  private val internalMap = new mutable.HashMap[String, MockFileSystemInternals]

  def connect(key: String, nameSuffix: String = "") = {
    val internals = internalMap.getOrElseUpdate(key, new MockFileSystemInternals())
    new FakeFileSystem(constructData(key + nameSuffix), FileSystemTest.createRuntimeData(null), internals)
  }

  /**
   * Constructs a {@link FakeFileSystem} using the provided {@link MockFileSystemInternals}. The internals are
   * added to the internal map using the provided name (for consistency with the other connect()).
   */
  def connect(name: String, internals: MockFileSystemInternals) = {
    internalMap.put(name, internals)
    new FakeFileSystem(constructData(name), FileSystemTest.createRuntimeData(null), internals)
  }

  private def constructData(_name: String): FileSystemData =
    new FileSystemData {
      val name = _name

      val hashInfo = HashInfo.md5

      val isIndexed = false

      val compressionProvider = new NullCompressionProvider

      val encryptionProvider = new NullEncryptionProvider

      val lockRefreshSupportData = LockRefreshSupport.defaults

      def observerContainer() = new FileSystemObserverContainer(
        TestUtil.logger, new NullObservableInputStreamListener, new NullObservableOutputStreamListener)
    }
}