package somersault.core.filesystems

import org.testng.annotations.Test
import somersault.core.encode.{NullCompressResult, NullEncryptResult}
import somersault.util.Assert

class LockCheckingFileSystemTest {

  @Test def root() {
    val unitTestName = this.getClass.getName + ".root"
    val innerFs = FakeFileSystem.connect(unitTestName)
    val lockFs = new LockCheckingFileSystem(innerFs)

    try {
      lockFs.unlock()
      lockFs.root
      throw new org.testng.TestException("expected IllegalStateException")
    }
    catch {
      case ex: IllegalStateException =>
      case ex: Exception => Assert.fail("expected IllegalStateException")
    }

    lockFs.unlock()
    lockFs.lock(LockingFileSystemMode.Read)
    lockFs.root

    lockFs.unlock()
    lockFs.lock(LockingFileSystemMode.Write)
    lockFs.root
  }

  @Test def children() {
    val unitTestName = this.getClass.getName + ".children"
    val innerFs = FakeFileSystem.connect(unitTestName)
    val lockFs = new LockCheckingFileSystem(innerFs)

    try {
      lockFs.unlock()
      lockFs.children(lockFs.root)
      throw new org.testng.TestException("expected IllegalStateException")
    }
    catch {
      case ex: IllegalStateException =>
      case ex: Exception => Assert.fail("expected IllegalStateException")
    }

    lockFs.unlock()
    lockFs.lock(LockingFileSystemMode.Read)
    lockFs.children(lockFs.root)

    lockFs.unlock()
    lockFs.lock(LockingFileSystemMode.Write)
    lockFs.children(lockFs.root)
  }

  @Test def read() {
    val unitTestName = this.getClass.getName + ".read"
    val innerFs = FakeFileSystem.connect(unitTestName)
    val lockFs = new LockCheckingFileSystem(innerFs)

    try {
      lockFs.unlock()
      lockFs.read(null)
      throw new org.testng.TestException("expected IllegalStateException")
    }
    catch {
      case ex: IllegalStateException =>
      case ex: Exception => Assert.fail("expected IllegalStateException")
    }
  }

  @Test def write() {
    val unitTestName = this.getClass.getName + ".write"
    val innerFs = FakeFileSystem.connect(unitTestName)
    val lockFs = new LockCheckingFileSystem(innerFs)

    // no permissions
    try {
      lockFs.write(null, null, null, null) // should not get a null error, should fail long before that
      throw new org.testng.TestException("expected IllegalStateException")
    }
    catch {
      case ex: IllegalStateException =>
      case ex: Exception => Assert.fail("expected IllegalStateException")
    }

    // read permissions
    try {
      lockFs.unlock()
      lockFs.lock(LockingFileSystemMode.Read)
      lockFs.write(null, null, null, null) // should not get a null error, should fail long before that
      throw new org.testng.TestException("expected IllegalStateException")
    }
    catch {
      case ex: IllegalStateException =>
      case ex: Exception => Assert.fail("expected IllegalStateException")
    }

    lockFs.unlock()
    lockFs.lock(LockingFileSystemMode.Write)
    lockFs.write(lockFs.root, new MockSomerFile("/file"), new NullCompressResult(""), new NullEncryptResult(""))
  }

  @Test def createDir() {
    val unitTestName = this.getClass.getName + ".createDir"
    val innerFs = FakeFileSystem.connect(unitTestName)
    val lockFs = new LockCheckingFileSystem(innerFs)

    // no permissions
    try {
      lockFs.unlock()
      lockFs.createDir(null, null) // should not get a null error, should fail long before that
      throw new org.testng.TestException("expected IllegalStateException")
    }
    catch {
      case ex: IllegalStateException =>
      case ex: Exception => Assert.fail("expected IllegalStateException")
    }

    // read permissions
    try {
      lockFs.unlock()
      lockFs.lock(LockingFileSystemMode.Read)
      lockFs.createDir(null, null) // should not get a null error, should fail long before that
      throw new org.testng.TestException("expected IllegalStateException")
    }
    catch {
      case ex: IllegalStateException =>
      case ex: Exception => Assert.fail("expected IllegalStateException")
    }

    lockFs.unlock()
    lockFs.lock(LockingFileSystemMode.Write)
    lockFs.createDir(lockFs.root, "dir")
  }

  @Test def remove() {
    val unitTestName = this.getClass.getName + ".remove"
    val innerFs = FakeFileSystem.connect(unitTestName)
    val lockFs = new LockCheckingFileSystem(innerFs)

    // no permissions
    try {
      lockFs.unlock()
      lockFs.remove(null) // should not get a null error, should fail long before that
      throw new org.testng.TestException("expected IllegalStateException")
    }
    catch {
      case ex: IllegalStateException =>
      case ex: Exception => Assert.fail("expected IllegalStateException")
    }

    // read permissions
    try {
      lockFs.unlock()
      lockFs.remove(null) // should not get a null error, should fail long before that
      lockFs.write(null, null, null, null) // should not get a null error, should fail long before that
      throw new org.testng.TestException("expected IllegalStateException")
    }
    catch {
      case ex: IllegalStateException =>
      case ex: Exception => Assert.fail("expected IllegalStateException")
    }

    lockFs.unlock()
    lockFs.lock(LockingFileSystemMode.Write)
    lockFs.createDir(lockFs.root, "dir")
    lockFs.remove(lockFs.children(lockFs.root).head)
  }

  @Test def reconcile() {
    val unitTestName = this.getClass.getName + ".reconcile"
    val innerFs = FakeFileSystem.connect(unitTestName)
    val lockFs = new LockCheckingFileSystem(innerFs)

    // no permissions
    try {
      lockFs.unlock()
      lockFs.reconcile(null) // should not get a null error, should fail long before that
      throw new org.testng.TestException("expected IllegalStateException")
    }
    catch {
      case ex: IllegalStateException =>
      case ex: Exception => Assert.fail("expected IllegalStateException")
    }

    // read permissions
    try {
      lockFs.unlock()
      lockFs.reconcile(null) // should not get a null error, should fail long before that
      throw new org.testng.TestException("expected IllegalStateException")
    }
    catch {
      case ex: IllegalStateException =>
      case ex: Exception => Assert.fail("expected IllegalStateException")
    }

    lockFs.unlock()
    lockFs.lock(LockingFileSystemMode.Write)
    lockFs.reconcile(new NullReconcileObserver())
  }

  @Test def disconnect1() {
    val unitTestName = this.getClass.getName + ".disconnect1"
    val innerFs = FakeFileSystem.connect(unitTestName)
    val lockFs = new LockCheckingFileSystem(innerFs)

    lockFs.lock(LockingFileSystemMode.Read)
    lockFs.disconnect()

    try {
      lockFs.root
      throw new org.testng.TestException("expected IllegalStateException")
    }
    catch {
      case ex: IllegalStateException =>
      case ex: Exception => Assert.fail("expected IllegalStateException")
    }
  }

  @Test def disconnect2() {
    val unitTestName = this.getClass.getName + ".disconnect2"
    val innerFs = FakeFileSystem.connect(unitTestName)
    val lockFs = new LockCheckingFileSystem(innerFs)

    lockFs.lock(LockingFileSystemMode.Write)
    lockFs.disconnect()

    try {
      lockFs.root
      throw new org.testng.TestException("expected IllegalStateException")
    }
    catch {
      case ex: IllegalStateException =>
      case ex: Exception => Assert.fail("expected IllegalStateException")
    }
  }

  /**
   * Calling removeAllLocks() should succeed no matter what kind of lock is held.
   */
  @Test def removeAllLocks() {
    val unitTestName = this.getClass.getName + ".removeAllLocks"
    val innerFs = FakeFileSystem.connect(unitTestName)
    val lockFs = new LockCheckingFileSystem(innerFs)

    lockFs.removeAllLocks()
    lockFs.lock(LockingFileSystemMode.Read)
    lockFs.removeAllLocks()
    lockFs.lock(LockingFileSystemMode.Write)
    lockFs.removeAllLocks()
    lockFs.unlock()
    lockFs.removeAllLocks()
  }
}