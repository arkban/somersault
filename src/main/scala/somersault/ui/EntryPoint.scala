package somersault.ui

import somersault.ui.cli.CLI
import somersault.util.Logger

/**
 * The main entry point object for Scala
 */

object EntryPoint
{
  def main( args: Array[ String ] ): Unit =
  {
    Logger.getListenerContainer.add( Logger.createStandardListener )

    new CLI().execute( args )
  }
}