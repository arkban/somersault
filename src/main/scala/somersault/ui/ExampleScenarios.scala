package somersault.ui

import java.io.File
import nu.xom.Element
import org.apache.commons.io.FileUtils
import scala.collection.immutable.Nil
import somersault.core.comparators._
import somersault.core.encode._
import somersault.core.filesystems.LockRefreshSupport
import somersault.core.filesystems.amazonS3.AmazonS3FileSystem.Credentials
import somersault.core.filesystems.amazonS3.{AmazonS3FileSystem, AmazonS3FileSystemData}
import somersault.core.filesystems.index.IndexSupport
import somersault.core.filesystems.local.{LocalIndexedFileSystemData, LocalDirectFileSystemData}
import somersault.core.filters._
import somersault.core.{ScenarioDataPersister, ScenarioData}
import somersault.util.persist.{PersisterContext, PersisterSuite, Persister}
import somersault.util.{FilenameUtils2, HashInfo}

/**
 * Contains example {@link ScenarioData}s to be used as examples for users.
 */
object ExampleScenarios {

  /**
   * Writes example {@link ScenarioData}s to the provided directory path. this will overwrite any files that that
   * exist in this directory.
   */
  def write(path: File) {
    FileUtils.forceMkdir(path)

    val scenarios = Seq(
      localToLocal(),
      standardComparator(),
      overwriteByDateModifiedThenSize(),
      overwriteByHash(),
      sharedComparator(),
      localToIndexedLocal(),
      standardBuilderFilters(),
      exampleBuilderFilters(),
      sharedFilters(),
      gzipCompression(),
      bzip2Compression(),
      weakBouncyCastleEncryption(),
      strongBouncyCastleEncryption(),
      standardCompressionFilters(),
      localToAmazon())
    scenarios.foreach(
      s => {
        val filePath = new File(path, s.name + ".xml")
        val context = new PersisterContext(PersisterSuite.create(), filePath)

        val fn = (root: Element) => new ScenarioDataPersister().write(context, s, root)
        Persister.write("scenario", fn, filePath)
      })
  }

  private def standardComparator(): ScenarioData = {
    val hashInfo = HashInfo.md5
    val source = new LocalDirectFileSystemData(
      "Home Directory", hashInfo, new File(System.getProperty("user.home")))
    val dest = new LocalDirectFileSystemData(
      "Backup Directory", hashInfo, new File("/backup"))
    val description = "Local-to-Local"
    val comparator = ExtendedComparators.standardComparator
    new ScenarioData("standard", description, source, dest, comparator)
  }

  private def localToLocal(): ScenarioData = {
    val hashInfo = HashInfo.md5
    val source = new LocalDirectFileSystemData(
      "Home Directory", hashInfo, new File(System.getProperty("user.home")))
    val dest = new LocalDirectFileSystemData(
      "Backup Directory", hashInfo, new File("/backup"))
    val description = "Basic Local FileSystem  to Local FileSystem backup of all files"
    new ScenarioData("localToLocal", description, source, dest)
  }

  private def overwriteByDateModifiedThenSize(): ScenarioData = {
    val hashInfo = HashInfo.md5
    val source = new LocalDirectFileSystemData(
      "Home Directory", hashInfo, new File(System.getProperty("user.home")))
    val dest = new LocalDirectFileSystemData(
      "Backup Directory", hashInfo, new File("/backup"))
    val description = "Local-to-Local"
    val comparator = new MultiSomerFileComparator(new ModifyDateComparator(), new SizeComparator())
    new ScenarioData("overwriteByDateModifiedThenSize", description, source, dest, comparator)
  }

  private def overwriteByHash(): ScenarioData = {
    val hashInfo = HashInfo.md5
    val source = new LocalDirectFileSystemData(
      "Home Directory", hashInfo, new File(System.getProperty("user.home")))
    val dest = new LocalDirectFileSystemData(
      "Backup Directory", hashInfo, new File("/backup"))
    val description = "Local-to-Local"
    val comparator = new HashComparator
    new ScenarioData("overwriteByHash", description, source, dest, comparator)
  }

  private def sharedComparator(): ScenarioData = {
    val hashInfo = HashInfo.md5
    val source = new LocalDirectFileSystemData(
      "Home Directory", hashInfo, new File(System.getProperty("user.home")))
    val dest = new LocalDirectFileSystemData(
      "Backup Directory", hashInfo, new File("/backup"))
    val description = "Local-to-Local"
    val comparator = new SharedComparator(
      ExtendedComparators.standardComparator, new File(FilenameUtils2.concat("shared", "comparators.xml")))
    new ScenarioData("sharedComparator", description, source, dest, comparator)
  }

  private def localToIndexedLocal(): ScenarioData = {
    val hashInfo = HashInfo.md5
    val compProv = new NullCompressionProvider
    val encProv = new NullEncryptionProvider

    val source = new LocalDirectFileSystemData(
      "Home Directory", hashInfo, new File(System.getProperty("user.home")))

    val dest = new LocalIndexedFileSystemData(
      "Backup Directory",
      hashInfo,
      compProv,
      encProv,
      LockRefreshSupport.defaults,
      IndexSupport.defaults,
      new File("/backup"))

    val description = "Local-to-Indexed-Local (indexing is required for compression and encryption)"
    new ScenarioData("localToIndexedLocal", description, source, dest)
  }

  private def standardBuilderFilters(): ScenarioData = {
    val hashInfo = HashInfo.md5
    val compProv = new NullCompressionProvider
    val encProv = new NullEncryptionProvider

    val source = new LocalDirectFileSystemData(
      "Home Directory", hashInfo, new File(System.getProperty("user.home")))

    val dest = new LocalIndexedFileSystemData(
      "Backup Directory",
      hashInfo,
      compProv,
      encProv,
      LockRefreshSupport.defaults,
      IndexSupport.defaults,
      new File("/backup"))

    val description = "Uses a standard set of filters to decide what to backup (this filter is hightly recommended!)"
    new ScenarioData(
      "standardBuilderFilters", description, source, dest, filter = ExtendedFilters.standardBuilderFilter)
  }

  private def exampleBuilderFilters(): ScenarioData = {
    val hashInfo = HashInfo.md5
    val source = new LocalDirectFileSystemData(
      "Home Directory", hashInfo, new File(System.getProperty("user.home")))
    val dest = new LocalDirectFileSystemData(
      "Backup Directory", hashInfo, new File("/backup"))

    val fileFilter = new AndFilter(
      new NameFilter("(?i)\\.doc$") ::
        new NotFilter(new NameFilter("secrets.txt")) ::
        new SizeFilter(100, 987654321) ::
        new FileAttributesFilter(Some(true), Some(false), None) ::
        Nil)
    val dirFilter = new AndFilter(
      new NameFilter("(?i)documents") ::
        new PathFilter("(?i)/home/user") ::
        new BooleanFilter(true) ::
        Nil)

    val filter = new OrFilter(fileFilter :: dirFilter :: Nil)

    val description = "Shows off all the different filters available (the overal filter presented is probably not very useful)"
    new ScenarioData("exampleBuilderFilters", description, source, dest, filter = filter)
  }

  private def sharedFilters(): ScenarioData = {
    val hashInfo = HashInfo.md5
    val compProv = new NullCompressionProvider
    val encProv = new NullEncryptionProvider

    val source = new LocalDirectFileSystemData(
      "Home Directory", hashInfo, new File(System.getProperty("user.home")))

    val dest = new LocalIndexedFileSystemData(
      "Backup Directory",
      hashInfo,
      compProv,
      encProv,
      LockRefreshSupport.defaults,
      IndexSupport.defaults,
      new File("/backup"))

    val sharedFilter = new SharedFilter(
      ExtendedFilters.standardBuilderFilter, new File(FilenameUtils2.concat("shared", "filters.xml")))

    val description = "Uses a SharedFilter to move the actual filtering definition to a separate file"
    new ScenarioData("sharedFilter", description, source, dest, filter = sharedFilter)
  }

  private def gzipCompression(): ScenarioData = {
    val hashInfo = HashInfo.md5
    val compProv = new GZIPCompressionProvider(new BooleanFilter(true))
    val encProv = new NullEncryptionProvider

    val source = new LocalDirectFileSystemData(
      "Home Directory", hashInfo, new File(System.getProperty("user.home")))

    val dest = new LocalIndexedFileSystemData(
      "Backup Directory",
      hashInfo,
      compProv,
      encProv,
      LockRefreshSupport.defaults,
      IndexSupport.defaults,
      new File("/backup"))

    val description = "Uses GZIP compression for all files (indexing required)"
    new ScenarioData("gzipCompression", description, source, dest)
  }

  private def bzip2Compression(): ScenarioData = {
    val hashInfo = HashInfo.md5
    val compProv = new BZIP2CompressionProvider(new BooleanFilter(true))
    val encProv = new NullEncryptionProvider

    val source = new LocalDirectFileSystemData(
      "Home Directory", hashInfo, new File(System.getProperty("user.home")))

    val dest = new LocalIndexedFileSystemData(
      "Backup Directory",
      hashInfo,
      compProv,
      encProv,
      LockRefreshSupport.defaults,
      IndexSupport.defaults,
      new File("/backup"))

    val description = "Uses BZIP2 compression for all files (indexing required)"
    new ScenarioData("bzip2Compression", description, source, dest)
  }

  private def weakBouncyCastleEncryption(): ScenarioData = {
    val hashInfo = HashInfo.md5
    val compProv = new NullCompressionProvider
    val encKey = BouncyCastleEncryptionKey.createWeakKey("myEncryptionKey")
    val encProv = new BouncyCastleEncryptionProvider(encKey)

    val source = new LocalDirectFileSystemData(
      "Home Directory", hashInfo, new File(System.getProperty("user.home")))

    val dest = new LocalIndexedFileSystemData(
      "Backup Directory",
      hashInfo,
      compProv,
      encProv,
      LockRefreshSupport.defaults,
      IndexSupport.defaults,
      new File("/backup"))

    val description = "Weak encryption using BouncyCastle directly (indexing required)"
    new ScenarioData("weakBouncyCastleEncryption", description, source, dest)
  }

  private def strongBouncyCastleEncryption(): ScenarioData = {
    val hashInfo = HashInfo.md5
    val compProv = new NullCompressionProvider
    val encKey = BouncyCastleEncryptionKey.createStrongKey("myEncryptionKey")
    val encProv = new BouncyCastleEncryptionProvider(encKey)

    val source = new LocalDirectFileSystemData(
      "Home Directory", hashInfo, new File(System.getProperty("user.home")))

    val dest = new LocalIndexedFileSystemData(
      "Backup Directory",
      hashInfo,
      compProv,
      encProv,
      LockRefreshSupport.defaults,
      IndexSupport.defaults,
      new File("/backup"))

    val description = "Strong encryption using BouncyCastle directly (indexing required)"
    new ScenarioData("strongBouncyCastleEncryption", description, source, dest)
  }

  private def standardCompressionFilters(): ScenarioData = {
    val hashInfo = HashInfo.md5
    val compProv = new GZIPCompressionProvider(ExtendedFilters.standardCompressionFilter)
    val encProv = new NullEncryptionProvider

    val source = new LocalDirectFileSystemData(
      "Home Directory", hashInfo, new File(System.getProperty("user.home")))

    val dest = new LocalIndexedFileSystemData(
      "Backup Directory",
      hashInfo,
      compProv,
      encProv,
      LockRefreshSupport.defaults,
      IndexSupport.defaults,
      new File("/backup"))

    val description = "Compression (doesn't matter which kind) that excludes already compressed files (highly recommended!)"
    new ScenarioData(
      "standardCompressionFilters", description, source, dest)
  }

  private def localToAmazon(): ScenarioData = {
    val hashInfo = HashInfo.md5
    val compProv = new NullCompressionProvider
    val encProv = new NullEncryptionProvider
    val credentials = new Credentials("accessKey", "secretKey")

    val source = new LocalDirectFileSystemData(
      "Home Directory", hashInfo, new File(System.getProperty("user.home")))

    val dest = new AmazonS3FileSystemData(
      "Amazon S3 Backup",
      hashInfo,
      compProv,
      encProv,
      LockRefreshSupport.defaults,
      IndexSupport.defaults,
      credentials,
      AmazonS3FileSystem.generateBucketName("bucketname"))

    val description = "Local-to-Amazon S3 FileSystem (indexing required for any Amazon S3 FileSystem)"
    new ScenarioData("localToAmazon", description, source, dest)
  }
}