package somersault.ui

import logahawk.Severity
import somersault.util.AbstractMultiObserver
import net.jcip.annotations.ThreadSafe

/**
 * Observer to be used by the UI implementation. This consolidates and exposes other Observer traits, as well as
 * adding new observer methods that a UI would need.
 */
trait UIObserver
{
  /**
   * Generic error method for all other errors.
   *
   * For simplicity in implementation, one possibility is to set the other Observers to simply call this method.
   */
  def error( severity: Severity, ex: Exception)

  def start( )

  def end( )
}

@ThreadSafe
class MultiUIObserver extends AbstractMultiObserver[ UIObserver ] with UIObserver
{
  def error( severity: Severity, ex: Exception)
  {observers.foreach( _.error( severity, ex))}

  def end( )
  {observers.foreach( _.end( ))}

  def start( )
  {observers.foreach( _.start( ))}
}