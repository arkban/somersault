package somersault.ui

import java.io.File
import somersault.core.actions.builders.backup.{BackupData, Backup}
import somersault.core.actions.containers.{ActionContainerBuilder, ActionContainerProvider}
import somersault.core.actions.runners.{ActionRunnerController, FileSystemRunnerData}
import somersault.core.filesystems.LockingFileSystemMode
import somersault.core.{Scenario, ScenarioData}

/**
 * Executes the provided scenario once, with nothing extra.
 */
class StandardScenarioExecutor(
  scenarioData: ScenarioData, scenarioOptions: ScenarioOptions, observer: ObserverConsolidator)
  extends ScenarioExecutor(scenarioData, scenarioOptions, observer) {

  override protected def executeImpl() {
    val srcTag = scenarioOptions.getSourceTag(scenarioData)
    val destTag = scenarioOptions.getDestTag(scenarioData)
    observer.logger.alert(
      "%1$s from %2$s to %3$s".format(if (scenarioOptions.restore) "Restore" else "Backup", srcTag, destTag))

    observer.ui.start()
    try {
      val (scenario, tempPath) = createScenario(Some(LockingFileSystemMode.Read), Some(LockingFileSystemMode.Write))

      val actionBuilder = buildActions(scenario, tempPath)
      val actionProvider = actionBuilder.createProvider()
      executeActions(scenario, actionProvider)
    }
    catch {
      case ex: Throwable => observer.logger.fatal("Scenario failed", ex)
    }
    finally {
      observer.ui.end()
    }
  }

  /**
   * Builds the action and returns them in an ActionContainerBuilder. This method should only be executed once.
   */
  private def buildActions(scenario: Scenario, tempPath: File): ActionContainerBuilder = {
    val actionContainer = createActionContainer(tempPath)
    val actionBuilder = new Backup()
    actionBuilder.build(new BackupData(scenario, actionContainer, observer.actionBuilder, scenarioOptions.restore))
    actionContainer
  }

  /**
   * Executes the actions in the provided ActionContainerBuilder
   */
  private def executeActions(scenario: Scenario, actionProvider: ActionContainerProvider) {
    val actionCount = actionProvider.count
    if (actionCount == 0)
      observer.logger.alert("No actions, source and destination are the same")
    else
      observer.logger.alert("%d actions created, source and destination are not the same".format(actionCount))

    if (!scenarioOptions.buildOnly) {
      val runnerData = new FileSystemRunnerData(scenario, observer.actionRunner)
      val runnerController = new ActionRunnerController(runnerData)
      runnerController.run(actionProvider)
    }
  }
}
