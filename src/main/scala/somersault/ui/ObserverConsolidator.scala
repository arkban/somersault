package somersault.ui

import somersault.core.actions.builders.MultiActionBuilderObserver
import somersault.core.actions.runners.MultiActionRunnerObserver
import somersault.util.{MultiObservableOutputStreamListener, MultiObservableInputStreamListener}
import logahawk.Logger
import somersault.core.filesystems.{MultiReconcileObserver, FileSystemObserverContainer}

/**
 * Consolidates multiple Observers into one instance. Each member is a multi-observer so that multiple types of the same
 * observer are also consolidated.
 */

class ObserverConsolidator( val logger: Logger )
{
  val actionBuilder = new MultiActionBuilderObserver

  val actionRunner = new MultiActionRunnerObserver

  val ui = new MultiUIObserver

  val inputStream = new MultiObservableInputStreamListener

  val outputStream = new MultiObservableOutputStreamListener

  val fileSystem = new FileSystemObserverContainer( logger, inputStream, outputStream )

  val reconcile = new MultiReconcileObserver
}