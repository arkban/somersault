package somersault.ui

import java.util.Arrays
import net.jcip.annotations.ThreadSafe
import org.joda.time.{Duration, DateTime}
import scala.math

/**
 * A class that helps determine the average number of bits transferred (or read or write or whatever) over a period
 * of time. This class keeps a limited number of samples which are used to calculate the average speed. The limited
 * number of sample allows one to see changes in speed over time; many old samples do not unduly bias a limited number
 * of new samples.
 */

@ThreadSafe
class SpeedCalculator(
  val entryCount: Int = 16, private var lastEpoch: DateTime = new DateTime, timeFidelityMs: Long = 200) {

  private val invalidEntry = -1

  /**
   * Each entry is a bits transferred per millisecond. Unpopulated values are set to invalidEntry. This buffer is
   * a rolling buffer, the next value is inserted at the index specified by currentIndex.
   */
  private val speedEntries = {
    val entries = new Array[Double](entryCount)
    Arrays.fill(entries, invalidEntry)
    entries
  }

  /**
   * Current index into the speedEntries array. This value will increment and at the maximum wrap around back to 0.
   */
  private var currentIndex = 0

  /**
   * Bits accumulated while millisecondsAccumulated is below the timeFidelityMs threshold.
   */
  private var bitsAccumulated = 0L

  /**
   * When the milliseconds accumulated equals or exceeds timeFidelityMs, then its time to add another entry to
   * speedEntries.
   */
  private var millisecondsAccumulated = 0L

  /**
   * Calculates the number of bits transferred over the number of milliseconds elapsed and stores the result. This
   * method should be called multiple times during an operation to provide an average speed from the speed() method.
   */
  def update(bits: Int, millisecondsElapsed: Long) {
    synchronized {
      bitsAccumulated += bits
      millisecondsAccumulated += millisecondsElapsed
      if (millisecondsAccumulated >= timeFidelityMs) {
        val duration = math.abs(millisecondsAccumulated)
        speedEntries(currentIndex) = math.max(0, bitsAccumulated).toDouble / duration
        currentIndex = if (currentIndex == entryCount - 1) 0 else currentIndex + 1
        lastEpoch = lastEpoch.plus(millisecondsAccumulated)
        bitsAccumulated = 0
        millisecondsAccumulated = 0
      }
    }
  }

  def update(bits: Int, elapsed: Duration) { update(bits, elapsed.getMillis) }

  def update(bits: Int, epoch: DateTime) { update(bits, new Duration(lastEpoch, epoch)) }

  def update(bits: Int) { update(bits, new DateTime) }

  /**
   * Calculates the speed in bits per millisecond by averaging all collected samples.
   */
  def speed(): Double = {
    synchronized {
      val validEntries = speedEntries.filter(_ != invalidEntry)
      validEntries.sum / validEntries.length.toDouble
    }
  }

  def reset() {
    synchronized {
      currentIndex = 0
      bitsAccumulated = 0
      millisecondsAccumulated = 0
      Arrays.fill(speedEntries, invalidEntry)
    }
  }
}