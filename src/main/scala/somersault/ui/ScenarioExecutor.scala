package somersault.ui

import java.io.File
import org.apache.commons.io.{FilenameUtils, FileUtils}
import org.apache.commons.lang.StringUtils
import scala.Predef._
import somersault.core.actions.containers.{H2ActionContainerBuilder, H2ActionContainerBuilderData, ActionContainerBuilder}
import somersault.core.filesystems._
import somersault.core.{Scenario, ScenarioData}
import somersault.util.FilenameUtils2

/**
 * A helper object that contains various bits of data used when executing a Scenario.
 *
 * Only one of these classes should be created for any execution of a scenario, and only one scenario should be
 * executed at a time, but the same scenario can be used in multiple instances at different times.
 */
abstract class ScenarioExecutor(
  val scenarioData: ScenarioData, val scenarioOptions: ScenarioOptions, val observer: ObserverConsolidator) {

  import ScenarioExecutor._

  // this is a field so this can be cleaned up in dispose(). we use null references instead of Option because it makes
  // the code simpler. in general neither of these will be null
  private var fileSystems: ScenarioPair[FileSystem] = new ScenarioPair[FileSystem]

  /**
   * Executes the scenario. Each implementation will perform different behaviors.
   */
  def execute() {
    try {
      observer.logger.alert("Executing scenario: " + scenarioData.name)
      if (scenarioOptions.dryRun)
        observer.logger.alert("!!! DRY RUN !!!")

      executeImpl()
    }
    finally {
      dispose()
    }
  }

  /**
   * Executes the specific function of this implementation.
   */
  protected def executeImpl()

  /**
   * Cleans up the ScenarioExecutor. This must be called when done with this instance.
   */
  def dispose() {
    fileSystems.all().foreach(
      fs =>
        try {
          fs.disconnect()
        }
        catch {
          case ex: Exception => observer.logger.error("Error on disconnecting source ", ex)
        })

    fileSystems.all().map(_.runtimeData.tempPath).foreach(f => if (f != null) FileUtils.forceDeleteOnExit(f))
  }

  /**
   * Builds the {@link Scenario}. This method should only be executed once. This will acquire the proper locks for the
   * {@link FileSystem}s.
   *
   * Note: This will swap the source and dest if this is dry-run.
   *
   * @param sourceMode The lock mode for the source { @link FileSystem}. If None, no locking will be performed.
   * @param destMode The lock mode for the dest { @link FileSystem}. If None, no locking will be performed.
   */
  protected def createScenario(
    sourceMode: Option[LockingFileSystemMode.Mode], destMode: Option[LockingFileSystemMode.Mode]): (Scenario, File) = {
    val tempPath = createTempPath(scenarioData.name)

    def mkTempPath(suffix: String): File = {
      val file = new File(FilenameUtils.concat(tempPath.getAbsolutePath, suffix))
      FileUtils.forceMkdir(file)
      file
    }

    var fsRuntimeData = new ScenarioPair[RuntimeFileSystemData](
      new RuntimeFileSystemData(observer.fileSystem, mkTempPath("src")),
      new RuntimeFileSystemData(observer.fileSystem, mkTempPath("dest")))

    val connectMsg = "Connecting to %s as the %s (Lock Mode: %s)..."
    def modeStr(mode: Option[LockingFileSystemMode.Mode]) =
      if (mode.isEmpty) "none" else if (mode.get == LockingFileSystemMode.Read) "read-only" else "read-write"

    observer.logger.info(
      String.format(connectMsg, scenarioData.source.name, "source", modeStr(sourceMode)))
    fileSystems = {
      val fs = FileSystemProvider.connect(scenarioData.source, fsRuntimeData.source)
      fileSystems.setSource(wrapFileSystem(fs))
    }
    observer.logger.info(
      String.format(connectMsg, scenarioData.dest.name, "destination", modeStr(destMode)))
    fileSystems = {
      val fs = FileSystemProvider.connect(scenarioData.dest, fsRuntimeData.dest)
      fileSystems.setDest(wrapFileSystem(fs))
    }

    if (scenarioOptions.restore) {
      fileSystems = fileSystems.swap()
      fsRuntimeData = fsRuntimeData.swap()
    }

    if (scenarioOptions.dryRun)
      fileSystems = fileSystems.setDest(
        DryRunFileSystem.connect(new DryRunFileSystemData(fileSystems.dest), fileSystems.dest.runtimeData))

    if (sourceMode.isDefined && !fileSystems.source.lock(sourceMode.get))
      throw new IllegalStateException("Cannot lock source FileSystem for " + sourceMode)
    if (destMode.isDefined && !fileSystems.dest.lock(destMode.get))
      throw new IllegalStateException("Cannot lock destination FileSystem for " + destMode)

    (new Scenario(scenarioData, fileSystems.source, fileSystems.dest), tempPath)
  }

  /**
   * Creates a temporary path for this particular run of this scenario. This path should be used as the root temp path
   * for all operations in the scenario.
   */
  protected def createTempPath(scenarioName: String): File = {
    val uniqueStamp = scenarioName + "." + StringUtils.leftPad(
      Integer.toHexString(new java.util.Random().nextInt), 4, '0')
    val tempPath = new File(FilenameUtils.concat(FilenameUtils2.tempPath.getAbsolutePath, uniqueStamp))
    FileUtils.forceMkdir(tempPath)
    observer.logger.debug("Created temporary path: " + tempPath.getAbsolutePath)
    tempPath
  }

  /**
   * Wraps the provided {@link FileSystem} with various wrappers, including {@link ConnectCheckingFileSystem}
   * and {@link LockCheckingFileSystem}.
   */
  protected def wrapFileSystem(fs: FileSystem): FileSystem = {
    val lockingFs = new LockCheckingFileSystem(fs)
    val lockRefreshFs = new LockRefreshingFileSystem(lockingFs, fs.data.lockRefreshSupportData)
    new ConnectCheckingFileSystem(lockRefreshFs)
  }

  /**
   * Creates and returns an ActionContainerBuilder.
   */
  protected def createActionContainer(tempPath: File): ActionContainerBuilder = {
    val tempFile = FilenameUtils2.createTempFile("actioncontainer", "", tempPath)
    observer.logger.debug("Created temporary file: " + tempFile.getAbsolutePath)
    val actionContainerData = new H2ActionContainerBuilderData(
      observer.logger, tempFile) // TODO need to decide AC class from scenario file
    new H2ActionContainerBuilder(actionContainerData)
  }
}

object ScenarioExecutor {

  class ScenarioPair[T](val source: T = null, val dest: T = null) {

    def setSource(newSource: T): ScenarioPair[T] = new ScenarioPair[T](newSource, dest)

    def setDest(newDest: T): ScenarioPair[T] = new ScenarioPair[T](source, newDest)

    def swap(): ScenarioPair[T] = new ScenarioPair[T](dest, source)

    def all(): Seq[T] = Seq(source, dest)
  }

}