package somersault.ui.util

/**
 * Modeled after the C# PropertyDescriptor class, this encapsulates the field for an object, allowing get/set for a
 * particular field but for any object. In other words it flips the statement "obj.getX()" to "getX( obj )", which is the
 * question each cell for a particular column needs to do. Each column gets a single PropertyDescriptor to figure out
 * the value for each cell.
 *
 * This is designed to only support reading, but could be modified to support writing without much trouble
 */
trait DataDescriptor[ E ]
{
  /**Returns the value of this descriptor for the given object */
  def getValue( e: E): Object

  /**Returns the class of the value of this descriptor for the given object */
  def valueClass: Class[ _ ]

  /**
   * Returns the user-friendly name of this column, which will be used as the header.
   */
  def name: String
}
