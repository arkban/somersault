package somersault.ui.util

import javax.swing.table.TableColumnModel;

/**A  {@link TableColumnModel} that provides {@link DataDescriptor}s for each column */
trait DataDescriptorColumnModel[ E ] extends TableColumnModel
{
  def getDescriptor( columnIndex: Int): DataDescriptor[ E ]
}
