package somersault.ui.util

import collection.mutable
import javax.swing.table.AbstractTableModel

/** A TableModel that uses DataDescriptors to get data from the objects given to it */
abstract class DataDescriptorTableModel[E](columnModel: DataDescriptorColumnModel[E])
  extends AbstractTableModel {

  protected val data = new mutable.ListBuffer[E]

  def getColumnCount = columnModel.getColumnCount

  def getRowCount = data.size

  def getValueAt(rowIndex: Int, columnIndex: Int): Object = {
    if (rowIndex < 0 || rowIndex >= getRowCount) return "Invalid row index"
    if (columnIndex < 0 || columnIndex >= getColumnCount()) return "Invalid column index"

    val e = data(rowIndex)
    val desc = columnModel.getDescriptor(columnIndex)
    desc.getValue(e)
  }

  override def getColumnName(columnIndex: Int): String =
    if (columnIndex < 0 || columnIndex >= getColumnCount) "Invalid column index"
    else columnModel.getColumn(columnIndex).getHeaderValue.toString

  override def getColumnClass(columnIndex: Int): Class[_] =
    if (columnIndex < 0 || columnIndex >= getColumnCount) classOf[Object]
    else columnModel.getDescriptor(columnIndex).valueClass
}
