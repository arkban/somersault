package somersault.ui.swing.progress

import javax.swing.{JLabel, BorderFactory, JProgressBar, JComponent}
import logahawk.Logger
import net.miginfocom.swing.MigLayout
import somersault.util.FilenameUtils2
import swing.Swing

/**
 * Displays several {@link ProgressBar}s for different parts of the process.
 */
class ProgressInfo(logger: Logger) extends JComponent {

  protected val lblPhase = new JLabel(" ")

  private val prgPhase = new JProgressBar

  protected val lblParentPath = new JLabel(" ")

  protected val lblCurrPath = new JLabel(" ")

  protected val lblAction = new JLabel(" ")

  private val prgFile = new JProgressBar

  setup()

  /**
   * Initializes for the start of a phase. This does not need to be called on the EDT.
   */
  def startPhase(phaseLabel: String, indeterminate: Boolean, min: Long, max: Long) {
    Swing.onEDT(
    {
      try {
        lblPhase.setText(phaseLabel)
        lblParentPath.setText(" ")
        lblCurrPath.setText(" ")
        prgPhase.setString(" ")
        prgPhase.setIndeterminate(indeterminate)
        prgPhase.setMinimum(min.toInt)
        prgPhase.setValue(min.toInt)
        prgPhase.setMaximum(max.toInt)
      }
      catch {
        case ex: Exception => logger.error("GUI error", ex)
      }
    })
  }

  def endPhase() {
    Swing.onEDT(
    {
      try {
        lblPhase.setText(" ")
        lblParentPath.setText(" ")
        lblAction.setText(" ")
        lblCurrPath.setText(" ")
        prgPhase.setString(formatPercent(100f))
        prgPhase.setIndeterminate(false)
        prgPhase.setMinimum(0)
        prgPhase.setValue(1)
        prgPhase.setMaximum(1)
      }
      catch {
        case ex: Exception => logger.error("GUI error", ex)
      }
    })
  }

  /**
   * Initializes for the start of a File. This does not need to be called on the EDT.
   */
  def startFile(indeterminate: Boolean, min: Long, max: Long) {
    Swing.onEDT(
    {
      try {
        prgFile.setString(" ")
        prgFile.setIndeterminate(indeterminate)
        prgFile.setMinimum(min.toInt)
        prgFile.setValue(min.toInt)
        prgFile.setMaximum(max.toInt)
      }
      catch {
        case ex: Exception => logger.error("GUI error", ex)
      }
    })
  }

  def endFile() {
    Swing.onEDT(
    {
      try {
        prgFile.setString(formatPercent(100f))
        prgFile.setIndeterminate(false)
        prgFile.setMinimum(0)
        prgFile.setValue(1)
        prgFile.setMaximum(1)
      }
      catch {
        case ex: Exception => logger.error("GUI error", ex)
      }
    })
  }

  private def setup() {
    lblPhase.setText("Initializing")
    lblPhase.setBorder(BorderFactory.createLoweredBevelBorder)
    prgPhase.setIndeterminate(true)
    prgPhase.setStringPainted(true)
    lblParentPath.setBorder(BorderFactory.createLoweredBevelBorder)
    lblAction.setBorder(BorderFactory.createLoweredBevelBorder)
    lblCurrPath.setBorder(BorderFactory.createLoweredBevelBorder)
    prgFile.setIndeterminate(false)
    prgFile.setStringPainted(true)
    prgFile.setString(" ")

    this.setLayout(new MigLayout)
    this.add(new JLabel("Phase"), "right")
    this.add(lblPhase, "width 0:10000:10000, wrap")
    this.add(prgPhase, "span, growx, wrap")
    this.add(new JLabel("Path"), "right")
    this.add(lblParentPath, "span, width 0:10000:10000, wrap")
    this.add(lblAction, "right, width 100!")
    this.add(lblCurrPath, "span, width 0:10000:10000, wrap")
    this.add(prgFile, "span, growx, wrap")
  }

  /**
   * Increments by one the Phase ProgressBar, updates the path of the file (which is constant for this increment),
   * This does not need to be called on the EDT.
   */
  def incrementPhaseProgress(path: Iterable[String], actionName: String) {
    Swing.onEDT(
    {
      try {
        val parentPath = if (path.size > 1) path.take(path.size - 1) else " " :: Nil
        val parentPathStr = FilenameUtils2.dirSeparator + FilenameUtils2.concat(parentPath)
        lblParentPath.setText(parentPathStr)
        lblAction.setText(actionName)
        lblCurrPath.setText(path.last)
        incrementProgress(prgPhase, 1)
      }
      catch {
        case ex: Exception => logger.error("GUI error", ex)
      }
    })
  }

  /**
   * Increments by one the File ProgressBar
   */
  def incrementFileProgress(amount: Int) {
    Swing.onEDT(
    {
      try {
        incrementProgress(prgFile, amount)
      }
      catch {
        case ex: Exception => logger.error("GUI error", ex)
      }
    })
  }

  private def incrementProgress(prg: JProgressBar, amount: Int) {
    if (!prg.isIndeterminate) {
      val value = prg.getValue + amount
      prg.setValue(value)
      val percent = value.toFloat / prg.getMaximum.toFloat
      prg.setString(formatPercent(percent * 100))
    }
  }

  private def formatPercent(value: Float) = "%.2f%".format(value.toDouble)
}
