package somersault.ui.swing.progress

import java.util.concurrent.atomic.AtomicLong
import javax.swing.JComponent
import logahawk.{Logger, Severity}
import net.miginfocom.swing.MigLayout
import somersault.core.ScenarioData
import somersault.core.actions.builders.ActionBuilderObserver
import somersault.core.actions.runners.ActionRunnerObserver
import somersault.core.actions.{DirectoryAction, RemoveAction, CreateDirAction, TransferAction}
import somersault.core.comparators.SomerFileComparator
import somersault.core.filesystems.SomerFile
import somersault.core.filters.SomerFilter
import somersault.ui.swing.ClosableComponent
import somersault.ui.{ScenarioOptions, SpeedCalculator, ObserverConsolidator, UIObserver}
import somersault.util.{ObservableInputStream, ObservableOutputStream, ObservableInputStreamListener, ObservableOutputStreamListener}

/**
 * Displays the progress of the Scenario currently being executed.
 */
class ScenarioProgressPanel(
  scenarioData: ScenarioData,
  scenarioOptions: ScenarioOptions,
  consolidator: ObserverConsolidator,
  logger: Logger)
  extends JComponent with ClosableComponent {

  private val speedCalc = new SpeedCalculator()

  private val statusInfo = new StatusInfo(logger, speedCalc)

  private val errorCount = new AtomicLong(0)

  setup()

  def close() {}

  def isClosable = false

  private def error(severity: Severity, ex: Exception) {
    statusInfo.error(errorCount.incrementAndGet)
  }

  private def setup() {
    val overviewInfo = new OverviewInfo(scenarioData, scenarioOptions)

    val progress = new ProgressInfo(logger)
    consolidator.actionBuilder.add(new ActionBuilderObs(progress))
    consolidator.actionRunner.add(new ActionRunnerObs(progress))
    consolidator.ui.add(new UIObs(statusInfo))

    val streamObs = new InputOutputStreamObs(progress, speedCalc)
    consolidator.inputStream.add(streamObs)
    consolidator.outputStream.add(streamObs)

    setLayout(new MigLayout("wrap"))
    add(overviewInfo, "width 0:10000:10000")
    add(progress, "width 0:10000:10000")
    add(statusInfo, "width 0:10000:10000")
  }

  private class UIObs(statusInfo: StatusInfo) extends UIObserver {

    def error(severity: Severity, ex: Exception) { ScenarioProgressPanel.this.error(severity, ex) }

    def log(severity: Severity, objs: Any*) {}

    def start() { statusInfo.start() }

    def end() { statusInfo.end() }
  }

  private class ActionBuilderObs(progress: ProgressInfo) extends ActionBuilderObserver {

    def start() { progress.startPhase("Building Actions", indeterminate = true, min = 0, max = 0) }

    def end() { progress.endPhase() }

    def recurse(path: List[String]) { progress.incrementPhaseProgress(path, "Searching") }

    def filterAccept(filter: SomerFilter, path: List[String]) {}

    def filterReject(filter: SomerFilter, path: List[String]) {}

    def comparatorAccept(comparator: SomerFileComparator, path: List[String]) {}

    def comparatorReject(comparator: SomerFileComparator, path: List[String]) {}

    def error(severity: Severity, ex: Exception) { ScenarioProgressPanel.this.error(severity, ex) }
  }

  private class ActionRunnerObs(progress: ProgressInfo) extends ActionRunnerObserver {

    def start(actionCount: Long) {
      progress.startPhase("Executing Actions", indeterminate = false, min = 0, max = actionCount)
    }

    def end() { progress.endPhase() }

    def recurse(path: List[String], action: DirectoryAction) {
      progress.incrementPhaseProgress(path ::: action.name :: Nil, "Working")
    }

    def transfer(path: List[String], action: TransferAction, file: SomerFile) {
      progress.incrementPhaseProgress(path ::: action.name :: Nil, "Transfer")
    }

    def createDir(path: List[String], action: CreateDirAction) {
      progress.incrementPhaseProgress(path ::: action.name :: Nil, "Create Dir")
    }

    def remove(path: List[String], action: RemoveAction) {
      progress.incrementPhaseProgress(path ::: action.name :: Nil, "Remove")
    }

    def error(severity: Severity, ex: Exception) { ScenarioProgressPanel.this.error(severity, ex) }
  }

  private class InputOutputStreamObs(progress: ProgressInfo, speedCalc: SpeedCalculator)
    extends ObservableInputStreamListener with ObservableOutputStreamListener {

    /**
     * Keeps track of whether we are watching the input or output streams. (We can't watch both, otherwise we start
     * double counting.)
     */
    private var inputFalseOutputTrue = false

    override def init(input: ObservableInputStream, length: Option[Long]) {
      init(inputFalseOutputTrue = false, length = length)
    }

    override def read(input: ObservableInputStream, b: Array[Byte], off: Int, len: Int, readResult: Int) {
      if (!inputFalseOutputTrue && readResult > 0) increment(readResult)
    }

    override def close(input: ObservableInputStream) { if (!inputFalseOutputTrue) progress.endFile() }

    override def init(output: ObservableOutputStream, length: Option[Long]) {
      init(inputFalseOutputTrue = true, length = length)
    }

    override def write(output: ObservableOutputStream, b: Array[Byte], off: Int, len: Int) {
      if (inputFalseOutputTrue) increment(len)
    }

    override def close(output: ObservableOutputStream) { if (inputFalseOutputTrue) progress.endFile() }

    private def init(inputFalseOutputTrue: Boolean, length: Option[Long]) {
      this.inputFalseOutputTrue = inputFalseOutputTrue
      speedCalc.reset()
      length match {
        case Some(len: Long) => progress.startFile(indeterminate = false, min = 0, max = len)
        case None => progress.startFile(indeterminate = true, min = 0, max = 1)
      }
    }

    private def increment(bytesRead: Int) {
      try {
        progress.incrementFileProgress(bytesRead)
        speedCalc.update(bytesRead * 8) // bytes to bits
      }
      catch {
        case ex: Exception => ScenarioProgressPanel.this.logger.error("GUI error", ex)
      }
    }
  }

}