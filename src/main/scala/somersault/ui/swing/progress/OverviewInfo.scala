package somersault.ui.swing.progress

import java.awt.GridLayout
import javax.swing.{BorderFactory, JPanel, JLabel, JComponent}
import net.miginfocom.swing.MigLayout
import somersault.core.ScenarioData
import somersault.ui.ScenarioOptions
import somersault.ui.swing.{FileSystemInfo, SwingUtil}

/**
 * Displays information about the {@link Scenario}, using two {@link FileSystemInfo} and other Swing components.
 */
class OverviewInfo(scenarioData: ScenarioData, scenarioOptions: ScenarioOptions) extends JComponent {

  setup()

  private def setup() {
    val basicPanel = new JPanel(new MigLayout("wrap 4", "[][:50%:,grow, fill][][:50%:,grow, fill]"))
    basicPanel.add(new JLabel("Name"))
    basicPanel.add(SwingUtil.createTextField(scenarioData.name, editable = false))
    basicPanel.add(new JLabel("Description:"))
    basicPanel.add(SwingUtil.createTextField(scenarioData.description, editable = false))

    val optionsPanel = new JPanel(
      new MigLayout("wrap 6", "[][:50%:,grow, fill][][:50%:,grow, fill][][:50%:,grow, fill]"))
    optionsPanel.add(new JLabel("Type:"))
    optionsPanel.add(
      SwingUtil.createTextField(if (scenarioOptions.restore) "Restore" else "Backup", editable = false))
    optionsPanel.add(new JLabel("Build Only:"))
    optionsPanel.add(SwingUtil.createTextField(if (scenarioOptions.buildOnly) "Yes" else "No", editable = false))
    optionsPanel.add(new JLabel("Dry Run"))
    optionsPanel.add(SwingUtil.createTextField(if (scenarioOptions.dryRun) "Yes" else "No", editable = false))


    val sourcePanel = new FileSystemInfo(scenarioData.source)
    sourcePanel.setBorder(BorderFactory.createTitledBorder(scenarioOptions.getSourceTag(scenarioData)))
    val destPanel = new FileSystemInfo(scenarioData.dest)
    destPanel.setBorder(BorderFactory.createTitledBorder(scenarioOptions.getDestTag(scenarioData)))
    val fsPanel = new JPanel(new GridLayout(1, 2))
    fsPanel.add(sourcePanel)
    fsPanel.add(destPanel)

    setLayout(new MigLayout(""))
    add(basicPanel, "grow, wrap")
    add(fsPanel, "grow, wrap")
  }
}
