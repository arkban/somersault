package somersault.ui.swing.progress

import java.awt.event.{ActionEvent, ActionListener}
import javax.swing.{JLabel, BorderFactory, Timer, JComponent}
import logahawk.Logger
import net.miginfocom.swing.MigLayout
import org.joda.time.{DateTime, Period}
import somersault.ui.SpeedCalculator
import somersault.ui.swing.SwingUtil
import swing.Swing

/**
 * Displays various statistics that are accumulated during the the process being watched. Currently this displays:
 * - Elapsed Time
 * - Transfer Speed
 * - Error Count
 * - Memory Used
 * - Maximum Memory Used
 */
class StatusInfo(logger: Logger, speedCalc: SpeedCalculator) extends JComponent {

  private val lblErrorCount = SwingUtil.createTextField("0", editable = false)

  private val lblTransferSpeed = SwingUtil.createTextField("...", editable = false)

  private val lblTimeElapsed = SwingUtil.createTextField("...", editable = false)

  private val lblMemoryUsed = SwingUtil.createTextField("...", editable = false)

  private val lblMemoryMax = SwingUtil.createTextField("...", editable = false)

  private var startTime = new DateTime

  private val timer = new Timer(
    250, new ActionListener {
      def actionPerformed(e: ActionEvent) {
        val period = new Period(startTime, new DateTime)
        lblTimeElapsed.setText("%01d:%02d:%02d".format(period.getHours, period.getMinutes, period.getSeconds))
        updateTransferSpeed(speedCalc.speed())
        lblMemoryUsed.setText(
          "%.2f MB/s".format((Runtime.getRuntime.totalMemory - Runtime.getRuntime.freeMemory) / 1048576f))
        lblMemoryMax.setText(
          "%.2f MB/s".format(Runtime.getRuntime.totalMemory / 1048576f))
      }
    })

  setup()

  def start() {
    Swing.onEDT(
    {
      try {
        startTime = new DateTime
        timer.start()
        updateTransferSpeed(0)
      }
      catch {
        case ex: Exception => logger.error("GUI error", ex)
      }
    })
  }

  def end() {
    Swing.onEDT(
    {
      try {
        timer.stop()
      }
      catch {
        case ex: Exception => logger.error("GUI error", ex)
      }
    })
  }

  def error(errorCount: Long) {
    Swing.onEDT(
    {
      try {
        lblErrorCount.setText(errorCount.toString)
      }
      catch {
        case ex: Exception => logger.error("GUI error", ex)
      }
    })
  }

  private def updateTransferSpeed(bitsPerMs: Double) {
    val bitsPerMsToKBytesPerSecMultiplier = (1024d / 1000d) * 8

    val bitStr = (
      if (bitsPerMs.isNaN || bitsPerMs.isInfinity) "??? KB/s"
      else "%.2f KB/s".format(bitsPerMs / bitsPerMsToKBytesPerSecMultiplier))

    lblTransferSpeed.setText(bitStr)
  }

  private def setup() {
    lblTimeElapsed.setBorder(BorderFactory.createLoweredBevelBorder)
    lblTransferSpeed.setBorder(BorderFactory.createLoweredBevelBorder)
    lblErrorCount.setBorder(BorderFactory.createLoweredBevelBorder)
    lblMemoryUsed.setBorder(BorderFactory.createLoweredBevelBorder)
    lblMemoryMax.setBorder(BorderFactory.createLoweredBevelBorder)

    this.setLayout(new MigLayout("wrap 6", "[][:50%:,grow, fill][][:50%:,grow, fill][][:50%:,grow, fill]"))

    this.add(new JLabel("Elapsed"))
    this.add(lblTimeElapsed)
    this.add(new JLabel("Speed"))
    this.add(lblTransferSpeed)
    this.add(new JLabel("Errors"))
    this.add(lblErrorCount)
    this.add(new JLabel("Mem Used"))
    this.add(lblMemoryUsed)
    this.add(new JLabel("Mem Max"))
    this.add(lblMemoryMax)
  }
}