package somersault.ui.swing

import javax.swing.{JComponent, JLabel}
import net.miginfocom.swing.MigLayout
import somersault.core.filesystems.FileSystemData

/**
 * Displays information about a single {@link FileSystem}.
 */
class FileSystemInfo(fsData: FileSystemData) extends JComponent {

  setup()

  private def setup() {
    setLayout(new MigLayout("wrap 4", "[][:50%:,grow, fill][][:50%:,grow, fill]"))

    add(new JLabel("Name"))
    add(SwingUtil.createTextField(fsData.name, editable = false))
    add(new JLabel("Indexed:"))
    add(SwingUtil.createTextField(if (fsData.isIndexed) "Yes" else "No", editable = false))

    add(new JLabel("Hash Name:"))
    add(SwingUtil.createTextField(fsData.hashInfo.name, editable = false))
    add(new JLabel("Hash Size:"))
    add(SwingUtil.createTextField(Predef.int2Integer(fsData.hashInfo.size).toString, editable = false))

    add(new JLabel("Compression:"))
    add(SwingUtil.createTextField(fsData.compressionProvider.name, editable = false), "span")

    add(new JLabel("Encryption:"))
    add(SwingUtil.createTextField(fsData.encryptionProvider.name, editable = false))
    add(new JLabel("Key:"))
    add(SwingUtil.createTextField(fsData.encryptionProvider.key.name, editable = false))
  }
}