package somersault.ui.swing.browser

import javax.swing.event.{TableModelEvent, TableModelListener, ListSelectionEvent, ListSelectionListener}
import javax.swing.{JLabel, JTable, JComponent}
import net.miginfocom.swing.MigLayout
import somersault.core.filesystems.SomerFile
import somersault.ui.swing.SwingUtil

/**
 * Displays information about the {@link SomerArtifacts} displayed in owning {@link FileSystemTableModel}.
 */
class FileSystemTableStatusBar(table: JTable, tableModel: FileSystemTableModel) extends JComponent {

  private val totalCount = SwingUtil.createTextField("0", editable = false)

  private val totalSize = SwingUtil.createTextField(SwingUtil.toBytes(0L), editable = false)

  private val selectedCount = SwingUtil.createTextField("0", editable = false)

  private val selectedSize = SwingUtil.createTextField(SwingUtil.toBytes(0L), editable = false)

  setup()

  private def setup() {
    tableModel.addTableModelListener(
      new TableModelListener {
        def tableChanged(e: TableModelEvent) {
          var count = 0
          var size = 0L
          tableModel.dataView().foreach(
            artifact => {
              count += 1
              size += (
                artifact match {
                  case f: SomerFile => f.size
                  case _ => 0L
                })
            })
          totalCount.setText(count.toString)
          totalSize.setText(SwingUtil.toBytes(size))

          FileSystemTableStatusBar.this.invalidate()
        }
      })

    table.getSelectionModel.addListSelectionListener(
      new ListSelectionListener {
        def valueChanged(e: ListSelectionEvent) {
          var count = 0
          var size = 0L

          val data = tableModel.dataView()
          table.getSelectedRows.foreach(
            index => {
              val artifact = data(table.getRowSorter.convertRowIndexToModel(index))

              count += 1
              size += (
                artifact match {
                  case f: SomerFile => f.size
                  case _ => 0L
                })
            })

          selectedCount.setText(count.toString)
          selectedSize.setText(SwingUtil.toBytes(size))
          FileSystemTableStatusBar.this.invalidate()
        }
      })

    setLayout(new MigLayout("", "[][grow,fill][][grow,fill][][grow,fill][][grow,fill]"))
    add(new JLabel("Count:"))
    add(totalCount)
    add(new JLabel("Size:"))
    add(totalSize)
    add(new JLabel("Selected Count:"))
    add(selectedCount)
    add(new JLabel("Selected Size:"))
    add(selectedSize)
  }
}