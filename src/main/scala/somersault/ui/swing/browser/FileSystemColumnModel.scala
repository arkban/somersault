package somersault.ui.swing.browser

import javax.swing.table.{TableColumn, DefaultTableColumnModel}
import org.apache.commons.io.FilenameUtils
import org.joda.time.DateTime
import scala.collection.convert.Wrappers
import somersault.core.filesystems._
import somersault.core.filesystems.index.{IndexFile, IndexArtifact}
import somersault.ui.swing.AlternatingRowColorTableCellRenderer
import somersault.ui.util.{DataDescriptor, DataDescriptorColumnModel}
import somersault.util.Hash

class FileSystemColumnModel(fs: FileSystem)
  extends DefaultTableColumnModel with DataDescriptorColumnModel[SomerArtifact] {

  private val descriptorMap = buildColumns()

  def getDescriptor(columnIndex: Int) = descriptorMap(columnIndex)

  private def buildColumns(): Map[Int, DataDescriptor[SomerArtifact]] = {
    val indexedDescMap =
      if (fs.data.isIndexed)
        Map(
          7 -> new IndexArtifactDataDescriptor(fs, new StoredNameDataDescriptor),
          8 -> new IndexArtifactDataDescriptor(fs, new IndexFileDataDescriptor(new CompressedDataDescriptor)),
          9 -> new IndexArtifactDataDescriptor(fs, new IndexFileDataDescriptor(new EncryptedDataDescriptor)))
      else
        Map.empty[Int, DataDescriptor[SomerArtifact]]

    val descriptorMap = Map(
      0 -> new NameDataDescriptor,
      1 -> new TypeDataDescriptor,
      2 -> new FileDataDescriptor(new SizeDataDescriptor),
      3 -> new FileDataDescriptor(new ModifiedDateDescriptor),
      4 -> new ExtensionDataDescriptor,
      5 -> new FileDataDescriptor(new AttributesDataDescriptor),
      6 -> new FileDataDescriptor(new HashDataDescriptor(fs))) ++ indexedDescMap

    val cellRenderer = new AlternatingRowColorTableCellRenderer

    // there is no clear()...
    Wrappers.JEnumerationWrapper(getColumns).foreach(c => removeColumn(c))

    (0 until descriptorMap.size).foreach(
      index => {
        val c = new TableColumn(index)
        c.setResizable(true)
        c.setHeaderValue(descriptorMap(index).name)
        c.setCellRenderer(cellRenderer)
        addColumn(c)
      })

    descriptorMap
  }
}

class NameDataDescriptor extends DataDescriptor[SomerArtifact] {

  def valueClass = classOf[String]

  def getValue(e: SomerArtifact) = e.name

  def name = "Name"
}

class ExtensionDataDescriptor extends DataDescriptor[SomerArtifact] {

  def valueClass = classOf[String]

  def getValue(e: SomerArtifact) = FilenameUtils.getExtension(e.name)

  def name = "Ext"
}

class TypeDataDescriptor extends DataDescriptor[SomerArtifact] {

  def valueClass = classOf[String]

  def getValue(e: SomerArtifact) = e match {
    case f: SomerFile => "File"
    case d: SomerDirectory => "Directory"
    case _ => throw new IllegalArgumentException("unknown SomerArtifact implementation")
  }

  def name = "Type"
}

class FileDataDescriptor(data: DataDescriptor[SomerFile]) extends DataDescriptor[SomerArtifact] {

  def valueClass = data.valueClass

  def getValue(e: SomerArtifact) = e match {
    case f: SomerFile => data.getValue(f)
    case _ => null
  }

  def name = data.name
}

class SizeDataDescriptor extends DataDescriptor[SomerFile] {

  /** This returns {@link java.lang.Long), the core type, because this will be used to look up a comparator */
  def valueClass = classOf[java.lang.Long]

  def getValue(f: SomerFile) = new java.lang.Long(f.size)

  def name = "Size"
}

class ModifiedDateDescriptor extends DataDescriptor[SomerFile] {

  def valueClass = classOf[DateTime]

  def getValue(f: SomerFile) = f.modified

  def name = "Modified"
}

class AttributesDataDescriptor extends DataDescriptor[SomerFile] {

  def valueClass = classOf[String]

  def getValue(f: SomerFile) = f.attributes.toString

  def name = "Attributes"
}

class HashDataDescriptor(fs: FileSystem) extends DataDescriptor[SomerFile] {

  def valueClass = classOf[Hash]

  def getValue(f: SomerFile) = f.hash.toString(hashInfoPrefix = false)

  def name = "Hash"
}

class IndexArtifactDataDescriptor(fs: FileSystem, private val data: DataDescriptor[IndexArtifact])
  extends DataDescriptor[SomerArtifact] {

  def valueClass = data.valueClass

  def getValue(a: SomerArtifact) = fs.getIndexArtifact(a) match {
    case Some(i: IndexArtifact) => data.getValue(i)
    case _ => null
  }

  def name = data.name
}

class StoredNameDataDescriptor extends DataDescriptor[IndexArtifact] {

  def valueClass = classOf[String]

  def getValue(a: IndexArtifact) = a.storedName

  def name = "Stored Name"
}

class IndexFileDataDescriptor(data: DataDescriptor[IndexFile])
  extends DataDescriptor[IndexArtifact] {

  def valueClass = data.valueClass

  def getValue(i: IndexArtifact) = i match {
    case f: IndexFile => data.getValue(f)
    case _ => null
  }

  def name = data.name
}

class CompressedDataDescriptor extends DataDescriptor[IndexFile] {

  def valueClass = classOf[Boolean]

  def getValue(f: IndexFile) = new java.lang.Boolean(f.compressed)

  def name = "Compressed"
}

class EncryptedDataDescriptor extends DataDescriptor[IndexFile] {

  def valueClass = classOf[Boolean]

  def getValue(f: IndexFile) = new java.lang.Boolean(f.encrypted)

  def name = "Encrypted"
}

