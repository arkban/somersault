package somersault.ui.swing.browser

import javax.swing.ImageIcon
import javax.swing.tree.DefaultTreeCellRenderer

class FileSystemTreeCellRenderer extends DefaultTreeCellRenderer {

  setIcon(FileSystemTreeCellRenderer.imageIcon) // doesn't seem to do anything...
  setOpenIcon(FileSystemTreeCellRenderer.imageIcon)
  setClosedIcon(FileSystemTreeCellRenderer.imageIcon)
}

object FileSystemTreeCellRenderer {

  /**
   * Somersault's icon.
   */
  lazy val imageIcon = {
    try {
      // found at http://findicons.com/icon/261256/folder_open?id=261653
      new ImageIcon(getClass.getResource("/folder.png"))
    }
    catch {
      case _: Exception => new ImageIcon
    }
  }
}