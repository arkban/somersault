package somersault.ui.swing.browser

import somersault.ui.util.DataDescriptorTableModel
import somersault.core.filesystems.{SomerDirectory, SomerArtifact, FileSystem}

class FileSystemTableModel( fs: FileSystem, val columnModel: FileSystemColumnModel)
    extends DataDescriptorTableModel[ SomerArtifact ]( columnModel)
{
  /**
   * Read-only view of the data in the table.
   */
  def dataView( ) = data.view

  def set( dir: SomerDirectory) =
  {
    val children = fs.children( dir)

    data.clear
    data ++= children

    fireTableDataChanged
  }
}
