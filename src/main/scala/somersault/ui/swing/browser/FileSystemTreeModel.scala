package somersault.ui.swing.browser

import javax.swing.event.TreeModelListener
import javax.swing.tree.{TreePath, TreeModel}
import org.apache.commons.lang.IllegalClassException
import somersault.core.filesystems.{SomerDirectory, FileSystem}
import somersault.ui.swing.MultiTreeModelListener

class FileSystemTreeModel(fs: FileSystem) extends TreeModel {

  /**
   * Used for a simple implementation of the listener methods needed for a {@link TreeModel}
   */
  private val listener = new MultiTreeModelListener

  private var cache: FileSystemTreeModel.Cache = null

  def getRoot: Object = new FileSystemTreeModel.Node(fs.root, 0)

  def getChildCount(parent: AnyRef): Int = parent match {
    case node: FileSystemTreeModel.Node => getCache(node).children.size
    case _ => throw new IllegalClassException(classOf[FileSystemTreeModel.Node], parent)
  }

  def getChild(parent: AnyRef, index: Int): Object = parent match {
    case node: FileSystemTreeModel.Node => getCache(node).children(index)
    case _ => throw new IllegalClassException(classOf[FileSystemTreeModel.Node], parent)
  }

  def valueForPathChanged(path: TreePath, newValue: AnyRef) {}

  def isLeaf(node: AnyRef) = node match {
    case node: FileSystemTreeModel.Node =>
      // avoid the expensive FS check (which disrupts our cache because it is called after each child is added). If there
      // are no children, the tree will safely handle it
      false
    case _ => throw new IllegalClassException(classOf[FileSystemTreeModel.Node], node)
  }

  def getIndexOfChild(parent: AnyRef, child: AnyRef): Int = parent match {
    case node: FileSystemTreeModel.Node => getCache(node).children.indexOf(child)
    case _ => throw new IllegalClassException(classOf[FileSystemTreeModel.Node], parent)
  }

  def addTreeModelListener(l: TreeModelListener) { listener.add(l) }

  def removeTreeModelListener(l: TreeModelListener) { listener.remove(l) }

  /**
   * Returns the {@link Cache} for the provided {@link Node}. This may return the same {@link Cache} as before if
   * its the same parent node.
   */
  private def getCache(parent: FileSystemTreeModel.Node): FileSystemTreeModel.Cache = {
    if (cache == null || !parent.equals(cache.node)) {
      val dirs = fs.children(parent.dir).filter(_.isInstanceOf[SomerDirectory]) // only dirs
      val nodes = dirs.zipWithIndex.map(
          e => new FileSystemTreeModel.Node(
            e._1.asInstanceOf[SomerDirectory], e._2))

      cache = new FileSystemTreeModel.Cache(parent, nodes.toList)
    }
    cache
  }
}

object FileSystemTreeModel {

  /**
   * Every {@link Node} gets an Id that makes equality tests quick and easy
   */
  private var masterId = Int.MinValue

  /**
   * Returns the next ID
   */
  private def getId = {
    val id = masterId
    masterId = masterId + 1
    id
  }

  /**
   * The object that will be used to represent a single node in the {@link TreeModel}. This intentionally does not
   * store any reference to parents or children to use as little memory as possible.
   */
  class Node(val dir: SomerDirectory, val index: Int) {

    private val id = getId

    override def toString = {
      // check for empty (which is the root)
      if (dir.name.size == 0) "/" else dir.name
    }

    override def equals(other: Any) = other match {
      case n: Node => id == n.id
      case _ => false
    }

    override def hashCode = id
  }

  /**
   * A cache to avoid performing unnecessary look ups. An instance of this will be held by our implementation of
   * the {@link TreeModel}.
   */
  private class Cache(val node: Node, val children: List[Node])

}
