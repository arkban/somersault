package somersault.ui.swing.browser

import somersault.core.filesystems._
import javax.swing._
import event.{TreeSelectionEvent, TreeSelectionListener}
import java.awt.{Toolkit, Dimension, BorderLayout}
import somersault.ui.swing.{FileSystemInfo, SwingUtil}

class FileSystemBrowser( val fs: FileSystem) extends JFrame
{
  private def setup( )
  {
    val tree = new JTree( new FileSystemTreeModel( fs))
    tree.setRootVisible( true)
    tree.setEditable( false)
    tree.setLargeModel( true)
    tree.setCellRenderer( new FileSystemTreeCellRenderer)

    val tableModel = new FileSystemTableModel( fs, new FileSystemColumnModel( fs))
    val table = new JTable
    // this must be false otherwise AND we must manually set the models -- otherwise the columns are created
    // without cellRenderers (this is a stupid, stupid design flaw in the Swing API)
    table.setAutoCreateColumnsFromModel( false)
    table.setAutoCreateRowSorter( false)
    table.setColumnModel( tableModel.columnModel)
    table.setModel( tableModel)
    table.setRowSorter( new FileSystemTableRowSorter( tableModel))

    val rightSide = new JPanel
    rightSide.setLayout( new BorderLayout)
    rightSide.add( new FileSystemInfo( fs.data), BorderLayout.PAGE_START)
    rightSide.add( new JScrollPane( table), BorderLayout.CENTER)
    rightSide.add( new FileSystemTableStatusBar( table, tableModel), BorderLayout.PAGE_END)

    tree.addTreeSelectionListener(
      new TreeSelectionListener
      {
        def valueChanged( e: TreeSelectionEvent)
        {
          e.getPath.getLastPathComponent match
          {
            case n: FileSystemTreeModel.Node => tableModel.set( n.dir)
            case _ => throw new IllegalArgumentException( )
          }
        }
      })

    val screen = Toolkit.getDefaultToolkit.getScreenSize
    val size = new Dimension( 800, 600)

    val split = new JSplitPane( JSplitPane.HORIZONTAL_SPLIT, false, new JScrollPane( tree), rightSide)
    // must use integer because proportional doesn't take effect till AFTER loaded
    split.setDividerLocation( ( size.width * 0.382d ).toInt) // golden ratio (inverted)

    setIconImage( SwingUtil.logoIcon.getImage)
    setLayout( new BorderLayout)
    add( split, BorderLayout.CENTER)
    setTitle( "Somersault - File System Browser")
    setJMenuBar( new FileSystemBrowserMenuBar( this, fs))
    setSize( size)
    setLocation( screen.width / 2 - size.width / 2, screen.height / 2 - size.height / 2)
    setDefaultCloseOperation( WindowConstants.DISPOSE_ON_CLOSE)
  }

  setup( )
}

