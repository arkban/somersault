package somersault.ui.swing.browser

import javax.swing.table.TableRowSorter
import collection.JavaConversions
import javax.swing.{RowSorter, SortOrder}

/**
 * Sorts the {@link SomerArtifacts} inside a {@link FileSystemTableModel }. This really just adds a special sorting
 * on the Name that will group by Directory and File
 */
class FileSystemTableRowSorter(tableModel: FileSystemTableModel)
  extends TableRowSorter[FileSystemTableModel](tableModel) {

  // by default sort on name
  setSortKeys(JavaConversions.seqAsJavaList(Seq(new RowSorter.SortKey(0, SortOrder.ASCENDING))))
}