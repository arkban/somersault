package somersault.ui.swing.browser

import java.awt.event._
import javax.swing._
import somersault.core.filesystems.FileSystem
import somersault.core.reports.filesystems.{DetailedReportXml, DetailedReportText, SummaryReport}
import somersault.core.reports.{ReportTask, Report}
import somersault.ui.swing.AboutFrame
import somersault.ui.swing.reports.ReportExecutor

class FileSystemBrowserMenuBar(owner: JFrame, fs: FileSystem) extends JMenuBar {

  private val reportExecutor = new ReportExecutor

  private def setup() = {
    val file = new JMenu("File")
    file.setMnemonic(KeyEvent.VK_F)

    val exit = new JMenuItem("Exit", KeyEvent.VK_X)
    exit.addActionListener(
      new ActionListener {
        def actionPerformed(e: ActionEvent) { owner.dispatchEvent(new WindowEvent(owner, WindowEvent.WINDOW_CLOSING)) }
      })

    val report = new JMenu("Reports")
    report.setMnemonic(KeyEvent.VK_R)

    val help = new JMenu("Help")
    help.setMnemonic(KeyEvent.VK_H)

    val about = new JMenuItem("About", KeyEvent.VK_A)
    about.addActionListener(
      new ActionListener {
        def actionPerformed(e: ActionEvent) { new AboutFrame(owner).setVisible(true) }
      })

    owner.addWindowListener(
      new WindowAdapter {
        override def windowClosing(e: WindowEvent) { reportExecutor.dispose() }
      })

    add(file)
    file.add(exit)
    add(report)
    report.add(createMenu("Summary", () => new SummaryReport(fs)))
    report.addSeparator()
    report.add(createMenu("Detailed (Text)", () => new DetailedReportText(fs)))
    report.add(createMenu("Detailed (XML)", () => new DetailedReportXml(fs)))
    add(help)
    help.add(about)
  }

  setup()

  private def createMenu(text: String, createReport: () => Report): JMenuItem = {
    val menu = new JMenuItem(text)
    menu.addActionListener(
      new ActionListener {
        def actionPerformed(e: ActionEvent) {
          reportExecutor
            .queue(new ReportTask(createReport(), fs.runtimeData.tempPath))
        }
      })
    menu
  }
}