package somersault.ui.swing

import java.awt.event.{ActionEvent, ActionListener}
import java.awt.{Dialog, BorderLayout, Dimension}
import javax.swing._
import net.miginfocom.swing.MigLayout
import somersault.util.Version

/**
 * Displays a very simple About dialog.
 */
class AboutFrame(owner: JFrame) extends JDialog {

  private def setup() {
    val icon = new JLabel
    icon.setMinimumSize(new Dimension(64, 64))
    icon.setIcon(SwingUtil.logoIcon)

    val centerPanel = new JPanel(new MigLayout("wrap", "[center,grow]"))
    centerPanel.add(icon)
    centerPanel.add(new JLabel("Somersault v" + Version.current))
    centerPanel.add(new JLabel("Apache Public License, v2.0"))
    centerPanel.add(new JLabel("http://bitbucket.org/arkban/somersault"))

    val btnClose = new JButton("Close")
    btnClose.addActionListener(
      new ActionListener {
        def actionPerformed(e: ActionEvent) { AboutFrame.this.dispose() }
      })

    val bottomPanel = new JPanel(new MigLayout("center", ""))
    bottomPanel.add(btnClose)

    val size = new Dimension(300, 200)

    getRootPane.setLayout(new BorderLayout)
    getRootPane.add(centerPanel, BorderLayout.CENTER)
    getRootPane.add(bottomPanel, BorderLayout.PAGE_END)
    setTitle("About")
    setIconImage(SwingUtil.logoIcon.getImage)
    setSize(size)
    setLocation(
      owner.getLocation.x + owner.getSize.width / 2 - size.width / 2,
      owner.getLocation.y + owner.getSize.height / 2 - size.height / 2)
    setResizable(false)
    setModalityType(Dialog.ModalityType.DOCUMENT_MODAL)
    setModal(true) // must be set after modality type
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE)
  }

  setup()
}
