package somersault.ui.swing

import javax.swing.table.DefaultTableCellRenderer
import javax.swing.JTable
import java.awt.{Component, Color}

/**A TableCellRenderer that will use a darker background color for each non-even row of a JTable. */
class AlternatingRowColorTableCellRenderer extends DefaultTableCellRenderer
{
  private var darker: Color = null

  private var original: Color = null

  override def getTableCellRendererComponent(
      jTable: JTable, o: AnyRef, isSelected: Boolean, hasFocus: Boolean, row: Int, column: Int): Component =
  {
    val component = super.getTableCellRendererComponent( jTable, o, isSelected, hasFocus, row, column)

    if( original == null )
    {
      original = component.getBackground

      val darkerDelta = 16;
      darker = new Color(
        original.getRed - darkerDelta, original.getGreen - darkerDelta, original.getBlue - darkerDelta)
    }

    if( isSelected == false && hasFocus == false )
      component.setBackground( if( row % 2 == 0 ) this.original else this.darker)

    component
  }
}
