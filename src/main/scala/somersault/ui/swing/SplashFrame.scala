package somersault.ui.swing

import java.awt.{BorderLayout, Dimension, Toolkit}
import javax.swing._
import net.miginfocom.swing.MigLayout
import somersault.util.Version
import swing.Swing

class SplashFrame extends JWindow {

  private def setup() {
    val icon = new JLabel
    icon.setMinimumSize(new Dimension(64, 64))
    icon.setIcon(SwingUtil.logoIcon)

    val panel = new JPanel
    panel.setBorder(BorderFactory.createRaisedBevelBorder())
    panel.setLayout(new MigLayout("wrap 2", "[][grow,fill]"))
    panel.add(icon)
    panel.add(new JLabel("Somersault v" + Version.current), "right")
    panel.add(new JLabel("Loading"), "span,center")

    val screen = Toolkit.getDefaultToolkit.getScreenSize
    val size = new Dimension(200, 100)

    setLayout(new BorderLayout)
    add(panel, BorderLayout.CENTER)
    setPreferredSize(size)
    setMaximumSize(size)
    setMinimumSize(size)
    setLocation(screen.width / 2 - size.width / 2, screen.height / 2 - size.height / 2)
    pack()
  }

  setup()
}

/**
 * Helps displaying the {@link SplashFrame}.
 */
class SplashFrameLoader {

  private val frame = new SplashFrame()

  /**
   * Displays the {@link SplashFrame}. It is recommended to wait on the calling thread to give the EDT thread time
   * to fully draw the {@link SplashFrame}.
   */
  def show() {
    if (SwingUtilities.isEventDispatchThread) frame.setVisible(true)
    else Swing.onEDTWait(show())
  }

  def dispose() {
    if (SwingUtilities.isEventDispatchThread) frame.dispose()
    else Swing.onEDTWait(dispose())
  }
}
