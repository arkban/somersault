package somersault.ui.swing

import java.awt.BorderLayout
import javax.swing.{JScrollPane, BoxLayout, JComponent, JPanel}

/**
 * Displays {@link JComponent}s in a list. The {@link JComponent}s are displayed in a standard vertical list. This
 * does not use a {@link JList} so that the child {@link JComponent}s can properly display things like
 * {@link JProgressBar}s.
 */
class ComponentList extends JComponent
{
  private val panel = new JPanel( )

  private def setup( )
  {
    panel.setLayout( new BoxLayout( panel, BoxLayout.PAGE_AXIS))
    val outerPanel = new JPanel( new BorderLayout)
    outerPanel.add( panel, BorderLayout.PAGE_START)

    setLayout( new BorderLayout)
    add( new JScrollPane( outerPanel), BorderLayout.CENTER)
  }

  setup( )

  /**
   * Adds the provided {@link JComponent} as the last item.
   */
  def addItem( view: JComponent)
  {
    view.setAlignmentX( 0)
    panel.add( view)
    panel.revalidate( )
  }

  /**
   * Removes the provided {@link JComponent}.
   */
  def removeItem( view: JComponent)
  {
    panel.remove( view)
    panel.revalidate( )
  }
}
