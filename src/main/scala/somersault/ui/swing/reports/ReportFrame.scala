package somersault.ui.swing.reports

import java.awt.BorderLayout
import java.awt.event._
import javax.swing._
import somersault.ui.swing.{ClosableComponent, SwingUtil}

/**
 * Window that displays all the {@link ReportComponent} instances.
 */
class ReportFrame(reportStatusView: ClosableComponent) extends JFrame {

  private val tabbedPane = new ReportTabbedPane

  private def setup() {
    tabbedPane.addTab(reportStatusView, "Status")

    val mnuSaveReport = new JMenuItem("Save Report", KeyEvent.VK_S)
    mnuSaveReport.setAccelerator(KeyStroke.getKeyStroke('S', InputEvent.CTRL_MASK))
    mnuSaveReport.addActionListener(
      new ActionListener {def actionPerformed(e: ActionEvent) { tabbedPane.saveCurrent() }})

    val mnuCloseReport = new JMenuItem("Close Report", KeyEvent.VK_R)
    mnuCloseReport.addActionListener(
      new ActionListener {def actionPerformed(e: ActionEvent) { tabbedPane.closeCurrentTab() }})

    val mnuClose = new JMenuItem("Close", KeyEvent.VK_C)
    mnuClose.addActionListener(
      new ActionListener {def actionPerformed(e: ActionEvent) { ReportFrame.this.dispose() }})

    val mnuFile = new JMenu("File")
    mnuFile.setMnemonic(KeyEvent.VK_F)
    mnuFile.add(mnuSaveReport)
    mnuFile.add(mnuCloseReport)
    mnuFile.addSeparator()
    mnuFile.add(mnuClose)

    val menu = new JMenuBar
    menu.add(mnuFile)

    setLayout(new BorderLayout)
    add(tabbedPane, BorderLayout.CENTER)
    setJMenuBar(menu)
    setTitle("Somersault - Reports")
    setSize(800, 600)
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE) // this may be changed by the caller
    setIconImage(SwingUtil.logoIcon.getImage)
  }

  setup()

  override def dispose() {
    super.dispose()
    tabbedPane.clearTabs() // ensure that the tabbedPane is empty, because
  }

  def addTab(component: ReportComponent, title: String) { tabbedPane.addTab(component, title) }
}
