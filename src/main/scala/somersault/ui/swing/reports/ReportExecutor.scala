package somersault.ui.swing.reports

import java.awt.BorderLayout
import java.util.concurrent.{Future, TimeUnit, LinkedBlockingQueue, ThreadPoolExecutor}
import javax.swing._
import somersault.core.reports.{ReportTaskResult, ReportTask}
import somersault.ui.swing.{ComponentList, ClosableComponent}
import swing.Swing

/**
 * Provides utility functions for dealing with {@link Report}s in the context of the GUI. This will also manage the
 * execution of reports and deciding whether to save them immediately or display them in the {@link ReportFrame}.
 */
class ReportExecutor {

  /**
   * We want to execute only one report being written at a time, so we use a single-threaded {@link Executor}.
   */
  private val executor = new ThreadPoolExecutor(
    1, 1, java.lang.Long.MAX_VALUE, TimeUnit.NANOSECONDS, new LinkedBlockingQueue[Runnable])

  private val reportTaskSummaryView = new ReportTaskStatusView

  private val monitor = new ReportTaskMonitor

  monitor.setDaemon(true)
  monitor.start()

  /**
   * The single {@link ReportFrame} instance that we will display to the user to show the {@link Report}s they
   * have created.
   */
  private val frame = {
    val frame = new ReportFrame(reportTaskSummaryView)
    frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE)
    frame
  }

  def dispose() {
    executor.shutdownNow
    monitor.interrupt()
    frame.dispose()
  }

  /**
   * Queues the provided {@link ReportTask}, and displays the {@link ReportFrame} if not already visible.
   */
  def queue(task: ReportTask) {
    val future = executor.submit(task)
    val view = reportTaskSummaryView.addReportTask(task, future)
    monitor.add(new ReportTaskMonitorEntry(future, () => view.finished()))

    Swing.onEDT(
    {
      if (!frame.isVisible)
        frame.setVisible(true)

      frame.toFront()
    })
  }

  /**
   * Manages {@link ReportTaskView} instances, displaying all known instances and allows adding new instances.
   */
  private class ReportTaskStatusView extends JComponent with ClosableComponent {

    private val compList = new ComponentList()

    private def setup() {
      setLayout(new BorderLayout())
      add(compList, BorderLayout.CENTER)
    }

    setup()

    /**
     * Creates and adds a single {@link ReportTask}, creating and setting up the displayed {@link ReportTaskView}.
     */
    def addReportTask(task: ReportTask, future: Future[_]): ReportTaskView = {
      val action = new StandardReportTaskViewAction(future)
      val view = new ReportTaskView(task, action)
      compList.addItem(view)
      view
    }

    def removeView(view: ReportTaskView) {
      compList.removeItem(view)
    }

    def isClosable = false

    def close() {}
  }

  /**
   * Standard implementation for all {@link ReportTaskView}s created by the {@link ReportExecutor}.
   */
  class StandardReportTaskViewAction(future: Future[_]) extends ReportTaskViewAction {

    def cancel(view: ReportTaskView) { if (!future.isDone) future.cancel(true) }

    def remove(view: ReportTaskView) {
      cancel(view) // ensure that the future is cancelled no matter what
      reportTaskSummaryView.removeView(view)
    }

    def show(view: ReportTaskView) {
      frame
        .addTab(new ReportComponent(future.get.asInstanceOf[ReportTaskResult]), view.task.report.name)
    }
  }

}
