package somersault.ui.swing.reports

import java.io.{FileOutputStream, FileInputStream}
import javax.swing.JFileChooser
import somersault.core.{StreamPumpData, StreamPump}
import somersault.ui.swing.ClosableTabbedPane

/**
 * A {@link JTabbedPane} that displays {@link ReportComponent}s.
 */
class ReportTabbedPane extends ClosableTabbedPane {

  private val fileChooser = new JFileChooser

  /**
   * Saves the current report to a file.
   */
  def saveCurrent() = {
    if (getCurrentTab.isInstanceOf[ReportComponent]) {
      val curr = getCurrentTab.asInstanceOf[ReportComponent]
      if (curr != null) {
        fileChooser.showSaveDialog(this) match {
          case JFileChooser.APPROVE_OPTION => {
            val input = new FileInputStream(curr.result.file)
            val output = new FileOutputStream(fileChooser.getSelectedFile)
            val pump = new StreamPump
            pump.pump(input, output, new StreamPumpData(true, Some(curr.result.file.length)))
          }
          case _ => None
        }
      }
    }
  }
}