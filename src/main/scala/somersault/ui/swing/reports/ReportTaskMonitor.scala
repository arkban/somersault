package somersault.ui.swing.reports

import java.util.concurrent.{CopyOnWriteArrayList, Future}
import scala.collection.convert.Wrappers

/**
 * A special thread that wakes up every 500 MS to check if any {@link Future}s created from {@link ReportTask}s are
 * done, and if so, calls the provided callback.
 */
class ReportTaskMonitor extends Thread {

  private val data = Wrappers.JListWrapper(new CopyOnWriteArrayList[ReportTaskMonitorEntry])

  /**
   * Adds an {@link ReportTaskMonitorEntry} to watch.
   */
  def add(entry: ReportTaskMonitorEntry) = data += entry

  override def run() {
    try {
      while (!Thread.interrupted) {
        data.filter(_.future.isDone).foreach(
          entry => {
            data -= entry

            if (!entry.future.isCancelled)
              entry.callback()
          })

        Thread.sleep(500)
      }
    }
    catch {
      case ex: InterruptedException => // swallow and allow the thread to die
    }
  }
}

class ReportTaskMonitorEntry(val future: Future[_], val callback: () => Unit)
