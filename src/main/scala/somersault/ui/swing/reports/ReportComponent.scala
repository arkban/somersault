package somersault.ui.swing.reports

import io.Source
import java.awt.{Font, BorderLayout}
import java.io.FileInputStream
import javax.swing._
import somersault.core.reports.ReportTaskResult
import somersault.ui.swing.ClosableComponent
import swing.Swing

/**
 * Displays a single report to the user. This handles the process of the report, displaying the report, and finally
 * allows saving the report.
 */
class ReportComponent(val result: ReportTaskResult) extends JComponent with ClosableComponent {

  private val text = new JTextArea

  private def setup() {
    text.setEditable(false)
    text.setTabSize(4)
    text.setFont(new Font(Font.MONOSPACED, text.getFont.getStyle, text.getFont.getSize))

    setLayout(new BorderLayout)
    add(new JScrollPane(text), BorderLayout.CENTER)

    val thread = new Thread(
      new Runnable {
        override def run() {
          val input = new FileInputStream(result.file)
          try {
            Source.fromFile(result.file).getLines().foreach(
              line => Swing.onEDT {
                text.append(line)
                text.append("\n")
                text.setCaretPosition(0)
              })
          }
          finally {
            input.close()
          }
        }
      })
    thread.setDaemon(true)
    thread.start()
  }

  setup()

  val isClosable = true

  def close() {}
}