package somersault.ui.swing.reports

import java.awt.event.{ActionEvent, ActionListener}
import java.awt.{Dimension, BorderLayout}
import javax.swing._
import somersault.core.reports.ReportTask
import somersault.ui.swing.SwingUtil
import swing.Swing

/**
 * Displays a single {@link ReportTask}, allowing the user to perform operations such as cancel the {@link ReportTask},
 * display the finished {@link Report}, remove old {@link ReportTask}s, etc.
 *
 * All of the functionality is decoupled by way of the {@link ReportTaskViewAction} trait.
 */
class ReportTaskView(val task: ReportTask, action: ReportTaskViewAction) extends JComponent {

  private val progress = new JProgressBar

  private val btnCancelOrRemove = new JButton(SwingUtil.cancelIcon)

  private var cancelOrRemove = "C"

  private val btnShow = new JButton(SwingUtil.magnifyingGlassIcon)

  private def setup() {
    val lblName = new JLabel(task.report.name)
    lblName.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2))

    progress.setIndeterminate(true)
    progress.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2))
    progress.setStringPainted(true)
    progress.setString("Working...")

    val btnSize = new Dimension(24, 24)

    btnCancelOrRemove.setMinimumSize(btnSize)
    btnCancelOrRemove.setPreferredSize(btnSize)
    btnCancelOrRemove.setMaximumSize(btnSize)
    btnCancelOrRemove.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2))
    btnCancelOrRemove.addActionListener(
      new ActionListener {
        def actionPerformed(e: ActionEvent) {
          if (cancelOrRemove.equals("C")) {
            action.cancel(ReportTaskView.this)
            progress.setIndeterminate(false)
            progress.setString("Canceled")
            cancelOrRemove = "R"
            btnCancelOrRemove.setIcon(SwingUtil.closeIcon)
            invalidate() // force a redraw
          }
          else {
            action.remove(ReportTaskView.this)
          }
        }
      })

    btnShow.setEnabled(false)
    btnShow.setMinimumSize(btnSize)
    btnShow.setPreferredSize(btnSize)
    btnShow.setMaximumSize(btnSize)
    btnShow.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2))
    btnShow.addActionListener(
      new ActionListener {def actionPerformed(e: ActionEvent) { action.show(ReportTaskView.this) }})

    val dataPanel = new JPanel
    dataPanel.setLayout(new BorderLayout)
    dataPanel.add(lblName, BorderLayout.PAGE_START)
    dataPanel.add(progress, BorderLayout.PAGE_END)

    val btnPanel = new JPanel
    btnPanel.setLayout(new BoxLayout(btnPanel, BoxLayout.LINE_AXIS))
    btnPanel.add(btnShow)
    btnPanel.add(btnCancelOrRemove)
    btnPanel.add(Box.createHorizontalGlue)

    setLayout(new BorderLayout)
    add(dataPanel, BorderLayout.CENTER)
    add(btnPanel, BorderLayout.LINE_END)
  }

  setup()

  /**
   * Called by {@link ReportTaskMonitor}'s callback when a {@link Report} is done.
   */
  def finished() {
    Swing.onEDTWait(
    {
      progress.setIndeterminate(false)
      progress.setString("Finished")
      cancelOrRemove = "R"
      btnCancelOrRemove.setIcon(SwingUtil.closeIcon)
      btnShow.setEnabled(true)
      invalidate() // force a redraw
    })
  }
}

/**
 * Defines all the actions that a {@link ReportTaskView} allows the user, allowing the creator of a
 * {@link ReportTaskView} to specify what needs to be done and the {@link ReportTaskView} to decide how it should
 * expose that functionality.
 */
trait ReportTaskViewAction {

  def cancel(view: ReportTaskView)

  def remove(view: ReportTaskView)

  def show(view: ReportTaskView)
}
