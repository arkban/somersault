package somersault.ui.swing

import collection.mutable
import javax.swing.event.{TreeModelEvent, TreeModelListener}

/**
 * A {@link TreeModelListener} that broadcasts to its inner {@link TreeModelListeners}.
 */
class MultiTreeModelListener(listeners: mutable.ListBuffer[TreeModelListener]) extends TreeModelListener {

  def this() = this(new mutable.ListBuffer[TreeModelListener])

  def add(l: TreeModelListener) = listeners += l

  def remove(l: TreeModelListener) = listeners -= l

  def clear() { listeners.clear() }

  def treeStructureChanged(e: TreeModelEvent) { listeners.foreach(_.treeStructureChanged(e)) }

  def treeNodesRemoved(e: TreeModelEvent) { listeners.foreach(_.treeNodesRemoved(e)) }

  def treeNodesInserted(e: TreeModelEvent) { listeners.foreach(_.treeNodesInserted(e)) }

  def treeNodesChanged(e: TreeModelEvent) { listeners.foreach(_.treeNodesChanged(e)) }
}