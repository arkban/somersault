package somersault.ui.swing

import java.awt._
import java.awt.event.{ActionEvent, ActionListener}
import javax.swing._

/**
 * A {@link JTabbedPane} that allows closing the tabs using a graphical X, much like tabs in a web browser.
 */
class ClosableTabbedPane extends JComponent {

  private val tabbedPane = new JTabbedPane

  setLayout(new BorderLayout)
  add(tabbedPane, BorderLayout.CENTER)

  /**
   * Creates a tab for the provided Service, which creates an Initializer and a corresponding an InitializerViewer.
   */
  def addTab(component: ClosableComponent, title: String) {
    val tabComponent = new ClosableTabbedPane.TabComponent(this, component, title)
    tabbedPane.add(component.asInstanceOf[Component])
    tabbedPane.setTabComponentAt(tabbedPane.getTabCount - 1, tabComponent)
    tabbedPane.setSelectedComponent(component.asInstanceOf[Component])
  }

  /**
   * Closes and removes all {@link ClosableComponent} from this frame.
   */
  def clearTabs() {
    (0 until tabbedPane.getTabCount).foreach(
      i => {
        val tabComp = tabbedPane.getTabComponentAt(i).asInstanceOf[ClosableTabbedPane.TabComponent]
        if (tabComp.component.isClosable) tabComp.component.close()
      })
    while (tabbedPane.getTabCount > 0)
      tabbedPane.removeTabAt(tabbedPane.getTabCount - 1)
  }

  /**
   * Returns the currently displayed {@link ClosableComponent}.
   *
   * @return Returns null if no tabs are displayed.
   */
  def getCurrentTab: ClosableComponent = tabbedPane.getSelectedComponent.asInstanceOf[ClosableComponent]

  /**
   * Removes the currently displayed {@link ClosableComponent}
   */
  def closeCurrentTab() {
    val curr = getCurrentTab
    if (curr != null && curr.isClosable)
      close(curr)
  }

  /**
   * Closes the provided {@link ClosableComponent} and removes the tab.
   */
  private def close(component: ClosableComponent) {
    tabbedPane.remove(component.asInstanceOf[Component])
    component.close()
  }
}

/**
 * All components that are added to {@link ClosableTabbedPane}.
 */
trait ClosableComponent {

  def isClosable: Boolean

  def close()
}

object ClosableTabbedPane {

  /**
   * The close button on the {@link TabComponent}
   *
   * @param component The owning { @link TabComponent}, used to be able to close the correct tab.
   */
  private final class TabCloseButton(tabPane: ClosableTabbedPane, component: ClosableComponent) extends JButton {

    private def setup() {
      setPreferredSize(new Dimension(16, 16))
      setFocusable(false)
      setRolloverEnabled(false)
      addActionListener(new ActionListener {def actionPerformed(e: ActionEvent) { tabPane.close(component) }})
    }

    setup()

    /**
     * Overrides to draw a graphical X for closing.
     */
    protected override def paintComponent(g: Graphics) {
      super.paintComponent(g)
      val r = g.getClipBounds
      val inset = 5
      val min = new Point(r.getMinX.asInstanceOf[Int] + inset, r.getMinY.asInstanceOf[Int] + inset)
      val max = new Point(r.getMaxX.asInstanceOf[Int] - inset, r.getMaxY.asInstanceOf[Int] - inset)
      val g2 = g.asInstanceOf[Graphics2D]
      g2.setColor(this.getForeground)
      g2.setStroke(new BasicStroke(3f))
      g2.drawLine(
        min.getX.asInstanceOf[Int],
        min.getY.asInstanceOf[Int],
        max.getX.asInstanceOf[Int],
        max.getY.asInstanceOf[Int])
      g2.drawLine(
        min.getX.asInstanceOf[Int],
        max.getY.asInstanceOf[Int],
        max.getX.asInstanceOf[Int],
        min.getY.asInstanceOf[Int])
    }
  }

  /**
   * A customized {@link JComponent} that will be used for the tabs on the {@link JTabPage}. This will display
   * a label and the X to allow closing the tab.
   */
  private final class TabComponent(tabPane: ClosableTabbedPane, val component: ClosableComponent, title: String)
    extends JComponent {

    private def setup() {
      val label = new JLabel(title)
      label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 4))

      setLayout(new BorderLayout)
      add(label, BorderLayout.CENTER)
      if (component.isClosable)
        add(new TabCloseButton(tabPane, component), BorderLayout.LINE_END)
    }

    setup()
  }

}