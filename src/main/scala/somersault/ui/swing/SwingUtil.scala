package somersault.ui.swing

import java.awt.SystemColor
import javax.swing.{ImageIcon, JTextField}

object SwingUtil {

  /**
   * Somersault's icon.
   */
  lazy val logoIcon = {
    try {
      new ImageIcon(getClass.getResource("/logo.png"))
    }
    catch {
      case _: Exception => new ImageIcon
    }
  }

  lazy val magnifyingGlassIcon = {
    try {
      new ImageIcon(getClass.getResource("/magglass.png"))
    }
    catch {
      case _: Exception => new ImageIcon
    }
  }

  lazy val cancelIcon = {
    try {
      new ImageIcon(getClass.getResource("/cancel.png"))
    }
    catch {
      case _: Exception => new ImageIcon
    }
  }

  lazy val closeIcon = {
    try {
      new ImageIcon(getClass.getResource("/close.png"))
    }
    catch {
      case _: Exception => new ImageIcon
    }
  }

  /**
   * Creates a {@link JTextField} that is editable or not depending on the flag. (It is suggested to use non-editable
   * {@link JTextField}s as labels because it allows the user to copy and paste from them.)
   */
  def createTextField(value: String, editable: Boolean) = {
    val field = new JTextField(value)
    field.setEditable(editable)
    if (!editable)
      field.setBackground(SystemColor.control)
    field
  }

  /**
   * Converts a value of raw bytes into a string displaying kilobytes (binary) with the suffix "KB".
   */
  def toBytes(value: Long): String = "%d KB".format(value / 1000L)
}