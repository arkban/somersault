package somersault.ui.cli

import java.io.File
import logahawk.listeners.ConsoleListener
import somersault.util.persist.Persister
import somersault.util.{Version, Logger}
import somersault.ui._
import com.martiansoftware.jsap._
import somersault.core.ScenarioData

/**
 * A command line interface for Somersault.
 */
class CLI {

  private val scenarioOption = new FlaggedOption("scenario").setRequired(false).setLongFlag("scenario").setHelp(
    "Specifies the scenario file")

  private val restoreOption = new Switch("restore").setLongFlag("restore").setHelp(
    "Reverse the operation from a backup to a restore. The source and destination will be swapped.")

  private val dryRunOption = new Switch("dryrun").setLongFlag("dryrun").setHelp(
    "No changes are made to the destination (or source if --restore is specified)")

  private val buildOnlyOption = new Switch("buildOnly").setLongFlag("buildonly").setHelp(
    "Builds actions but does not execute them.")

  // miscellaneous (default command is backup)
  private val reconcileCommand = new Switch("reconcile").setLongFlag("reconcile").setHelp(
    "Performs reconciliation on the destination")

  private val unlockCommand = new Switch("unlock").setLongFlag("unlock").setHelp(
    "Forcibly removes all locks on the destination")

  private val examplesCommand = new FlaggedOption("examples").setRequired(false).setLongFlag("examples").setHelp(
    "Writes example Scenario XML files into the provided directory and exits.")

  private val browserCommand = new Switch("browser").setLongFlag("browser").setHelp(
    "allows browsing the source (or dest) file system in the provided scenario file")

  def execute(args: Array[String]) {
    val jsap = new SimpleJSAP(
      "Somersault", "Generic backup/synchronization tool, designed to communicate with various different storage"
        + " systems, most notably Amazon S3.", Array[Parameter](
        scenarioOption,
        restoreOption,
        dryRunOption,
        buildOnlyOption,
        reconcileCommand,
        unlockCommand,
        examplesCommand,
        browserCommand))

    Logger.getListenerContainer.add(new ConsoleListener)

    val jsapResult = jsap.parse(args)
    if (!jsapResult.success) {
      // from JSAP docs ch03s06.html
      System.err.println()
      System.err.println("Usage: java " + this.getClass.getName)
      System.err.println("                " + jsap.getUsage)
      System.err.println()
      System.err.println(jsap.getHelp)
      System.exit(1)
    }

    val scenarioSpecified = jsapResult.userSpecified(scenarioOption.getID)

    val observer = new ObserverConsolidator(Logger)
    new LogObserver(observer)

    Logger.info("Started Somersault v" + Version.current)
    try {
      // --------------------------------------------
      // commands NOT requiring the "scenario" option
      // --------------------------------------------
      if (!scenarioSpecified) {
        try {
          if (jsapResult.userSpecified(examplesCommand.getID)) {
            writeExampleScenarios(jsapResult)
          }
          else {
            System.err.println()
            System.err.println("invalid command line arguments")
            System.exit(1)
          }
        }
        catch {
          case t: Throwable => Logger.fatal(t)
        }
      }
      // --------------------------------------------
      // commands requiring the "scenario" option
      // --------------------------------------------
      else {
        val scenarioData = Persister.readScenarioData(new File(jsapResult.getString(scenarioOption.getID)))
        val scenarioOptions = new ScenarioOptions(
          jsapResult.getBoolean(restoreOption.getID),
          jsapResult.getBoolean(dryRunOption.getID),
          jsapResult.getBoolean(
            buildOnlyOption.getID))

        try {
          if (jsapResult.userSpecified(browserCommand.getID))
            showBrowser(scenarioData, scenarioOptions, observer)
          else
            if (jsapResult.userSpecified(unlockCommand.getID))
              forciblyUnlock(scenarioData, scenarioOptions, observer)
            else
              if (jsapResult.getBoolean(reconcileCommand.getID))
                performReconcile(scenarioData, scenarioOptions, jsapResult, observer)
              else
                performBackup(scenarioData, scenarioOptions, jsapResult, observer)
        }
        catch {
          case t: Throwable => Logger.fatal(t)
        }
      }
    }
    finally {
      Logger.info("Somersault exited")
    }
  }

  /**
   * Writes examples scenarios to the provided file path.
   */
  private def writeExampleScenarios(result: JSAPResult) {
    val path = result.getString(examplesCommand.getID)
    try {
      ExampleScenarios.write(new File(path))
    }
    catch {
      case ex: Exception =>
        val message = "Failed to write example scenario to: " + path
        Logger.error(message, ex)
        System.err.println(message)
        System.err.println(ex)
    }
  }

  private def showBrowser(
    scenarioData: ScenarioData, scenarioOptions: ScenarioOptions, observer: ObserverConsolidator) {
    val executor = new BrowserExecutor(scenarioData, scenarioOptions, observer)
    executor.execute()
  }

  private def forciblyUnlock(
    scenarioData: ScenarioData, scenarioOptions: ScenarioOptions, observer: ObserverConsolidator) {
    val executor = new UnlockExecutor(scenarioData, scenarioOptions, observer)
    executor.execute()
  }

  private def performBackup(
    scenarioData: ScenarioData, scenarioOptions: ScenarioOptions, result: JSAPResult, observer: ObserverConsolidator) {
    val executor = new StandardScenarioExecutor(scenarioData, scenarioOptions, observer)
    executor.execute()
  }

  private def performReconcile(
    scenarioData: ScenarioData, scenarioOptions: ScenarioOptions, result: JSAPResult, observer: ObserverConsolidator) {
    val executor = new ReconcileScenarioExecutor(scenarioData, scenarioOptions, observer)
    executor.execute()
  }
}