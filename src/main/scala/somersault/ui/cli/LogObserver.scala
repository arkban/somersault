package somersault.ui.cli

import java.lang.String
import java.util.concurrent.atomic.{AtomicBoolean, AtomicLong}
import logahawk.Severity
import net.jcip.annotations.ThreadSafe
import somersault.core.actions.builders.ActionBuilderObserver
import somersault.core.actions.runners.ActionRunnerObserver
import somersault.core.actions.{TransferAction, CreateDirAction, RemoveAction, DirectoryAction}
import somersault.core.comparators.SomerFileComparator
import somersault.core.filesystems.{SomerFile, ReconcileObserver}
import somersault.core.filters.SomerFilter
import somersault.ui.{ObserverConsolidator, UIObserver}
import somersault.util.{Logger, FilenameUtils2}

/**
 * Implementation of the various observers for the command line interface. The constructor will add the newly
 * constructed {@link LogObserver} to the provided {@link ObserverConsolidator}.
 */
@ThreadSafe
class LogObserver(consolidator: ObserverConsolidator) {

  private val errorCount = new AtomicLong(0)

  consolidator.actionBuilder.add(new ActionBuilderObs())
  consolidator.actionRunner.add(new ActionRunnerObs())
  consolidator.ui.add(new UIObs())
  consolidator.reconcile.add(new ReconcileObs())

  private def error(severity: Severity, ex: Exception) {
    errorCount.incrementAndGet
    consolidator.logger.log(severity, ex)
  }

  private class UIObs extends UIObserver {

    private val memThreadActive = new AtomicBoolean(true)

    def error(severity: Severity, ex: Exception) { LogObserver.this.error(severity, ex) }

    def start() {
      val t = new Thread(
        new Runnable() {
          def run() {
            while (memThreadActive.get) {
              val memUsed = (Runtime.getRuntime.totalMemory - Runtime.getRuntime.freeMemory) / 1048576f
              val memMax = Runtime.getRuntime.totalMemory / 1048576f
              consolidator.logger.debug(

                "Memory Used: %.2f MB/s, Memory Max: %.2f MB/s".format(memUsed, memMax))
              Thread.sleep(15 * 1000)
            }
          }
        })
      t.setDaemon(true)
      t.start()
    }

    def end() {
      memThreadActive.set(false)
      consolidator.logger.alert("Scenario finish")
    }
  }

  private class ActionBuilderObs extends ActionBuilderObserver {

    def start() {
      LogObserver.this.errorCount.set(0)
      consolidator.logger.alert("Starting Phase: Building Actions")
    }

    def end() {
      consolidator.logger.alert("Ending phase (with " + LogObserver.this.errorCount.get + " errors): Building Actions")
    }

    def recurse(path: List[String]) {
      consolidator.logger.info(
        String.format(
          "Searching: %1$s%2$s", FilenameUtils2.dirSeparator, Logger.escape(FilenameUtils2.concat(path))))
    }

    def filterAccept(filter: SomerFilter, path: List[String]) {
      consolidator.logger.info(
        String.format(
          "Filter Accept: %1$s%2$s", FilenameUtils2.dirSeparator, Logger.escape(FilenameUtils2.concat(path))))
    }

    def filterReject(filter: SomerFilter, path: List[String]) {
      consolidator.logger.info(
        String.format(
          "Filter Reject: %1$s%2$s", FilenameUtils2.dirSeparator, Logger.escape(FilenameUtils2.concat(path))))
    }

    def comparatorAccept(comparator: SomerFileComparator, path: List[String]) {
      consolidator.logger.info(
        String.format(
          "Comparator Accept: %1$s%2$s", FilenameUtils2.dirSeparator, Logger.escape(
            FilenameUtils2.concat(path))))
    }

    def comparatorReject(comparator: SomerFileComparator, path: List[String]) {
      consolidator.logger.info(
        String.format(
          "Comparator Reject: %1$s%2$s", FilenameUtils2.dirSeparator, Logger.escape(
            FilenameUtils2.concat(path))))
    }

    def error(severity: Severity, ex: Exception) { LogObserver.this.error(severity, ex) }
  }

  private class ActionRunnerObs extends ActionRunnerObserver {

    private var totalActions = 0L

    private var currAction = 0L

    def start(actionCount: Long) {
      totalActions = actionCount
      currAction = 0
      LogObserver.this.errorCount.set(0)
      consolidator.logger.alert("Starting Phase (with " + actionCount + " actions): Executing Actions")
    }

    def end() {
      consolidator.logger.alert(
        "Ending phase (%1$d of %2$d actions executed, with %3$d errors): Executing Actions".format(
          currAction, totalActions, LogObserver.this.errorCount.get))
    }

    def recurse(path: List[String], action: DirectoryAction) {
      consolidator.logger.info(
        "Working %1$s%2$s".format(
          FilenameUtils2.dirSeparator,
          Logger.escape(FilenameUtils2.concat(path ::: action.name :: Nil))))
    }

    def transfer(path: List[String], action: TransferAction, file: SomerFile) {
      currAction += 1
      consolidator.logger.info(
        "Transfer (%1$d of %2$d): %3$s%4$s (%5$d KB)".format(
          currAction,
          totalActions,
          FilenameUtils2.dirSeparator,
          Logger.escape(FilenameUtils2.concat(path ::: action.name :: Nil)),
          math.ceil(file.size / 1000d).toLong))
    }

    def createDir(path: List[String], action: CreateDirAction) {
      currAction += 1
      consolidator.logger.info(
        "Create Directory (%1$d of %2$d): %3$s%4$s".format(
          currAction,
          totalActions,
          FilenameUtils2.dirSeparator,
          Logger.escape(FilenameUtils2.concat(path ::: action.name :: Nil))))
    }

    def remove(path: List[String], action: RemoveAction) {
      currAction += 1
      consolidator.logger.info(
        "Remove (%1$d of %2$d): %3$s%4$s".format(
          currAction,
          totalActions,
          FilenameUtils2.dirSeparator,
          Logger.escape(FilenameUtils2.concat(path ::: action.name :: Nil))))
    }

    def error(severity: Severity, ex: Exception) { LogObserver.this.error(severity, ex) }
  }

  private class ReconcileObs extends ReconcileObserver {

    def start() { consolidator.logger.info("Starting reconciliation...") }

    def end() { consolidator.logger.info("End reconciliation") }

    def removeFromFileSystem(path: String) { consolidator.logger.info("Remove from FileSystem: " + path) }

    def removeFromIndex(path: String) { consolidator.logger.info("Remove from Index: " + path) }

    def error(severity: Severity, ex: Exception) { LogObserver.this.error(severity, ex) }
  }

}
