package somersault.ui

import somersault.core.ScenarioData
/**
 * Runs removeAllLocks() for the specified {@link FileSystem}.
 */
class UnlockExecutor(
    scenarioData: ScenarioData, scenarioOptions: ScenarioOptions, observer: ObserverConsolidator)
    extends ScenarioExecutor( scenarioData, scenarioOptions, observer)
{
  override protected def executeImpl( )
  {
    observer.logger.alert( String.format( "Unlock %1$s", scenarioOptions.getDestTag( scenarioData)))

    observer.ui.start( )
    try
    {
      // we do not lock, because in the next step we'd remove the lock we just added
      val (scenario, tempPath) = createScenario( None, None)

      scenario.dest.removeAllLocks( )
    }
    catch
    {
      case ex: Throwable => observer.logger.fatal( "Remove all locks failed", ex)
    }
    finally
    {
      observer.ui.end( )
    }
  }
}