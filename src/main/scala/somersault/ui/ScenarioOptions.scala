package somersault.ui

import somersault.core.ScenarioData
import somersault.util.Logger

/**
 * Options specified when executing a {@link Scenario}.
 *
 * @param restore The source and destination are swapped, and the (new) destination is not pruned.
 * @param dryRun The destination is wrapped with a { @link DryRunFileSystem}.
 * @param buildOnly The { @link Action}s are created but are not executed.
 */
class ScenarioOptions(val restore: Boolean, val dryRun: Boolean, val buildOnly: Boolean) {

  /**
   * Returns the name of the correct name of "source" (after taking into account whether this is a restore).
   */
  def getSourceTag(scenarioData: ScenarioData) =
    if (restore) Logger.escape(scenarioData.dest.name) + " (destination)"
    else scenarioData.source.name + " (source)"

  /**
   * Returns the name of the correct name of "destination" (after taking into account whether this is a restore).
   */
  def getDestTag(scenarioData: ScenarioData) =
    if (restore) Logger.escape(scenarioData.source.name) + " (source)"
    else scenarioData.dest.name + " (destination)"
}