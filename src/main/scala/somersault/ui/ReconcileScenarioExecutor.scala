package somersault.ui

import somersault.core.ScenarioData
import somersault.core.filesystems.LockingFileSystemMode

/**
 * Reconciles the destination {@link FileSystem}.
 */
class ReconcileScenarioExecutor(
  scenarioData: ScenarioData, scenarioOptions: ScenarioOptions, observer: ObserverConsolidator)
  extends ScenarioExecutor(scenarioData, scenarioOptions, observer) {

  override protected def executeImpl() {
    observer.logger.alert(String.format("Reconcile %1$s", scenarioOptions.getDestTag(scenarioData)))

    observer.ui.start()
    try {
      val (scenario, _) = createScenario(None, Some(LockingFileSystemMode.Write))
      try {
        scenario.dest.reconcile(observer.reconcile)
      }
      finally {
        scenario.dest.unlock()
      }
    }
    catch {
      case ex: Throwable => observer.logger.fatal("Reconcile failed", ex)
    }
    finally {
      observer.ui.end()
    }
  }
}