package somersault.ui

import java.awt.event.{WindowEvent, WindowAdapter}
import java.util.concurrent.CountDownLatch
import javax.swing.{JFrame, UIManager}
import logahawk.Severity
import scala.swing.Swing
import somersault.core.ScenarioData
import somersault.core.filesystems.LockingFileSystemMode
import swing.SplashFrameLoader
import swing.browser.FileSystemBrowser

/**
 * Launches a {@link FileSystemBrowser} for the specified {@link FileSystem}.
 */
class BrowserExecutor(
  scenarioData: ScenarioData, scenarioOptions: ScenarioOptions, observer: ObserverConsolidator)
  extends ScenarioExecutor(scenarioData, scenarioOptions, observer) {

  private val splash = new SplashFrameLoader

  override protected def executeImpl() {
    observer.logger.alert(String.format("Browse %1$s", scenarioOptions.getDestTag(scenarioData)))

    splash.show()

    // force this thread to wait to allow the EDT thread to fully draw the splash screen
    Thread.sleep(100)

    // Used to prevent returning from this method until the GUI is finished
    val signal: CountDownLatch = new CountDownLatch(1)

    Swing.onEDTWait({launch(signal)})

    signal.await()
  }

  /**
   * Launches the {@link FileSystemBrowser}. (This is a separate method because Scala got mad with a call to
   * {@link Swing#onEDTWait()} with a try...catch as the first statement.)
   */
  private def launch(signal: CountDownLatch) {
    try {
      val (scenario, _) = createScenario(None, Some(LockingFileSystemMode.Read))

      // this must execute before we build the components (or else they have the old look and feel)
      JFrame.setDefaultLookAndFeelDecorated(true)
      try {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName)
      }
      catch {
        case ex: Exception => observer.logger.error("System Look and Feel failed to initialize", ex)
      }

      try {
        val frame = new FileSystemBrowser(scenario.dest)
        frame.addWindowListener(
          new WindowAdapter() {
            override def windowClosing(e: WindowEvent) { signal.countDown() }
          })
        frame.setVisible(true)
      }
      finally {
        scenario.dest.unlock()
      }
    }
    catch {
      case ex: Exception =>
        observer.ui.error(Severity.FATAL, ex)
        signal.countDown()
    }
    finally {
      splash.dispose()
    }
  }
}