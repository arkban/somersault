package somersault.core

import java.io.{OutputStream, InputStream}
import somersault.core.actions.TransferAction
import somersault.core.exceptions.{DestWriteActionRunnerException, SourceReadActionRunnerException}

/**
 * A utility class that pumps data from an InputStream into an OutputStream.
 *
 * The StreamPump is not itself observable, although it seems like it would a good idea. The reason is that not all
 * file transferring is done by Somersault. That is why the ObservableInputStream and ObservableOutputStream exist --
 * to allow observing transfers that occur inside and outside Somersault.
 */

class StreamPump(bufferSize: Int) {

  // hopefully approximates a page size
  def this() = this(32768)

  /**
   * Copies the data from InputStream to the OutputStream, using a buffer of the size specified in the constructor.
   * The stream positions will not be finished, and the entire InputStream will be copied to the OutputStream. Since
   * this method did not create streams, it will not close them.
   *
   * This will accept an ActionStreamPumpData for advanced exception handling. If an error occurs reading from the
   * InputStream it will be wrapped into a SourceReadActionRunnerException. If an error occurs writing to the
   * OutputStream it will be wrapped into a DestWriteActionRunnerException.
   */
  def pump(input: InputStream, output: OutputStream, data: StreamPumpData) {
    try {
      val advData: ActionStreamPumpData = data match {
        case a: ActionStreamPumpData => a
        case _ => null
      }

      val buffer = new Array[Byte](bufferSize)
      var read = 0

      do {
        if (advData == null) // I dropped { } here to make the code more compact, which I think is easier to read
          read = input.read(buffer)
        else
          try
            read = input.read(buffer)
          catch {
            case ex: Exception => throw new SourceReadActionRunnerException(advData.action, advData.parentPath, ex)
          }

        // delay here for rate control?

        if (read != -1) {
          if (advData == null) // I dropped { } here to make the code more compact, which I think is easier to read
            output.write(buffer, 0, read)
          else
            try
              output.write(buffer, 0, read)
            catch {
              case ex: Exception => throw new DestWriteActionRunnerException(advData.action, advData.parentPath, ex)
            }
        }
      }
      while (read != -1)
    }
    finally {
      if (data.closeWhenDone)
        StreamPump.safeClose(input, output)
    }
  }
}

object StreamPump {

  /**
   * Helper method to ensure that both streams are closed, even if they throw exceptions.
   */
  def safeClose(input: InputStream, output: OutputStream) {
    try {
      input.close()
    }
    finally {
      try {
        output.close()
      }
      finally {}
    }
  }
}

/**
 * Provides all ancillary data when using a StreamPump.
 *
 * @param inputSize Unused, needs to be removed.
 * @param singleByte If true the StreamPump will read a single byte a time.
 */
class StreamPumpData(val closeWhenDone: Boolean, val inputSize: Option[Long], val singleByte: Boolean = false)

/**
 * Provides ancillary data related to executing a TransferAction with a StreamPump.
 */
class ActionStreamPumpData(
  closeWhenDone: Boolean, inputSize: Option[Long], val action: TransferAction, val parentPath: List[String])
  extends StreamPumpData(closeWhenDone, inputSize)
