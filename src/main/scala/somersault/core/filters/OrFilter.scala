package somersault.core.filters

import nu.xom.Element
import somersault.core.filesystems.SomerArtifact
import somersault.util.persist.PersisterContext

/**
 * Logical OR's each of the child filters.
 */
class OrFilter(filters: Iterable[SomerFilter]) extends GroupFilter(filters) {

  def this(filters: SomerFilter*) = this(filters)

  def accept(parentPath: Iterable[String], a: SomerArtifact) = filters.exists(_.accept(parentPath, a))
}

class OrFilterPersister extends GroupFilterPersister {

  def canRead(context: PersisterContext, element: Element): Boolean = checkClass(element, classOf[OrFilter])

  def read[T](context: PersisterContext, element: Element) =
    new OrFilter(readChildFilters(context, element)).asInstanceOf[T]

  def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false) {
    obj match {
      case f: OrFilter =>
        writeClass(obj, element, dynamic)
        writeChildFilters(context, f.filters, element)
      case _ => throw new IllegalArgumentException("cannot persist: " + obj.getClass.getName)
    }
  }
}

