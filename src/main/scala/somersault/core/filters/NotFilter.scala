package somersault.core.filters

import nu.xom.Element
import somersault.core.filesystems.SomerArtifact
import somersault.util.persist.{PersisterContext, Persister}

/**
 * Negates the provided filter.
 */
class NotFilter(val filter: SomerFilter) extends SomerFilter {

  def accept(parentPath: Iterable[String], a: SomerArtifact) = !filter.accept(parentPath, a)
}

class NotFilterPersister extends SomerFilterPersister {

  def canRead(context: PersisterContext, element: Element): Boolean = checkClass(element, classOf[NotFilter])

  def read[T](context: PersisterContext, element: Element) = {
    val elements = Persister.elementsToIterable(element.getChildElements).toList
    if (elements.size > 1)
      throw new IllegalArgumentException("multiple filters are not allowed in a NotFilter")
    new NotFilter(context.container.read[SomerFilter](context, elements.head)).asInstanceOf[T]
  }

  def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false) {
    obj match {
      case f: NotFilter =>
        writeClass(obj, element, dynamic)
        context.container.write(
          context, f.filter, Persister.createElement("filter", parent = element), dynamic = true)
      case _ => throw new IllegalArgumentException("cannot persist: " + obj.getClass.getName)
    }
  }
}
