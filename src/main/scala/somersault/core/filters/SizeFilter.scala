package somersault.core.filters

import nu.xom.Element
import org.apache.commons.lang.math.NumberUtils
import somersault.core.filesystems.{SomerArtifact, SomerFile}
import somersault.util.persist.{PersisterContext, Persister}

/**
 * Matches sizes of SomerFiles that fall within the provided range (inclusively). Returns true for SomerDirectory
 * because that simplifies usage, since this filter is usually used without a NotFilter.
 */
class SizeFilter(val min: Long, val max: Long) extends SomerFilter {

  def this(max: Long) = this(0, max)

  def accept(parentPath: Iterable[String], a: SomerArtifact) =
    a match {
      case f: SomerFile => f.size >= min && f.size <= max
      case _ => true
    }
}

class SizeFilterPersister extends SomerFilterPersister {

  def canRead(context: PersisterContext, element: Element): Boolean = checkClass(element, classOf[SizeFilter])

  def read[T](context: PersisterContext, element: Element): T =
    new SizeFilter(
      NumberUtils.toLong(element.getAttributeValue("min")), NumberUtils.toLong(element.getAttributeValue("max")))
      .asInstanceOf[T]

  def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false) {
    obj match {
      case f: SizeFilter =>
        writeClass(obj, element, dynamic)
        Persister.createAttribute("max", f.max.toString, element)
        Persister.createAttribute("min", f.min.toString, element)
      case _ => throw new IllegalArgumentException("cannot write: " + obj.getClass.getName)
    }
  }
}
