package somersault.core.filters

import java.util.regex.Pattern
import nu.xom.Element
import somersault.core.filesystems.SomerArtifact
import somersault.util.FilenameUtils2
import somersault.util.persist.PersisterContext

/**
 * Matches the path of SomerArtifacts using regular expressions. This path does NOT include the file name, that
 * is handled by the NameFilter.
 *
 * The path uses "/" as the separator character, even on MS Windows operating systems.
 *
 * This does a complete match again the SomerArtifact's path, it will not return true if the pattern is found within
 * the path. (So it is advised to append and prepend ".*" if that is the behavior you want.)
 */
class PathFilter(patterns: Iterable[Pattern]) extends RegexFilter(patterns) {

  def this(pattern: Pattern, extra: Pattern*) = this(pattern :: Nil ++ extra)

  def this(pattern: String, extra: String*) = this((pattern :: Nil ++ extra).map(Pattern.compile(_)))

  def accept(parentPath: Iterable[String], a: SomerArtifact) = {
    val pathStr = FilenameUtils2.dirSeparator + FilenameUtils2.concat(parentPath.toList ::: (a.name :: Nil))
    patterns.exists(_.matcher(pathStr).find)
  }
}

class PathFilterPersister extends RegexFilterPersister {

  def canRead(context: PersisterContext, element: Element): Boolean = checkClass(element, classOf[PathFilter])

  def read[T](context: PersisterContext, element: Element): T = new PathFilter(readPatterns(element))
    .asInstanceOf[T]

  def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false) {
    obj match {
      case f: PathFilter =>
        writeClass(obj, element, dynamic)
        writePatterns(f, element)
      case _ => throw new IllegalArgumentException("cannot write: " + obj)
    }
  }
}