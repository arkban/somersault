package somersault.core.filters

import nu.xom.Element
import somersault.util.persist.{PersisterContext, Persister}

abstract class GroupFilter(val filters: Iterable[SomerFilter]) extends SomerFilter {

  def this(filters: SomerFilter*) = this(filters)
}

abstract class GroupFilterPersister extends SomerFilterPersister {

  protected def readChildFilters(context: PersisterContext, element: Element) =
    Persister.elementsToIterable(element.getChildElements)
      .map(e => context.container.read[SomerFilter](context, e))

  protected def writeChildFilters(context: PersisterContext, filters: Iterable[SomerFilter], element: Element) {
    filters.foreach(
      child =>
        context.container.write(context, child, Persister.createElement("filter", parent = element), dynamic = true))
  }
}