package somersault.core.filters

import nu.xom.Element
import somersault.core.filesystems.{SomerFile, SomerArtifact}
import somersault.util.persist.PersisterContext

/**
 * Returns true if this is a SomerFile.
 */
class FileFilter extends SomerFilter {

  def accept(parentPath: Iterable[String], a: SomerArtifact) = a.isInstanceOf[SomerFile]
}

class FileFilterPersister extends SomerFilterPersister {

  def canRead(context: PersisterContext, element: Element): Boolean = checkClass(element, classOf[FileFilter])

  def read[T](context: PersisterContext, element: Element): T = new FileFilter().asInstanceOf[T]

  def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false) {
    obj match {
      case f: FileFilter => writeClass(obj, element, dynamic)
      case _ => throw new IllegalArgumentException("cannot write: " + obj.getClass.getName)
    }
  }
}
