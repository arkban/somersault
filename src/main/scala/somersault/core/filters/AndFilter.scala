package somersault.core.filters

import nu.xom.Element
import somersault.core.filesystems.SomerArtifact
import somersault.util.persist.PersisterContext

/**
 * Logical AND's each of the child filters.
 */
class AndFilter(filters: Iterable[SomerFilter]) extends GroupFilter(filters) {

  def this(filters: SomerFilter*) = this(filters)

  def accept(parentPath: Iterable[String], a: SomerArtifact) = filters.forall(_.accept(parentPath, a))
}

class AndFilterPersister extends GroupFilterPersister {

  def canRead(context: PersisterContext, element: Element): Boolean = checkClass(element, classOf[AndFilter])

  def read[T](context: PersisterContext, element: Element) =
    new AndFilter(readChildFilters(context, element)).asInstanceOf[T]

  def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false) {
    obj match {
      case f: AndFilter =>
        writeClass(obj, element, dynamic)
        writeChildFilters(context, f.filters, element)
      case _ => throw new IllegalArgumentException("cannot persist: " + obj.getClass.getName)
    }
  }
}

