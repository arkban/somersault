package somersault.core.filters

/**
 * Provides some advanced {@link SomerFilter}s built from the basic {@link SomerFilter}s.
 */
object ExtendedFilters {

  /**
   * Standard to decide which file should be accepted by an ActionBuilder for transfer between the source and
   * destination. This excludes many temporary, cache, and other files and directories.
   */
  val standardBuilderFilter: SomerFilter = {

    val pathFilter = new PathFilter(
      "/ssl/certs$", "/etc/bash_completion.d$", "/Windows/Temp$")

    val nameFilter = new NameFilter(
      // general
      "(?i)\\.?te?mp(orary)?$", // lots of word contain or start with "temp", so we anchor to the end
      "(?i)\\.cache$",
      "(?i)\\.?old$",
      "(?i)\\.?bak$",
      "(?i)\\.?backup$",
      "(?i)cache", // there are /very/ few words that contain "cache", and all are very rare (like "cachemia")
      "(?i)^logs?$",
      // unix directories
      "^bin$",
      "^boot$",
      "^dev$",
      "^dev$",
      "^lib(\\d\\d)?$",
      "^media$",
      "^mnt$",
      "^opt$",
      "^proc$",
      "^selinux$",
      "^srv$",
      "^sys$",
      // windows directories
      "^UserData$", // internet explorer temporary files
      "^History$", // windows history
      "^System Volume Information$", // NTFS data      // cvs ignore - http://docs.freebsd.org/info/cvs/cvs.info.cvsignore.html
      "RCS",
      "SCCS",
      "CVS",
      "CVS.adm",
      "RCSLOG",
      "^cvslog\\.",
      "(?i)tags",
      "\\.make\\.state",
      "\\.nse_depinfo",
      "~$", // vim/emacs backup file
      "^#",
      "^\\.#",
      "^,",
      "^_\\$",
      "\\$$",
      "\\.old$",
      "\\.bak$",
      "\\.BAK$",
      "\\.orig$",
      "\\.rej$",
      "^\\.del-",
      "\\.a$",
      "\\.olb$",
      "\\.o$",
      "\\.obj$",
      "\\.so$",
      "\\.exe$",
      "\\.Z$",
      "\\.elc$",
      "\\.ln$",
      "\\.svn", // subversion directory
      "(?i)ntuser\\.dat", // windows registry files (which are always locked)
      "(?i)pagefile.sys", // windows page file
      "(?i)hiberfil.sys") // windows hibernate state file

    new NotFilter(new OrFilter(pathFilter, nameFilter))
  }

  /**
   * Standard filter to decide which files should be compressed and which should not. This filter excludes many file
   * types that are already compressed.
   */
  val standardCompressionFilter: SomerFilter = new NotFilter(
    NameFilter.createExtensionFilter(
      // video
      "avi", "mpeg", "ogv", "mkv", "asf", "f[l4]v", "3gp", "mj2", "rm",
      // audio
      "mp[234]", "ogg", "[fa]lac", "au", "m4p", "ra", "wma", "xmf", "aac",
      // image
      "gif", "jpe?g", "jp[2x]", "[mp]ng", "ps[dp]", "ai", "exif", "webp", "svg", "pdf", "swf",
      // compressed
      "7z", "ace", "arc", "arj", "bz2?", "bzip2?", "cab", "gzi?p?", "lzh?", "lzma", "lzo", "pkg", "rar", "r\\d+",
      "sitx?", "taz", "tbz2?", "tz", "xz", "z", "zip",
      // misc
      "cb[7rz]", "deb", "rpm"))
}
