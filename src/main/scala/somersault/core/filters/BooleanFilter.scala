package somersault.core.filters

import somersault.core.filesystems.SomerArtifact
import nu.xom.Element
import org.apache.commons.lang.BooleanUtils
import somersault.util.persist.{PersisterContext, Persister}

/**
 * Returns the constructor set boolean for all SomerArtifacts. This exists to be used as a default filter to include or
 * exclude everything.
 */
class BooleanFilter( val value: Boolean) extends SomerFilter
{
  def accept( parentPath: Iterable[ String ], a: SomerArtifact) = value
}

class BooleanFilterPersister extends SomerFilterPersister
{
  def canRead( context: PersisterContext, element: Element): Boolean = checkClass( element, classOf[ BooleanFilter ])

  def read[ T ]( context: PersisterContext, element: Element) = new BooleanFilter(
    BooleanUtils.toBoolean( element.getAttribute( "value").getValue)).asInstanceOf[ T ]

  def write( context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false)
  {
    obj match
    {
      case f: BooleanFilter =>
        writeClass( obj, element, dynamic)
        Persister.createAttribute( "value", BooleanUtils.toStringTrueFalse( f.value), element)
      case _ => throw new IllegalArgumentException( "cannot persist: " + obj.getClass.getName)
    }
  }
}
