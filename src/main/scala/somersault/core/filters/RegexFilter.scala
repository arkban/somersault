package somersault.core.filters

import java.util.regex.Pattern
import nu.xom.Element
import somersault.util.persist.Persister

/**
 * Base class for Filters that match SomerArtifacts using regular expressions.
 */
abstract class RegexFilter(val patterns: Iterable[Pattern]) extends SomerFilter {

  def this(pattern: Pattern, extra: Pattern*) = this(pattern :: Nil ++ extra)

  def this(pattern: String, extra: String*) = this((pattern :: Nil ++ extra).map(Pattern.compile(_)))
}

abstract class RegexFilterPersister extends SomerFilterPersister {

  protected def readPatterns(element: Element): Iterable[Pattern] =
    Persister.elementsToIterable(element.getChildElements("pattern")).map(e => Pattern.compile(e.getValue))

  protected def writePatterns(f: RegexFilter, element: Element) {
    f.patterns.foreach(p => Persister.createElement("pattern", p.toString, element))
  }
}