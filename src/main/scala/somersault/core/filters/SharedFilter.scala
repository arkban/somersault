package somersault.core.filters

import java.io.File
import nu.xom.Element
import somersault.core.filesystems.SomerArtifact
import somersault.util.persist.{SharedPersister, PersisterContext}

/**
 * A special {@link SomerFilter} that persists its internal {@link SomerFilter} in an external file. This allows
 * sharing a {@link SomerFilter} with multiple {@link Scenario}s. The filtering logic acts as a simple pass through
 * to the root {@link SomerFilter} in that external file.
 */
class SharedFilter(val filter: SomerFilter, val filePath: File) extends GroupFilter(filter :: Nil) {

  def accept(parentPath: Iterable[String], a: SomerArtifact) =
    filters.headOption.map(filter => filter.accept(parentPath, a)).getOrElse(false)
}

class SharedFilterPersister extends GroupFilterPersister with SharedPersister {

  def canRead(context: PersisterContext, element: Element): Boolean = checkClass(element, classOf[SharedFilter])

  def read[T](context: PersisterContext, element: Element): T = {
    val rawFilePath = readRawFilePath(element)
    val filter: SomerFilter = readSharedObject(context, rawFilePath)
    new SharedFilter(filter, rawFilePath).asInstanceOf[T]
  }

  def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false) {
    obj match {
      case f: SharedFilter =>
        writeClass(obj, element, dynamic)
        writeSharedObject(context, f.filePath, f.filter)
        writeRawFilePath(element, f.filePath)
      case _ => throw new IllegalArgumentException("cannot write: " + obj.getClass.getName)
    }
  }
}
