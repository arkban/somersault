package somersault.core.filters

import somersault.core.filesystems.{SomerFile, SomerArtifact}
import nu.xom.Element
import org.apache.commons.lang.BooleanUtils
import somersault.util.persist.{PersisterContext, Persister}

/**
 * Filters on a file attributes, returning true if the file's attributes match the filter's attributes. A value of
 * None means that any file attribute value will be accepted. Returns true for SomerDirectory because that simplifies
 * usage, since this filter is usually used without a NotFilter.
 */
class FileAttributesFilter(
    val executable: Option[ Boolean ], val readable: Option[ Boolean ], val writable: Option[ Boolean ])
    extends SomerFilter
{
  def accept( parentPath: Iterable[ String ], a: SomerArtifact) =
    a match
    {
      case f: SomerFile =>
      {
        // if none use what the file has so the comparison below we will return true
        val exec = executable.getOrElse( f.attributes.executable)
        val read = readable.getOrElse( f.attributes.readable)
        val write = writable.getOrElse( f.attributes.writable)

        exec == f.attributes.executable &&
            read == f.attributes.readable &&
            write == f.attributes.writable
      }
      case _ => true
    }
}

class FileAttributesFilterPersister extends SomerFilterPersister
{
  def canRead( context: PersisterContext, element: Element): Boolean = checkClass(
    element, classOf[ FileAttributesFilter ])

  def read[ T ]( context: PersisterContext, element: Element): T =
    new FileAttributesFilter(
      convert( element.getAttributeValue( "executable")), convert( element.getAttributeValue( "readable")), convert(
        element.getAttributeValue( "writable"))).asInstanceOf[ T ]

  def write( context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false)
  {
    obj match
    {
      case f: FileAttributesFilter =>
        writeClass( obj, element, dynamic)
        Persister.createAttribute( "executable", convert( f.executable), element)
        Persister.createAttribute( "readable", convert( f.readable), element)
        Persister.createAttribute( "writable", convert( f.writable), element)
      case _ => throw new IllegalArgumentException( "cannot write: " + obj)
    }
  }

  private def convert( value: String): Option[ Boolean ] =
    value.trim.toLowerCase match
    {
      case "any" => None
      case _ => Some( BooleanUtils.toBoolean( value))
    }

  private def convert( value: Option[ Boolean ]): String =
    value match
    {
      case Some( b) => BooleanUtils.toStringYesNo( b)
      case None => "any"
    }
}