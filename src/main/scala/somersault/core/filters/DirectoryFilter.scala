package somersault.core.filters

import somersault.core.filesystems.{SomerDirectory, SomerArtifact}
import nu.xom.Element
import somersault.util.persist.PersisterContext

/**
 * Returns true if this is a SomerDirectory.
 */
class DirectoryFilter extends SomerFilter
{
  def accept( parentPath: Iterable[ String ], a: SomerArtifact) = a.isInstanceOf[ SomerDirectory ]
}

class DirectoryFilterPersister extends SomerFilterPersister
{
  def canRead( context: PersisterContext, element: Element): Boolean = checkClass( element, classOf[ DirectoryFilter ])

  def read[ T ]( context: PersisterContext, element: Element): T = new DirectoryFilter( ).asInstanceOf[ T ]

  def write( context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false)
  {
    obj match
    {
      case f: DirectoryFilter => writeClass( obj, element, dynamic)
      case _ => throw new IllegalArgumentException( "cannot write: " + obj.getClass.getName)
    }
  }
}
