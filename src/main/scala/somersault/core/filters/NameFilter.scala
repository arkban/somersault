package somersault.core.filters

import java.util.regex.Pattern
import nu.xom.Element
import somersault.core.filesystems.SomerArtifact
import somersault.util.persist.PersisterContext

/**
 * Matches names of SomerArtifacts using regular expressions.
 *
 * This does a complete match again the SomerArtifact's path, it will not return true if the pattern is found within
 * the name. (So it is advised to append and prepend ".*" if that is the behavior you want.)
 */
class NameFilter(patterns: Iterable[Pattern]) extends RegexFilter(patterns) {

  def this(pattern: Pattern, extra: Pattern*) = this(pattern :: Nil ++ extra)

  def this(pattern: String, extra: String*) = this((pattern :: Nil ++ extra).map(Pattern.compile(_)))

  def accept(parentPath: Iterable[String], a: SomerArtifact) = patterns.exists(_.matcher(a.name).find)
}

object NameFilter {

  /**
   * Creates a NameFilter that matches a case-insensitive file name extension SomerArtifacts using a regular
   * expression. The "." separator should not be included; it will be added by this method.
   */
  def createExtensionFilter(ext: String, extra: String*) = new NameFilter(wrap(ext) :: Nil ++ extra.map(wrap(_)))

  private def wrap(ext: String): Pattern = Pattern.compile("(?i)\\." + ext + "$")
}

class NameFilterPersister extends RegexFilterPersister {

  def canRead(context: PersisterContext, element: Element): Boolean = checkClass(element, classOf[NameFilter])

  def read[T](context: PersisterContext, element: Element): T = new NameFilter(readPatterns(element))
    .asInstanceOf[T]

  def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false) {
    obj match {
      case f: NameFilter =>
        writeClass(obj, element, dynamic)
        writePatterns(f, element)
      case _ => throw new IllegalArgumentException("cannot write: " + obj)
    }
  }
}
