package somersault.core.filters

import somersault.core.filesystems.SomerArtifact
import somersault.util.persist.Persister

/**
 * Base class for filters to be used by SomerFileSystemComparators in deciding which actions to create. In general the
 * usage should be that anything the filter accepts should be kept or added, and anything the filter rejects should be
 * removed.
 */

abstract class SomerFilter {

  def accept(parentPath: Iterable[String], a: SomerArtifact): Boolean
}

trait SomerFilterPersister extends Persister
