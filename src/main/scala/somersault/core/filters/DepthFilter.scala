package somersault.core.filters

import somersault.core.filesystems.SomerArtifact
import nu.xom.Element
import org.apache.commons.lang.math.NumberUtils
import somersault.util.persist.{PersisterContext, Persister}

/**
 * Filters based on the depth of the path. The artifact in question counts as 1, so root artifacts have a depth of 1.
 * The check is inclusive on both checks.
 */
class DepthFilter( val min: Int, val max: Int) extends SomerFilter
{
  def accept( parentPath: Iterable[ String ], a: SomerArtifact) =
  {
    val depth = parentPath.size + 1
    depth >= min && depth <= max
  }
}

class DepthFilterPersister extends SomerFilterPersister
{
  def canRead( context: PersisterContext, element: Element): Boolean = checkClass( element, classOf[ DepthFilter ])

  def read[ T ]( context: PersisterContext, element: Element): T =
    new DepthFilter(
      NumberUtils.toInt( element.getAttributeValue( "min")), NumberUtils.toInt( element.getAttributeValue( "max")))
        .asInstanceOf[ T ]

  def write( context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false)
  {
    obj match
    {
      case f: DepthFilter =>
        writeClass( obj, element, dynamic)
        Persister.createAttribute( "max", f.max.toString, element)
        Persister.createAttribute( "min", f.min.toString, element)
      case _ => throw new IllegalArgumentException( "cannot write: " + obj.getClass.getName)
    }
  }
}
