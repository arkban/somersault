package somersault.core

import filters.{BooleanFilter, SomerFilter}
import nu.xom.Element
import org.apache.commons.lang.math.NumberUtils
import somersault.core.comparators.{BooleanComparator, SomerFileComparator}
import somersault.core.filesystems.{FileSystemData, FileSystem}
import somersault.util.persist.{PersisterContext, Persister}
import somersault.util.{VersionMismatchException, VersionPersister, Version}

/**
 * A Scenario contains all the information necessary to compare to {@link FileSystem}s with an {@link ActionBuilder}
 * and execute the created {@link Action}s using an {@link ActionRunner}.
 *
 * This class does not contain any logic to build the {@link FileSystem}s or execute the {@link ActionBuilder} or
 * {@link ActionRunner}. All of that logic, and the logic that will create a {@link Scenario} from a
 * {@link ScenarioPersisted}, should be contained in execution code for the user interface.
 */
class Scenario(val data: ScenarioData, val source: FileSystem, val dest: FileSystem)

/**
 * @param name A user-friendly name for this scenario
 * @param description A user-friendly description to provide more information about this scenario
 */
class ScenarioData(
  val name: String,
  val description: String,
  val source: FileSystemData,
  val dest: FileSystemData,
  val comparator: SomerFileComparator = new BooleanComparator(true),
  val filter: SomerFilter = new BooleanFilter(true),
  val maxRepeatAttempts: Int = 1) {

  def this(orig: ScenarioData, newFilter: SomerFilter) =
    this(orig.name, orig.description, orig.source, orig.dest, orig.comparator, newFilter, orig.maxRepeatAttempts)

  val version = ScenarioData.version
}

object ScenarioData {

  val version = new Version(0, 1, 0)
}

class ScenarioDataPersister extends Persister {

  def canRead(context: PersisterContext, element: Element) = checkClass(element, classOf[ScenarioData])

  def read[T](context: PersisterContext, element: Element) = {
    val version = new VersionPersister().read[Version](context, Persister.getChildElement(element, "version"))
    if (version != ScenarioData.version)
      throw new VersionMismatchException(ScenarioData.version, version)

    val name = Persister.getChildElement(element, "name").getValue
    val desc = Persister.findChildElement(element, "description") match {
      case Some(e: Element) => e.getValue
      case None => ""
    }
    val source = context.container.read[FileSystemData](context, Persister.getChildElement(element, "source"))
    val dest = context.container.read[FileSystemData](context, Persister.getChildElement(element, "dest"))
    val comparator = context.container.read[SomerFileComparator](
      context, Persister.getChildElement(element, "comparator"))
    val filter = context.container.read[SomerFilter](context, Persister.getChildElement(element, "filter"))
    val maxRepeatAttempts = NumberUtils.toInt(Persister.getChildElement(element, "maxRepeatAttempts").getValue)
    new ScenarioData(name, desc, source, dest, comparator, filter, maxRepeatAttempts).asInstanceOf[T]
  }

  def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean) {
    obj match {
      case d: ScenarioData =>
        writeClass(obj, element, dynamic)
        new VersionPersister().write(context, d.version, Persister.createElement("version", parent = element))
        Persister.createElement("name", d.name, element)
        Persister.createElement("description", d.description, element)
        val source = Persister.createElement("source", parent = element, comment = "source file system definition")
        context.container.write(context, d.source, source, dynamic = true)
        val dest = Persister.createElement("dest", parent = element, comment = "dest file system definition")
        context.container.write(context, d.dest, dest, dynamic = true)
        val comparator = Persister.createElement(
          "comparator", parent = element, comment = "used to compare files in the source against the dest")
        context.container.write(context, d.comparator, comparator, dynamic = true)
        val filter = Persister.createElement(
          "filter", parent = element, comment = "determines what is transferred from source to dest")
        context.container.write(context, d.filter, filter, dynamic = true)
        Persister.createElement(
          "maxRepeatAttempts", d.maxRepeatAttempts.toString, element, "number of attempts before failing an operation")
      case _ => throw new IllegalArgumentException("cannot persist: " + obj.getClass.getName)
    }
  }
}
