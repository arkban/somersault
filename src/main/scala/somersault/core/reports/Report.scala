package somersault.core.reports

import java.io._
import java.util.concurrent.Callable
import somersault.util.FilenameUtils2

/**
 * Base trait for all reports. Reports are generic "data dumps", in some sort of text format, often human-redable,
 * about a particular object -- usually a {@link FileSystem} or {@link ActionContainerBuilder}. They are good ways to get
 * information about the things that Somersault produces, usually to support enterprise-y usages.
 *
 * Reports are designed to be run in threads, and they should listen to the {@link Thread#interrupted} flag in case
 * they are cancelled.
 */
trait Report {

  /**
   * The name of the report. This should include something about the object it works on, for example a {@link Report}
   * on a {@link FileSystem} should included the {@link FileSystemData#name)
   */
  def name: String

  /**
   * Writes the report to the provided {@link Writer}. The writer is expected to be open for writing.
   *
   * The implementation is expected to append only, and to NOT close the writer when done.
   */
  def write(writer: Writer)
}

/**
 * A {@link Callable} that will execute a {@link Report}, writing its contents to a temporary file. The {@link File}
 * can be obtained via the {@link ReportTaskResult} returned when finished.
 */
class ReportTask(val report: Report, val tempPath: File) extends Callable[ReportTaskResult] {

  def call(): ReportTaskResult = {
    val file = FilenameUtils2.createTempFile("report", ".report", tempPath)
    val writer = new FileWriter(file)
    try {
      report.write(writer)
      new ReportTaskResult(file)
    }
    finally {
      writer.close()
    }
  }
}

/**
 * Result of executing a {@link ReportTask}. This indicates the temporary file that holds the {@link Report}'s data.
 */
class ReportTaskResult(val file: File)
