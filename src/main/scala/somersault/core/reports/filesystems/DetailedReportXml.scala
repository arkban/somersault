package somersault.core.reports.filesystems

import java.io.{PrintWriter, Writer}
import org.joda.time.DateTime
import somersault.core.filesystems._
import somersault.core.filesystems.index.IndexFile
import somersault.core.reports.Report
import somersault.util.AlphaNumComparator

/**
 * Produces a detailed report that indicates every file and directory in the {@link FileSystem}. The contents
 * are in XML format. This is closely related to (and should be kept in sync with) {@link DetailedReportText}.
 *
 * The implementation of this class uses manually created XML for speed. Its harder to work with but a lot faster
 * and less memory intensive than actually creating the XML DOM using objects.
 */
class DetailedReportXml(fs: FileSystem) extends Report {

  private val comparator =
    (x: SomerArtifact, y: SomerArtifact) => new AlphaNumComparator().compare(x.name, y.name) < 0

  private val indent = " " * 4

  def name: String = fs.data.name + " Detailed (XML)"

  def write(writer: Writer) {
    val printer = new PrintWriter(writer, true)

    printer.println("<report>")
    printer.println(indent + "<header>")
    printer.println(indent + "<name>File System Detailed Report (XML)</name>")
    printer.println(
      indent + String.format("<created>%s</created>", new DateTime().toString("yyyy-MM-dd HH:mm:ss")))
    printer.println(indent + "</header>")
    printer.println(indent + "<contents>")
    recurse(2, fs.root, printer)
    printer.println(indent + "</contents>")
    printer.println("</report>")
  }

  private def recurse(indentLevel: Int, dir: SomerDirectory, printer: PrintWriter) {
    if (Thread.currentThread.isInterrupted) return // can't clear the interrupt flag, as this is a recursive call
    val currIndent = indent * indentLevel

    val children = fs.children(dir).toList.sortWith(comparator)
    children.foreach {
      case f: SomerFile =>

        val indexedData = if (fs.data.isIndexed) {
          val index = fs.getIndexArtifact(f).asInstanceOf[IndexFile]
          String.format(
            " hash='%s' readable='%s' writable='%s' executable='%s'",
            index.hash.toString(hashInfoPrefix = false),
            index.attributes.readable.toString,
            index.attributes.writable.toString,
            index.attributes.executable.toString)
        }
        else ""

        val normal = "%s<file size='%d' modified='%s'%s>%s</file>"
        printer.println(
          String.format(
            normal,
            currIndent,
            Predef.long2Long(f.size),
            f.modified.toString("yyyy-MM-dd HH:mm:ss"),
            indexedData,
            f.name))
      case d: SomerDirectory =>
        printer.println(String.format("%s<dir>%s</dir>", currIndent, d.name))
        recurse(indentLevel + 1, d, printer)
    }
  }
}