package somersault.core.reports.filesystems

import java.io.{Writer, PrintWriter}
import org.apache.commons.io.FilenameUtils
import org.joda.time.DateTime
import scala.collection.mutable
import somersault.core.filesystems.{SomerFile, SomerDirectory, FileSystem}
import somersault.core.reports.Report
import somersault.util.AlphaNumComparator

/**
 * Prints out summary information about a {@link FileSystem} in a text format.
 */
class SummaryReport(fs: FileSystem) extends Report {

  def name: String = fs.data.name + " Summary"

  def write(writer: Writer) {
    val map = new mutable.HashMap[String, SummaryReport.Accumulator]
    map += (SummaryReport.directoryTag -> new SummaryReport.Accumulator)

    recurse(fs.root, map)

    val fileTotal = new SummaryReport.Accumulator
    map.values.foreach(
      acc => {
        fileTotal.size += acc.size
        fileTotal.count += acc.count
      })

    val printer = new PrintWriter(writer, true)

    printer.println("File System Summary Report")
    printer.println(String.format("Created: %s", new DateTime().toString("yyyy-MM-dd HH:mm:ss")))
    printer.println()
    printer.println("-------------------------------------------------------------------------------")
    printer.println("[ General ] ")
    printer.println()
    printer.println(String.format("Name: %s", fs.data.name))
    printer.println(String.format("Indexed: %s", if (fs.data.isIndexed) "Yes" else "No"))
    printer.println(String.format("Hash:"))
    printer.println(String.format("\tName: %s", fs.data.hashInfo.name))
    printer.println("\tSize: %d".format(fs.data.hashInfo.size))
    printer.println(String.format("Compression: %s", fs.data.compressionProvider.name))
    printer.println(String.format("Encryption: %s", fs.data.encryptionProvider.name))
    printer.println(String.format("\tKey: %s", fs.data.encryptionProvider.key.name))
    printer.println()
    printer.println("-------------------------------------------------------------------------------")
    printer.println("[ Statistics ]")
    printer.println()
    printer.println("Type\tCount\tSize")
    printer.println()
    printer.println(String.format("(dirs)\t%1$s\t0", Predef.long2Long(map(SummaryReport.directoryTag).count)))
    printer.println("(files)\t%1$d\t%2$d".format(fileTotal.count, fileTotal.size))

    val comparator = (x: String, y: String) => new AlphaNumComparator().compare(x, y) < 0
    val keys = map.keys.filter(!_.equals(SummaryReport.directoryTag)).toList.sortWith(comparator)
    keys.foreach(
      key => {
        if (Thread.currentThread.isInterrupted) return

        val acc = map(key)
        printer.println("%1$s\t%2$d\t%3$d".format(key, acc.count, acc.size))
      })
  }

  private def recurse(dir: SomerDirectory, map: mutable.HashMap[String, SummaryReport.Accumulator]) {
    if (Thread.currentThread.isInterrupted) return // can't clear the interrupt flag, as this is a recursive call

    fs.children(dir).foreach {
      case f: SomerFile =>
        val ext = FilenameUtils.getExtension(f.name)
        if (ext.length > 0) {
          // ignore files with no extension
          val acc = map.getOrElseUpdate(ext, new SummaryReport.Accumulator)
          acc.size += f.size
          acc.count += 1
        }
      case d: SomerDirectory =>
        map(SummaryReport.directoryTag).count += 1
        recurse(d, map)
    }
  }
}

object SummaryReport {

  private val directoryTag = "."

  /** Reports statistics for a single type of file. */
  private class Accumulator {

    var count = 0L

    var size = 0L
  }

}