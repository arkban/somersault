package somersault.core.reports.filesystems

import java.io.{PrintWriter, Writer}
import org.joda.time.DateTime
import somersault.core.filesystems._
import somersault.core.filesystems.index.IndexFile
import somersault.core.reports.Report
import somersault.util.AlphaNumComparator

/**
 * Produces a detailed report that indicates every file and directory in the {@link FileSystem}. The contents
 * are in CSV format. This is closely related to (and should be kept in sync with) {@link DetailedReportXml}.
 */
class DetailedReportText(fs: FileSystem) extends Report {

  private val comparator =
    (x: SomerArtifact, y: SomerArtifact) => new AlphaNumComparator().compare(x.name, y.name) < 0

  def name: String = fs.data.name + " Detailed (Text)"

  def write(writer: Writer) {
    val printer = new PrintWriter(writer, true)

    printer.println("File System Detailed Report (CSV)")
    printer.println(String.format("Created: %s", new DateTime().toString("yyyy-MM-dd HH:mm:ss")))
    printer.println()
    printer.print("Type, Path, Size, Modified Date")
    if (fs.data.isIndexed) printer.print(", Hash, Readable, Writable, Executable")
    printer.println()

    recurse("", fs.root, printer)
  }

  private def recurse(parentPath: String, dir: SomerDirectory, printer: PrintWriter) {
    if (Thread.currentThread.isInterrupted) return // can't clear the interrupt flag, as this is a recursive call
    val currPath = parentPath + dir.name + "/"

    val children = fs.children(dir).toList.sortWith(comparator)
    children.foreach {
      case f: SomerFile =>
        printer.print(
          "File, %s%s, %d, %s".format(currPath, f.name, f.size, f.modified.toString("yyyy-MM-dd HH:mm:ss")))
        if (fs.data.isIndexed) {
          val index = fs.getIndexArtifact(f).asInstanceOf[IndexFile]
          printer.print(
            String.format(
              "%s, %s, %s, %s",
              index.hash.toString(hashInfoPrefix = false),
              if (index.attributes.readable) "Yes" else "No",
              if (index.attributes.writable) "Yes" else "No",
              if (index.attributes.executable) "Yes" else "No"))
        }
        printer.println()
      case d: SomerDirectory =>
        printer.println(String.format("Dir , %s%s", currPath, d.name))
        recurse(currPath, d, printer)
    }
  }
}