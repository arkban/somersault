package somersault.core.actions.runners

import logahawk.Severity
import net.jcip.annotations.ThreadSafe
import somersault.core.actions.{DirectoryAction, CreateDirAction, TransferAction, RemoveAction, ActionComparator}
import somersault.core.filesystems.{SomerFile, SomerDirectory}
import somersault.core.{Scenario, StreamPump}
import somersault.util.AbstractMultiObserver

/**
 * Base class for all classes that execute a single SomerAction
 *
 * Instances of these classes are designed to be used for multiple actions within the same parent directory.
 */
trait ActionRunner[T] {

  def run(action: T)
}

/**
 * Base class for the various data holder classes defined below.
 */
trait ActionRunnerData {

  def scenario: Scenario

  def actionObserver: ActionRunnerObserver
}

/**
 * A utility class for holding common data for ActionRunners acting within a two FileSystems.
 */
class FileSystemRunnerData(
  val scenario: Scenario, val actionObserver: ActionRunnerObserver, val pump: StreamPump = new StreamPump)
  extends ActionRunnerData {

  val actionComparator = new ActionComparator
}

/**
 * A utility class for holding common data for ActionRunners acting within a two SomerDirectories.
 *
 * @param path The current path, mostly intended to be used when creating Exceptions. This path includes the name of
 *             the directory described by src and dest.
 */
class DirectoryRunnerData(
  fsData: FileSystemRunnerData,
  val path: List[String],
  val srcDir: SomerDirectory,
  val destDir: SomerDirectory)
  extends FileSystemRunnerData(fsData.scenario, fsData.actionObserver, fsData.pump) {

  val srcFs = fsData.scenario.source
  val destFs = fsData.scenario.dest
}

/**
 * Observer for tracking and watching the progress of an ActionRunner.
 */
trait ActionRunnerObserver {

  /**
   * @param actionCount The result of a call to ActionContainerBuilder.count.
   */
  def start(actionCount: Long)

  def end()

  def recurse(path: List[String], action: DirectoryAction)

  def transfer(path: List[String], action: TransferAction, file: SomerFile)

  def createDir(path: List[String], action: CreateDirAction)

  def remove(path: List[String], action: RemoveAction)

  def error(severity: Severity, ex: Exception)
}

/**
 * Forwards each call to this observer to the underlying observers.
 */
@ThreadSafe
class MultiActionRunnerObserver extends AbstractMultiObserver[ActionRunnerObserver] with ActionRunnerObserver {

  def start(actionCount: Long) { observers.foreach(_.start(actionCount)) }

  def end() { observers.foreach(_.end()) }

  def recurse(path: List[String], action: DirectoryAction) { observers.foreach(_.recurse(path, action)) }

  def transfer(path: List[String], action: TransferAction, file: SomerFile) {
    observers.foreach(_.transfer(path, action, file))
  }

  def createDir(path: List[String], action: CreateDirAction) { observers.foreach(_.createDir(path, action)) }

  def remove(path: List[String], action: RemoveAction) { observers.foreach(_.remove(path, action)) }

  def error(severity: Severity, ex: Exception) { observers.foreach(_.error(severity, ex)) }
}
