package somersault.core.actions.runners

import logahawk.Severity
import somersault.core.actions.CreateDirAction
import somersault.core.exceptions.{UnknownActionRunnerException, SomerFileSystemException, SomerActionRunnerException}
import somersault.util.Repeater

/**
 * Executes a CreateDirAction, which creates a new directory in the destination.
 */
class CreateDirActionRunner(dirData: DirectoryRunnerData) extends ActionRunner[CreateDirAction] {

  def run(action: CreateDirAction) {
    dirData.destFs.find(dirData.destDir, action.name) match {
      case Some(d) => // directory exists, do nothing
      case None =>
        try {
          dirData.actionObserver.createDir(dirData.path, action)
          new Repeater(
            () => dirData.scenario.dest.createDir(dirData.destDir, action.name),
            dirData.scenario.data.maxRepeatAttempts)
            .repeat()

          None
        }
        catch {
          case ex: SomerActionRunnerException => dirData.actionObserver.error(Severity.ERROR, ex)
          case ex: SomerFileSystemException => dirData.actionObserver.error(
            Severity.ERROR, new UnknownActionRunnerException(action, ex))
          case ex: Exception => dirData.actionObserver.error(
            Severity.ERROR, new UnknownActionRunnerException(action, dirData.path, ex))
        }
      case x => dirData.actionObserver.error(
        Severity.ERROR, new SomerActionRunnerException("Unexpected match: " + x, action, dirData.path))
    }
  }
}