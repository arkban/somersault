package somersault.core.actions.runners

import logahawk.Severity
import scala.Some
import scala.collection.mutable
import somersault.core.actions.CreateDirAction
import somersault.core.actions.DirectoryAction
import somersault.core.actions.RemoveAction
import somersault.core.actions.SomerAction
import somersault.core.actions.TransferAction
import somersault.core.actions.containers.ActionContainerProvider
import somersault.core.exceptions._
import somersault.core.filesystems.{SomerFile, SomerDirectory}
import somersault.util.FilenameUtils2

/**
 * An object that ingests Actions and creates ActionRunners to execute them. This interface exists to allow flexibility
 * and and provide clarity to the objects that fulfill that role.
 */
class ActionRunnerController(val fsData: FileSystemRunnerData) {

  /**
   * Executes the SomerActions contained in the provided ActionContainerBuilder and returns any Exceptions encountered
   * along the way. This will execute the SomerActions according to the hierarchy it defines.
   */
  def run(actionContainer: ActionContainerProvider) {
    try {
      fsData.actionObserver.start(actionContainer.count)

      // we use a queue and a loop instead of the more obvious recursive solution to process DirectoryActions because
      // we want to avoid any potential stack overflow problems, as well as to (hopefully) allow quickly garbage
      // collection of the (potentially) large data structures created for child directories (such as
      // DirectoryRunnerData instances). A recursive solution would need to somehow allocate the Maps in such a way
      // that they didn't stick around as we descended the directory tree.
      val entryQueue = new mutable.Queue[DirActionEntry]
      entryQueue += new DirActionEntry(
        Nil, actionContainer.root, fsData.scenario.source.root, fsData.scenario.dest.root)

      while (!entryQueue.isEmpty)
        run(actionContainer, entryQueue.dequeue()).foreach(e => entryQueue.enqueue(e))
    }
    finally {
      fsData.actionObserver.end()
    }
  }

  /**
   * Runs the provided actions and creates actions for the sub-directories.
   */
  private def run(actionContainer: ActionContainerProvider, entry: DirActionEntry): Iterable[DirActionEntry] = {
    try {
      val actions = actionContainer.children(entry.dirAction).toList
        .sortWith((a, b) => fsData.actionComparator.compare(a, b) < 0)

      val dirData = new DirectoryRunnerData(fsData, entry.path, entry.src, entry.dest)

      runLocalActions(dirData, actions, entry)

      createSubDirActionEntries(dirData, actions, entry)
    }
    catch {
      case ex: Exception =>
        val msg = "Error running actions"
        fsData.actionObserver.error(
          Severity.ERROR,
          new Exception(msg + ": " + FilenameUtils2.dirSeparator + FilenameUtils2.concat(entry.path), ex))
        Nil
    }
  }

  /**
   * Runs the provided actions which execute on the current directory. Any exceptions created or caught will be
   * returned.
   *
   * This is a separate method so that any allocations within this will hopefully be garbage collected ASAP.
   *
   * @param actions This method will silently ignore any DirectoryActions.
   */
  private def runLocalActions(dirData: DirectoryRunnerData, actions: Iterable[SomerAction], entry: DirActionEntry) {
    val createDirRunner = new CreateDirActionRunner(dirData)
    val removeRunner = new RemoveActionRunner(dirData)
    val transferRunner = new TransferActionRunner(dirData)

    actions.foreach {
      case a: DirectoryAction => // handled by the caller
      case a: CreateDirAction => createDirRunner.run(a)
      case a: RemoveAction => removeRunner.run(a)
      case a: TransferAction => transferRunner.run(a)
      case a => dirData.actionObserver.error(
        Severity.ERROR, new UnknownActionRunnerException("Unrecognized action", a, entry.path))
    }
  }

  /**
   * Creates new DirActionEntry instances based on the DirectoryActions in the actions iterable. Any new DirEntryActions
   * will be enqueued in the provided Queue. Any exceptions created or caught will be returned.
   *
   * This is a separate method so that any allocations within this will hopefully be garbage collected ASAP.
   *
   * @param actions This method will silently ignore any SomerActions except DirectoryActions.
   */
  private def createSubDirActionEntries(
    dirData: DirectoryRunnerData,
    actions: Iterable[SomerAction],
    entry: DirActionEntry): Iterable[DirActionEntry] = {
    val entryQueue = new mutable.ListBuffer[DirActionEntry]

    // find DirectoryActions and append them to our queue
    actions.foreach {
      case dirAction: DirectoryAction => {
        val dirPath = entry.path ::: dirAction.name :: Nil

        fsData.actionObserver.recurse(dirPath, dirAction)

        val srcArtifact = dirData.srcFs.find(dirData.srcDir, dirAction.name)
        val destArtifact = dirData.destFs.find(dirData.destDir, dirAction.name)

        (srcArtifact, destArtifact) match {
          case (Some(src: SomerDirectory), Some(dest: SomerDirectory)) =>
            entryQueue += new DirActionEntry(dirPath, dirAction, src, dest)
          // rare but not completely unexpected cases
          case (Some(src: SomerDirectory), Some(dest: SomerFile)) =>
            dirData.actionObserver.error(
              Severity.WARN, new FileExistsWhereDirExpectedActionRunnerException(dirAction, dirPath))
          case (Some(src: SomerFile), Some(dest: SomerDirectory)) =>
            dirData.actionObserver.error(
              Severity.WARN, new DirExistsWhereFileExpectedActionRunnerException(dirAction, dirPath))
          case (Some(src: SomerDirectory), None) =>
            dirData.actionObserver.error(
              Severity.WARN, new DestDoesNotExistActionRunnerException(dirAction, dirPath))
          case (Some(src: SomerFile), None) =>
            dirData.actionObserver.error(
              Severity.WARN, new FileExistsWhereDirExpectedActionRunnerException(dirAction, dirPath))
          case (None, Some(dest)) =>
            dirData.actionObserver.error(
              Severity.WARN, new SourceDoesNotExistActionRunnerException(dirAction, dirPath))
          // impossible (so we think) cases
          case (Some(src: SomerFile), Some(dest: SomerFile)) =>
            dirData.actionObserver.error(
              Severity.ALERT, new UnknownActionRunnerException("Source and Dest are both files", dirAction, dirPath))
          case (None, None) =>
            dirData.actionObserver.error(
              Severity.ALERT, new UnknownActionRunnerException("Neither Source nor Dest exist", dirAction, dirPath))
          case x => dirData.actionObserver.error(
            Severity.ERROR, new SomerActionRunnerException("Unexpected match: " + x, dirAction, dirData.path))
        }
      }
      case _ => // handled by runLocalActions()
    }

    entryQueue
  }

  /**
   * A DirectoryAction plus all the additional data needed to process the actions within that DirectoryAction. This
   * class exists to help us avoid recursion as described in apply() above.
   *
   * @param path Same as for DirectoryRunnerData.path
   */
  private final class DirActionEntry(
    val path: List[String], val dirAction: DirectoryAction, val src: SomerDirectory, val dest: SomerDirectory)

}