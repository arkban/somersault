package somersault.core.actions.runners

import logahawk.Severity
import somersault.core.ActionStreamPumpData
import somersault.core.actions.TransferAction
import somersault.core.exceptions.{UnknownActionRunnerException, SourceDoesNotExistActionRunnerException, DirExistsWhereFileExpectedActionRunnerException, SomerFileSystemException, SomerActionRunnerException}
import somersault.core.filesystems.{SomerDirectory, SomerFile}
import somersault.util.Repeater

/**
 * Executes a TransferAction, which involves transferring the source file to the destination directory.
 */
class TransferActionRunner(dirData: DirectoryRunnerData) extends ActionRunner[TransferAction] {

  def run(action: TransferAction) {
    dirData.srcFs.find(dirData.srcDir, action.name) match {
      case Some(f: SomerFile) =>
        try {
          dirData.actionObserver.transfer(dirData.path, action, f)

          val transferOperation = () => {
            val input = dirData.scenario.source.read(f)
            val compResult = dirData.scenario.dest.data.compressionProvider.encode(dirData.path, f)
            val encResult = dirData.scenario.dest.data.encryptionProvider.encode(dirData.path, f)
            val output = dirData.scenario.dest.write(dirData.destDir, f, compResult, encResult)

            val pumpData = new ActionStreamPumpData(true, Some(f.size), action, dirData.path)
            dirData.pump.pump(input, output, pumpData)
          }

          new Repeater[Unit](transferOperation, dirData.scenario.data.maxRepeatAttempts).repeat()

          None
        }
        catch {
          case ex: SomerActionRunnerException => dirData.actionObserver.error(Severity.ERROR, ex)
          case ex: SomerFileSystemException => dirData.actionObserver.error(
            Severity.ERROR, new UnknownActionRunnerException(action, ex))
          case ex: Exception => dirData.actionObserver.error(
            Severity.ERROR, new UnknownActionRunnerException(action, dirData.path, ex))
        }
      case Some(d: SomerDirectory) =>
        dirData.actionObserver.error(
          Severity.ERROR, new DirExistsWhereFileExpectedActionRunnerException(action, dirData.path))
      case None => dirData.actionObserver.error(
        Severity.ERROR, new SourceDoesNotExistActionRunnerException(action, dirData.path))
      case x => dirData.actionObserver.error(
        Severity.ERROR, new SomerActionRunnerException("Unexpected match: " + x, action, dirData.path))
    }
  }
}
