package somersault.core.actions.runners

import logahawk.Severity
import somersault.core.actions.RemoveAction
import somersault.core.exceptions.{SomerFileSystemException, UnknownActionRunnerException, SomerActionRunnerException}
import somersault.core.filesystems.SomerArtifact
import somersault.util.Repeater

/**
 * Executes a RemoveAction, which removes a SomerArtifact from the destination.
 */

class RemoveActionRunner(dirData: DirectoryRunnerData) extends ActionRunner[RemoveAction] {

  def run(action: RemoveAction) {
    dirData.destFs.find(dirData.destDir, action.name) match {
      case Some(artifact: SomerArtifact) =>
        try {
          dirData.actionObserver.remove(dirData.path, action)

          new Repeater(() => dirData.scenario.dest.remove(artifact), dirData.scenario.data.maxRepeatAttempts)
            .repeat()

          None
        }
        catch {
          case ex: SomerActionRunnerException => dirData.actionObserver.error(Severity.ERROR, ex)
          case ex: SomerFileSystemException => dirData.actionObserver.error(
            Severity.ERROR, new UnknownActionRunnerException(action, ex))
          case ex: Exception => dirData.actionObserver.error(
            Severity.ERROR, new UnknownActionRunnerException(action, dirData.path, ex))
        }
      case None => // artifact does not exist, do nothing
    }
  }
}