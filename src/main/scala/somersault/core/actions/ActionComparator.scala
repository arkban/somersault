package somersault.core.actions

import java.util._

/**
 * Compares SomerActions in the order they should be executed. The basic order is: remove, transfer, create directory,
 * recurse directory. That order frees up space first, then builds the local directory (following the assumption that
 * data closer to the root is more important), prepares to recurse to sub-directories, and finally recurse to
 * sub-directories.
 */

class ActionComparator extends Comparator[ SomerAction ]
{
  def compare( input1: SomerAction, input2: SomerAction ) =
    (input1, input2) match
    {
      case (a: RemoveAction, b: RemoveAction) => a.name.compareTo( b.name )
      case (a: TransferAction, b: TransferAction) => a.name.compareTo( b.name )
      case (a: CreateDirAction, b: CreateDirAction) => a.name.compareTo( b.name )
      case (a: DirectoryAction, b: DirectoryAction) => a.name.compareTo( b.name )
      case (a: RemoveAction, b) => -1
      case (a, b: RemoveAction) => 1
      case (a: TransferAction, b) => -1
      case (a, b: TransferAction) => 1
      case (a: CreateDirAction, b) => -1
      case (a, b: CreateDirAction) => 1
      case (a: DirectoryAction, b) => -1
      case (a, b: DirectoryAction) => 1
      case _ => 0
    }
}