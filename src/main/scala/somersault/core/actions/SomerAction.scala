package somersault.core.actions

/**
 * Describes an action involving a SomerArtifact and one or more FileSystems. Stated another way, SomerActions are
 * actions to be executed against the destination FileSystem, using the source FileSystem as a base. SomerActions are
 * created by comparing two FileSystems using a FileSystemComparator.
 *
 * Actions are arranged in a hierarchy using DirectoryAction as the nodes, and TransferAction, CreateDirAction, and
 * RemoveAction as leaves. This means that SomerActions make sense only in the context of a parent SomerAction -- they
 * are not complete in and of themselves. This is a conscious design decision because it makes the SomerActions
 * extremely light, which is good (and arguably necessary) because Somersault may create many thousands of instances.
 */

trait SomerAction {

  def name: String
}

/**
 * A placeholder action for descending into and executing other actions.
 *
 * The "name" is the name of the directory (the same in both source and dest)
 */
trait DirectoryAction extends SomerAction

/**
 * Copies a file from the source to the destination. This can be used to overwrite a SomerFile that exists in the
 * destination.
 */
trait TransferAction extends SomerAction

/**
 * Creates a new directory in the FileSystem.
 */
trait CreateDirAction extends SomerAction

/**
 * Removes the file or directory.
 */
trait RemoveAction extends SomerAction
