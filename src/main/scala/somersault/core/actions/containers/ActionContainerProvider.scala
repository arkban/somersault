package somersault.core.actions.containers

import somersault.core.actions.{SomerAction, DirectoryAction}

/**
 * Provides SomerActions to ActionRunnerController. SomerActions are created using via an ActionContainerBuilder.
 *
 * NOTE: The final goal of this class is to provide a single Iterator[SomerAction]; this is a half-way step.
 *
 * @see ActionContainerBuilder
 */
trait ActionContainerProvider {

  /**
   * Returns the root directory.
   */
  def root: DirectoryAction

  /**
   * Return a new Collection contain all actions for the provided DirectoryAction. The returned actions are those that
   * should be executed within the provided DirectoryAction.
   */
  def children(parent: DirectoryAction): Iterable[SomerAction]

  /**
   * Returns the count of all SomerActions stored in this container, excluding DirectorySomerActions.
   */
  def count: Long

}
