package somersault.core.actions.containers

import java.io.File
import java.sql.{Types, ResultSet, Connection, PreparedStatement, Statement}
import javax.sql.DataSource
import logahawk.Logger
import org.apache.commons.lang.IllegalClassException
import org.springframework.dao.DuplicateKeyException
import org.springframework.jdbc.`object`.MappingSqlQuery
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate
import org.springframework.jdbc.core.{JdbcTemplate, SqlParameter, PreparedStatementCreator}
import org.springframework.jdbc.support.GeneratedKeyHolder
import scala.collection.convert.Wrappers
import scala.collection.mutable
import somersault.core.actions.{SomerAction, RemoveAction, CreateDirAction, TransferAction, DirectoryAction}
import somersault.util.{VersionMismatchException, H2DataAccessObject, VersionedDataAccessObject, Version}

/**
 * An ActionContainerBuilder implementation uses an H2 database to store the SomerActions.
 *
 * This implementation is slower than MemoryActionContainer, but trades performance for low memory overhead -- since
 * this uses a database in a file, it can handle a much larger number of SomerActions but is slower to access. Since
 * the slowest part of Somersault is transferring files, the performance loss should be relatively low.
 */
class H2ActionContainerBuilder(data: H2ActionContainerBuilderData)
  extends ActionContainerBuilder with ActionContainerProvider {

  private val dao = new DAO(data)

  def addTransferAction(parent: DirectoryAction, name: String): TransferAction =
    parent match {
      case p: H2DirectoryAction => dao.insert(
        new ActionPrototype(Some(p.id), name, TransferActionType)).asInstanceOf[TransferAction]
      case _ => throw new IllegalClassException(classOf[H2DirectoryAction], parent)
    }

  def addCreateDirAction(parent: DirectoryAction, name: String): CreateDirAction =
    parent match {
      case p: H2DirectoryAction => dao.insert(
        new ActionPrototype(Some(p.id), name, CreateDirActionType)).asInstanceOf[CreateDirAction]
      case _ => throw new IllegalClassException(classOf[H2DirectoryAction], parent)
    }

  def addRemoveAction(parent: DirectoryAction, name: String): RemoveAction =
    parent match {
      case p: H2DirectoryAction => dao.insert(
        new ActionPrototype(Some(p.id), name, RemoveActionType)).asInstanceOf[RemoveAction]
      case _ => throw new IllegalClassException(classOf[H2DirectoryAction], parent)
    }

  def addDirectoryAction(parent: DirectoryAction, name: String): DirectoryAction =
    parent match {
      case p: H2DirectoryAction => dao.insert(
        new ActionPrototype(Some(p.id), name, DirectoryActionType)).asInstanceOf[DirectoryAction]
      case _ => throw new IllegalClassException(classOf[H2DirectoryAction], parent)
    }

  def children(parent: DirectoryAction): Iterable[SomerAction] =
    parent match {
      case p: H2DirectoryAction => dao.select(p)
      case _ => throw new IllegalClassException(classOf[H2DirectoryAction], parent)
    }

  def remove(action: SomerAction) {
    action match {
      case a: H2SomerAction => dao.remove(a)
      case _ => throw new IllegalClassException(classOf[H2SomerAction], action)
    }
  }

  def createProvider(): ActionContainerProvider = this

  val root: DirectoryAction = dao.insert(
    new ActionPrototype(None, "", DirectoryActionType)).asInstanceOf[H2DirectoryAction]

  def count: Long = dao.count

  private trait H2SomerAction extends SomerAction {

    def id: Long
  }

  private class H2DirectoryAction(val name: String, val id: Long) extends DirectoryAction with H2SomerAction

  private class H2TransferAction(val name: String, val id: Long) extends TransferAction with H2SomerAction

  private class H2CreateDirAction(val name: String, val id: Long) extends CreateDirAction with H2SomerAction

  private class H2RemoveAction(val name: String, val id: Long) extends RemoveAction with H2SomerAction

  /**
   * Used to help construct H2SomerAction
   *
   * @param parentId Specify None for the root.s
   * @param name Specify None for the root.
   * @param actionType Defines the type of SomerAction to create.
   */
  private class ActionPrototype(val parentId: Option[Long], val name: String, val actionType: SomerActionType)

  /**
   * Generic definition of the types of H2SomerActions that can be created
   *
   * @param value The raw value to be stored in the column.
   */
  private abstract case class SomerActionType(value: Byte)

  private object SomerActionType {

    implicit def byteToAction(b: Byte): SomerActionType = b match {
      case DirectoryActionType.value => DirectoryActionType
      case RemoveActionType.value => RemoveActionType
      case CreateDirActionType.value => CreateDirActionType
      case TransferActionType.value => TransferActionType
    }
  }

  import SomerActionType._

  private object DirectoryActionType extends SomerActionType(0)

  private object RemoveActionType extends SomerActionType(1)

  private object CreateDirActionType extends SomerActionType(2)

  private object TransferActionType extends SomerActionType(3)

  /**
   * This acts as a DAO for the H2ActionContainerBuilder implementation.
   */
  private class DAO(data: H2ActionContainerBuilderData) extends H2DataAccessObject with VersionedDataAccessObject {

    private val tableAction: String = "ACTION"

    private val colId: String = "ID"

    private val colParentId: String = "PARENT_ID"

    private val colName: String = "NAME"

    private val colType: String = "TYPE"

    /**
     * Provides a place to cache MappingSqlQuery instances.
     */
    private val mappingSqlQueryCache = new mutable.HashMap[String, MappingSqlQuery[H2SomerAction]]

    private val dataSource = createDataSource(data.file, deleteIfExists = true)

    def schemaVersion = new Version(0, 0, 1)

    override def createSchema(dataSource: DataSource) {
      super.createSchema(dataSource)

      val statements = List(
        "CREATE TABLE " + tableAction + " ( "
          + colId + " IDENTITY, "
          + colParentId + " INT8 NULL, "
          + colName + " VARCHAR(255), "
          + colType + " TINYINT, "
          + "PRIMARY KEY (" + colId + "), "
          + "FOREIGN KEY (" + colParentId + ") REFERENCES (" + colId + "), "
          + "CONSTRAINT NO_DUP UNIQUE( "
          + colParentId + "," + colName + "," + colType + ") )")

      val jdbc = new SimpleJdbcTemplate(dataSource)
      statements.foreach(jdbc.getJdbcOperations.update)
    }

    override def initialize(dataSource: DataSource) {
      super.initialize(dataSource)

      data.logger.info("Opened H2 ActionContainerBuilder at: %1$s", data.file)

      if (readVersion(dataSource) != schemaVersion)
        throw new VersionMismatchException(schemaVersion, readVersion(dataSource))
    }

    /**
     * Creates H2 SomerActions based on the provided ActionPrototypes. The new ID of each action created is returned,
     * in the same order as they were provided. The new H2SomerActions have no parent.
     */
    def insert(p: ActionPrototype): H2SomerAction = {
      val insertSql =
        "INSERT INTO " + tableAction +
          " ( " + colParentId +
          ", " + colName +
          ", " + colType +
          " ) VALUES ( ?, ?, ? )"

      val statementCreator = new PreparedStatementCreator {
        def createPreparedStatement(c: Connection): PreparedStatement = {
          val statement = c.prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS)
          if (p.parentId.isDefined) statement.setLong(1, p.parentId.get)
          else statement.setNull(1, Types.BIGINT)
          statement.setString(2, p.name)
          statement.setByte(3, p.actionType.value)
          statement
        }
      }

      val jdbc = new SimpleJdbcTemplate(dataSource)
      val keyHolder = new GeneratedKeyHolder

      try {
        jdbc.getJdbcOperations.update(statementCreator, keyHolder)
      }
      catch {
        case ex: DuplicateKeyException =>
          throw new IllegalArgumentException("Action already exists or primary key violated", ex)
      }

      construct(keyHolder.getKey.longValue, p)
    }

    /**
     * Returns all child SomerActions for the provided parent.
     */
    def select(parent: H2DirectoryAction): Iterable[H2SomerAction] = {
      val mapper = mappingSqlQueryCache.get("select") match {
        case Some(m) => m
        case None => {
          val sql =
            "SELECT " + colId +
              ", " + colName +
              ", " + colType +
              " FROM " + tableAction +
              " WHERE " + colParentId + " = ?"

          val mapper = new MappingSqlQuery[H2SomerAction](dataSource, sql) {
            def mapRow(rs: ResultSet, rowNum: Int) =
              construct(rs.getLong(1), new ActionPrototype(Some(parent.id), rs.getString(2), rs.getByte(3)))
          }
          mapper.declareParameter(new SqlParameter(Types.BIGINT))

          mappingSqlQueryCache.put("select", mapper)
          mapper
        }
      }

      Wrappers.JCollectionWrapper(mapper.execute(parent.id))
    }

    /**
     * Removes the provided artifact, and any children.
     *
     * @throws PathDoesNotExistIndexException If no artifacts are removed.
     */
    def remove(action: H2SomerAction) {
      action match {
        case dir: H2DirectoryAction => removeChildren(dir)
        case _ =>
      }

      val sql = "DELETE FROM " + tableAction + " WHERE " + colId + " = ?"
      val deleted = new JdbcTemplate(dataSource).update(sql, Predef.long2Long(action.id))

      if (deleted == 0)
        throw new IllegalArgumentException("action does not exist")
      if (deleted > 1)
        throw new IllegalArgumentException("multiple instances of the action existed")
    }

    def count: Long = new JdbcTemplate(dataSource).queryForLong(
      "SELECT COUNT(*) FROM " + tableAction + " WHERE NOT " + colType + " = " + DirectoryActionType.value)

    /**
     * Removes the children Actions. This recursively deletes the contents of all child actions first. This does not
     * throw if no rows are removed.
     */
    private def removeChildren(parent: H2DirectoryAction) {
      select(parent).foreach(remove)

      val sql = "DELETE FROM " + tableAction + " WHERE " + colParentId + " = ?"
      new JdbcTemplate(dataSource).update(sql, Predef.long2Long(parent.id))
    }

    /**
     * Constructs an H2SomerAction from a provided ActionPrototype.
     *
     * @param id The ID of the instance being created.
     */
    private def construct(id: Long, p: ActionPrototype): H2SomerAction = {
      p.actionType match {
        case DirectoryActionType => new H2DirectoryAction(p.name, id)
        case RemoveActionType => new H2RemoveAction(p.name, id)
        case CreateDirActionType => new H2CreateDirAction(p.name, id)
        case TransferActionType => new H2TransferAction(p.name, id)
      }
    }
  }

}

class H2ActionContainerBuilderData(val logger: Logger, val file: File)
