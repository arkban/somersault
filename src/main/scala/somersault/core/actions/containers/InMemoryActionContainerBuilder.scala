package somersault.core.actions.containers

import java.util.concurrent.atomic.AtomicInteger
import net.jcip.annotations.NotThreadSafe
import org.apache.commons.lang.IllegalClassException
import scala.None
import scala.collection.mutable
import somersault.core.actions.{SomerAction, RemoveAction, CreateDirAction, TransferAction, DirectoryAction}

/**
 * An ActionContainerBuilder implementation that stores the tree of SomerActions entirely in memory. This is only suitable for
 * small tree's of SomerActions.
 */

@NotThreadSafe
class InMemoryActionContainerBuilder extends ActionContainerBuilder with ActionContainerProvider {

  private val nextId = new AtomicInteger(0)

  /**
   * Map from DirectoryAction.Id to a list of child SomerActions.
   */
  private val map = new mutable.HashMap[Int, mutable.ListBuffer[InMemSomerAction]]

  val root: DirectoryAction = {
    val root = new InMemDirectoryAction("", nextId.getAndIncrement, nextId.getAndIncrement)
    map += root.id -> new mutable.ListBuffer[InMemSomerAction]
    root
  }

  def addTransferAction(parent: DirectoryAction, name: String): TransferAction =
    parent match {
      case p: InMemDirectoryAction => add(parent, new InMemTransferAction(name, p.id))
      case _ => throw new IllegalClassException(classOf[InMemDirectoryAction], parent)
    }

  def addCreateDirAction(parent: DirectoryAction, name: String): CreateDirAction =
    parent match {
      case p: InMemDirectoryAction => add(parent, new InMemCreateDirAction(name, p.id))
      case _ => throw new IllegalClassException(classOf[InMemDirectoryAction], parent)
    }

  def addRemoveAction(parent: DirectoryAction, name: String): RemoveAction =
    parent match {
      case p: InMemDirectoryAction => add(parent, new InMemRemoveAction(name, p.id))
      case _ => throw new IllegalClassException(classOf[InMemDirectoryAction], parent)
    }

  def addDirectoryAction(parent: DirectoryAction, name: String): DirectoryAction =
    parent match {
      case p: InMemDirectoryAction => add(parent, new InMemDirectoryAction(name, p.id, nextId.getAndIncrement))
      case _ => throw new IllegalClassException(classOf[InMemDirectoryAction], parent)
    }

  def children(parent: DirectoryAction): Iterable[SomerAction] =
    parent match {
      case d: InMemDirectoryAction => map.get(d.id).get
      case _ => throw new IllegalClassException(classOf[InMemDirectoryAction], parent)
    }

  def remove(action: SomerAction) {
    action match {
      case somerAction: InMemSomerAction =>
        val actions = map(somerAction.parentId)

        if (!actions.contains(somerAction))
          throw new IllegalArgumentException("action is not part of this container")

        actions -= somerAction

        // remove child somerActions
        somerAction match {
          case a: InMemDirectoryAction =>
            children(a).foreach(remove)
            map -= a.id
          case _ => // will be removed automatically when parent sis removed
        }

      case _ => throw new IllegalClassException(classOf[InMemSomerAction], action)
    }
  }

  def createProvider(): ActionContainerProvider = this

  def count: Long = map.map(_._2.size.toLong).sum

  /**
   * Adds the action as a child of the parent DirectoryAction. If the action is also a DirectoryAction, it will be
   * added to the map as a key. This returns the child action it was passed (to simplify usage).
   */
  private def add[T <: InMemSomerAction](parent: DirectoryAction, action: T): T =
    parent match {
      case d: InMemDirectoryAction => map.get(d.id) match {
        case Some(actions) => {
          if (actions.exists(x => x.getClass == action.getClass && x.name == action.name))
            throw new IllegalArgumentException("Action already exists")

          actions += action

          // add new child DirectoryActions
          action match {
            case d: InMemDirectoryAction => map += (d.id -> new mutable.ListBuffer[InMemSomerAction])
            case _ =>
          }
          action
        }
        case None => throw new IllegalArgumentException("Parent has not been previously added")
      }
      case _ => throw new IllegalClassException(classOf[InMemDirectoryAction], parent)
    }

  private trait InMemSomerAction extends SomerAction {

    def parentId: Int
  }

  /**
   * Override to add the ID, which is used to load and store child actions. Only the DirectoryAction needs ID because
   * it is a parent of the other IDs.
   */
  private class InMemDirectoryAction(val name: String, val parentId: Int, val id: Int)
    extends DirectoryAction with InMemSomerAction

  private class InMemTransferAction(val name: String, val parentId: Int)
    extends TransferAction with InMemSomerAction

  private class InMemCreateDirAction(val name: String, val parentId: Int)
    extends CreateDirAction with InMemSomerAction

  private class InMemRemoveAction(val name: String, val parentId: Int)
    extends RemoveAction with InMemSomerAction

}