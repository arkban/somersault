package somersault.core.actions.containers

import somersault.core.actions.{CreateDirAction, TransferAction, RemoveAction, SomerAction, DirectoryAction}

/**
 * A container for SomerAction instances created by the various ActionBuilders. This class exists to allow different
 * ways of storing SomerActions. This trait focuses on the creating of SomerActions, and defers the usage of those
 * SomerActions to ActionContainerProvider.
 *
 * The reason for such a container class is that the number of SomerActions created by ActionBuilder can be
 * extremely large. This interface allows one to easily alter how those Actions are stored, whether in-memory, cached
 * on disk, or any other technique.
 *
 * @see ActionContainerProvider
 */
trait ActionContainerBuilder {

  /**
   * Returns the root directory.
   */
  def root: DirectoryAction

  /**
   * Return a new Collection contain all actions for the provided DirectoryAction. The returned actions are those that
   * should be executed within the provided DirectoryAction.
   */
  def children(parent: DirectoryAction): Iterable[SomerAction]

  /**
   * Remove the provided SomerAction, and any child actions if this is a DirectoryAction. This will not remove the
   * parent DirectoryAction, even if this removes the last child SomerAction.
   */
  def remove(action: SomerAction)

  /**
   * Creates and returns a DirectoryAction.
   *
   * @param name Should not be null or empty (as that would imply the root).
   * @throws IllegalArgumentException If action to create already exists.
   */
  def addDirectoryAction(parent: DirectoryAction, name: String): DirectoryAction

  /**
   * Creates and returns a RemoveAction.
   *
   * @param name Should not be null or empty (as that would imply the root).
   * @throws IllegalArgumentException If action to create already exists.
   */
  def addRemoveAction(parent: DirectoryAction, name: String): RemoveAction

  /**
   * Creates and returns a CreateDirAction.
   *
   * @param name Should not be null or empty (as that would imply the root).
   * @throws IllegalArgumentException If action to create already exists.
   */
  def addCreateDirAction(parent: DirectoryAction, name: String): CreateDirAction

  /**
   * Creates and returns a TransferAction.
   *
   * @param name Should not be null or empty (as that would imply the root).
   */
  def addTransferAction(parent: DirectoryAction, name: String): TransferAction

  /**
   * Creates a ActionContainerProvider from this instance. This is a necessary transition step from defining the
   * SomerActions to using them.
   */
  def createProvider(): ActionContainerProvider
}

/**
 * A utility class for holding common data for ActionContainers. This should be passed to the constructor of
 * ActionContainers implementation.
 */
trait ActionContainerBuilderData
