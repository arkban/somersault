package somersault.core.actions.builders

import logahawk.Severity
import net.jcip.annotations.ThreadSafe
import somersault.util.AbstractMultiObserver
import somersault.core.filters.SomerFilter
import somersault.core.comparators.SomerFileComparator

/**
 * Observer for tracking and watching the progress of an ActionBuilder.
 */
trait ActionBuilderObserver
{
  def start()

  def end()

  def recurse( path: List[ String ] )

  def filterAccept( filter: SomerFilter, path: List[ String ] )

  def filterReject( filter: SomerFilter, path: List[ String ] )

  def comparatorAccept( comparator: SomerFileComparator, path: List[ String ] )

  def comparatorReject( comparator: SomerFileComparator, path: List[ String ] )

  def error( severity: Severity, ex: Exception )
}

/**
 * Forwards each call to this observer to the underlying observers.
 */
@ThreadSafe
class MultiActionBuilderObserver extends AbstractMultiObserver[ ActionBuilderObserver ] with ActionBuilderObserver
{
  def start() = observers.foreach( _.start )

  def end() = observers.foreach( _.end )

  def recurse( path: List[ String ] ) = observers.foreach( _.recurse( path ) )

  def filterAccept( filter: SomerFilter, path: List[ String ] ) = observers.foreach( _.filterAccept( filter, path ) )

  def filterReject( filter: SomerFilter, path: List[ String ] ) = observers.foreach( _.filterReject( filter, path ) )

  def comparatorAccept( comparator: SomerFileComparator, path: List[ String ] ) =
    observers.foreach( _.comparatorAccept( comparator, path ) )

  def comparatorReject( comparator: SomerFileComparator, path: List[ String ] ) =
    observers.foreach( _.comparatorReject( comparator, path ) )

  def error( severity: Severity, ex: Exception ) = observers.foreach( _.error( severity, ex ) )
}