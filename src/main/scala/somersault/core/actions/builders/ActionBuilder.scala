package somersault.core.actions.builders

import somersault.core.actions.containers.ActionContainerBuilder
import somersault.core.Scenario

/**
 * Compares two FileSystems, creating a tree of SomerActions that modify one or both of the FileSystems. The
 * SomerActions are stored in the provided ActionContainerBuilder. The actions, if executed, would synchronize the file
 * systems. The definition if "synchronize" is left to the implementators of this class.
 *
 * It is recommended that empty DirectoryActions are not left in the ActionContainerBuilder, since they take up space and don't
 * provide any useful behavior.
 */
trait ActionBuilder
{
  /**
   * Compares the provided FileSystems and returns a list of Actions that would synchronize them. This may return an
   * empty collection but should never return null. If the Actions are executed they will be done so in the order
   * returned by this method.
   */
  def build( data: ActionBuilderData )
}

/**
 * A utility class for holding common data for ActionBuilders. This should be passed to the constructor of
 * ActionBuilder implementation.
 *
 * @param actions This ActionContainerBuilder must be empty.
 */
abstract class ActionBuilderData(
    val scenario: Scenario,
    val actions: ActionContainerBuilder,
    val observer: ActionBuilderObserver )
