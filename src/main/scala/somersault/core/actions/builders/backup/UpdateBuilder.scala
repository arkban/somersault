package somersault.core.actions.builders.backup

import logahawk.Severity
import scala.collection.mutable
import somersault.core.actions.DirectoryAction
import somersault.core.exceptions.{FileExistsWhereDirExpectedActionBuilderException, DirExistsWhereFileExpectedActionBuilderException}
import somersault.core.filesystems.{SomerFile, SomerArtifact, SomerDirectory}
import somersault.util.Repeater

/**
 * An UpdateBuilder creates a list of updates between directories that exist in both FileSystems. The actions can be
 * of any type: RemoveAction, CreateDirAction, TransferAction, etc. Both directories are assumed to exist in both
 * FileSystems. This may create other ActionBuilder instances if appropriate.
 */
class UpdateBuilder(
  data: BackupData,
  protected val rootDirAction: DirectoryAction,
  protected val rootSrcDir: SomerDirectory,
  protected val rootDestDir: SomerDirectory) {

  def build() { build(rootDirAction: DirectoryAction, rootSrcDir, rootDestDir, Nil) }

  private def build(
    parentDirAction: DirectoryAction, srcDir: SomerDirectory, destDir: SomerDirectory, parentPath: List[String]) {
    val currPath = parentPath ::: parentDirAction.name :: Nil
    data.observer.recurse(currPath)

    val (childUpdates, childAdds) = buildActions(parentDirAction, srcDir, destDir, currPath)

    childUpdates.foreach {
      case (dirAction, pair) =>
        try {
          build(
            dirAction, pair.src.get.asInstanceOf[SomerDirectory], pair.dest.get.asInstanceOf[SomerDirectory], currPath)
        }
        catch {
          case ex: Exception => data.observer.error(Severity.ERROR, ex)
        }

        if (data.actions.children(dirAction).size == 0)
          data.actions.remove(dirAction)
    }

    childAdds.foreach {
      case (dirAction, src) =>
        try {
          new AddBuilder(data, dirAction, src, currPath).build()
        }
        catch {
          case ex: Exception => data.observer.error(Severity.ERROR, ex)
        }
    }
  }

  private def buildActions(
    parentDirAction: DirectoryAction, srcDir: SomerDirectory, destDir: SomerDirectory, currPath: List[String]): (
    Iterable[(DirectoryAction, SrcDestPair)], Iterable[(DirectoryAction, SomerDirectory)]) = {
    // capture directories to recurse into after we are done updating this directory. (this simplifies the logic and
    // means we will update all files in this directory before descending. we're following a
    val childUpdates = new mutable.ListBuffer[(DirectoryAction, SrcDestPair)]
    // capture directories we need to create from scratch. (same reason as above)
    val childAdds = new mutable.ListBuffer[(DirectoryAction, SomerDirectory)]

    val srcChildren = filterSource(currPath, srcDir)
    val destChildren = filterDest(currPath, destDir)

    createPairings(srcChildren, destChildren).foreach(
      pair => {
        try {
          pair match {
            case SrcDirAndDestDirExists(src, dest) => {
              val dirAction = data.actions.addDirectoryAction(parentDirAction, src.name)
              childUpdates += ((dirAction, pair))
            }
            case SrcDirAndDestFileExists(src, dest) => {
              if (data.restore) {
                data.observer.error(
                  Severity.ERROR, new DirExistsWhereFileExpectedActionBuilderException(currPath ::: src.name :: Nil))
              }
              else {
                data.actions.addRemoveAction(parentDirAction, dest.name)
                data.actions.addCreateDirAction(parentDirAction, src.name)
                val dirAction = data.actions.addDirectoryAction(parentDirAction, src.name)
                childAdds += ((dirAction, src))
              }
            }
            case SrcFileAndDestDirExists(src, dest) => {
              if (data.restore) {
                data.observer.error(
                  Severity.ERROR, new FileExistsWhereDirExpectedActionBuilderException(currPath ::: src.name :: Nil))
              }
              else {
                data.actions.addRemoveAction(parentDirAction, dest.name)
                data.actions.addTransferAction(parentDirAction, src.name)
              }
            }
            case SrcFileAndDestFileExists(src, dest) => {
              if (data.scenario.data.comparator.compare(src, dest) != 0)
                data.actions.addTransferAction(parentDirAction, src.name)
            }
            case OnlyDestFileExists(dest) => {
              if (!data.restore)
                data.actions.addRemoveAction(parentDirAction, dest.name)
            }
            case OnlyDestDirExists(dest) => {
              if (!data.restore)
                data.actions.addRemoveAction(parentDirAction, dest.name)
            }
            case OnlySrcDirExists(src) => {
              data.actions.addCreateDirAction(parentDirAction, src.name)
              val dirAction = data.actions.addDirectoryAction(parentDirAction, src.name)
              childAdds += ((dirAction, src))
            }
            case OnlySrcFileExists(src) => data.actions.addTransferAction(parentDirAction, src.name)
            case _ => throw new Exception("Unrecognized Pairing: " + pair)
          }
        }
        catch {
          case ex: Exception => data.observer.error(Severity.ERROR, ex)
        }
      })

    (childUpdates, childAdds)
  }

  /**
   * Retrieves and filters the source artifacts. This should be done before creating pairs so we will correctly remove
   * artifacts in the dest when even if exist in the source. This behavior is especially desirable when filters become
   * more restrictive after a prior run
   */
  private def filterSource(currPath: List[String], srcDir: SomerDirectory): Iterable[SomerArtifact] = {
    val children =
      new Repeater(() => data.scenario.source.children(srcDir), data.scenario.data.maxRepeatAttempts).repeat()

    val sourceLockFilter = data.scenario.source.generateLockNameFilter()

    children.foldLeft(new mutable.ListBuffer[SomerArtifact]) {
      case (result, child) =>
        val currFilePath = currPath ::: child.name :: Nil
        if (data.scenario.data.filter.accept(currPath, child)) {
          // silently ignore the lock file (hence this is a separate check to avoid falling into the else and the
          // following observer.filterReject() call)
          if (!sourceLockFilter.accept(currPath, child)) {
            data.observer.filterAccept(data.scenario.data.filter, currFilePath)
            result += child
          }
        }
        else {
          data.observer.filterReject(data.scenario.data.filter, currFilePath)
        }
        result
    }
  }

  /**
   * Retrieves and filters the destination artifacts. This should be done before creating pairs so we will correctly
   * ignore artifacts in the dest.
   */
  private def filterDest(currPath: List[String], destDir: SomerDirectory): Iterable[SomerArtifact] = {
    val children =
      new Repeater(() => data.scenario.dest.children(destDir), data.scenario.data.maxRepeatAttempts).repeat()

    val destLockFilter = data.scenario.dest.generateLockNameFilter()

    children.filter(!destLockFilter.accept(currPath, _))
  }

  /**
   * Creates a map from a SomerArtifact's name to the SomerArtifact instance. This designed to allow fast lookups of
   * files when comparing.
   */
  private def createPairings(src: Iterable[SomerArtifact], dest: Iterable[SomerArtifact]): List[SrcDestPair] = {
    // TODO: verify that the maps used are the (reasonably) fastest map impl we have (look at Google Collections)

    val srcMap = src.foldLeft(new mutable.HashMap[String, SomerArtifact]) {case (map, x) => map += x.name -> x}
    val destMap = dest.foldLeft(new mutable.HashMap[String, SomerArtifact]) {case (map, x) => map += x.name -> x}

    val list = new mutable.ListBuffer[SrcDestPair]
    // add all entries from source
    src.foreach(s => list += new SrcDestPair(s, destMap.get(s.name)))
    // add all entries from dest where not already added from source
    dest.filter(d => (srcMap.get(d.name).isEmpty)).foreach(d => list += new SrcDestPair(None, d))

    list.toList
  }

  /**
   * A container class for a source and dest SomerArtifact pair. One or the other may be non-existent.
   *
   * Instances of this class can be created by createPairings() to so that the logic that decides what to do is
   * separate from determining whether the source or dest exist.
   *
   * Data in instances of this class should be extracted using the extractors defined below.
   */
  private case class SrcDestPair private(src: Option[SomerArtifact], dest: Option[SomerArtifact]) {

    def this(src: SomerArtifact, dest: Option[SomerArtifact]) = this(new Some[SomerArtifact](src), dest)

    def this(src: Option[SomerArtifact], dest: SomerArtifact) = this(src, new Some[SomerArtifact](dest))
  }

  private object SrcFileAndDestFileExists {

    def unapply(p: SrcDestPair): Option[(SomerFile, SomerFile)] =
      (p.src, p.dest) match {
        case (Some(src: SomerFile), Some(dest: SomerFile)) => Some(src, dest)
        case _ => None
      }
  }

  private object SrcDirAndDestDirExists {

    def unapply(p: SrcDestPair): Option[(SomerDirectory, SomerDirectory)] =
      (p.src, p.dest) match {
        case (Some(src: SomerDirectory), Some(dest: SomerDirectory)) => Some(src, dest)
        case _ => None
      }
  }

  private object SrcFileAndDestDirExists {

    def unapply(p: SrcDestPair): Option[(SomerFile, SomerDirectory)] =
      (p.src, p.dest) match {
        case (Some(src: SomerFile), Some(dest: SomerDirectory)) => Some(src, dest)
        case _ => None
      }
  }

  private object SrcDirAndDestFileExists {

    def unapply(p: SrcDestPair): Option[(SomerDirectory, SomerFile)] =
      (p.src, p.dest) match {
        case (Some(src: SomerDirectory), Some(dest: SomerFile)) => Some(src, dest)
        case _ => None
      }
  }

  private object OnlyDestFileExists {

    def unapply(p: SrcDestPair): Option[SomerFile] =
      (p.src, p.dest) match {
        case (None, Some(f: SomerFile)) => Some(f)
        case _ => None
      }
  }

  private object OnlyDestDirExists {

    def unapply(p: SrcDestPair): Option[SomerDirectory] =
      (p.src, p.dest) match {
        case (None, Some(d: SomerDirectory)) => Some(d)
        case _ => None
      }
  }

  private object OnlySrcDirExists {

    def unapply(p: SrcDestPair): Option[SomerDirectory] =
      (p.src, p.dest) match {
        case (Some(d: SomerDirectory), None) => Some(d)
        case _ => None
      }
  }

  private object OnlySrcFileExists {

    def unapply(p: SrcDestPair): Option[SomerFile] =
      (p.src, p.dest) match {
        case (Some(f: SomerFile), None) => Some(f)
        case _ => None
      }
  }

}
