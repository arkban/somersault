package somersault.core.actions.builders.backup

import org.apache.commons.lang.IllegalClassException
import somersault.core.Scenario
import somersault.core.actions.builders.{ActionBuilderObserver, ActionBuilderData, ActionBuilder}
import somersault.core.actions.containers.ActionContainerBuilder

/**
 * Backups the source FileSystem to the destination FileSystem by forcing the dest to mimic the source.
 */

class Backup() extends ActionBuilder {

  override def build(data: ActionBuilderData) {
    if (!data.isInstanceOf[BackupData])
      throw new IllegalClassException(classOf[BackupData], data)

    data.observer.start()
    try {
      new UpdateBuilder(
        data.asInstanceOf[BackupData], data.actions.root, data.scenario.source.root, data.scenario.dest.root)
        .build()
    }
    finally {
      data.observer.end()
    }
  }
}

/**
 * @param restore True if acting in restore mode. Restore mode does not create RemoveActions. Being in Restore mode
 *                may create Exceptions as it
 */
class BackupData(scenario: Scenario, actions: ActionContainerBuilder, observer: ActionBuilderObserver, val restore: Boolean)
  extends ActionBuilderData(scenario, actions, observer)