package somersault.core.actions.builders.backup

import logahawk.Severity
import scala.collection.mutable
import somersault.core.actions.DirectoryAction
import somersault.core.filesystems.{SomerFile, SomerDirectory}
import somersault.util.Repeater

/**
 * An AddBuilder creates a list of updates between a directory that exists in one FileSystem and directory that needs
 * to be created in the other FileSystem. In essence this builds actions to copy a directory tree from one FileSystem
 * to another. The actions will be either CreateDirAction or TransferActions.
 *
 * The constructor assumes that the caller will create the
 */

class AddBuilder(
  data: BackupData,
  rootDirAction: DirectoryAction,
  rootSrcDir: SomerDirectory,
  rootParentPath: List[String]) {

  def build() { build(rootDirAction, rootSrcDir, rootParentPath) }

  /**
   * Creates a DirectoryAction containing actions for
   */
  private def build(parentDirAction: DirectoryAction, srcDir: SomerDirectory, parentPath: List[String]) {
    val currPath = parentPath ::: parentDirAction.name :: Nil
    data.observer.recurse(currPath)

    val childAdds = addChildren(parentDirAction, srcDir, currPath)

    childAdds.foreach {
      case (dirAction, c) =>
        try {build(dirAction, c, currPath)}
        catch {case ex: Exception => data.observer.error(Severity.ERROR, ex)}
    }
  }

  /**
   * Handles adding the children files and returning directories to recurse into.
   */
  private def addChildren(
    parentDirAction: DirectoryAction, srcDir: SomerDirectory, currPath: List[String]
    ): Iterable[(DirectoryAction, SomerDirectory)] = {

    val childAdds = new mutable.ListBuffer[(DirectoryAction, SomerDirectory)]
    val srcChildren =
      new Repeater(() => data.scenario.source.children(srcDir), data.scenario.data.maxRepeatAttempts).repeat()

    srcChildren.foreach(
      child => {
        try {
          val currFilePath = currPath ::: child.name :: Nil
          if (data.scenario.data.filter.accept(currPath, child)) {
            data.observer.filterAccept(data.scenario.data.filter, currFilePath)
            child match {
              case srcDir: SomerDirectory => {
                data.actions.addCreateDirAction(parentDirAction, srcDir.name)
                val dirAction = data.actions.addDirectoryAction(parentDirAction, srcDir.name)
                childAdds += ((dirAction, srcDir))
              }
              case f: SomerFile => data.actions.addTransferAction(parentDirAction, f.name)
              case _ => throw new Exception("Unrecognized type: " + child)
            }
          }
          else {
            data.observer.filterReject(data.scenario.data.filter, currFilePath)
          }
        }
        catch {
          case ex: Exception => data.observer.error(Severity.ERROR, ex)
        }
      })

    childAdds
  }
}