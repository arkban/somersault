package somersault.core.comparators

import nu.xom.Element
import org.apache.commons.lang.BooleanUtils
import somersault.core.filesystems.SomerFile
import somersault.util.persist.{PersisterContext, Persister}

/**
 * A comparator that always returns that files are the same or not the same.
 *
 * @param value If true, the comparator returns 0, otherwise returns -1.
 */
class BooleanComparator( val value: Boolean) extends SomerFileComparator
{
  def compare( x: SomerFile, y: SomerFile): Int = if( value ) 0 else -1
}

class BooleanComparatorPersister extends SomerFileComparatorPersister
{
  def canRead( context: PersisterContext, element: Element): Boolean = checkClass(
    element, classOf[ BooleanComparator ])

  def read[ T ]( context: PersisterContext, element: Element): T =
    new BooleanComparator( BooleanUtils.toBoolean( element.getAttributeValue( "value"))).asInstanceOf[ T ]

  def write( context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false)
  {
    obj match
    {
      case f: BooleanComparator =>
        writeClass( obj, element, dynamic)
        Persister.createAttribute( "value", BooleanUtils.toStringTrueFalse( f.value), element)
      case _ => throw new IllegalArgumentException( "cannot write: " + obj.getClass.getName)
    }
  }
}
