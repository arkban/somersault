package somersault.core.comparators

import somersault.core.filesystems.SomerFile
import org.joda.time.DateTime
import org.apache.commons.lang.math.NumberUtils
import nu.xom.Element
import somersault.util.persist.{PersisterContext, Persister}

/**
 * Compares SomerFiles by their modified date, using a provided granularity.
 *
 * @param granularityMs Granularity used for comparisons. If the modified dates differ by less or equal to this amount,
 * they will be considered equal.
 */
class ModifyDateComparator( val granularityMs: Long) extends SomerFileComparator
{
  def this( ) = this ( 5 * 1000)

  def compare( x: SomerFile, y: SomerFile): Int = compare( x.modified, y.modified)

  def compare( x: DateTime, y: DateTime): Int =
  {
    val diff = x.getMillis - y.getMillis

    if( diff == 0 || math.abs( diff) <= granularityMs ) 0
    else if( diff < 0 ) -1
    else 1
  }
}

class ModifyDateComparatorPersister extends SomerFileComparatorPersister
{
  def canRead( context: PersisterContext, element: Element): Boolean = checkClass(
    element, classOf[ ModifyDateComparator ])

  def read[ T ]( context: PersisterContext, element: Element): T =
    new ModifyDateComparator( NumberUtils.toLong( element.getAttributeValue( "granularityMs"))).asInstanceOf[ T ]

  def write( context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false)
  {
    obj match
    {
      case f: ModifyDateComparator =>
        writeClass( obj, element, dynamic)
        Persister.createAttribute( "granularityMs", f.granularityMs.toString, element)
      case _ => throw new IllegalArgumentException( "cannot write: " + obj.getClass.getName)
    }
  }
}
