package somersault.core.comparators

import somersault.core.filesystems.SomerFile
import java.util.Comparator
import somersault.util.persist.Persister
/**
 * Used by builders to compare files to determine which ones are equivalent and which should be transferred. This
 * is applied AFTER a SomerFilter accepts or rejects the file for consideration.
 *
 * This comparator is used only to determine if the files are different, it does not order the files as a normal
 * comparator.
 */

trait SomerFileComparator extends Comparator[ SomerFile ]

trait SomerFileComparatorPersister extends Persister