package somersault.core.comparators

/**
 * Provides some advanced {@link SomerFileComparator}s built from the basic {@link SomerFileComparator}s.
 */
object ExtendedComparators
{
  val standardComparator = new MultiSomerFileComparator( new ModifyDateComparator( ), new SizeComparator( ))
}