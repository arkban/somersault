package somersault.core.comparators

import somersault.util.Version
import nu.xom.Element
import java.io.File
import somersault.core.filesystems.SomerFile
import somersault.util.persist.{SharedPersister, PersisterContext}

/**
 * A special {@link SomerFileComparator} that persists its internal {@link SomerFileComparator} in an external file.
 * This allows sharing a {@link SomerFileComparator} with multiple {@link Scenario}s. The comparing logic acts as a
 * simple pass through to the root {@link SomerFileComparator} in that external file.
 */
class SharedComparator( val comparator: SomerFileComparator, val filePath: File) extends SomerFileComparator
{
  def compare( x: SomerFile, y: SomerFile) = comparator.compare( x, y)
}

class SharedComparatorPersister extends SomerFileComparatorPersister with SharedPersister
{
  val version = new Version( 0, 2, 0)

  def canRead( context: PersisterContext, element: Element): Boolean = checkClass( element, classOf[ SharedComparator ])

  def read[ T ]( context: PersisterContext, element: Element): T =
  {
    val rawFilePath = readRawFilePath( element)
    val comparator: SomerFileComparator = readSharedObject( context, rawFilePath)
    new SharedComparator( comparator, rawFilePath).asInstanceOf[ T ]
  }

  def write( context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false)
  {
    obj match
    {
      case c: SharedComparator =>
        writeClass( obj, element, dynamic)
        writeSharedObject( context, c.filePath, c.comparator)
        writeRawFilePath( element, c.filePath)
      case _ => throw new IllegalArgumentException( "cannot write: " + obj.getClass.getName)
    }
  }
}

object SharedComparatorPersister
{
  val version = new Version( 0, 2, 0)
}
