package somersault.core.comparators

import nu.xom.Element
import somersault.core.filesystems.SomerFile
import somersault.util.MultiComparator
import somersault.util.persist.{PersisterContext, Persister}

/**
 * A variant of MultiComparator for SomerFileComparator. This exists mostly to provide a hook for persisting.
 */
class MultiSomerFileComparator(comparators: Iterable[SomerFileComparator])
  extends MultiComparator[SomerFile](comparators) with SomerFileComparator {

  def this(comparators: SomerFileComparator*) = this(comparators)
}

class MultiSomerFileComparatorPersister extends SomerFileComparatorPersister {

  def canRead(context: PersisterContext, element: Element): Boolean = checkClass(
    element, classOf[MultiSomerFileComparator])

  def read[T](context: PersisterContext, element: Element): T = {
    val comparators = Persister.elementsToIterable(element.getChildElements)
      .map(e => context.container.read[SomerFileComparator](context, e))

    new MultiSomerFileComparator(comparators).asInstanceOf[T]
  }

  def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false) {
    obj match {
      case f: MultiSomerFileComparator =>
        writeClass(obj, element, dynamic)
        f.comparators.foreach(
          c => context.container.write(
            context, c, Persister.createElement("comparator", parent = element), dynamic = true))
      case _ => throw new IllegalArgumentException("cannot write: " + obj.getClass.getName)
    }
  }
}