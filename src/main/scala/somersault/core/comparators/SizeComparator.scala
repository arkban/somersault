package somersault.core.comparators

import somersault.core.filesystems.SomerFile
import nu.xom.Element
import somersault.util.persist.PersisterContext

/**
 * Compares SomerFiles and returns which file is smaller. (This is an arbitrary comparison, and is probably best
 * changed into a "not equal" than a comparison.)
 */
class SizeComparator extends SomerFileComparator
{
  def compare( x: SomerFile, y: SomerFile): Int = ( x.size - y.size ).toInt
}

class SizeComparatorPersister extends SomerFileComparatorPersister
{
  def canRead( context: PersisterContext, element: Element): Boolean = checkClass( element, classOf[ SizeComparator ])

  def read[ T ]( context: PersisterContext, element: Element): T = new SizeComparator( ).asInstanceOf[ T ]

  def write( context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false)
  {
    obj match
    {
      case f: SizeComparator => writeClass( obj, element, dynamic)
      case _ => throw new IllegalArgumentException( "cannot write: " + obj.getClass.getName)
    }
  }
}
