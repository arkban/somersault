package somersault.core.comparators

import nu.xom.Element
import somersault.core.filesystems.SomerFile
import somersault.util.persist.PersisterContext
import somersault.util.{HashInfo, Hash}

/**
 * Compares files based on their hash. Since hashes do create a useful comparison, this is really only useful for
 * equality, not comparing. If used for comparison this will return a stable result to allow this comparator being used
 * for Sets.
 */
class HashComparator extends SomerFileComparator {

  def compare(x: SomerFile, y: SomerFile): Int = compare(x.hash, y.hash)

  def compare(x: Hash, y: Hash): Int = {
    val hashInfoResult = compare(x.info, y.info)
    if (hashInfoResult == 0) {
      if (x == y) 0
      // anything stable and sure to be different
      else (x.hashCode - y.hashCode)
    }
    else hashInfoResult
  }

  def compare(x: HashInfo, y: HashInfo): Int = {
    val nameResult = x.name.compareTo(y.name)
    if (nameResult == 0) (x.size - y.size)
    else nameResult
  }
}

class HashComparatorPersister extends SomerFileComparatorPersister {

  def canRead(context: PersisterContext, element: Element): Boolean = checkClass(element, classOf[HashComparator])

  def read[T](context: PersisterContext, element: Element): T = new HashComparator().asInstanceOf[T]

  def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false) {
    obj match {
      case f: HashComparator => writeClass(obj, element, dynamic)
      case _ => throw new IllegalArgumentException("cannot write: " + obj.getClass.getName)
    }
  }
}
