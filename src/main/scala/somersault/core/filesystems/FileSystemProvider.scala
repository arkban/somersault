package somersault.core.filesystems

import amazonS3.{AmazonS3FileSystem, AmazonS3FileSystemData}
import local.{LocalIndexedFileSystem, LocalIndexedFileSystemData, LocalDirectFileSystem, LocalDirectFileSystemData}

/**
 * Defines a class that creates the {@link FileSystem} instance. Creating a {@link FileSystem} is considered an
 * involved process, and so a separate object is used to better contain that process.
 */
trait FileSystemProvider
{
  /**
   * Construct and connect to the {@link FileSystem}, performing any necessary initialization activities. This
   * should return the most direct {@link FileSystem} implementation possible. (In other words this should not
   * return a {@link FileSystem} wrapped by a {@link ConnectCheckingFileSystem} or similar. The caller will do
   * that as necessary.)
   *
   * @throws Exception Thrown if something prevents creating or connecting to the FileSystem.
   */
  def connect( data: FileSystemData, runtimeData: RuntimeFileSystemData): FileSystem
}

object FileSystemProvider extends FileSystemProvider
{
  /**
   * A generic implementation that will examine the provided {@link FileSystemData} to determine which
   * {@link FileSystem} to create. This is a convience method.
   */
  def connect( data: FileSystemData, runtimeData: RuntimeFileSystemData) =
    data match
    {
      case d: LocalDirectFileSystemData => LocalDirectFileSystem.connect( d, runtimeData)
      case d: LocalIndexedFileSystemData => LocalIndexedFileSystem.connect( d, runtimeData)
      case d: AmazonS3FileSystemData => AmazonS3FileSystem.connect( d, runtimeData)
      case _ => throw new IllegalArgumentException(
        "unrecognized FileSystemData: " + ( if( data == null ) "null" else data.getClass.getCanonicalName ))
    }
}
