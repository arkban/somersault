package somersault.core.filesystems.amazonS3

import collection.mutable
import com.amazonaws.auth.{BasicAWSCredentials, AWSCredentials}
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.{S3ObjectSummary, ObjectListing, ObjectMetadata, ListObjectsRequest}
import java.io.{ByteArrayInputStream, FileInputStream, FileOutputStream, InputStream}
import logahawk.Severity
import net.jcip.annotations.{NotThreadSafe, Immutable}
import nu.xom.Element
import org.apache.commons.io.FileUtils
import org.apache.commons.lang.{StringUtils, IllegalClassException}
import org.apache.tools.bzip2.CBZip2InputStream
import org.joda.time.DateTime
import scala.Some
import scala.collection.JavaConversions._
import scala.io.Source
import somersault.core.encode.{EncryptResult, CompressResult}
import somersault.core.exceptions.{AlreadyExistsException, PathDoesNotExistException, CannotReadFileException}
import somersault.core.filesystems._
import somersault.core.filesystems.index.{IndexArtifact, Index, H2IndexData, H2Index, IndexFile, IndexDirectory, IndexDirectoryPrototype}
import somersault.core.{StreamPumpData, StreamPump}
import somersault.util.persist.{PersisterContext, Persister}
import somersault.util.{H2DataAccessObject, FilenameUtils2, ObservableInputStream, ObservableOutputStream, ObservableOutputStreamListener, Base32}

/**
 * A FileSystem backed by an Amazon S3 bucket. This uses JetS3t to simplify communication. This FileSystem is
 * (naturally) Indexed.
 *
 * All usage of the JetS3t library is intentionally isolated and locked to this class. While that means this class
 * needs
 */
@NotThreadSafe
class AmazonS3FileSystem private(
  val data: AmazonS3FileSystemData,
  val runtimeData: RuntimeFileSystemData,
  private[amazonS3] val service: AmazonS3FileSystem.Service,
  private[amazonS3] val indexSupport: AmazonS3IndexSupport,
  streamPump: StreamPump)
  extends FileSystem {

  runtimeData.observerContainer.logger.info("AmazonS3FileSystem created for Bucket: %1$s", data.bucketName)

  val root: SomerDirectory = new AmazonS3FileSystem.S3Directory(this, None, indexSupport.index.root)

  override def getIndexArtifact(a: SomerArtifact) = a match {
    case s: AmazonS3FileSystem.S3Artifact => Some(s.index())
    case _ => throw new IllegalClassException(classOf[AmazonS3FileSystem.S3Artifact], a)
  }

  def remove(artifact: SomerArtifact) {
    artifact match {
      case f: AmazonS3FileSystem.S3File =>
        if (!(f.owner eq this))
          throw new IllegalArgumentException("artifact from a different FileSystems")
        indexSupport.index.remove(f.index)
        service.removeObject(f.index.storedName)
        indexSupport.update(f)
      case d: AmazonS3FileSystem.S3Directory =>
        if (!(d.owner eq this))
          throw new IllegalArgumentException("artifact from a different FileSystems")
        children(d).foreach(remove)
        indexSupport.index.remove(d.index)
        indexSupport.update(d)
      case _ => throw new IllegalClassException(classOf[AmazonS3FileSystem.S3Artifact], artifact)
    }
  }

  def createDir(parent: SomerDirectory, name: String) =
    parent match {
      case p: AmazonS3FileSystem.S3Directory =>
        if (!(p.owner eq this))
          throw new IllegalArgumentException("parent from a different FileSystems")

        val compResult = data.compressionProvider.encodeFile(name)
        val encResult = data.encryptionProvider.encodeFile(name)
        val newName = FileSystemUtils.getEncodedName(name, compResult, encResult)

        // we only create the directory in the index, S3 doesn't store directories, its just part of the key
        val dir = indexSupport.index.createDir(p.index, new IndexDirectoryPrototype(name, newName))
        val newDir = new AmazonS3FileSystem.S3Directory(this, new Some(p), dir)
        indexSupport.update(newDir)

        newDir
      case _ => throw new IllegalClassException(classOf[AmazonS3FileSystem.S3Directory], parent)
    }

  def write(parent: SomerDirectory, file: SomerFile, compResult: CompressResult, encResult: EncryptResult) =
    parent match {
      case p: AmazonS3FileSystem.S3Directory =>
        if (!(p.owner eq this))
          throw new IllegalArgumentException("parent from a different FileSystems")

        // check that a directory does not exist where a file is requested
        indexSupport.index.children(p.index).find(_.origName == file.name) match {
          case Some(d: IndexDirectory) =>
            throw new AlreadyExistsException(AmazonS3FileSystem.getPath(p) ::: file.name :: Nil)
          case _ =>
        }

        // sets up writing to temporary file
        val key = AmazonS3FileSystem.createKey(data, Some(p), file.name, compResult, encResult)
        val tempFile = FilenameUtils2.createTempFile(file.name, ".tmp", runtimeData.tempPath)
        val tempFileOutput = new FileOutputStream(tempFile, false)
        val listenOut = FileSystemUtils.write(
          data, runtimeData, key, compResult, encResult, indexSupport.index, p.index, file, tempFileOutput)

        // on close of the file stream, we will write the file to amazon
        listenOut.addListener(
          new ObservableOutputStreamListener {
            override def close(output: ObservableOutputStream) {
              val input = new ObservableInputStream(new FileInputStream(tempFile), Some(tempFile.length))
              input.addListener(runtimeData.observerContainer.inputStreamListener)
              service.writeObject(key, tempFile.length, input)
              indexSupport.update(file)
              FileUtils.deleteQuietly(tempFile)
            }
          })
        listenOut
      case _ => throw new IllegalClassException(classOf[AmazonS3FileSystem.S3Directory], parent)
    }

  def read(file: SomerFile) =
    file match {
      case f: AmazonS3FileSystem.S3File =>
        if (!(f.owner eq this))
          throw new IllegalArgumentException("file from a different FileSystems")

        val input = try {
          service.readObject(f.index.storedName)
        }
        catch {
          case ex: Exception =>
            throw new PathDoesNotExistException(AmazonS3FileSystem.getPath(f.parent) ::: f.name :: Nil, ex)
        }

        try {
          FileSystemUtils.read(data, runtimeData, f.index, input)
        }
        catch {
          case ex: Exception =>
            throw new CannotReadFileException(AmazonS3FileSystem.getPath(f.parent) ::: f.name :: Nil, ex)
        }
      case _ => throw new IllegalClassException(classOf[AmazonS3FileSystem.S3File], file)
    }

  def children(parent: SomerDirectory) =
    parent match {
      case p: AmazonS3FileSystem.S3Directory =>
        if (!(p.owner eq this))
          throw new IllegalArgumentException("parent from a different FileSystems")
        indexSupport.index.children(p.index).map {
          case f: IndexFile => new AmazonS3FileSystem.S3File(this, p, f)
          case d: IndexDirectory => new AmazonS3FileSystem.S3Directory(this, new Some(p), d)
        }
      case _ => throw new IllegalClassException(classOf[AmazonS3FileSystem.S3Directory], parent)
    }

  def find(parent: SomerDirectory,name:String) =
    parent match {
      case p: AmazonS3FileSystem.S3Directory =>
        if (!(p.owner eq this))
          throw new IllegalArgumentException("parent from a different FileSystems")
        indexSupport.index.find(p.index, name).map {
          case f: IndexFile => new AmazonS3FileSystem.S3File(this, p, f)
          case d: IndexDirectory => new AmazonS3FileSystem.S3Directory(this, new Some(p), d)
        }
      case _ => throw new IllegalClassException(classOf[AmazonS3FileSystem.S3Directory], parent)
    }

  override def disconnect() {
    indexSupport.flushIndex()
    indexSupport.index.disconnect()
    runtimeData.observerContainer.logger.debug("Disconnecting AmazonS3FileSystem for Bucket: %1$s", data.bucketName)
  }

  def reconcile(obs: ReconcileObserver) {
    obs.start()
    try {
      // The strategy for reconciliation is to copy the index and delete everything that is in both the index and the
      // FileSystem. That leaves us in our temporary index with just what needs to be removed from the Index. While
      // performing that search we remove anything from the FileSystem we do not find in the Index.

      indexSupport.index.flush()

      // copy the index so we can delete everything that exists on Amazon S3
      val tempFile = FilenameUtils2.createTempFile(
        AmazonS3FileSystem.indexFilenamePrefix, H2DataAccessObject.databaseFileExt, runtimeData.tempPath)

      FileUtils.copyFile(indexSupport.index.data.file, tempFile, true)
      val reconcileIndex = new H2Index(new H2IndexData(data, runtimeData, tempFile))

      val indexKey = AmazonS3FileSystem.createIndexKey(data)

      // remove from FileSystem and our index copy (for later)
      val fn: (S3ObjectSummary => Unit) = obj => {
        if (obj.getKey != indexKey) {
          try {
            reconcileIndex.select(obj.getKey) match {
              case Some(a) =>
                reconcileIndex.remove(a)
              case None =>
                obs.removeFromFileSystem(obj.getKey)
                service.removeObject(obj.getKey)
            }
          }
          catch {
            case ex: Exception => obs.error(Severity.ERROR, ex)
          }
        }
      }
      forAll(service.getKeys(1024), fn)

      // pruning leaves us only with the orphan files, so pruneIndex() only walks directory paths with files that we
      // really want to prove. besides improving performance, this simplifies the implementation
      reconcileIndex.pruneEmptyDirectories()

      pruneIndex(obs, Nil, reconcileIndex, reconcileIndex.root, indexSupport.index, indexSupport.index.root)
    }
    catch {
      case ex: Exception => obs.error(Severity.ERROR, ex)
    }
    finally {
      obs.end()
    }
  }

  protected[filesystems] def putLock(lockName: String, timestamp: DateTime) {
    val timestampStr = timestamp.toString(LockingFileSystem.lockTimestampFormatter)
    val data = new ByteArrayInputStream(timestampStr.getBytes)
    service.writeObject(lockName, data.available(), data)
  }

  protected[filesystems] def removeLock(lockName: String) { service.removeObject(lockName) }

  protected[filesystems] def findExistingLocks() = {
    val pattern = generateLockNameRegex()
    val locks = new mutable.HashMap[String, DateTime]
    val fn: (S3ObjectSummary => Unit) = obj => {
      val lockName = obj.getKey

      val timestamp = {
        val input = service.readObject(obj.getKey)
        val string = Source.fromInputStream(input).getLines().mkString("")
        LockingFileSystem.lockTimestampFormatter.parseDateTime(string)
      }
      if (pattern.matcher(lockName).matches())
        locks += lockName -> timestamp
    }
    forAll(service.getKeys(options.lockNamePrefix, 1024), fn)
    locks
  }

  /**
   * Executes a standard forAll() on the provided {@link ObjectListing}. This method handles all the complexity of
   * an {@link ObjectListing}'s truncated behavior
   */
  private def forAll(initialObjectListing: ObjectListing, fn: (S3ObjectSummary) => Unit) {
    var objectListing = initialObjectListing
    var continueLoop = false
    do {
      objectListing.getObjectSummaries.foreach(fn)

      continueLoop = objectListing.isTruncated
      if (continueLoop)
        objectListing = service.client.listNextBatchOfObjects(objectListing)
    }
    while (continueLoop)
  }

  /**
   * Removes any items in the source index from the dest index. This is intended to be used by reconcile(),
   * where the source contains only the orphan files.
   *
   * This will NOT remove directories from the dest. It is highly recommended that you run pruneEmptyDirectories()
   * on the source Index before executing this method. That will reduce the source to just the orphan files we want
   * to remove. Doing so will improve performance by avoiding recursion into empty directory paths.
   */
  private def pruneIndex(
    obs: ReconcileObserver,
    parentPath: List[String],
    src: Index,
    srcParentDir: IndexDirectory,
    dest: Index,
    destParentDir: IndexDirectory) {

    val destMap = dest.children(destParentDir).foldLeft(new mutable.HashMap[String, IndexArtifact]) {
      case (map, a) => map += (a.origName -> a)
    }

    src.children(srcParentDir).foreach(
      a => {
        try {
          a match {
            case srcFile: IndexFile =>
              obs.removeFromIndex(FilenameUtils2.concat(parentPath ::: srcFile.origName :: Nil))
              dest.remove(destMap(srcFile.origName))
              indexSupport.update(a)
            case srcDir: IndexDirectory => destMap(srcDir.origName) match {
              case destFile: IndexFile =>
              case destDir: IndexDirectory => pruneIndex(
                obs, parentPath ::: destParentDir.origName :: Nil, src, srcDir, dest, destDir)
            }
          }
        }
        catch {
          case ex: Exception => obs.error(Severity.ERROR, ex)
        }
      })
  }
}

object AmazonS3FileSystem extends FileSystemProvider {

  private[filesystems] val indexFilenamePrefix = "somersault.index"

  private[filesystems] val indexFilename = indexFilenamePrefix + H2DataAccessObject.databaseFileExt

  /**
   * Retrieves or creates the Index, and returns the FileSystem.
   */
  def connect(fsData: FileSystemData, runtimeData: RuntimeFileSystemData): AmazonS3FileSystem =
    fsData match {
      case data: AmazonS3FileSystemData =>

        runtimeData.observerContainer.logger.debug(
          "Checking AmazonS3FileSystem for Bucket: %1$s", data.bucketName)

        val service = data.credentials.createService(data.bucketName)

        val indexKey = createIndexKey(data)

        val indexFile = {
          if (service.objectExists(indexKey)) {
            runtimeData.observerContainer.logger.info("Index exists, downloading...")
            val indexFile = FilenameUtils2.createTempFile(
              indexFilenamePrefix, H2DataAccessObject.databaseFileExt, runtimeData.tempPath)
            val input = new CBZip2InputStream(service.readObject(indexKey))
            val output = new FileOutputStream(indexFile, false)
            // no observer for the getting of the index, the UI is not setup to understand that
            new StreamPump().pump(input, output, new StreamPumpData(true, None))
            indexFile
          }
          else {
            runtimeData.observerContainer.logger.info("No Index exists, creating new Index...")
            FilenameUtils2.createTempFile(
              indexFilenamePrefix, H2DataAccessObject.databaseFileExt, runtimeData.tempPath)
          }
        }

        val index = new H2Index(new H2IndexData(data, runtimeData, indexFile))
        val streamPump = new StreamPump()
        val indexSupport = new AmazonS3IndexSupport(data, runtimeData, service, index, streamPump)
        new AmazonS3FileSystem(data, runtimeData, service, indexSupport, streamPump)
      case _ => throw new IllegalClassException(classOf[AmazonS3FileSystemData], fsData)
    }

  /**
   * Creates a pseudo-random bucket name, using the provided string. This method exists to help create a good bucket
   * name, since Bucket names must be unique across all of Amazon S3. The bucket name will be something like
   * "somersault.<value>.<random>. It is not required to use this method.
   */
  def generateBucketName(value: String): String = {
    if (!StringUtils.containsOnly(value, "abcdefghijklmnopqrstuvwxyz1234567890.-"))
      throw new IllegalArgumentException(
        "value many only contain lowercase letters, numbers, period, dash, and underscore")
    if (value.length >= 192)
      throw new IllegalArgumentException("value must be 192 characters or less")

    val result = new StringBuilder
    result.append("somersault.")
    result.append(value)
    result.append(".")

    val bytes = new Array[Byte](4)
    val random = new java.util.Random()
    random.nextBytes(bytes)
    result.append(Base32.encode(bytes).toLowerCase)

    result.toString()
  }

  /**
   * Creates the complete key for an S3Directory. If encryption is not used the key uses the familiar "path/path/path"
   * format. If encryption is used the path will be a hash of the encrypted names of the entire path.
   */
  private[filesystems] def createKey(
    data: FileSystemData,
    parent: Option[AmazonS3FileSystem.S3Directory],
    origName: String,
    compResult: CompressResult,
    encResult: EncryptResult): String = {
    val name = FileSystemUtils.getEncodedName(origName, compResult, encResult)

    if (encResult.encoded) {
      val md = data.hashInfo.createDigest()
      getPath(parent).foreach(p => md.update(data.encryptionProvider.encodeDir(p).newName.getBytes))
      md.update(name.getBytes)
      Base32.encode(md.digest)
    }
    else {
      val builder = getPath(parent).foldLeft(new StringBuilder) {
        case (b, p) => b.append(p).append(FilenameUtils2.dirSeparator)
      }
      builder.append(name).toString()
    }
  }

  /**
   * Returns the key for the Index file.
   */
  private[filesystems] def createIndexKey(data: AmazonS3FileSystemData) = {
    val compResult = data.compressionProvider.encodeFile(indexFilename)
    val encResult = data.encryptionProvider.encodeFile(indexFilename)
    createKey(data, None, indexFilename, compResult, encResult)
  }

  /**
   * Returns the path a list of names from the provided artifact.
   */
  private[filesystems] def getPath(dir: AmazonS3FileSystem.S3Directory): List[String] =
    dir.parent match {
      case Some(p) => getPath(p) ::: dir.name :: Nil
      // no parent means this is the root
      case None => Nil
    }

  /**
   * Returns the path a list of names from the provided artifact.
   */
  private[filesystems] def getPath(parent: Option[AmazonS3FileSystem.S3Directory]): List[String] =
    parent match {
      case Some(p) => getPath(p)
      // no parent means this is the root
      case None => Nil
    }

  /*
  * These classes exist to isolate and firewall all the JetS3 implementation details away from the rest of
  * the AmazonS3FileSystem implementation.
  */
  @Immutable
  class Credentials(private[filesystems] val credentials: AWSCredentials) {

    def this(accessKey: String, secretKey: String) = this(new BasicAWSCredentials(accessKey, secretKey))

    def accessKey = credentials.getAWSAccessKeyId

    def secretKey = credentials.getAWSSecretKey

    private[AmazonS3FileSystem] def createService(bucketName: String) =
      new Service(new AmazonS3Client(credentials), bucketName)
  }

  class CredentialsPersister extends Persister {

    def canRead(context: PersisterContext, element: Element) = checkClass(element, classOf[Credentials])

    def read[T](context: PersisterContext, element: Element) = {
      val accessKey = Persister.getChildElement(element, "accessKey").getValue
      val secretKey = Persister.getChildElement(element, "secretKey").getValue
      new Credentials(accessKey, secretKey).asInstanceOf[T]
    }

    def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false) {
      obj match {
        case c: Credentials =>
          writeClass(c, element, dynamic)
          Persister.createElement("accessKey", c.accessKey, element)
          Persister.createElement("secretKey", c.secretKey, element)
        case _ => throw new IllegalArgumentException("cannot write: " + obj.getClass.getName)
      }
    }
  }

  /**
   * Provides access to Amazon S3. This encapsulates both a service and a bucket.
   */
  @Immutable
  class Service(private[filesystems] val client: AmazonS3Client, bucketName: String) {

    // will this throw if already exists?
    client.createBucket(bucketName)

    /** Returns true if the object with the provided key exists.   */
    def objectExists(key: String) = {
      val lor = new ListObjectsRequest(bucketName, key, null, null, null)
      client.listObjects(lor).getObjectSummaries.size == 1
    }

    /** Gets the InputStream for the object specified by the provided key. This will throw if no object exists.  */
    def readObject(key: String): InputStream = client.getObject(bucketName, key).getObjectContent

    /** Writes the provided InputStream to the provided key.  */
    def writeObject(key: String, contentLength: Long, input: InputStream) {
      val metadata = new ObjectMetadata()
      metadata.setContentLength(contentLength)
      metadata.setContentType("binary/octet-stream")
      client.putObject(bucketName, key, input, metadata)
    }

    /** Deletes the object with the specified key. */
    def removeObject(key: String) { client.deleteObject(bucketName, key) }

    /** Returns all keys via an {@link ObjectListing}.  */
    def getKeys(maxKeys: Int): ObjectListing =
      client.listObjects(new ListObjectsRequest(bucketName, null, null, null, maxKeys))

    /** Returns all keys via an {@link ObjectListing}.  */
    def getKeys(prefix: String, maxKys: Int): ObjectListing =
      client.listObjects(new ListObjectsRequest(bucketName, prefix, null, null, maxKys))
  }

  private[filesystems] trait S3Artifact extends SomerArtifact {

    /**
     * The owning class of this for checking that only the FileSystem that created this instance can use this instance.
     */
    private[AmazonS3FileSystem] def owner(): AmazonS3FileSystem

    def index(): IndexArtifact

    def name = index().origName
  }

  private[filesystems] class S3Directory(
    val owner: AmazonS3FileSystem, val parent: Option[S3Directory], val index: IndexDirectory)
    extends SomerDirectory with S3Artifact

  private[filesystems] class S3File(val owner: AmazonS3FileSystem, val parent: S3Directory, val index: IndexFile)
    extends SomerFile with S3Artifact {

    def size = index.size

    def modified = index.modified

    def attributes = index.attributes

    def hash = index.hash
  }

}
