package somersault.core.filesystems.amazonS3

import java.io.{FileInputStream, FileOutputStream}
import net.jcip.annotations.NotThreadSafe
import org.apache.commons.io.FileUtils
import org.apache.tools.bzip2.CBZip2OutputStream
import somersault.core.filesystems.RuntimeFileSystemData
import somersault.core.filesystems.index.{IndexSupport, H2Index}
import somersault.core.{StreamPump, StreamPumpData}
import somersault.util.{Repeater, FilenameUtils2, ObservableInputStream, ObservableOutputStream, ObservableOutputStreamListener}

/**
 * A FileSystem backed by an Amazon S3 bucket. This uses JetS3t to simplify communication. This FileSystem is
 * (naturally) Indexed.
 *
 * All usage of the JetS3t library is intentionally isolated and locked to this class. While that means this class
 * needs
 */
/**
 * A utility class that helps read and write the Index file.
 */
@NotThreadSafe
private class AmazonS3IndexSupport(
  fsData: AmazonS3FileSystemData,
  runtimeData: RuntimeFileSystemData,
  service: AmazonS3FileSystem.Service,
  private[filesystems] val index: H2Index,
  streamPump: StreamPump)
  extends IndexSupport(fsData.indexSupportData) {

  def flushIndex() {
    runtimeData.observerContainer.logger.debug("Transferring Index file...")

    try {
      val flushOp = () => {
        index.flush()

        val tempFile = FilenameUtils2.createTempFile(AmazonS3FileSystem.indexFilename, ".tmp", runtimeData.tempPath)

        // write the index to a temporary location
        val input = new ObservableInputStream(new FileInputStream(index.data.file), Some(index.data.file.length))
        input.addListener(runtimeData.observerContainer.inputStreamListener)
        val tempOutput = new ObservableOutputStream(
          new CBZip2OutputStream(new FileOutputStream(tempFile, false)), None)

        // write the index to amazon
        tempOutput.addListener(
          new ObservableOutputStreamListener() {
            override def close(output: ObservableOutputStream) {
              val tempInput = new ObservableInputStream(new FileInputStream(tempFile), Some(tempFile.length))
              tempInput.addListener(runtimeData.observerContainer.inputStreamListener)

              val indexKey = AmazonS3FileSystem.createIndexKey(fsData)
              service.writeObject(indexKey, tempFile.length, tempInput)

              FileUtils.deleteQuietly(tempFile)
            }
          })

        streamPump.pump(input, tempOutput, new StreamPumpData(true, Some(index.data.file.length)))
      }

      new Repeater(flushOp, runtimeData.maxRepeatAttempts).repeat()

      reset()
    }
    catch {
      case ex: Exception => runtimeData.observerContainer.logger.error(
        "Failed to transfer index, will retry after next file transfer", ex)
    }
  }
}
