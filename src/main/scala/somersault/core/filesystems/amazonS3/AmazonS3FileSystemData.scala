package somersault.core.filesystems.amazonS3

import java.lang.String
import nu.xom.Element
import somersault.core.encode.{CompressionProvider, EncryptionProvider}
import somersault.core.filesystems.amazonS3.AmazonS3FileSystem.{CredentialsPersister, Credentials}
import somersault.core.filesystems.{PeriodicFileSystemEventControllerData, FileSystemDataPersister, FileSystemData}
import somersault.util.HashInfo
import somersault.util.persist.{PersisterContext, Persister}

class AmazonS3FileSystemData(
  val name: String,
  val hashInfo: HashInfo,
  val compressionProvider: CompressionProvider,
  val encryptionProvider: EncryptionProvider,
  val lockRefreshSupportData: PeriodicFileSystemEventControllerData,
  val indexSupportData: PeriodicFileSystemEventControllerData,
  val credentials: AmazonS3FileSystem.Credentials,
  val bucketName: String)
  extends FileSystemData {

  val isIndexed = true
}

class AmazonS3FileSystemDataPersister extends FileSystemDataPersister(true) {

  def canRead(context: PersisterContext, element: Element) = checkClass(element, classOf[AmazonS3FileSystemData])

  def read[T](context: PersisterContext, element: Element) = {
    val credentials: Credentials = new CredentialsPersister().read(
      context, Persister.getChildElement(element, "credentials"))
    val bucketName = Persister.getChildElement(element, "bucketName").getValue
    new AmazonS3FileSystemData(
      readName(element),
      readHashInfo(context, element),
      readCompProvider(context, element),
      readEncProvider(context, element),
      readLockRefreshSupportData(context, element),
      readIndexSupportData(context, element),
      credentials,
      bucketName
    ).asInstanceOf[T]
  }

  override def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean) {
    obj match {

      case data: AmazonS3FileSystemData =>
        super.write(context, data, element, dynamic)
        new CredentialsPersister().write(
          context, data.credentials, Persister.createElement("credentials", parent = element))
        Persister.createElement(
          "bucketName",
          data.bucketName,
          element,
          "name of the bucket, only alpha, numbers, underscores, less than 64 characters")
        context.container.write(
          context,
          data.indexSupportData,
          Persister.createElement("indexSupportData", parent = element),
          dynamic = true)

      case _ => throw new IllegalArgumentException("cannot write: " + obj.getClass.getName)
    }
  }
}
