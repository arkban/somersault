package somersault.core.filesystems

import java.io.File
import util.Random
import org.apache.commons.lang.StringUtils

/**
 * Data need for a FileSystem when it is created and used. This is not specialized to each {@link FileSystem}; all
 * of the data can be used at will whenever the {@link FileSystem} decides it needs it.
 *
 * @param tempPath Returns a temporary directory for use by this FileSystem.
 * @param maxRepeatAttempts Number of times various operations should be attempted before giving up. This currently
 * is used for any operation that can be re-attempted.
 */
class RuntimeFileSystemData(
    val observerContainer: FileSystemObserverContainer, val tempPath: File, val maxRepeatAttempts: Int = 1)
{
  /**
   * A random for this instance of the {@link FileSystem}. This is used to distinguish two different
   * {@link FileSystem}s. The format is 6 hexadecimal digits.
   */
  val id = StringUtils.leftPad( RuntimeFileSystemData.idGenerator.nextInt( 0xFFFFFF).toHexString, 6, '0')
}

object RuntimeFileSystemData
{
  val idGenerator = new Random
}