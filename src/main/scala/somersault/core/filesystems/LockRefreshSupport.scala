package somersault.core.filesystems

import net.jcip.annotations.ThreadSafe

/** A base class for refreshing the lock for {@link LockRefreshingFileSystem}s. */
@ThreadSafe
class LockRefreshSupport(fileSystem: LockCheckingFileSystem, val data: PeriodicFileSystemEventControllerData)
  extends PeriodicFileSystemEventController {

  protected def fire() {
    try fileSystem.lock(fileSystem.currentLock.mode)
    finally reset()
  }
}

object LockRefreshSupport {

  /**
   * Default {@link PeriodicFileSystemEventControllerData} for when the lock held by a {@link LockingFileSystem} should
   * be refreshed. The lock should be refreshed relatively infrequently, assuming the default
   * {@link LockingFileSystem#Options#lockTimeout} of 3 hours.
   */
  val defaults = new PeriodicFileSystemEventControllerData(
    bytesMin = 10 * 1000000, bytesMax = 100 * 1000000, // 100 MB to 500 MB
    actionsMin = 100, actionsMax = 1000,
    msMin = 1 * 60 * 60 * 1000, msMax = 2 * 60 * 60 * 1000 // 1 hours to 2 hours
  )
}

