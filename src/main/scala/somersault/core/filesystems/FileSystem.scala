package somersault.core.filesystems

import java.io.{OutputStream, InputStream}
import net.jcip.annotations.{NotThreadSafe, Immutable}
import org.joda.time.DateTime
import somersault.core.encode.{EncryptResult, CompressResult}
import somersault.util.Hash

/**
 * The basic definition of a file system that Somersault will interact with. The functionality described here is common
 * to all file systems, whether they use an index or not. (Non-indexed FileSystems are usually called "Direct".)
 *
 * The FileSystem and its child SomerArtifacts are intentionally designed such that the SomerArtifacts are simply
 * data holders, they contain no methods or logic. This is to allow more flexible and simpler implementation of
 * the SomerArtifacts such that they can be created at whim and use as little memory as possible, perhaps as little as
 * just an ID that the FileSystem needs.
 *
 * All operations are expected to be blocking unless otherwise stated.
 *
 * FileSystems are expected to be not thread-safe unless otherwise stated.
 */
@NotThreadSafe
trait FileSystem extends CoreFileSystem with IndexedFileSystem with LockingFileSystem {

  /**
   * Disconnect from the underlying file system, performing any necessary clean-up and termination activities. The
   * FileSystem object should not be used after this is called.
   */
  def disconnect()

  /**
   * Returns the root of the file system.
   *
   * @return This should never return null.
   */
  def root: SomerDirectory

  /**
   * Returns the artifacts directly under the provided SomerDirectory. This should return all files, hidden and
   * non-hidden. The user of this class will decide what to do with hidden files.
   *
   * For indexed FileSystems, this should use the Index to answer this query.
   *
   * @throws PathDoesNotExistException If the parent does not exist.
   */
  def children(parent: SomerDirectory): Iterable[SomerArtifact]

  /**
   * Returns the SomerArtifact directly under the provided SomerDirectory. This should return any file, hidden and or
   * non-hidden. The user of this class will decide what to do with hidden files.
   *
   * For indexed FileSystems, this should use the Index to answer this query.
   *
   * @throws PathDoesNotExistException If the parent does not exist.
   */
  def find(parent: SomerDirectory, name: String): Option[SomerArtifact]

  /**
   * Opens the provided file and returns a stream that contains the contents of the file. The expected usage is to
   * give write the data to the WriteResult returned from the write() method.
   *
   * @throws PathDoesNotExistException If the file does not exist.
   * @throws CannotReadFileException If trying to read a directory or if reading is not allowed
   */
  def read(f: SomerFile): InputStream

  /**
   * Creates or overwrites the file specified, and return an OutputStream that can be used to write to the file. The
   * expected usage is to write the data from an InputStream returned from a call to read().
   *
   * For indexed FileSystems which support compression AND encryption, files should be compressed before encrypted.
   * Encryption intentionally increases randomness of the data and compression does poorly on random data, so encrypting
   * before compressing generally makes compression ineffective. Also if encryption is used, the file name should be
   * encrypted, as that helps hide the identify of the file. (Note that usually only Indexed FileSystems support
   * compression and encryption.)
   *
   * @param parent Parent { @link SomerFile} in this { @link FileSystem}
   * @param file The { @link SomerFile} in this { @link FileSystem}
   * @param compResult Whether the file is compressed and how, determined by { @link CompressionProvider#encode)
   * @param encResult Whether the file is encrypted and how, determined by { @link EncryptionProvider#encode)
   *
   * @throws PathDoesNotExistException If the parent does not exist.
   * @throws AlreadyExistsException If a directory exists with the same name.
   * @throws CannotWriteFileException If writing is not allowed.
   */
  def write(
    parent: SomerDirectory, file: SomerFile, compResult: CompressResult, encResult: EncryptResult): OutputStream

  /**
   * Creates the specified directory. This should silently do nothing if the directory already exists.
   *
   * @throws PathDoesNotExistException If the parent does not exist.
   * @throws AlreadyExistsException If a file or directory exists with the same name.
   */
  def createDir(parent: SomerDirectory, name: String): SomerDirectory

  /**
   * Removes (deletes) the artifact from the file system.
   *
   * If necessary (based on the idiosyncrasies of the file system) this should delete all child files and directories.
   * (This sort of bulk delete is handled by the FileSystem implementation because it most likely can create a better,
   * faster, smaller memory implementation than Somersault, perhaps by using OS or protocol specific bulk delete
   * functionality.)
   *
   * @throws PathDoesNotExistException If the artifact does not exist.
   */
  def remove(a: SomerArtifact)

  /**
   * Reconciles any data integrity issues that might exist. This method should search for inconsistencies and correct
   * them.
   *
   * To simplify implementations this method should be the ONLY method called and only called once between
   * connect() and disconnect().
   *
   * For indexed FileSystems this most likely will compare the Index with the real FileSystem and remove any
   * un-paired artifacts. For non-indexed FileSystems this may do nothing (as there may be no way to detect internal
   * inconsistencies).
   *
   * Implementations may prune empty directories, especially when directories are stored only in the Index. This
   * is acceptable as re-creating directories is generally a cheap operation.
   */
  def reconcile(obs: ReconcileObserver)
}

/**
 * Somersault's representation of files and directories in a FileSystem, both existing and those to be created. All
 * functionality for manipulating artifacts will be exposed by the FileSystem in terms of artifacts.
 *
 * Each instance should uniquely define a single artifact. It is highly recommended that each instance contain enough
 * information to stand alone. The intent is to not have a giant tree of artifacts that will unnecessarily hog up
 * memory. The users of these instances are smart enough to discard unused artifacts so they can be garbage collected.
 *
 * That being said having a reference from child to parent artifact is usually an acceptable solution (assuming the
 * parent artifact is relatively small). But having a parent contain a reference to its children artifacts is probably
 * going to cause problems.
 */
@NotThreadSafe
trait SomerArtifact {

  /**
   * Returns the name of this file or directory.
   */
  def name: String
}

/**
 * Represents a directory, some construct that contains other directories and/or files.
 *
 * This intentionally does not have a children method, that is the responsibility of the owner FileSystem.
 */
@NotThreadSafe
trait SomerDirectory extends SomerArtifact

/**
 * Represents a file, with a size and modification date.
 */
@NotThreadSafe
trait SomerFile extends SomerArtifact {

  def modified: DateTime

  def size: Long

  def attributes: FileAttributes

  /**
   * Returns a hash for this file. For indexed FileSystems this should be a cached value. For non-Indexed FileSystems
   * the hash may need to be computed, and in those situations this should be lazily computed. (Standard usage may
   * decide that files are different based on something cheaper like the file size, so it would be a waste to compute
   * the hash unless needed.)
   */
  def hash: Hash
}

/**
 * Attributes supported by Java 1.6. These are only the attributes for the current user.
 */
@Immutable
class FileAttributes(val executable: Boolean, val readable: Boolean, val writable: Boolean) {

  private def canEqual(other: Any): Boolean = other.isInstanceOf[FileAttributes]

  override def equals(other: Any): Boolean = other match {
    case that: FileAttributes =>
      this.canEqual(that) && executable == that.executable && readable == that.readable && writable == that.writable
    case _ => false
  }

  override def hashCode: Int =
    41 * (41 * (if (executable) 1 else 0) + (if (readable) 1 else 0) + (if (writable) 1 else 0))

  override def toString = String.format(
    "E=%1$s, R=%2$s, W=%3$s", if (executable) "Y" else "N", if (readable) "Y" else "N", if (writable) "Y" else "N")
}
