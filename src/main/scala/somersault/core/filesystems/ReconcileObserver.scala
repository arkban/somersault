package somersault.core.filesystems

import logahawk.Severity
import somersault.util.AbstractMultiObserver

/**
 * Used to watch the progress of a call to FileSystem.reconcile().
 */

trait ReconcileObserver {

  def start()

  def end()

  /**
   * Removes an entry from the Index. The path should be the full file path, but is provided as a String to allow more
   * freedom with the implementation.
   */
  def removeFromIndex(path: String)

  /**
   * Removes an entry from the FileSystem. This should be the path, but may be something else if the FileSystem does
   * not use file paths. If it is not a full file path it should be some other fully identifying information.
   */
  def removeFromFileSystem(path: String)

  def error(severity: Severity, ex: Exception)
}

class NullReconcileObserver extends ReconcileObserver {

  def start() {}

  def end() {}

  def removeFromFileSystem(path: String) {}

  def removeFromIndex(path: String) {}

  def error(severity: Severity, ex: Exception) {}
}

class MultiReconcileObserver extends AbstractMultiObserver[ReconcileObserver] with ReconcileObserver {

  def start() { observers.foreach(_.start()) }

  def end() { observers.foreach(_.end()) }

  def removeFromFileSystem(path: String) { observers.foreach(_.removeFromFileSystem(path)) }

  def removeFromIndex(path: String) { observers.foreach(_.removeFromIndex(path)) }

  def error(severity: Severity, ex: Exception) { observers.foreach(_.error(severity, ex)) }

}