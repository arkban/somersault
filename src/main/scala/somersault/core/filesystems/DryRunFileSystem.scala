package somersault.core.filesystems

import index.{H2IndexData, IndexArtifact, IndexDirectory, IndexFile, IndexDirectoryPrototype, IndexFilePrototype, H2Index}
import java.io.File
import org.apache.commons.io.FilenameUtils
import org.apache.commons.io.input.NullInputStream
import org.apache.commons.io.output.NullOutputStream
import org.apache.commons.lang.IllegalClassException
import scala.collection.Map
import somersault.core.encode.{NullEncryptionProvider, NullCompressionProvider, EncryptResult, CompressResult}
import somersault.util.{ObservableOutputStreamListener, ObservableOutputStream, HashInfo, Hash}

/**
 * Wraps a FileSystem so that no changes are persisted to the underlying FileSystem. But this FileSystem is not
 * immutable, it uses an Index to accept changes. This retains everything an Index can store, but all File contents
 * are sent to /dev/null.
 *
 * The construct is public for unit testing ONLY!
 */
class DryRunFileSystem(val fs: FileSystem, index: H2Index) extends ProxyFileSystem(fs) {

  runtimeData.observerContainer.logger.info("DryRunFileSystem created for FileSystem " + fs.data.name)

  override def children(parent: SomerDirectory) =
    parent match {
      case p: InMemoryDirectory =>
        if (!(p.owner eq this))
          throw new IllegalArgumentException("parent from a different FileSystems")

        // create a map containing all the artifacts in the real FileSystem for creating the InMemoryArtifacts below
        val fsChildMap: Map[String, SomerArtifact] =
          p.artifact match {
            case Some(fsParent: SomerDirectory) =>
              populateIndex(p)
              fs.children(fsParent).map(a => a.name -> a).toMap
            case None => Map.empty
            case x => throw new Exception("Unexpected response: " + x)
          }

        // construct the InMemoryArtifact, using the real artifact if it exists
        index.children(p.index).map {
          case f: IndexFile => new InMemoryFile(fsChildMap.get(f.origName), p, f)
          case d: IndexDirectory => new InMemoryDirectory(fsChildMap.get(d.origName), new Some(p), d)
        }

      case _ => throw new IllegalClassException(classOf[InMemoryDirectory], parent)
    }

  override def find(parent: SomerDirectory, name: String) =
    parent match {
      case p: InMemoryDirectory =>
        if (!(p.owner eq this))
          throw new IllegalArgumentException("parent from a different FileSystems")

        // find the artifact in the real FileSystem (if it does not exist then it only exists in our Index)
        val artifact: Option[SomerArtifact] =
          p.artifact.collect {
            case fsParent: SomerDirectory =>
              populateIndex(p)
              fs.find(fsParent, name)
          }.flatten

        // construct the InMemoryArtifact, using the real artifact if it exists
        index.find(p.index, name).map {
          case f: IndexFile => new InMemoryFile(artifact, p, f)
          case d: IndexDirectory => new InMemoryDirectory(artifact, new Some(p), d)
        }

      case _ => throw new IllegalClassException(classOf[InMemoryDirectory], parent)
    }

  /**
   * This is lazy to avoid calling fs.root until some external caller calls root. (This also avoids calling
   * fs.root until the proper lock has been acquired.)
   */
  private lazy val rootArtifact: SomerDirectory = new InMemoryDirectory(Some(fs.root), None, index.root)

  override def root: SomerDirectory = rootArtifact

  override def read(file: SomerFile) =
    file match {
      case f: InMemoryFile =>
        if (!(f.owner eq this))
          throw new IllegalArgumentException("file from a different FileSystems")
        f.artifact match {
          case Some(f: SomerFile) => fs.read(f)
          case Some(d: SomerDirectory) => throw new IllegalArgumentException(
            "InMemoryFile has a SomerDirectory as its artifact")
          case None => new NullInputStream(f.size)
          case x => throw new Exception("Unexpected response: " + x)
        }
      case _ => throw new IllegalClassException(classOf[InMemoryFile], file)
    }

  override def write(parent: SomerDirectory, file: SomerFile, compResult: CompressResult, encResult: EncryptResult) =
    parent match {
      case p: InMemoryDirectory =>
        if (!(p.owner eq this))
          throw new IllegalArgumentException("parent from a different FileSystems")

        val output = new NullOutputStream
        val listenOut = new ObservableOutputStream(output, Some(file.size))

        listenOut.addListener(runtimeData.observerContainer.outputStreamListener)
        listenOut.addListener(
          new ObservableOutputStreamListener {
            override def close(out: ObservableOutputStream) {
              val prototype = new IndexFilePrototype(
                file.name, file.name, file.size, file.modified, file.attributes, false, false, file.hash)
              index.writeFile(p.index, prototype)
            }
          })
        listenOut
      case _ => throw new IllegalClassException(classOf[InMemoryDirectory], parent)
    }

  override def createDir(parent: SomerDirectory, name: String) =
    parent match {
      case p: InMemoryDirectory =>
        if (!(p.owner eq this))
          throw new IllegalArgumentException("parent from a different FileSystems")
        val dir = index.createDir(p.index, new IndexDirectoryPrototype(name, name))
        new InMemoryDirectory(None, new Some(p), dir)
      case _ => throw new IllegalClassException(classOf[InMemoryDirectory], parent)
    }

  override def remove(artifact: SomerArtifact) {
    artifact match {
      case a: InMemoryArtifact =>
        if (!(a.owner eq this))
          throw new IllegalArgumentException("artifact from a different FileSystems")
        index.remove(a.index)
      case _ => throw new IllegalClassException(classOf[InMemoryArtifact], artifact)
    }
  }

  override def disconnect() {
    index.disconnect()
    fs.disconnect()
  }

  override def reconcile(obs: ReconcileObserver) {}

  override def lock(mode: LockingFileSystemMode.Mode) = fs.lock(LockingFileSystemMode.Read)

  private class InMemoryArtifact(val artifact: Option[SomerArtifact], val index: IndexArtifact)
    extends SomerArtifact {

    /**
     * The owning class of this for checking that only the FileSystem that created this instance can use this instance.
     */
    private[DryRunFileSystem] def owner(): DryRunFileSystem = DryRunFileSystem.this

    def name = index.origName
  }

  private class InMemoryDirectory(
    artifact: Option[SomerArtifact], val parent: Option[InMemoryDirectory], override val index: IndexDirectory)
    extends InMemoryArtifact(artifact, index) with SomerDirectory {

    /**
     * False if we've never queried the underlying FileSystem for this directories children and populated our index
     * with the results.
     */
    var queried: Boolean = false
  }

  private class InMemoryFile(
    artifact: Option[SomerArtifact], val parent: InMemoryDirectory, override val index: IndexFile)
    extends InMemoryArtifact(artifact, index) with SomerFile {

    def size = index.size

    def modified = index.modified

    def attributes = index.attributes

    def hash = index.hash
  }

  private def populateIndex(p: InMemoryDirectory) {

    p.artifact match {
      // directory exists in our index and in real FileSystem, so we need attach the SomerArtifacts from the
      // FileSystem to the returned InMemoryArtifacts to enable future operations
      case Some(fsParent: SomerDirectory) =>

        // populate our index if necessary
        if (!p.queried) {
          fs.children(fsParent).foreach {
            case file: SomerFile =>
              val hash = new Hash(fs.data.hashInfo, new Array[Byte](0))
              val pt = new IndexFilePrototype(
                file.name, file.name, file.size, file.modified, file.attributes, false, false, hash)
              index.writeFile(p.index, pt)
            case dir: SomerDirectory =>
              val pt = new IndexDirectoryPrototype(dir.name, dir.name)
              index.createDir(p.index, pt)
          }
          p.queried = true
        }


      // directory exists only in our index
      case None =>

      case x => throw new Exception("Unexpected response: " + x)
    }
  }
}

object DryRunFileSystem extends FileSystemProvider {

  val indexFilenamePrefix = "somersault.dryrun.index"

  def connect(fsData: FileSystemData, runtimeData: RuntimeFileSystemData): DryRunFileSystem =
    fsData match {
      case data: DryRunFileSystemData =>
        val indexPath = FilenameUtils.concat(
          runtimeData.tempPath.getAbsolutePath, DryRunFileSystem.indexFilenamePrefix)
        val index = new H2Index(
          new H2IndexData(
            HashInfo.md5,
            new NullCompressionProvider,
            new NullEncryptionProvider,
            runtimeData.observerContainer.logger,
            new File(indexPath)))
        new DryRunFileSystem(data.fs, index)
      case _ => throw new IllegalClassException(classOf[DryRunFileSystemData], fsData)
    }
}

class DryRunFileSystemData(val fs: FileSystem) extends FileSystemData {

  def name = fs.data.name

  def isIndexed = fs.data.isIndexed

  def hashInfo = fs.data.hashInfo

  def compressionProvider = fs.data.compressionProvider

  def encryptionProvider = fs.data.encryptionProvider

  def lockRefreshSupportData = fs.data.lockRefreshSupportData
}