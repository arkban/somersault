package somersault.core.filesystems

import java.util.concurrent.atomic.AtomicBoolean
import net.jcip.annotations.ThreadSafe
import somersault.core.encode.{EncryptResult, CompressResult}

/**
 * A wrapper that ensures that once {@link FileSystem#disconnect()} is called, all other methods fail (because we are
 * no longer ocnnected). It is a good safe-guard for all {@link FileSystem}s.
 */
@ThreadSafe
class ConnectCheckingFileSystem(underlying: FileSystem) extends ProxyFileSystem(underlying) {

  /**
   * When a {@link FileSystem} is first created by a {@link FileSystemProvider} it is automatically connected.
   */
  private val connected = new AtomicBoolean(true)

  override def disconnect() {
    connected.set(false)

    underlying.disconnect()
  }

  override def remove(a: SomerArtifact) {
    if (!connected.get) throw new IllegalStateException("not connected")

    underlying.remove(a)
  }

  override def createDir(parent: SomerDirectory, name: String) = {
    if (!connected.get) throw new IllegalStateException("not connected")

    underlying.createDir(parent, name)
  }

  override def write(parent: SomerDirectory, file: SomerFile, compResult: CompressResult, encResult: EncryptResult) = {
    if (!connected.get) throw new IllegalStateException("not connected")

    underlying.write(parent, file, compResult, encResult)
  }

  override def read(f: SomerFile) = {
    if (!connected.get) throw new IllegalStateException("not connected")

    underlying.read(f)
  }

  override def children(parent: SomerDirectory) = {
    if (!connected.get) throw new IllegalStateException("not connected")

    underlying.children(parent)
  }

  override def find(parent: SomerDirectory, name: String): Option[SomerArtifact] = {
    if (!connected.get) throw new IllegalStateException("not connected")

    underlying.find(parent, name)
  }

  override def root = {
    if (!connected.get) throw new IllegalStateException("not connected")

    underlying.root
  }

  override def reconcile(obs: ReconcileObserver) {
    if (!connected.get) throw new IllegalStateException("not connected")

    underlying.reconcile(obs)
  }

  override def lock(mode: LockingFileSystemMode.Mode) = {
    if (!connected.get) throw new IllegalStateException("not connected")

    underlying.lock(mode)
  }

  override def unlock() {
    if (!connected.get) throw new IllegalStateException("not connected")

    underlying.unlock()
  }

  override def removeAllLocks() {
    if (!connected.get) throw new IllegalStateException("not connected")

    underlying.removeAllLocks()
  }
}
