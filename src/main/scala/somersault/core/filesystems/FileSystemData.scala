package somersault.core.filesystems

import logahawk.Logger
import net.jcip.annotations.Immutable
import nu.xom.Element
import somersault.core.encode.{CompressionProvider, EncryptionProvider}
import somersault.util.persist.{PersisterContext, Persister}
import somersault.util.{HashInfoPersister, ObservableInputStreamListener, ObservableOutputStreamListener, HashInfo}

/**
 * Provides various configuration settings for the construction of a {@link FileSystem}. This contains settings that
 * will not vary with the life of the {@link FileSystem}, and may uniquely define how this particular
 * {@link FileSystem} will work. Hence it should be obvious that this object will be specialized for each
 * {@link FileSystem} implementation.
 *
 * This object (and the data on it) should be persistable.
 */
@Immutable
trait FileSystemData {

  /**
   * The user-friendly name of the FileSystem,used to distinguish this FileSystem from others.
   */
  def name: String

  /**
   * Returns whether or not the {@link FileSystem} implementation uses an {@link Index}. (This flag is informational
   * only, and is not implied to mean that a user should be able to somehow find and interact with the {@link Index}
   * directly. However it should signal that {@link IndexArtifact#index()} will return a non-{@link None} result.
   */
  def isIndexed: Boolean

  def hashInfo: HashInfo

  /**
   * This may be a NullCompressionProvider for non-indexed FileSystems.
   */
  def compressionProvider: CompressionProvider

  /**
   * This may be a NullEncryptionsProvider for non-indexed FileSystems.
   */
  def encryptionProvider: EncryptionProvider

  /** Used to help construct the wrapping {@link LockRefreshingFileSystem}. */
  def lockRefreshSupportData: PeriodicFileSystemEventControllerData
}

/**
 * Persists FileSystemData instances.
 *
 * @param supportsEncoders If true this will persist the CompressionProvider and EncryptionProvider.
 */
abstract class FileSystemDataPersister(protected val supportsEncoders: Boolean) extends Persister {

  def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean) {
    obj match {
      case data: FileSystemData =>
        writeClass(obj, element, dynamic)
        Persister.createElement(
          "name",
          data.name,
          element,
          "The user-friendly name of the FileSystem,used to distinguish this FileSystem from others.")

        new HashInfoPersister().write(
          context, data.hashInfo, Persister.createElement(
            "hashInfo", parent = element))

        if (supportsEncoders) {
          context.container.write(
            context,
            data.compressionProvider,
            Persister.createElement("compProvider", parent = element),
            dynamic = true)
          context.container.write(
            context,
            data.encryptionProvider,
            Persister.createElement("encProvider", parent = element),
            dynamic = true)
          context.container.write(
            context,
            data.lockRefreshSupportData,
            Persister.createElement("lockRefreshData", parent = element),
            dynamic = true)
        }
      case _ => throw new IllegalArgumentException("cannot write: " + obj.getClass.getName)
    }
  }

  protected def readName(element: Element): String = Persister.getChildElement(element, "name").getValue

  protected def readHashInfo(context: PersisterContext, element: Element): HashInfo =
    new HashInfoPersister().read(context, Persister.getChildElement(element, "hashInfo"))

  protected def readCompProvider(context: PersisterContext, element: Element): CompressionProvider =
    context.container.read(context, Persister.getChildElement(element, "compProvider"))

  protected def readEncProvider(context: PersisterContext, element: Element): EncryptionProvider =
    context.container.read(context, Persister.getChildElement(element, "encProvider"))

  protected def readIndexSupportData(
    context: PersisterContext, element: Element): PeriodicFileSystemEventControllerData =
    context.container.read(context, Persister.getChildElement(element, "indexSupportData"))

  protected def readLockRefreshSupportData(
    context: PersisterContext, element: Element): PeriodicFileSystemEventControllerData =
    context.container.read(context, Persister.getChildElement(element, "lockRefreshData"))
}

/**
 * A container class that holds all the observer classes for a FileSystemData.
 */
class FileSystemObserverContainer(
  val logger: Logger,
  val inputStreamListener: ObservableInputStreamListener,
  val outputStreamListener: ObservableOutputStreamListener)


