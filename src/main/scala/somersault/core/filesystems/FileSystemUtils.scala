package somersault.core.filesystems

import index.{IndexFilePrototype, IndexDirectory, Index, IndexFile}
import java.io.{OutputStream, InputStream}
import java.security.{DigestInputStream, DigestOutputStream}
import org.apache.commons.io.input.CountingInputStream
import org.apache.commons.io.output.CountingOutputStream
import somersault.core.encode.{EncryptResult, CompressResult}
import somersault.util.{HashMismatchException, ObservableInputStreamListener, ObservableInputStream, ObservableOutputStreamListener, ObservableOutputStream, Hash}

/**
 * A suite of helper methods for implementing a FileSystem.
 */
object FileSystemUtils {

  /**
   * Reads the data from the provided stream, handling decryption and decompression as needed.
   *
   * @param data Provides CompressionProvider and EncryptionProvider.
   */
  def read(data: FileSystemData, runtimeData: RuntimeFileSystemData, file: IndexFile, input: InputStream) = {
    val decryptIn = if (file.encrypted) data.encryptionProvider.decode(file.origName, file.size, input) else input
    val decompIn = if (file.compressed) data.compressionProvider.decode(
      file.origName, file.size, decryptIn)
    else decryptIn
    val digestIn = new DigestInputStream(decompIn, data.hashInfo.createDigest())
    val countingIn = new CountingInputStream(digestIn)
    val listenIn = new ObservableInputStream(countingIn, Some(file.size))

    listenIn.addListener(runtimeData.observerContainer.inputStreamListener)
    listenIn.addListener(
      new ObservableInputStreamListener {
        override def close(in: ObservableInputStream) {
          val hash = new Hash(data.hashInfo, digestIn.getMessageDigest.digest)
          if (hash != file.hash)
            throw new HashMismatchException(file.hash, hash)
          if (countingIn.getByteCount != file.size)
            throw new Exception(
              "Size does not match, Original %d, Read %d".format(file.size, countingIn.getByteCount))
        }
      })
    listenIn
  }

  /**
   * Writes the provided data stream to the fileSystem, handling encryption, compression, and updating the index.
   *
   * @param index Index will be updated when writing finishes.
   * @param storedName This will be set as the IndexFile.storedName. This is provided, not calculated, to allow
   *                   the FileSystem to completely control how it stores data in the index.
   * @param compResult Caller generated EncodeResult from an CompressionProvider
   * @param encResult Caller generated EncodeResult from an EncryptionProvider
   * @param file Used to decide whether compression and encryption should be applied.
   * @param parentIndex Needed for updating the Index.
   */
  def write(
    data: FileSystemData,
    runtimeData: RuntimeFileSystemData,
    storedName: String,
    compResult: CompressResult,
    encResult: EncryptResult,
    index: Index,
    parentIndex: IndexDirectory,
    file: SomerFile,
    output: OutputStream): ObservableOutputStream = {
    val encOut = encResult.encode(output)
    val compOut = compResult.encode(encOut)
    val digestOut = new DigestOutputStream(compOut, data.hashInfo.createDigest())
    val countingOut = new CountingOutputStream(digestOut)
    val listenOut = new ObservableOutputStream(countingOut, Some(file.size))

    listenOut.addListener(runtimeData.observerContainer.outputStreamListener)
    listenOut.addListener(
      new ObservableOutputStreamListener {
        override def close(out: ObservableOutputStream) {
          val hash = new Hash(data.hashInfo, digestOut.getMessageDigest.digest)
          val prototype = new IndexFilePrototype(
            file.name,
            storedName,
            countingOut.getByteCount,
            file.modified,
            file.attributes,
            compResult.encoded,
            encResult.encoded,
            hash)
          index.writeFile(parentIndex, prototype)
        }
      })
    listenOut
  }

  /**
   * Given the result of a CompressionProvider and an EncryptionProvider, figures out what the name of the file
   * should be. This is the same as the IndexFile.storedName.
   */
  def getEncodedName(origName: String, compResult: CompressResult, encResult: EncryptResult): String =
    if (encResult.encoded) encResult.newName
    else if (compResult.encoded) compResult.newName
    else origName
}