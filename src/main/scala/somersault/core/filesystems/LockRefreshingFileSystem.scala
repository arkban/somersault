package somersault.core.filesystems

import java.io.OutputStream
import somersault.core.encode.{EncryptResult, CompressResult}

/**
 * A wrapper that will periodically refresh the lock managed by {@link LockCheckingFileSystem}. This prevents the
 * lock from getting stale. Without this the lock will expire, see {@link LockingFileSystem#Options#lockTimeout}.
 */
class LockRefreshingFileSystem(underlying: LockCheckingFileSystem, data: PeriodicFileSystemEventControllerData)
  extends ProxyFileSystem(underlying) {

  private val support = new LockRefreshSupport(underlying, data)

  override def write(
    parent: SomerDirectory, file: SomerFile, compResult: CompressResult, encResult: EncryptResult): OutputStream = {
    support.update(file)
    underlying.write(parent, file, compResult, encResult)
  }

  override def createDir(parent: SomerDirectory, name: String): SomerDirectory = {
    val dir = underlying.createDir(parent, name)
    support.update(dir)
    dir
  }

  override def remove(a: SomerArtifact) {
    underlying.remove(a)
    support.update(a)
  }
}
