package somersault.core.filesystems.local

import java.io.File
import java.lang.String
import net.jcip.annotations.NotThreadSafe
import nu.xom.Element
import org.apache.commons.lang.IllegalClassException
import org.joda.time.DateTime
import somersault.core.encode._
import somersault.core.filesystems._
import somersault.util.HashInfo
import somersault.util.persist.{PersisterContext, Persister}

/**
 * Exposes a local hard drive as a FileSystem.
 *
 * This implementation makes use of Apache Commons IO to handle a lot of the detail work.
 */
@NotThreadSafe
class LocalDirectFileSystem private(
  val data: LocalDirectFileSystemData, val runtimeData: RuntimeFileSystemData) extends FileSystem {

  runtimeData.observerContainer.logger.info(
    "LocalDirectFileSystem created for: %1$s", data.rootFile.getAbsolutePath)

  private val support = new LocalFileSystemSupporter(data, runtimeData)

  override def getIndexArtifact(a: SomerArtifact) = None

  override def disconnect() {}

  def remove(artifact: SomerArtifact) {
    artifact match {
      case a: LocalArtifact => support.remove(a)
      case _ => throw new IllegalClassException(classOf[LocalArtifact], artifact)
    }
  }

  def createDir(parent: SomerDirectory, name: String) =
    parent match {
      case p: LocalDirectory => support.createDir(p, name)
      case _ => throw new IllegalClassException(classOf[LocalDirectory], parent)
    }

  def write(parent: SomerDirectory, file: SomerFile, compResult: CompressResult, encResult: EncryptResult) =
    parent match {
      case p: LocalDirectory => support.write(p, file.name, file.modified)
      case _ => throw new IllegalClassException(classOf[LocalDirectory], parent)
    }

  def read(f: SomerFile) = f match {
    case localFile: LocalFile => support.read(localFile)
    case _ => throw new IllegalClassException(classOf[LocalFile], f)
  }

  def children(parent: SomerDirectory) =
    parent match {
      case p: LocalDirectory => support.children(p)
      case _ => throw new IllegalClassException(classOf[LocalDirectory], parent)
    }

  def find(parent: SomerDirectory, name:String) =
    parent match {
      case p: LocalDirectory => support.find(p, name)
      case _ => throw new IllegalClassException(classOf[LocalDirectory], parent)
    }

  def root = support.root

  def reconcile(obs: ReconcileObserver) {}

  protected[filesystems] def putLock(lockName: String, timestamp: DateTime) { support.putLock(lockName, timestamp); }

  protected[filesystems] def removeLock(lockName: String) { support.removeLock(lockName); }

  protected[filesystems] def findExistingLocks() = support.findExistingLocks(generateLockNameRegex())
}

object LocalDirectFileSystem extends FileSystemProvider {

  def connect(data: FileSystemData, runtimeData: RuntimeFileSystemData): LocalDirectFileSystem =
    data match {
      case d: LocalDirectFileSystemData =>
        if (!d.rootFile.exists)
          throw new IllegalArgumentException("root file does not exist: " + d.rootFile.getAbsolutePath)
        if (d.rootFile.isFile)
          throw new IllegalArgumentException("root file is not a directory: " + d.rootFile.getAbsolutePath)
        new LocalDirectFileSystem(d, runtimeData)
      case _ => throw new IllegalClassException(classOf[LocalFileSystemData], data)
    }
}

class LocalDirectFileSystemData(
  name: String, hashInfo: HashInfo, rootFile: File)
  extends LocalFileSystemData(
    name, hashInfo, new NullCompressionProvider, new NullEncryptionProvider, LockRefreshSupport.defaults, rootFile) {

  val isIndexed = false
}

class LocalDirectFileSystemDataPersister extends FileSystemDataPersister(false) {

  def canRead(context: PersisterContext, element: Element): Boolean = checkClass(
    element, classOf[LocalDirectFileSystemData])

  def read[T](context: PersisterContext, element: Element) = {
    val rootPath = new File(Persister.getChildElement(element, "rootPath").getValue)
    new LocalDirectFileSystemData(readName(element), readHashInfo(context, element), rootPath).asInstanceOf[T]
  }

  override def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean) {
    obj match {
      case data: LocalDirectFileSystemData =>
        super.write(context, data, element, dynamic)
        Persister.createElement("rootPath", data.rootFile.getAbsolutePath, element)
      case _ => throw new IllegalArgumentException("cannot write: " + obj.getClass.getName)
    }
  }
}
