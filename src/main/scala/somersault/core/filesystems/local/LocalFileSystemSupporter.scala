package somersault.core.filesystems.local

import java.io._
import java.lang.{String, IllegalArgumentException}
import java.util.regex.Pattern
import net.jcip.annotations.{Immutable, ThreadSafe, NotThreadSafe}
import org.apache.commons.io.FileUtils
import org.joda.time.DateTime
import scala.Some
import scala.io.Source
import somersault.core.encode.{CompressionProvider, EncryptionProvider}
import somersault.core.exceptions.{CannotReadFileException, PathDoesNotExistException, CannotWriteFileException, CannotCreateDirException, AlreadyExistsException}
import somersault.core.filesystems._
import somersault.util.{Repeater, FilenameUtils2, HashInfo, Hash, ObservableOutputStreamListener, ObservableOutputStream}

/**
 * A support class that contains functionality for building a FileSystem implementation for a local FileSystem with and
 * without an Index. The method here do not conform to FileSystem.
 */
@NotThreadSafe
class LocalFileSystemSupporter(val data: LocalFileSystemData, val runtimeData: RuntimeFileSystemData) {

  val root: LocalDirectory = new LocalFileSystemSupporter.RootDirectory(this, data)

  def remove(artifact: LocalArtifact) {
    checkOwner(artifact)
    FileUtils.forceDelete(artifact.file)
  }

  def createDir(parent: LocalDirectory, name: String): SomerDirectory = {
    checkOwner(parent)
    val dir = new File(parent.file, name)
    if (dir.exists)
      throw new AlreadyExistsException(parent.getFilePath ::: name :: Nil)
    if (!dir.mkdir)
      throw new CannotCreateDirException(parent.getFilePath ::: name :: Nil)
    new LocalFileSystemSupporter.LocalDirectoryImpl(this, new Some(parent), name)
  }

  def write(parent: LocalDirectory, name: String, modified: DateTime): OutputStream = {
    checkOwner(parent)
    val file = new File(parent.file, name)
    if (file.exists && file.isDirectory)
      throw new AlreadyExistsException(parent.getFilePath ::: name :: Nil)
    if (file.exists && !file.canWrite)
      throw new CannotWriteFileException(parent.getFilePath ::: name :: Nil)

    val listenOut = new ObservableOutputStream(new FileOutputStream(file), Some(file.length))
    listenOut.addListener(
      new ObservableOutputStreamListener {
        override def close(los: ObservableOutputStream) { file.setLastModified(modified.getMillis) }
      })
    listenOut
  }

  def read(file: LocalFile): InputStream = {
    checkOwner(file)
    if (!file.file.exists)
      throw new PathDoesNotExistException(file.getFilePath)
    if (file.file.isDirectory || !file.file.canRead)
      throw new CannotReadFileException(file.getFilePath)
    new FileInputStream(file.file)
  }

  def children(parent: LocalDirectory): Iterable[LocalArtifact] = {
    checkOwner(parent)
    if (!parent.file.exists)
      throw new PathDoesNotExistException(parent.getFilePath, null)

    wrapFiles(parent, parent.file.listFiles.toIterable)
  }

  def find(parent: LocalDirectory, name: String): Option[LocalArtifact] = {
    checkOwner(parent)
    if (!parent.file.exists)
      throw new PathDoesNotExistException(parent.getFilePath, null)

    wrapFiles(parent, Seq(new File(parent.file, name))).headOption
  }

  /**
   * Wraps the provided Files (under the parent) with LocalArtifact instances.
   */
  private def wrapFiles(parent: LocalDirectory, files: Iterable[File]): Iterable[LocalArtifact] = {
    files
      .filter(f => f.exists()) // sanity check #1 -- may eliminate "weird" file types
      .filter(f => f.isFile || f.isDirectory) // sanity check #2 -- may eliminate "weird" file types
      // NOTE: isSymlink() does not seem to work consistently
      .filter(f => !FileUtils.isSymlink(f))
      .map {
      case f =>
        if (f.isDirectory) new LocalFileSystemSupporter.LocalDirectoryImpl(this, new Some(parent), f.getName)
        else new LocalFileSystemSupporter.LocalFileImpl(this, new Some(parent), f.getName)
    }
  }

  def putLock(lockName: String, timestamp: DateTime) {
    val lockFile = new File(root.file, lockName)

    val fn = () => {
      if (!lockFile.createNewFile())
        throw new IOException("Failed to create lock file: " + lockFile.getAbsolutePath)

      val timestampStr = timestamp.toString(LockingFileSystem.lockTimestampFormatter)
      val output = new OutputStreamWriter(new FileOutputStream(lockFile))
      try output.write(timestampStr)
      finally output.close()
    }

    new Repeater(fn, runtimeData.maxRepeatAttempts).repeat()
  }

  def removeLock(lockName: String) {
    val lockFile = new File(root.file, lockName)
    if (lockFile.exists) {
      // we rename before we delete, because it is more likely that rename will occur atomically then delete
      val nonLockFile = new File(root.file, lockName + ".delete")
      val fn = () => {
        val moved = lockFile.renameTo(nonLockFile)
        if (!moved || !FileUtils.deleteQuietly(nonLockFile))
          throw new IOException("Failed to delete lock file: " + lockFile.getAbsolutePath)
      }

      new Repeater(fn, runtimeData.maxRepeatAttempts).repeat()
    }
  }

  def findExistingLocks(pattern: Pattern): Map[String, DateTime] = {
    val filter = new FilenameFilter() {
      def accept(dir: File, file: String) = pattern.matcher(file).matches()
    }
    root.file.list(filter).foldLeft(Map.empty[String, DateTime]) {
      case (result, fileName) =>
        val timestampStr = Source.fromFile(new File(root.file, fileName)).getLines().mkString("")
        result + (fileName -> LockingFileSystem.lockTimestampFormatter.parseDateTime(timestampStr))
    }
  }

  private[filesystems] def checkOwner(artifact: LocalArtifact) {
    if (!(artifact.owner eq this))
      throw new IllegalArgumentException("artifact from a different FileSystems")
  }
}

object LocalFileSystemSupporter {

  class LocalDirectoryImpl(
    private[filesystems] val owner: LocalFileSystemSupporter,
    val parent: Option[LocalDirectory],
    val name: String)
    extends LocalDirectory

  class RootDirectory(owner: LocalFileSystemSupporter, data: LocalFileSystemData)
    extends LocalDirectoryImpl(owner, None, "") {

    override protected def getData = data
  }

  class LocalFileImpl(
    private[filesystems] val owner: LocalFileSystemSupporter,
    val parent: Option[LocalDirectory],
    val name: String)
    extends LocalFile {

    def modified = new DateTime(file.lastModified)

    def size = file.length

    def attributes = new FileAttributes(file.canExecute, file.canRead, file.canWrite)

    // cache the hash only if requested (this also makes it safe to override hash()
    private lazy val lazyHash = {
      val input = new FileInputStream(file)
      try {
        Hash.create(getData.hashInfo, input)
      }
      finally input.close()
    }

    override def hash: Hash = lazyHash
  }

}

/**
 * Base artifact for BaseLocalFileSystem
 */
@ThreadSafe
trait LocalArtifact extends SomerArtifact {

  lazy val file = new File(getData.rootFile, FilenameUtils2.concat(getFilePath))

  /**
   * The owning class of this for checking that only the FileSystem that created this instance can use this instance.
   */
  private[filesystems] def owner(): LocalFileSystemSupporter

  def parent(): Option[LocalDirectory]

  /**
   * Returns the LocalFileSystemData used to create the LocalFileSystem. The instance is only stored in the
   * RootDirectory, and this method recursively retrieves it from the RootDirectory.
   */
  protected def getData: LocalFileSystemData = parent() match {
    case Some(a) => a.getData
    case None => throw new Exception("no root found")
  }

  /**
   * Returns the path of this file as it is actually stored for the purposes of creating a java.io.File object.
   * This returns an empty list for the root.
   */
  def getFilePath: List[String] = parent() match {
    case Some(d) => d.getFilePath ::: name :: Nil
    case None => Nil
  }
}

trait LocalDirectory extends LocalArtifact with SomerDirectory

trait LocalFile extends LocalArtifact with SomerFile

/**
 * This class intentionally does not contain an FileSystemObserverContainer, all observation should be handled by the
 * users of this class.
 */
@Immutable
abstract class LocalFileSystemData(
  val name: String,
  val hashInfo: HashInfo,
  val compressionProvider: CompressionProvider,
  val encryptionProvider: EncryptionProvider,
  val lockRefreshSupportData: PeriodicFileSystemEventControllerData,
  val rootFile: File)
  extends FileSystemData
