package somersault.core.filesystems.local

import java.io.File
import java.lang.String
import logahawk.Severity
import net.jcip.annotations.NotThreadSafe
import nu.xom.Element
import org.apache.commons.io.FilenameUtils
import org.apache.commons.lang.IllegalClassException
import org.joda.time.DateTime
import scala.Some
import scala.collection.mutable
import somersault.core.encode.{EncryptionProvider, CompressionProvider, EncryptResult, CompressResult}
import somersault.core.filesystems._
import somersault.core.filesystems.index.{IndexArtifact, H2IndexData, IndexSupport, Index, IndexDirectoryPrototype, IndexDirectory, IndexFile, H2Index}
import somersault.util.persist.{PersisterContext, Persister}
import somersault.util.{ObservableOutputStreamListener, ObservableOutputStream, FilenameUtils2, H2DataAccessObject, HashInfo}

/**
 * A local FileSystem that uses an H2Index which is part of the FileSystem. The use of an index allows compression
 * and encryption.
 */
@NotThreadSafe
class LocalIndexedFileSystem private(
  val data: LocalIndexedFileSystemData,
  val runtimeData: RuntimeFileSystemData,
  private[filesystems] val index: H2Index)
  extends FileSystem {

  runtimeData.observerContainer.logger.info("LocalIndexedFileSystem created for: %1$s", data.rootFile.getAbsolutePath)

  private val support = new LocalFileSystemSupporter(data, runtimeData)

  private val indexSupport = new LocalIndexedIndexSupport(data.indexSupportData)

  private val rootArtifact: SomerDirectory = new LocalIndexedFileSystem.RootIndexDirectory(
    support, data, index.root)

  override def getIndexArtifact(a: SomerArtifact) = a match {
    case l: LocalIndexedFileSystem.LocalIndexArtifact => Some(l.index)
    case _ => throw new IllegalClassException(classOf[LocalIndexedFileSystem.LocalIndexArtifact], a)
  }

  def children(parent: SomerDirectory) =
    parent match {
      case p: LocalIndexedFileSystem.LocalIndexDirectory =>
        support.checkOwner(p)
        index.children(p.index).map {
          case f: IndexFile => new LocalIndexedFileSystem.LocalIndexFileImpl(support, new Some(p), f)
          case d: IndexDirectory => new LocalIndexedFileSystem.LocalIndexDirectoryImpl(support, new Some(p), d)
        }
      case _ => throw new IllegalClassException(classOf[LocalIndexedFileSystem.LocalIndexDirectory], parent)
    }

  def find(parent: SomerDirectory, name: String) =
    parent match {
      case p: LocalIndexedFileSystem.LocalIndexDirectory =>
        support.checkOwner(p)
        index.find(p.index, name).map {
          case f: IndexFile => new LocalIndexedFileSystem.LocalIndexFileImpl(support, new Some(p), f)
          case d: IndexDirectory => new LocalIndexedFileSystem.LocalIndexDirectoryImpl(support, new Some(p), d)
        }
      case _ => throw new IllegalClassException(classOf[LocalIndexedFileSystem.LocalIndexDirectory], parent)
    }

  def root: SomerDirectory = rootArtifact

  def read(file: SomerFile) =
    file match {
      case f: LocalIndexedFileSystem.LocalIndexFile =>
        support.checkOwner(f)
        FileSystemUtils.read(data, runtimeData, f.index, support.read(f))
      case _ => throw new IllegalClassException(classOf[LocalIndexedFileSystem.LocalIndexFile], file)
    }

  def write(parent: SomerDirectory, file: SomerFile, compResult: CompressResult, encResult: EncryptResult) =
    parent match {
      case p: LocalIndexedFileSystem.LocalIndexDirectory =>
        support.checkOwner(p)
        val storedName = FileSystemUtils.getEncodedName(file.name, compResult, encResult)
        val output = support.write(p, storedName, file.modified)
        val listenOut = FileSystemUtils.write(
          data, runtimeData, storedName, compResult, encResult, index, p.index, file, output)

        // on close of the file stream, we will flush the index
        listenOut.addListener(
          new ObservableOutputStreamListener {
            override def close(output: ObservableOutputStream) {
              indexSupport.update(file)
            }
          })
        listenOut
      case _ => throw new IllegalClassException(classOf[LocalIndexedFileSystem.LocalIndexDirectory], parent)
    }

  def createDir(parent: SomerDirectory, name: String) =
    parent match {
      case p: LocalIndexedFileSystem.LocalIndexDirectory =>
        support.checkOwner(p)

        val compResult = data.compressionProvider.encodeFile(name)
        val encResult = data.encryptionProvider.encodeFile(name)
        val newName = FileSystemUtils.getEncodedName(name, compResult, encResult)

        support.createDir(p, newName)
        val dir = index.createDir(p.index, new IndexDirectoryPrototype(name, newName))
        val newDir = new LocalIndexedFileSystem.LocalIndexDirectoryImpl(support, new Some(p), dir)
        indexSupport.update(newDir)

        newDir
      case _ => throw new IllegalClassException(classOf[LocalIndexedFileSystem.LocalIndexDirectory], parent)
    }

  def remove(artifact: SomerArtifact) {
    artifact match {
      case a: LocalIndexedFileSystem.LocalIndexArtifact =>
        support.checkOwner(a)
        index.remove(a.index)
        support.remove(a)
        indexSupport.update(a)
      case _ => throw new IllegalClassException(classOf[LocalIndexedFileSystem.LocalIndexArtifact], artifact)
    }
  }

  override def disconnect() {
    indexSupport.flushIndex()
    index.disconnect()
  }

  def reconcile(obs: ReconcileObserver) {
    obs.start()
    try {
      // The strategy for reconciliation is to walk the index and the support, and remove from either which ever does not
      // exist in both
      reconcile(obs, Nil, index, index.root, support, support.root)
    }
    catch {
      case ex: Exception => obs.error(Severity.ERROR, ex)
    }
    finally {
      obs.end()
    }
  }

  protected[filesystems] def putLock(lockName: String, timestamp: DateTime) { support.putLock(lockName, timestamp); }

  protected[filesystems] def removeLock(lockName: String) { support.removeLock(lockName); }

  protected[filesystems] def findExistingLocks() = support.findExistingLocks(generateLockNameRegex())

  private def reconcile(
    obs: ReconcileObserver,
    parentPath: List[String],
    index: Index,
    indexParentDir: IndexDirectory,
    real: LocalFileSystemSupporter,
    realParentDir: LocalDirectory) {

    // we will remove from this map as we find matches in the index. when we are done we will have everything not
    // referenced by the index that we need to delete.
    val realMap = real.children(realParentDir)
      .filterNot(_.name.startsWith(LocalIndexedFileSystem.indexFilenamePrefix))
      .foldLeft(new mutable.HashMap[String, LocalArtifact]) {case (map, a) => map += (a.name -> a)}

    index.children(indexParentDir).foreach(
      a => {
        try {
          a match {
            case indexFile: IndexFile => realMap.get(indexFile.storedName) match {
              case Some(_) => realMap -= indexFile.storedName
              case None =>
                obs.removeFromIndex(FilenameUtils2.concat(parentPath ::: indexFile.origName :: Nil))
                index.remove(indexFile)
                indexSupport.update(a)
            }
            case indexDir: IndexDirectory => realMap.get(indexDir.storedName) match {
              case Some(realDir: LocalDirectory) =>
                reconcile(obs, parentPath ::: realParentDir.name :: Nil, index, indexDir, real, realDir)
                realMap -= indexDir.storedName
              case None =>
                obs.removeFromIndex(FilenameUtils2.concat(parentPath ::: indexDir.origName :: Nil))
                index.remove(indexDir)
                indexSupport.update(a)
              case x => throw new Exception("Unexpected response: " + x)
            }
          }
        }
        catch {
          case ex: Exception => obs.error(Severity.ERROR, ex)
        }
      })

    realMap.values.foreach(
      a => {
        try {
          obs.removeFromFileSystem(FilenameUtils2.concat(parentPath ::: a.name :: Nil))
          real.remove(a)
          indexSupport.update(a)
        }
        catch {
          case ex: Exception => obs.error(Severity.ERROR, ex)
        }
      })
  }

  private class LocalIndexedIndexSupport(indexData: PeriodicFileSystemEventControllerData)
    extends IndexSupport(indexData) {

    def flushIndex() {
      index.flush()
      reset()
    }
  }

}

object LocalIndexedFileSystem extends FileSystemProvider {

  private[filesystems] val indexFilenamePrefix = "somersault.index"

  private[filesystems] val indexFilename = "somersault.index" + H2DataAccessObject.databaseFileExt

  def connect(fsData: FileSystemData, runtimeData: RuntimeFileSystemData): LocalIndexedFileSystem =
    fsData match {
      case data: LocalIndexedFileSystemData =>
        val indexPath = FilenameUtils.concat(data.rootFile.getAbsolutePath, LocalIndexedFileSystem.indexFilename)
        val index = new H2Index(new H2IndexData(data, runtimeData, new File(indexPath)))
        new LocalIndexedFileSystem(data, runtimeData, index)
      case _ => throw new IllegalClassException(classOf[LocalIndexedFileSystemData], fsData)
    }

  /**
   * Returns a Filter that can be used to match all files created by the Index.
   */
  private[filesystems] trait LocalIndexArtifact extends LocalArtifact {

    def index: IndexArtifact

    /**
     * Override so that we used the storedName instead of the originalName for building File objects.
     */
    override def getFilePath: List[String] = {
      parent() match {
        case Some(d: LocalIndexDirectory) => d.getFilePath ::: index.storedName :: Nil
        case None => Nil
        case x => throw new Exception("Unexpected response: " + x)
      }
    }
  }

  private trait LocalIndexDirectory extends LocalIndexArtifact with LocalDirectory {

    def index: IndexDirectory
  }

  private trait LocalIndexFile extends LocalIndexArtifact with LocalFile {

    def index: IndexFile
  }

  private class LocalIndexDirectoryImpl(
    owner: LocalFileSystemSupporter, parent: Option[LocalDirectory], val index: IndexDirectory)
    extends LocalFileSystemSupporter.LocalDirectoryImpl(owner, parent, index.origName) with LocalIndexDirectory

  private class RootIndexDirectory(
    owner: LocalFileSystemSupporter, data: LocalFileSystemData, val index: IndexDirectory)
    extends LocalFileSystemSupporter.RootDirectory(owner, data) with LocalIndexDirectory

  private class LocalIndexFileImpl(
    owner: LocalFileSystemSupporter, parent: Option[LocalDirectory], val index: IndexFile)
    extends LocalFileSystemSupporter.LocalFileImpl(owner, parent, index.origName) with LocalIndexFile {

    override def modified = index.modified

    override def size = index.size

    override def attributes = index.attributes

    override def hash = index.hash
  }

}

class LocalIndexedFileSystemData(
  name: String,
  hashInfo: HashInfo,
  compressionProvider: CompressionProvider,
  encryptionProvider: EncryptionProvider,
  lockRefreshSupportData: PeriodicFileSystemEventControllerData,
  val indexSupportData: PeriodicFileSystemEventControllerData,
  rootFile: File)
  extends LocalFileSystemData(
    name,
    hashInfo,
    compressionProvider,
    encryptionProvider,
    lockRefreshSupportData,
    rootFile) {

  val isIndexed = true
}

class LocalIndexedFileSystemDataPersister extends FileSystemDataPersister(true) {

  def canRead(context: PersisterContext, element: Element) = checkClass(
    element, classOf[LocalIndexedFileSystemData])

  def read[T](context: PersisterContext, element: Element) = {
    val indexSupportData = new PeriodicFileSystemEventControllerDataPersister()
      .read(context, Persister.getChildElement(element, "indexSupportData"))
      .asInstanceOf[PeriodicFileSystemEventControllerData]

    val rootPath = new File(Persister.getChildElement(element, "rootPath").getValue)
    new LocalIndexedFileSystemData(
      readName(element),
      readHashInfo(context, element),
      readCompProvider(context, element),
      readEncProvider(context, element),
      readLockRefreshSupportData(context, element),
      indexSupportData,
      rootPath).asInstanceOf[T]
  }

  override def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean) {
    obj match {

      case data: LocalIndexedFileSystemData =>
        super.write(context, data, element, dynamic)
        Persister.createElement("rootPath", data.rootFile.getAbsolutePath, element)
        context.container.write(
          context,
          data.indexSupportData,
          Persister.createElement("indexSupportData", parent = element),
          dynamic = true)

      case _ => throw new IllegalArgumentException("cannot write: " + obj.getClass.getName)
    }
  }
}
