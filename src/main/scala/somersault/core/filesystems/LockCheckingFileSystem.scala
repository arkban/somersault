package somersault.core.filesystems

import java.util.concurrent.atomic.AtomicReference
import net.jcip.annotations.ThreadSafe
import somersault.core.encode.{EncryptResult, CompressResult}

/**
 * A wrapper that ensures the locks are properly set before interacting with the underling {@link FileSystem}. The
 * lock methods on the {@link FileSystem} interface are more focused on preventing multiple {@link FileSystem}s from
 * interacting. This wrapper is focused on ensuring that a {@link FileSystem} has the proper locks.
 *
 * This implementation will also call unlock() before disconnect() on the underlying {@link FileSystem}.
 */
@ThreadSafe
class LockCheckingFileSystem(underlying: FileSystem) extends ProxyFileSystem(underlying) {

  private val lockState = new AtomicReference(new LockCheckingFileSystem.LockState(false, LockingFileSystemMode.Read))

  def currentLock: LockCheckingFileSystem.LockState = lockState.get()

  override def disconnect() {
    unlock()
    underlying.disconnect()
  }

  override def remove(a: SomerArtifact) {
    lockState.get.check(LockingFileSystemMode.Write)

    underlying.remove(a)
  }

  override def createDir(parent: SomerDirectory, name: String) = {
    lockState.get.check(LockingFileSystemMode.Write)

    underlying.createDir(parent, name)
  }

  override def write(parent: SomerDirectory, file: SomerFile, compResult: CompressResult, encResult: EncryptResult) = {
    lockState.get.check(LockingFileSystemMode.Write)

    underlying.write(parent, file, compResult, encResult)
  }

  override def read(f: SomerFile) = {
    lockState.get.check(LockingFileSystemMode.Read)

    underlying.read(f)
  }

  override def children(parent: SomerDirectory) = {
    lockState.get.check(LockingFileSystemMode.Read)

    underlying.children(parent)
  }

  override def find(parent: SomerDirectory, name: String) = {
    lockState.get.check(LockingFileSystemMode.Read)

    underlying.find(parent, name)
  }

  override def root = {
    lockState.get.check(LockingFileSystemMode.Read)

    underlying.root
  }

  override def reconcile(obs: ReconcileObserver) {
    lockState.get.check(LockingFileSystemMode.Write)

    underlying.reconcile(obs)
  }

  override def lock(mode: LockingFileSystemMode.Mode) = {
    val result = underlying.lock(mode)
    if (result) lockState.set(new LockCheckingFileSystem.LockState(true, mode))
    result
  }

  override def unlock() {
    underlying.unlock()
    lockState.set(new LockCheckingFileSystem.LockState(false, LockingFileSystemMode.Read))
  }

  override def removeAllLocks() {
    underlying.removeAllLocks()
    lockState.set(new LockCheckingFileSystem.LockState(false, LockingFileSystemMode.Read))
  }
}

object LockCheckingFileSystem {

  /**
   * Records the current locking state.
   */
  class LockState(val locked: Boolean, val mode: LockingFileSystemMode.Mode) {

    /**
     * Throws an {@link IllegalStateException} if the requested action is not allowed according to the current state.
     */
    def check(required: LockingFileSystemMode.Mode) {
      val allowed = required match {
        case LockingFileSystemMode.Read =>
          locked && (mode == LockingFileSystemMode.Read || mode == LockingFileSystemMode.Write)
        case LockingFileSystemMode.Write =>
          locked && mode == LockingFileSystemMode.Write
      }
      if (!allowed)
        throw new IllegalStateException("invalid lock")
    }
  }

}