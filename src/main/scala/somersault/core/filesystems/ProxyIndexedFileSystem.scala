package somersault.core.filesystems

/**
 * A handy base class for {@link IndexedFileSystem} wrappers that forwards all methods to the underlying class. With this
 * one can override only the methods that you need to.
 */
abstract class ProxyIndexedFileSystem(underlying: IndexedFileSystem) extends IndexedFileSystem {

  def getIndexArtifact(a: SomerArtifact) = underlying.getIndexArtifact(a)
}
