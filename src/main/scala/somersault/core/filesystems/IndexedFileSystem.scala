package somersault.core.filesystems

import index.IndexArtifact

/**
 * Defines various methods for dealing with the {@link Index}. This is for both kinds of {@link FileSystem}s -- those
 * that use an {@link Index} and those that don't.
 */
trait IndexedFileSystem
{
  /**
   * Returns the {@link IndexArtifact} associated with the provided {@link SomerArtifact}, allowing the caller to
   * learn more about the {@link SomerArtifact}.
   *
   * This may seem like an odd design, that the more straightforward design would be to simply expose a "getIndex"
   * method on the {@link SomerArtifact}. This does not follow that design to prevent forcing implementers of
   * {@link SomerArtifact} to maintain a perhaps unwanted referenced to the {@link IndexArtifact}.
   *
   * The {@link IndexArtifact} is only really needed for interacting with the {@link Index}. One can implement
   * a lighter {@link SomerArtifact} implementation by transferring only the data needed from the
   * {@link IndexArtifact} to the {@link SomerArtifact}.
   *
   * It is possible that all implementations of this method simply expose the {@link IndexArtifact} stored on
   * the {@link SomerArtifact}, but I did not want to force that design on any {@link FileSystem} implementation.
   *
   * This should return a non-{@link None} result if {@link FileSystemData#isIndexed()} returns true.
   */
  def getIndexArtifact( a: SomerArtifact): Option[ IndexArtifact ]
}