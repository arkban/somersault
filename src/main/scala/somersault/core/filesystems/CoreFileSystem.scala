package somersault.core.filesystems

/**
 * The core definition of a {@link FileSystem}. This is the top of the {@link FileSystem} hierarchy.
 *
 * This interface was extracted from {@link FileSystem} as it began to extends with other {@link FileSystem}-related
 * interfaces.
 */
trait CoreFileSystem
{
  /**
   * Exposes the FileSystemData used by this FileSystem.
   */
  def data: FileSystemData

  /**
   * Exposes the {@link RuntimeFileSystemData} used by this {@link FileSystem}.
   */
  def runtimeData: RuntimeFileSystemData
}