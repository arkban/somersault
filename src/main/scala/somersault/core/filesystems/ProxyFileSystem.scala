package somersault.core.filesystems

import org.joda.time.DateTime
import somersault.core.encode.{EncryptResult, CompressResult}

/**
 * A handy base class for {@link FileSystem} wrappers that forwards all methods to the underlying class. With this
 * one can override only the methods that you need to.
 */
abstract class ProxyFileSystem(underlying: FileSystem) extends FileSystem {

  def data = underlying.data

  def runtimeData = underlying.runtimeData

  def disconnect() { underlying.disconnect() }

  def root = underlying.root

  def children(parent: SomerDirectory) = underlying.children(parent)

  def find(parent: SomerDirectory, name: String) = underlying.find(parent, name)

  def read(f: SomerFile) = underlying.read(f)

  def write(parent: SomerDirectory, file: SomerFile, compResult: CompressResult, encResult: EncryptResult) =
    underlying.write(parent, file, compResult, encResult)

  def createDir(parent: SomerDirectory, name: String) = underlying.createDir(parent, name)

  def remove(a: SomerArtifact) { underlying.remove(a) }

  def reconcile(obs: ReconcileObserver) { underlying.reconcile(obs) }

  def getIndexArtifact(a: SomerArtifact) = underlying.getIndexArtifact(a)

  protected[filesystems] def putLock(lockName: String, timestamp: DateTime) { underlying.putLock(lockName, timestamp) }

  protected[filesystems] def removeLock(lockName: String) { underlying.removeLock(lockName) }

  protected[filesystems] def findExistingLocks() = underlying.findExistingLocks()
}
