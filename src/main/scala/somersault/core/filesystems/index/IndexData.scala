package somersault.core.filesystems.index

import logahawk.Logger
import somersault.core.encode.{EncryptionProvider, CompressionProvider}
import net.jcip.annotations.Immutable
import somersault.util.HashInfo

/**
 * Very similar to FileSystemData, but provides data for Indexes.
 */
@Immutable
trait IndexData
{
  def hashInfo: HashInfo

  def compressionProvider( ): CompressionProvider

  def encryptionProvider( ): EncryptionProvider

  /**
   * Logger to be used by the Index to reporting anything of importance.
   */
  def logger( ): Logger
}

