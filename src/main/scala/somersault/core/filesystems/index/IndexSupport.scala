package somersault.core.filesystems.index

import net.jcip.annotations.ThreadSafe
import somersault.core.filesystems._

/** A base class for building logic to manage the index for {@link IndexedFileSystem}s. */
@ThreadSafe
abstract class IndexSupport(val data: PeriodicFileSystemEventControllerData) extends PeriodicFileSystemEventController {

  protected def fire() {
    flushIndex()
    reset()
  }

  /**
   * Updates the index in the actual {@link FileSystem} immediately. This should flush the index file and as necessary
   * copy it to the file system.
   */
  def flushIndex()
}

object IndexSupport {

  /**
   * Default {@link PeriodicFileSystemEventControllerData} for when an Index should be transmitted or committed while
   * a FileSystem is being modified.
   *
   * When a FileSystem that uses an index is modified, the index should be periodically transferred so that the
   * FileSystem does not get too out-of-sync with the index. But the types of data that are pushed to the FileSystem
   * may vary widely: it might be lots of data in a few files or lots of files with little data.
   */
  val defaults = new PeriodicFileSystemEventControllerData(
    bytesMin = 10 * 1000000, bytesMax = 100 * 1000000, // 10 MB to 100 MB
    actionsMin = 10, actionsMax = 100,
    msMin = 1 * 60 * 1000, msMax = 15 * 60 * 1000 // 1 minute to 15 minutes
  )
}

