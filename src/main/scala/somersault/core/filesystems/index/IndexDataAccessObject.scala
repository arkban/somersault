package somersault.core.filesystems.index

import javax.sql.DataSource
import somersault.core.encode.{EncryptionKey, CompressionProvider}
import somersault.util.{Hash, Base32, HashInfo, MetaDataAccessObject}

/**
 * Defines a DAO for an Index, to store the contents of HashInfo in the index for verification on re-opening that the
 * same hash wil be used. (Storing data with HashInfo X and then using HashInfo Y to read another FileSystem is going
 * to give useless comparison results.
 *
 * It is not required to derive from this class, but all the functionality in this class should be replicated in all
 * Index implementations.
 */
trait IndexDataAccessObject extends MetaDataAccessObject {

  private val keyHashName = "HASH_NAME"
  private val keyHashSize = "HASH_SIZE"
  private val keyCompressName = "COMPRESS_NAME"
  private val keyEncryptHash = "ENCRYPT_HASH"

  /**
   * The expected HashInfo of the DAO.
   */
  def schemaHashInfo: HashInfo

  def schemaCompressionProvider: CompressionProvider

  def schemaEncryptionProviderKey: EncryptionKey

  /**
   * Writes the expected schemaVersion.
   */
  override def createSchema(dataSource: DataSource) {
    super.createSchema(dataSource)

    writeHashInfo(dataSource, schemaHashInfo)
    writeCompressionProviderName(dataSource, schemaCompressionProvider.name)
    writeEncryptionProviderKeyHash(dataSource, schemaEncryptionProviderKey)
  }

  /**
   * Reads the schemaVersion currently stored in the schema.
   */
  def readHashInfo(dataSource: DataSource): HashInfo =
    new HashInfo(get(dataSource, keyHashName), Integer.parseInt(get(dataSource, keyHashSize)))

  /**
   * Writes the provided schemaVersion to the database
   */
  def writeHashInfo(dataSource: DataSource, hashInfo: HashInfo) {
    set(dataSource, keyHashName, hashInfo.name)
    set(dataSource, keyHashSize, Integer.toString(hashInfo.size))
  }

  def readCompressionProviderName(dataSource: DataSource): String = get(dataSource, keyCompressName)

  def writeCompressionProviderName(dataSource: DataSource, name: String) = set(dataSource, keyCompressName, name)

  /**
   * Returns the previously stored EncryptionKey. The stored key is not the same as the original key, as
   * the stored key is modified using generateStoreableKey(). To be clear, this encryption key CANNOT be used to decrypt
   * any data.
   */
  def readEncryptionProviderKeyHash(dataSource: DataSource): Hash =
    new Hash(schemaHashInfo, Base32.decode(get(dataSource, keyEncryptHash)))

  /**
   * Stores the cipher algorithm, key algorithm, and user key from calling generateStoreableKey() on the provided key.
   */
  def writeEncryptionProviderKeyHash(dataSource: DataSource, key: EncryptionKey) {
    set(dataSource, keyEncryptHash, Base32.encode(key.hashCodeStrong(schemaHashInfo).toBytes))
  }
}