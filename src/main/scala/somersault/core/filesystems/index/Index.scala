package somersault.core.filesystems.index

import net.jcip.annotations.{Immutable, NotThreadSafe}
import org.joda.time.DateTime
import somersault.core.filesystems.FileAttributes
import somersault.util.Hash

/**
 * An Index records information about SomerArtifacts stored in a FileSystem, but it records the information separately
 * from the native FileSystem. When an Index is paired with a FileSystem, more information can be recorded, and the
 * Index can be queried instead of the FileSystem. This is ideal when querying the FileSystem is slow and/or expensive,
 * or when the FileSystem's natural storage mechanisms are limited.
 *
 * Usually the Index is stored as a special file on the FileSystem, and can usually be copied from the FileSystem to
 * the local machine on connect(), and then all queries can reference the Index instead of the FileSystem. FileSystems
 * that can use Indexes implement IndexedFileSystem.
 *
 * The Index generally mirrors the functionality provided by a FileSystem to make using the index in place of the
 * FileSystem easier.
 */
@NotThreadSafe
trait Index {

  /**
   * Returns the root directory.
   */
  def root: IndexDirectory

  /**
   * Returns all children of the provided parent.
   *
   * @throws PathDoesNotExistException If the parent does not exist.
   */
  def children(parent: IndexDirectory): Iterable[IndexArtifact]

  /**
   * Returns all children of the provided parent.
   *
   * @throws PathDoesNotExistException If the parent does not exist.
   */
  def find(parent: IndexDirectory, name: String): Option[IndexArtifact]

  /**
   * Removes the provided artifact, and all children artifacts, recursive. If the artifact does not exist, nothing
   * should happen.
   *
   * @throws PathDoesNotExistException If the parent does not exist.
   */
  def remove(a: IndexArtifact)

  /**
   * Creates a new directory based on the prototype.
   *
   * @throws PathDoesNotExistException If the parent does not exist.
   * @throws DirExistsWhereFileExpectedIndexException If a directory exists with the same name.
   * @throws DirectoryAlreadyExistsException If a directory already exists with the same original name.
   */
  def createDir(parent: IndexDirectory, pt: IndexDirectoryPrototype): IndexDirectory

  /**
   * Creates a new file based on the prototype.
   *
   * @throws PathDoesNotExistException If the parent does not exist.
   * @throws FileExistsWhereDirExpectedIndexException If a file exists with the same name.
   * @throws IllegalArgumentException If a file already exists with the same original name.
   */
  def writeFile(parent: IndexDirectory, pt: IndexFilePrototype): IndexFile
}

trait IndexArtifact {

  /**
   * The true, original name of the artifact. The actual artifact's name inside the FileSystem may be different due to
   * encryption or FileSystem limitations, and this should be represented by the storedName. This method is the original
   * name, the one that should be used for SomerArtifact.name().
   */
  def origName: String

  /**
   * The name of the artifact as it exists inside the FileSystem that owns this index. This name may be different from
   * origName due to limitations in the fileSystem or encryption.
   *
   * Note that the storedName does not need to be hierarchical. The storedName might be the entire path and not resemble
   * the original path at all for purposes such as obfuscating the path when stored. (How this is achieved is left up
   * to the implementation.)
   */
  def storedName: String
}

trait IndexDirectory extends IndexArtifact

trait IndexFile extends IndexArtifact {

  def size: Long

  def modified: DateTime

  def attributes: FileAttributes

  /**
   * True if the file is compressed. The IndexData will identify which files are to be encrypted, and with what cipher
   * and key.
   */
  def encrypted: Boolean

  /**
   * True if the file is compressed. The IndexData will identify which files are to be compressed, and with what
   * algorithm.
   */
  def compressed: Boolean

  /**
   * A hash of the unencrypted, uncompressed contents of the file. This is stored to allow strong comparisons between
   * files. The type of hash is intentionally not specified here to allow different Index implementations to use
   * different hashes.
   */
  def hash: Hash
}

/**
 * A prototype of an IndexArtifact, intended solely to used as the base class for IndexDirectoryPrototype and
 * IndexFilePrototype.
 */
@Immutable
abstract class IndexArtifactPrototype(val origName: String, val storedName: String) extends IndexArtifact

/**
 * A prototype of an IndexDirectory for creating real IndexDirectories. This is to be supplied to Index.createDir().
 *
 * Derived implementations are forbidden because the FileSystem that is using an Index will be instantiating this class
 * directly, not via any indirection that would allow derived instances to be created.
 */
final class IndexDirectoryPrototype(origName: String, storedName: String)
  extends IndexArtifactPrototype(origName, storedName) with IndexDirectory

/**
 * A prototype of an IndexFile for creating real IndexFiles. This is to be supplied to Index.writeFile().
 *
 * Derived implementations are forbidden because the FileSystem that is using an Index will be instantiating this class
 * directly, not via any indirection that would allow derived instances to be created.
 */
final class IndexFilePrototype(
  origName: String,
  storedName: String,
  val size: Long,
  val modified: DateTime,
  val attributes: FileAttributes,
  val encrypted: Boolean,
  val compressed: Boolean,
  val hash: Hash)
  extends IndexArtifactPrototype(origName, storedName) with IndexFile

