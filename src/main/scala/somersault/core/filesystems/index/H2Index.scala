package somersault.core.filesystems.index

import java.io.File
import java.sql.{Timestamp, Statement, PreparedStatement, Connection, Types, ResultSet}
import javax.sql.DataSource
import logahawk.Logger
import net.jcip.annotations.NotThreadSafe
import org.apache.commons.lang.IllegalClassException
import org.joda.time.DateTime
import org.springframework.dao.DuplicateKeyException
import org.springframework.jdbc.`object`.MappingSqlQuery
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate
import org.springframework.jdbc.core.{RowMapper, JdbcTemplate, PreparedStatementCreator, SqlParameter}
import org.springframework.jdbc.support.GeneratedKeyHolder
import scala.collection.JavaConversions._
import scala.collection.mutable
import scala.{None, Option, Nil}
import somersault.core.encode.{EncryptionProviderMismatchException, CompressionProviderMismatchException, EncryptionProvider, CompressionProvider}
import somersault.core.exceptions.{UnknownException, PathDoesNotExistException, AlreadyExistsException}
import somersault.core.filesystems.{RuntimeFileSystemData, FileSystemData, FileAttributes}
import somersault.util.{HashInfoMismatchException, VersionMismatchException, Version, HashInfo, VersionedDataAccessObject, H2DataAccessObject, Hash}

/**
 * An Index implementation that stores its information inside an H2 database file.
 *
 * This can handle data stores that store their data hierarchically or flat.
 */
@NotThreadSafe
class H2Index(val data: H2IndexData) extends Index {

  private val dao = new DAO(data)

  private val rootFile = dao.getRoot

  def root: IndexDirectory = rootFile

  def children(parent: IndexDirectory): Iterable[IndexArtifact] =
    parent match {
      case p: H2IndexDirectory => dao.select(p)
      case _ => throw new IllegalClassException(classOf[H2IndexDirectory], parent)
    }

  def find(parent: IndexDirectory, name: String): Option[IndexArtifact] =
    parent match {
      case p: H2IndexDirectory => dao.select(p, name)
      case _ => throw new IllegalClassException(classOf[H2IndexDirectory], parent)
    }

  def remove(artifact: IndexArtifact) {
    if (artifact eq root)
      throw new IllegalArgumentException("cannot delete root")

    artifact match {
      case a: H2IndexArtifact => dao.remove(a)
      case _ => throw new IllegalClassException(classOf[H2IndexDirectory], artifact)
    }
  }

  def createDir(parent: IndexDirectory, pt: IndexDirectoryPrototype): IndexDirectory =
    parent match {
      case p: H2IndexDirectory => dao.createDir(Some(p.id), pt)
      case _ => throw new IllegalClassException(classOf[H2IndexDirectory], parent)
    }

  def writeFile(parent: IndexDirectory, pt: IndexFilePrototype): IndexFile =
    parent match {
      case p: H2IndexDirectory => dao.writeFile(p.id, pt)
      case _ => throw new IllegalClassException(classOf[H2IndexDirectory], parent)
    }

  /**
   * Disconnects from the index database file.
   */
  def disconnect() { dao.dispose() }

  /**
   * Finds the artifact with the provided stored name. This method is designed for flat FileSystems, where the
   * storedName is the entire key needed to find the artifact.
   */
  def select(storedName: String): Option[IndexArtifact] = dao.select(storedName)

  /**
   * Removes empty directories. This method exists for helping implement FileSystem.reconcile(), where a copy of the
   * index is inverted to keep track of what needs to be removed from the real FileSystem.
   */
  def pruneEmptyDirectories() { pruneEmptyDirectories(root) }

  def flush() { dao.flush() }

  /**
   * Prunes this directory and any empty sub-directories, recursively.
   *
   * @return True if parent is empty and was removed.
   */
  private def pruneEmptyDirectories(parent: IndexDirectory): Boolean = {
    // head-recursive implementation so we correctly delete the current directory if we make it empty
    val c = children(parent)
    var childCount = c.size // when reaches 0, directory is empty, and we can remove

    children(parent).foreach {
      case d: IndexDirectory =>
        val result = pruneEmptyDirectories(d)
        childCount -= (if (result) 1 else 0)
      case _ =>
    }

    if (childCount == 0 && parent != root) {
      remove(parent)
      true
    }
    else {
      false
    }
  }

  private abstract class H2IndexArtifact(val id: Long, val origName: String, val storedName: String)
    extends IndexArtifact

  private class H2IndexDirectory(id: Long, origName: String, storedName: String)
    extends H2IndexArtifact(id, origName, storedName) with IndexDirectory

  private class H2IndexFile(
    id: Long,
    origName: String,
    storedName: String,
    val size: Long,
    val modified: DateTime,
    val attributes: FileAttributes,
    override val encrypted: Boolean,
    override val compressed: Boolean,
    val hash: Hash)
    extends H2IndexArtifact(id, origName, storedName) with IndexFile

  /**
   * This acts as a DAO for the H2ActionContainerBuilder implementation.
   */
  private class DAO(data: H2IndexData)
    extends H2DataAccessObject with IndexDataAccessObject with VersionedDataAccessObject {

    private val tableArtifact: String = "ARTIFACT"

    private val tableFile: String = "FILE"

    private val colId: String = "ID"

    private val colParentId: String = "PARENT_ID"

    private val colType: String = "TYPE"

    // 'D' for directory, 'F' for file
    private val colOrigName: String = "ORIG_NAME"

    private val colStoredName: String = "STORED_NAME"

    private val colSize: String = "SIZE"

    private val colModified: String = "MODIFIED"

    private val colAttrExec: String = "ATTR_EXEC"

    private val colAttrRead: String = "ATTR_READ"

    private val colAttrWrite: String = "ATTR_WRITE"

    private val colCompressed: String = "COMPRESSED"

    private val colEncrypted: String = "ENCRYPTED"

    private val colHash: String = "HASH"

    /**
     * Provides a place to cache MappingSqlQuery instances.
     */
    private val mappingSqlQueryCache = new mutable.HashMap[String, MappingSqlQuery[H2IndexArtifact]]

    private var dataSource = createDataSource(data.file, deleteIfExists = false)

    def schemaVersion = new Version(0, 0, 1)

    def schemaHashInfo = data.hashInfo

    def schemaCompressionProvider = data.compressionProvider

    def schemaEncryptionProviderKey = data.encryptionProvider.key

    def dispose() { dispose(dataSource) }

    override def createSchema(dataSource: DataSource) {
      super.createSchema(dataSource)

      val statements = List(
        "CREATE TABLE " + tableArtifact + " ( "
          + colId + " IDENTITY, "
          + colParentId + " INT8 NULL, "
          + colType + " VARCHAR(1) CHECK ( " + colType + " IN ( 'F', 'D' ) ), "
          + colOrigName + " VARCHAR(256), "
          + colStoredName + " VARCHAR(256), "
          + "PRIMARY KEY (" + colId + "), "
          + "FOREIGN KEY (" + colParentId + ") REFERENCES (" + colId + "), "
          + "CONSTRAINT NO_DUP_ARTIFACT UNIQUE( " + colParentId + "," + colOrigName + ") )",
        "CREATE TABLE " + tableFile + " ( "
          + colId + " INT8, "
          + colSize + " INT8 CHECK ( " + colSize + " >= 0 ), "
          + colAttrExec + " BOOLEAN, "
          + colAttrRead + " BOOLEAN, "
          + colAttrWrite + " BOOLEAN, "
          + colModified + " TIMESTAMP, "
          + colCompressed + " BOOLEAN, "
          + colEncrypted + " BOOLEAN, "
          + colHash + " BINARY(" + data.hashInfo.size + "), "
          + "PRIMARY KEY (" + colId + "), "
          + "FOREIGN KEY (" + colId + ") REFERENCES " + tableFile + " (" + colId + ") )")

      val jdbc = new SimpleJdbcTemplate(dataSource)
      statements.foreach(jdbc.getJdbcOperations.update)
    }

    override def initialize(dataSource: DataSource) {
      super.initialize(dataSource)

      data.logger.info("Index opened: %1$s", data.file)

      if (readVersion(dataSource) != schemaVersion)
        throw new VersionMismatchException(schemaVersion, readVersion(dataSource))
      if (readHashInfo(dataSource) != schemaHashInfo)
        throw new HashInfoMismatchException(readHashInfo(dataSource), schemaHashInfo)
      if (readCompressionProviderName(dataSource) != schemaCompressionProvider.name)
        throw new CompressionProviderMismatchException(
          readCompressionProviderName(dataSource), schemaCompressionProvider.name)
      if (readEncryptionProviderKeyHash(dataSource) != schemaEncryptionProviderKey.hashCodeStrong(schemaHashInfo))
        throw new EncryptionProviderMismatchException
    }

    /**
     * Returns the root, either finding an existing root or creating a new one.
     */
    def getRoot: H2IndexDirectory = {
      val mapper = mappingSqlQueryCache.get("getRoot") match {
        case Some(m) => m
        case None => {
          val sql = "SELECT " + colId +
            ", " + colOrigName +
            ", " + colStoredName +
            " FROM " + tableArtifact +
            " WHERE " + colParentId + " IS NULL" +
            " AND " + colType + " = 'D'"

          val mapper = new MappingSqlQuery[H2IndexArtifact](dataSource, sql) {
            def mapRow(rs: ResultSet, rowNum: Int) =
              new H2IndexDirectory(rs.getLong(1), rs.getString(2), rs.getString(3))
          }
          mappingSqlQueryCache.put("getRoot", mapper)
          mapper
        }
      }

      val dirs = mapper.execute()
      if (dirs.size > 1)
        throw new UnknownException("Duplicate Root found", Nil, null)

      if (dirs.size == 0)
        createDir(None, new IndexDirectoryPrototype("", ""))
      else
        dirs.get(0).asInstanceOf[H2IndexDirectory]
    }

    /**
     * Create an H2IndexDirectory based on the provided prototype.
     *
     * @param parentId Should be None for the root only.
     *
     * @throws DuplicateKeyException If a directory already exists with the same origName and parent.
     */
    def createDir(parentId: Option[Long], pt: IndexDirectoryPrototype): H2IndexDirectory = {
      try {
        val id = insertArtifact(parentId, pt) // this may throw

        new H2IndexDirectory(id, pt.origName, pt.storedName)
      }
      catch {
        case ex: DuplicateKeyException =>
          throw new AlreadyExistsException(queryPath(parentId) ::: pt.origName :: Nil, ex)
      }
    }

    /**
     * Writes an H2IndexFile based on the provided prototype. This will overwrite any previous H2IndexFile that has
     * the same parent ID and origName.
     *
     * @throws DuplicateKeyException If a directory already exists with the same origName and parent.
     */
    def writeFile(parentId: Long, pt: IndexFilePrototype): H2IndexFile = {
      val selectIdSql =
        "SELECT " + colId +
          " FROM " + tableArtifact +
          " WHERE " + colParentId + " = ?" +
          " AND " + colOrigName + " = ?"

      val ids =
        new JdbcTemplate(dataSource).queryForList(
          selectIdSql, classOf[java.lang.Long], Predef.long2Long(parentId), pt.origName)
      if (ids.size > 1)
        throw new UnknownException("Duplicate ID found", pt.origName :: Nil, null)

      val id = if (ids.size == 0) this.insertFile(parentId, pt) else this.updateFile(ids(0).longValue, pt)

      new H2IndexFile(
        id, pt.origName, pt.storedName, pt.size, pt.modified, pt.attributes, pt.compressed, pt.encrypted, pt.hash)
    }

    /**
     * Inserts a new record into the Artifact table.
     *
     * @param parentId Should be None for the root only.
     * @return ID of the new record.     *
     * @throws DuplicateKeyException If a directory already exists with the same origName and parent.
     */
    private def insertArtifact(parentId: Option[Long], pt: IndexArtifactPrototype): Long = {
      val sql =
        "INSERT INTO " + tableArtifact +
          " ( " + colParentId +
          ", " + colType +
          ", " + colOrigName +
          ", " + colStoredName +
          " ) VALUES ( ?, ?, ?, ? )"

      val statementCreator = new PreparedStatementCreator {
        def createPreparedStatement(c: Connection): PreparedStatement = {
          val statement = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
          if (parentId.isDefined) statement.setLong(1, parentId.get) else statement.setNull(1, Types.BIGINT)
          pt match {
            case d: IndexDirectoryPrototype => statement.setString(2, "D")
            case f: IndexFilePrototype => statement.setString(2, "F")
          }
          statement.setString(3, pt.origName)
          statement.setString(4, pt.storedName)
          statement
        }
      }

      val keyHolder = new GeneratedKeyHolder
      new SimpleJdbcTemplate(dataSource).getJdbcOperations.update(statementCreator, keyHolder) // this may throw
      keyHolder.getKey.longValue
    }

    /**
     * Inserts a new file.
     *
     * @return The ID of the new file.
     * @throws AlreadyExistsException Thrown if a file exists with the same ID.
     */
    private def insertFile(parentId: Long, pt: IndexFilePrototype): Long = {
      try {
        val id = insertArtifact(new Some(parentId), pt) // this may throw
        val sql =
          "INSERT INTO " + tableFile +
            " ( " + colId +
            ", " + colSize +
            ", " + colModified +
            ", " + colAttrExec +
            ", " + colAttrRead +
            ", " + colAttrWrite +
            ", " + colCompressed +
            ", " + colEncrypted +
            ", " + colHash +
            " ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ? )"

        new SimpleJdbcTemplate(dataSource).update(
          sql,
          Predef.long2Long(id),
          Predef.long2Long(pt.size),
          new Timestamp(pt.modified.getMillis),
          Predef.boolean2Boolean(pt.attributes.executable),
          Predef.boolean2Boolean(pt.attributes.readable),
          Predef.boolean2Boolean(pt.attributes.writable),
          Predef.boolean2Boolean(pt.compressed),
          Predef.boolean2Boolean(pt.encrypted),
          pt.hash.toBytes)

        id
      }
      catch {
        case ex: DuplicateKeyException =>
          throw new AlreadyExistsException(queryPath(new Some(parentId)) ::: pt.origName :: Nil, ex)
      }
    }

    /**
     * Updates a file with the provided ID.
     *
     * @return The ID of the file (this helps the caller).
     */
    private def updateFile(id: Long, pt: IndexFilePrototype): Long = {
      val updateArtifactSql =
        "UPDATE " + tableArtifact +
          " SET " + colStoredName + " = ?" +
          " WHERE " + colId + " = ?"
      new SimpleJdbcTemplate(dataSource).update(updateArtifactSql, pt.storedName, Predef.long2Long(id))

      val updateFileSql =
        "UPDATE " + tableFile +
          " SET " + colSize + " = ?" +
          ", " + colModified + " = ?" +
          ", " + colAttrExec + " = ?" +
          ", " + colAttrRead + " = ?" +
          ", " + colAttrWrite + " = ?" +
          ", " + colCompressed + " = ?" +
          ", " + colEncrypted + " = ?" +
          ", " + colHash + " = ?" +
          " WHERE " + colId + " = ?"
      new SimpleJdbcTemplate(dataSource).update(
        updateFileSql,
        Predef.long2Long(pt.size),
        new Timestamp(pt.modified.getMillis),
        Predef.boolean2Boolean(pt.attributes.executable),
        Predef.boolean2Boolean(pt.attributes.readable),
        Predef.boolean2Boolean(pt.attributes.writable),
        Predef.boolean2Boolean(pt.compressed),
        Predef.boolean2Boolean(pt.encrypted),
        pt.hash.toBytes,
        Predef.long2Long(id))
      id
    }

    /**
     * Returns all child IndexArtifacts for the provided parent.
     *
     * @throws PathDoesNotExistException Thrown if the parent does not exist.
     */
    def select(parent: H2IndexDirectory): Iterable[H2IndexArtifact] = {
      // check for existence of specified parent
      val sql = "SELECT COUNT(" + colId + " ) FROM " + tableArtifact + " WHERE " + colId + " = ?"
      val result = new JdbcTemplate(dataSource).queryForInt(sql, Predef.long2Long(parent.id))
      if (result == 0)
        throw new PathDoesNotExistException(parent.origName :: Nil)
      if (result > 1)
        throw new UnknownException("Duplicate ID found", parent.origName :: Nil, null)

      selectDirs(parent) ++ selectFiles(parent)
    }

    /**
     * Returns the child IndexArtifacts for the provided parent and name.
     *
     * @throws PathDoesNotExistException Thrown if the parent does not exist.
     */
    def select(parent: H2IndexDirectory, name: String): Option[H2IndexArtifact] = {
      // check for existence of specified parent
      val sql = "SELECT COUNT(" + colId + " ) FROM " + tableArtifact + " WHERE " + colId + " = ?"
      val result = new JdbcTemplate(dataSource).queryForInt(sql, Predef.long2Long(parent.id))
      if (result == 0)
        throw new PathDoesNotExistException(parent.origName :: Nil)
      if (result > 1)
        throw new UnknownException("Duplicate ID found", parent.origName :: Nil, null)

      selectDirs(parent, name) match {
        case Some(x) => Some(x)
        case None => selectFiles(parent, name)
      }
    }

    /**
     * Returns either all child IndexDirectories for the provided parent.
     */
    private def selectDirs(parent: H2IndexDirectory): Iterable[H2IndexDirectory] = {
      val mapper = mappingSqlQueryCache.get("selectDirs") match {
        case Some(m) => m
        case None =>
          val sql =
            "SELECT " + colId +
              ", " + colOrigName +
              ", " + colStoredName +
              " FROM " + tableArtifact +
              " WHERE " + colParentId + " = ?" +
              " AND " + colType + " = 'D'" //+

          val mapper = new MappingSqlQuery[H2IndexArtifact](dataSource, sql) {
            def mapRow(rs: ResultSet, rowNum: Int) =
              new H2IndexDirectory(rs.getLong(1), rs.getString(2), rs.getString(3))
          }
          mapper.declareParameter(new SqlParameter(Types.BIGINT))

          mappingSqlQueryCache.put("selectDirs", mapper)
          mapper
      }

      val params = Array(Predef.long2Long(parent.id))
      mapper.execute(params: _ *).map(x => x.asInstanceOf[H2IndexDirectory])
    }

    /**
     * Returns either a single child for the provided parent,
     */
    private def selectDirs(parent: H2IndexDirectory, name: String): Option[H2IndexDirectory] = {

      val mapper = mappingSqlQueryCache.get("selectDirsWithName") match {
        case Some(m) => m
        case None =>
          val sql =
            "SELECT " + colId +
              ", " + colOrigName +
              ", " + colStoredName +
              " FROM " + tableArtifact +
              " WHERE " + colParentId + " = ?" +
              " AND " + colType + " = 'D'" +
              " AND " + colOrigName + " = ?"

          val mapper = new MappingSqlQuery[H2IndexArtifact](dataSource, sql) {
            def mapRow(rs: ResultSet, rowNum: Int) =
              new H2IndexDirectory(rs.getLong(1), rs.getString(2), rs.getString(3))
          }
          mapper.declareParameter(new SqlParameter(Types.BIGINT))
          mapper.declareParameter(new SqlParameter(Types.VARCHAR))

          mappingSqlQueryCache.put("selectDirsWithName", mapper)
          mapper
      }

      val params = Array(Predef.long2Long(parent.id), name)
      mapper.execute(params: _ *).map(x => x.asInstanceOf[H2IndexDirectory]).headOption
    }

    /**
     * Returns all child IndexFiles for the provided parent.
     */
    private def selectFiles(parent: H2IndexDirectory): Iterable[H2IndexFile] = {
      val mapper = mappingSqlQueryCache.get("selectFiles") match {
        case Some(m) => m
        case None =>
          val sql =
            "SELECT " + tableArtifact + "." + colId +
              ", " + tableArtifact + "." + colOrigName +
              ", " + tableArtifact + "." + colStoredName +
              ", " + tableFile + "." + colSize +
              ", " + tableFile + "." + colModified +
              ", " + tableFile + "." + colAttrExec +
              ", " + tableFile + "." + colAttrRead +
              ", " + tableFile + "." + colAttrWrite +
              ", " + tableFile + "." + colCompressed +
              ", " + tableFile + "." + colEncrypted +
              ", " + tableFile + "." + colHash +
              " FROM " + tableArtifact + ", " + tableFile +
              " WHERE " + tableArtifact + "." + colParentId + " = ?" +
              " AND " + tableArtifact + "." + colType + " = 'F'" +
              " AND " + tableArtifact + "." + colId + " = " + tableFile + "." + colId

          val mapper = new MappingSqlQuery[H2IndexArtifact](dataSource, sql) {
            def mapRow(rs: ResultSet, rowNum: Int) =
              new H2IndexFile(
                rs.getLong(1),
                rs.getString(2),
                rs.getString(3),
                rs.getLong(4),
                new DateTime(rs.getTimestamp(5)),
                new FileAttributes(rs.getBoolean(6), rs.getBoolean(7), rs.getBoolean(8)),
                rs.getBoolean(9),
                rs.getBoolean(10),
                new Hash(data.hashInfo, rs.getBytes(11)))
          }
          mapper.declareParameter(new SqlParameter(Types.BIGINT))

          mappingSqlQueryCache.put("selectFiles", mapper)
          mapper
      }

      val params = Array(Predef.long2Long(parent.id))
      mapper.execute(params: _ *).map(x => x.asInstanceOf[H2IndexFile])
    }

    /**
     * Returns a single child or all child IndexFiles for the provided parent.
     */
    private def selectFiles(parent: H2IndexDirectory, name: String): Option[H2IndexFile] = {
      val mapper = mappingSqlQueryCache.get("selectFilesWithName") match {
        case Some(m) => m
        case None =>
          val sql =
            "SELECT " + tableArtifact + "." + colId +
              ", " + tableArtifact + "." + colOrigName +
              ", " + tableArtifact + "." + colStoredName +
              ", " + tableFile + "." + colSize +
              ", " + tableFile + "." + colModified +
              ", " + tableFile + "." + colAttrExec +
              ", " + tableFile + "." + colAttrRead +
              ", " + tableFile + "." + colAttrWrite +
              ", " + tableFile + "." + colCompressed +
              ", " + tableFile + "." + colEncrypted +
              ", " + tableFile + "." + colHash +
              " FROM " + tableArtifact + ", " + tableFile +
              " WHERE " + tableArtifact + "." + colParentId + " = ?" +
              " AND " + tableArtifact + "." + colType + " = 'F'" +
              " AND " + tableArtifact + "." + colId + " = " + tableFile + "." + colId +
              " AND " + tableArtifact + "." + colOrigName + " = ?"

          val mapper = new MappingSqlQuery[H2IndexArtifact](dataSource, sql) {
            def mapRow(rs: ResultSet, rowNum: Int) =
              new H2IndexFile(
                rs.getLong(1),
                rs.getString(2),
                rs.getString(3),
                rs.getLong(4),
                new DateTime(rs.getTimestamp(5)),
                new FileAttributes(rs.getBoolean(6), rs.getBoolean(7), rs.getBoolean(8)),
                rs.getBoolean(9),
                rs.getBoolean(10),
                new Hash(data.hashInfo, rs.getBytes(11)))
          }
          mapper.declareParameter(new SqlParameter(Types.BIGINT))
          mapper.declareParameter(new SqlParameter(Types.VARCHAR))

          mappingSqlQueryCache.put("selectFilesWithName", mapper)
          mapper
      }

      val params = Array(Predef.long2Long(parent.id), name)
      mapper.execute(params: _ *).map(x => x.asInstanceOf[H2IndexFile]).headOption
    }

    /**
     * Finds the artifact with the provided stored name. This method is designed for flat FileSystems, where the
     * storedName is the entire key needed to find the artifact.
     */
    def select(storedName: String): Option[H2IndexArtifact] = {
      val sql =
        "SELECT " + tableArtifact + "." + colId +
          ", " + tableArtifact + "." + colOrigName +
          ", " + tableArtifact + "." + colType +
          ", " + tableFile + "." + colSize +
          ", " + tableFile + "." + colModified +
          ", " + tableFile + "." + colAttrExec +
          ", " + tableFile + "." + colAttrRead +
          ", " + tableFile + "." + colAttrWrite +
          ", " + tableFile + "." + colCompressed +
          ", " + tableFile + "." + colEncrypted +
          ", " + tableFile + "." + colHash +
          " FROM " + tableArtifact +
          " LEFT OUTER JOIN " + tableFile +
          " ON " + tableFile + "." + colId + " = " + tableArtifact + "." + colId +
          " WHERE " + tableArtifact + "." + colStoredName + " = ?"

      val mapper = new MappingSqlQuery[H2IndexArtifact](dataSource, sql) {
        def mapRow(rs: ResultSet, rowNum: Int) = {
          val artifactType = rs.getString(3)
          if (artifactType == "D") {
            new H2IndexDirectory(rs.getLong(1), rs.getString(2), storedName)
          }
          else if (artifactType == "F") {
            new H2IndexFile(
              rs.getLong(1),
              rs.getString(2),
              storedName,
              rs.getLong(4),
              new DateTime(rs.getTimestamp(5)),
              new FileAttributes(rs.getBoolean(6), rs.getBoolean(7), rs.getBoolean(8)),
              rs.getBoolean(9),
              rs.getBoolean(10),
              new Hash(data.hashInfo, rs.getBytes(11)))
          }
          else {
            throw new Exception("Type code " + rs.getString(3) + " not recognized")
          }
        }
      }

      mapper.declareParameter(new SqlParameter(Types.VARCHAR))

      mappingSqlQueryCache.put("select", mapper)
      mapper.findObject(storedName) match {
        case x: H2IndexArtifact => Some(x)
        case null => None
      }
    }

    /**
     * Removes the provided artifact, and any children.
     *
     * @throws PathDoesNotExistIndexException If no artifacts are removed.
     */
    def remove(artifact: H2IndexArtifact) {
      artifact match {
        case dir: H2IndexDirectory => removeChildren(dir)
        case _ =>
      }

      val sql = "DELETE FROM " + tableArtifact + " WHERE " + colId + " = ?"
      val deleted = new JdbcTemplate(dataSource).update(sql, Predef.long2Long(artifact.id))

      if (deleted == 0)
        throw new PathDoesNotExistException(artifact.origName :: Nil, null)
      if (deleted > 1)
        throw new UnknownException("Multiple deletes occurred", artifact.origName :: Nil, null)

      artifact match {
        case file: H2IndexFile =>
          val sqlFile = "DELETE FROM " + tableFile + " WHERE " + colId + " = ?"
          new JdbcTemplate(dataSource).update(sqlFile, Predef.long2Long(artifact.id))
        case _ =>
      }
    }

    /**
     * Removes the children a directory by taking advantage of the fact that we only need to run a couple of well
     * crafted delete queries to remove all children. This recursively deletes the contents of all child directories
     * first. This does not throw if no rows are removed.
     */
    private def removeChildren(parent: H2IndexDirectory) {
      selectDirs(parent).foreach(removeChildren)

      val sql = List(
        "DELETE FROM " + tableFile + " WHERE " + colId + " IN ("
          + "SELECT " + colId + " FROM " + tableArtifact + " WHERE " + colParentId + " = ? )",
        "DELETE FROM " + tableArtifact + " WHERE " + colParentId + " = ?")

      sql.foreach(s => new JdbcTemplate(dataSource).update(s, Predef.long2Long(parent.id)))
    }

    /**
     * Queries the database for the full path from the parentId back to the root. This method is slow -- it will make
     * multiple queries to build the path. This is intended only to be used for exceptions.
     */
    private def queryPath(parentId: Option[Long]): List[String] =
      parentId match {
        case Some(id) if id == rootFile.id => Nil
        case Some(id) => {
          val sql = "SELECT " + colParentId + ", " + colOrigName + " FROM " + tableArtifact + " WHERE " + colId + " = ?"
          val rowMapper = new RowMapper[(Option[Long], String)] {
            def mapRow(rs: ResultSet, rowNum: Int) =
              (if (rs.getObject(1) == null) None else Some(rs.getLong(1)), rs.getString(2))
          }
          val result = new JdbcTemplate(dataSource).queryForObject(sql, rowMapper, Predef.long2Long(id))
          queryPath(result._1) ::: result._2 :: Nil
        }
        case None => Nil
      }

    /**
     * Disconnects and re-connects to the database. This forces a flush of all data to the database.
     */
    def flush() {
      mappingSqlQueryCache.clear() // some of the cached queries reference the old dataSource
      dataSource = dao.flush(dataSource, data.file)
    }
  }

}

/**
 * @param file The file for the local copy of the H2 database file (that contains the index). The actual database
 *             will consist of several files that use this path as a prefix.
 */
class H2IndexData(
  val hashInfo: HashInfo,
  val compressionProvider: CompressionProvider,
  val encryptionProvider: EncryptionProvider,
  val logger: Logger,
  val file: File)
  extends IndexData {

  def this(data: FileSystemData, runtimeData: RuntimeFileSystemData, file: File) =
    this(
      data.hashInfo,
      data.compressionProvider,
      data.encryptionProvider,
      runtimeData.observerContainer.logger,
      file)
}
