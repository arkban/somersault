package somersault.core.filesystems

import java.util.regex.Pattern
import org.joda.time.format.ISODateTimeFormat
import org.joda.time.{Duration, DateTimeZone, DateTime}
import scala.collection.Map
import somersault.core.filters.{NameFilter, AndFilter, FileFilter, SomerFilter}
import util.Random

/**
 * Defines how a {@link FileSystem} locks the underlying file system to control concurrent access.
 *
 * For implementers:
 *
 * Locks should bypass the {@link Index}, if one is being used by the {@link FileSystem}. The lock must be immediately
 * visible to other Somersaults that might be trying to access the same underlying file system.
 *
 * One comment about this implementation -- it uses special {@link String}s as the "lock name" to perform all work.
 * But the interface does not require ths implementation style. If you need a different implementation, you'll can do
 * that but you'll need to separate out the interface from this particular implementation.
 */
trait LockingFileSystem extends CoreFileSystem {

  protected val options: LockingFileSystem.Options = new LockingFileSystem.Options

  /**
   * This locks the {@link FileSystem} for exclusive write access. This will return true if the lock was successfully
   * acquired. This will return false if there are errors or if another {@link FileSystem} has already locked.
   *
   * A lock cannot be upgraded or downgraded. An attempt to will not affect the existing lock.
   */
  def lock(mode: LockingFileSystemMode.Mode): Boolean = {
    // The strategy here is fairly simple, and relies on random back-offs to avoid contention. This uses the
    // canAcquireLock() to decide if a lock can be acquired.
    // 1) Remove any expired locks (no matter who created them). (This essentially eliminates all extra complexity
    // that might be involved with the timestamps in the following steps. Yay!)
    // 2) if we already have acquired our lock (hasLock()), update the lock's timestamp and return true
    // 3) if we cannot acquire our lock (canAcquireLock()), return false
    // 4) put lock (overwrite if already exists)
    // 5) wait random amount of time (in case someone else is trying to acquire a lock)
    // 6) if we successfully acquired our lock (hasLock()), update the lock's timestamp and return true
    // 7) remove our lock
    // 8) wait random amount of time (all sides do this, and whoever had the smallest wait will likely win)
    // 9) start at step 2
    // It may appear that step 2 through 4 and step 5 through 8 are the same, but they are not. The distinction is
    // the difference in step 2 and step 8, step 8 adds the wait of an extra amount of time after removing the lock.
    // It would be possible to factor out this difference in steps, but it would obfuscate the logic.
    // -----------------------------------------------------------------------

    val now = new DateTime(DateTimeZone.UTC)

    // step 1
    findExistingLocks()
      .filter {case (_, timestamp) => timestamp.plus(options.lockTimeout).isBefore(now)}
      .foreach {
      case (lock, _) =>
        removeLock(lock)
        runtimeData.observerContainer.logger.alert("Removed stale lock: " + lock)
    }

    val lockName = generateLockName(mode)

    // step 2
    if (hasLock(mode)) {
      putLock(lockName, now)
      runtimeData.observerContainer.logger.alert(mode + " lock acquired for " + data.name)
      true
    }
    // step 3
    else if (!canAcquireLock(mode)) {
      runtimeData.observerContainer.logger.alert(
        "Cannot acquire " + mode + " lock for " + data.name + "; already locked")
      removeLock(lockName)
      false
    }
    else {
      val random = new Random()
      putLock(lockName, now) // step 4
      Thread.sleep(random.nextInt(options.backOffMsMax)) // step 5
      // step 6
      if (hasLock(mode)) {
        runtimeData.observerContainer.logger.alert(mode + " lock acquired for " + data.name)
        true
      }
      else {
        removeLock(lockName) // step 7
        Thread.sleep(random.nextInt(options.backOffMsMax)) // step 8
        lock(mode) // step 9
      }
    }
  }

  /**
   * Removes any previously placed lock (for any and all {@link LockingFileSystemMode.Mode}s). This can be called
   * regardless of whether the lock was previous acquired or not.
   */
  def unlock() {
    LockingFileSystemMode.values.foreach(x => removeLock(generateLockName(x)))
  }

  /**
   * A safety method that will remove all locks for the {@link FileSystem}, regardless of who created them. This
   * should be used if a {@link FileSystem} fails to remove its lock. This is a fail-safe method.
   *
   * The default implementation of this method uses {@link #findExistingLockNames()} and {@link #removeLock(String)}.
   */
  def removeAllLocks() {
    findExistingLocks().keys.foreach(removeLock)

    runtimeData.observerContainer.logger.info("All locks removed from " + data.name)
  }

  /**
   * Writes the provided lock. This method should not do any checking of existing locks, just write the lock. This
   * should overwrite the lock if it already exists, with the new timestamp.
   */
  protected[filesystems] def putLock(lockName: String, timestamp: DateTime)

  /**
   * Removes the provided lock. This method should not search for or remove other locks. This should do nothing if
   * the lock does not exist.
   */
  protected[filesystems] def removeLock(lockName: String)

  /**
   * Finds all existing locks and their timestamps.
   */
  protected[filesystems] def findExistingLocks(): Map[String, DateTime]

  /**
   * Generates the name of the lock for this {@link FileSystem} instance. The name is a file name in the (common)
   * case that it is used in that way.
   *
   * The name of a {@link FileSystem} is not used because it is possible for two different {@link FileSystem}
   * configurations to access the same underlying file system. In this case the name won't help, but it might make
   * the job of distinguishing locks more difficult.
   *
   * @param mode The requested access mode. The name should be embedded in the returned lock name such that
   *             { @link #getModeFromLockName()} can recover it.
   */
  protected def generateLockName(mode: LockingFileSystemMode.Mode): String =
    options.lockNamePrefix + mode.toString + "." + runtimeData.id + options.lockNameSuffix

  /**
   * Creates a {@link Regex} that will match the lockName.
   */
  protected def generateLockNameRegex(): Pattern = {
    val mode = "(%s|%s)".format(LockingFileSystemMode.Read.toString, LockingFileSystemMode.Write.toString)
    val id = "[0-9a-fA-F]{6}"
    Pattern.compile(
      "^%s%s\\.%s%s$"
        .format(Pattern.quote(options.lockNamePrefix), mode, id, Pattern.quote(options.lockNameSuffix)))
  }

  /**
   * Creates a {@link SomerFilter} that will match the lock (assuming it is persisted as a file). This should be used
   * to avoid trying to transfer the lock file. This should be added to any existing {@link SomerFilter} BEFORE using
   * in any {@link ActionBuilder}. If the lock is not persisted as a file some modification may be necessary.
   */
  def generateLockNameFilter(): SomerFilter =
    new AndFilter(new FileFilter(), new NameFilter(generateLockNameRegex() :: Nil))

  /**
   * Determines the {@link LockingFileSystem#Mode} from the provided lock name.
   */
  protected def getModeFromLockName(lockName: String): LockingFileSystemMode.Mode = {
    if (lockName.contains(LockingFileSystemMode.Read.toString)) LockingFileSystemMode.Read
    else if (lockName.contains(LockingFileSystemMode.Write.toString)) LockingFileSystemMode.Write
    else throw new IllegalArgumentException("Unrecognized mode value")
  }

  /**
   * Returns true if we have our lock without any contention. More specifically, this returns true IFF we have the
   * only write lock, or we have one of many read locks.
   */
  protected def hasLock(mode: LockingFileSystemMode.Mode): Boolean = {
    val existingLocks = findExistingLocks()
    val lockName = generateLockName(mode)

    val haveLock = existingLocks.get(lockName).isDefined
    mode match {
      case LockingFileSystemMode.Read =>
        haveLock && existingLocks.forall {case (name, _) => getModeFromLockName(name) == LockingFileSystemMode.Read}
      case LockingFileSystemMode.Write =>
        haveLock && existingLocks.size == 1
      case _ =>
        throw new IllegalArgumentException("Unrecognized mode value")
    }
  }

  /**
   * Determines if we can acquire the lock, but does not actually acquire the lock.
   * The logic works like so:
   * 1) If we already have the requested lock, return true.
   * 2) If multiple locks exists (which means all locks are be read locks):
   * 2a) If requested mode is read, return true.
   * 2b) If requested mode is write, return false.
   * 3) If a single lock exists:
   * 3a) If existing lock is read and requested mode is read, return true.
   * 3b) Otherwise existing lock or requested mode is write, return false.
   * 4) If no other locks, return true
   */
  protected def canAcquireLock(mode: LockingFileSystemMode.Mode): Boolean = {
    val existingLocks = findExistingLocks()

    // step 1
    val lockName = generateLockName(mode)
    if (existingLocks.exists {case (key, _) => key == lockName}) {
      true
    }
    // step 2
    else if (existingLocks.size > 1) {
      // step 2a & 2b
      mode == LockingFileSystemMode.Read
    }
    // step 3
    else if (existingLocks.size == 1) {
      // step 3a & 3b
      getModeFromLockName(existingLocks.head._1) == LockingFileSystemMode.Read && mode == LockingFileSystemMode.Read
    }
    // step 4
    else {
      true
    }
  }
}

object LockingFileSystem {

  /**
   * Various settings and options for {@link LockingFileSystem}.
   *
   * @param backOffMsMax Maximum amount of time for any back-off attempt. (See { @link #lock()}.
   * @param lockTimeout Maximum amount of time a { @link LockingFileSystem} is allowed to hold its lock. This is to
   *                    avoid a stale lock (usually a crashed Somersault) from languishing forever.
   */
  class Options(
    val lockNamePrefix: String = "lock.",
    val lockNameSuffix: String = ".lock",
    val backOffMsMax: Int = 3 * 1000,
    val lockTimeout: Duration = new Duration(3 * 60 * 60 * 1000))

  /** Recommended {@link DateTimeFormatter} for serializing timestamp passed to {@link #putLock()}. */
  val lockTimestampFormatter = ISODateTimeFormat.dateTime
}

object LockingFileSystemMode extends Enumeration {

  /** Specifies the access mode requested for a FileSystem. */
  type Mode = Value

  /** Read-only */
  val Read = Value("Read")

  /** Read and write */
  val Write = Value("Write")
}



