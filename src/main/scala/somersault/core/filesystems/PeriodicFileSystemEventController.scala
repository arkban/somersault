package somersault.core.filesystems

import java.util.concurrent.atomic.{AtomicInteger, AtomicLong}
import nu.xom.Element
import somersault.core.filesystems.index.{IndexDirectory, IndexFile, IndexArtifact}
import somersault.util.persist.{PersisterContext, Persister}

/**
 * A helper trait for periodic events that occur on (or within) a {@link FileSystem}. These events can be anything and
 * are often "commit" or "clean up" actions that should happen often so that in the event of a crash or failure, the
 * difference between the last event and the crash time is minimal.
 *
 * This trait contains all the various counters and logic to control when to trigger the event. The event itself is
 * defined by implementers of this trait.
 */
trait PeriodicFileSystemEventController {

  protected def data: PeriodicFileSystemEventControllerData

  // Note the thread-safety accuracy required by this class is low enough that we can use Atomic instances, we don't
  // need to use the heavier weight synchronized blocks. if one of the counters is not updated there isn't much lost

  private val bytes = new AtomicLong(0)

  private val actions = new AtomicInteger(0)

  private val prevMs = new AtomicLong(System.currentTimeMillis())
  private val ms = new AtomicLong(0)

  /**
   * Resets all counters. This should be called when the event is complete, regardless of whether the event
   * was triggered normally or manually.
   */
  def reset() {
    bytes.set(0)
    actions.set(0)
    ms.set(0)
    prevMs.set(System.currentTimeMillis())
  }

  /**
   * This will update any counters being tracked based on the provided file, and if the thresholds are passed, this
   * will call {@link fire()}.
   */
  def update(artifact: SomerArtifact) {
    val fileSize = artifact match {
      case f: SomerFile => f.size
      case d: SomerDirectory => 0
    }

    updateAndFire(fileSize)
  }

  /**
   * This will update any counters being tracked based on the provided file, and if the thresholds are passed, this
   * will call {@link fire()}.
   */
  def update(artifact: IndexArtifact) {
    val fileSize = artifact match {
      case f: IndexFile => f.size
      case d: IndexDirectory => 0
    }

    updateAndFire(fileSize)
  }

  /**
   * Updates the counters based on the provided data, and if the thresholds are passed, this will call {@link fire()}.
   */
  private def updateAndFire(fileSize: Long) {
    bytes.addAndGet(fileSize)
    actions.incrementAndGet()
    ms.set(System.currentTimeMillis() - prevMs.get())
    prevMs.set(System.currentTimeMillis())

    if (shouldFire(resetFlag = true)) {
      try fire()
      finally reset()
    }
  }

  /**
   * Triggers the event. This will be called when any of the various limits are exceeded. This is where the implementer
   * of the trait does whatever they need to do on a periodic basis.
   *
   * After this is called {@link #reset()} will be called (even if an Exception is thrown).
   */
  protected def fire()

  /**
   * Returns true if the event should fire. Firing occurs when all minimums are passed and at least one maximum is
   * passed. This is to avoid one threshold forcing excessive firing.
   *
   * @param resetFlag Automatically call { @link #reset()} if the method will return true. If not the caller must
   *                  manually call { @link #reset()}.
   */
  protected def shouldFire(resetFlag: Boolean): Boolean = {
    val localBytes = bytes.get
    val localActions = actions.get
    val localMs = ms.get

    val minPassed = (localBytes >= data.bytesMin && localActions >= data.actionsMin && localMs >= data.msMin)
    val maxPassed = (localBytes >= data.bytesMax || localActions >= data.actionsMax || localMs >= data.msMax)
    val result = minPassed && maxPassed
    if (result && resetFlag)
      reset()
    result
  }
}

/** Contains configuration for the {@link PeriodicFileSystemEventController}. */
class PeriodicFileSystemEventControllerData(
  val bytesMin: Long, val bytesMax: Long,
  val actionsMin: Int, val actionsMax: Int,
  val msMin: Long, val msMax: Long) {

  if (bytesMin < 0) throw new IllegalArgumentException("bytesMin must be non-negative")
  if (bytesMax <= 0) throw new IllegalArgumentException("bytesMax must be positive")
  if (bytesMin > bytesMax) throw new IllegalArgumentException("bytes range is invalid")

  if (actionsMin < 0) throw new IllegalArgumentException("actionsMin must be non-negative")
  if (actionsMax <= 0) throw new IllegalArgumentException("actionsMax must be positive")
  if (actionsMin > actionsMax) throw new IllegalArgumentException("actions range is invalid")

  if (msMin < 0) throw new IllegalArgumentException("msMin must be non-negative")
  if (msMax <= 0) throw new IllegalArgumentException("msMax must be positive")
  if (msMin > msMax) throw new IllegalArgumentException("ms range is invalid")
}

class PeriodicFileSystemEventControllerDataPersister extends Persister {

  def canRead(context: PersisterContext, element: Element) =
    checkClass(element, classOf[PeriodicFileSystemEventControllerData])

  def read[T](context: PersisterContext, element: Element) = {
    val byteThreshold = Persister.getChildElement(element, "bytes")
    val actionCount = Persister.getChildElement(element, "actions")
    val msElapsed = Persister.getChildElement(element, "ms")
    new PeriodicFileSystemEventControllerData(
      byteThreshold.getAttributeValue("min").toLong, byteThreshold.getAttributeValue("max").toLong,
      actionCount.getAttributeValue("min").toInt, actionCount.getAttributeValue("max").toInt,
      msElapsed.getAttributeValue("min").toLong, msElapsed.getAttributeValue("max").toLong)
      .asInstanceOf[T]
  }

  override def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean) {
    obj match {
      case data: PeriodicFileSystemEventControllerData =>
        super.writeClass(data, element, dynamic)

        val bytes = Persister.createElement(
          "bytes", parent = element, comment = "number of bytes transferred")
        Persister.createAttribute("min", data.bytesMin.toString, bytes)
        Persister.createAttribute("max", data.bytesMax.toString, bytes)

        val actions = Persister.createElement(
          "actions", parent = element, comment = "number of actions")
        Persister.createAttribute("min", data.actionsMin.toString, actions)
        Persister.createAttribute("max", data.actionsMax.toString, actions)

        val ms = Persister.createElement(
          "ms", parent = element, comment = "time elapsed")
        Persister.createAttribute("min", data.msMin.toString, ms)
        Persister.createAttribute("max", data.msMin.toString, ms)

      case _ => throw new IllegalArgumentException("cannot write: " + obj.getClass.getName)
    }
  }

}

