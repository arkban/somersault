package somersault.core.encode

import somersault.core.filesystems.SomerFile
import javax.crypto.Cipher
import somersault.util.Base32
import org.apache.commons.lang.ArrayUtils
import java.io.{OutputStream, InputStream}
import nu.xom.Element
import somersault.util.persist.Persister
import somersault.util.persist.PersisterContext

/**
 * Uses the BouncyCastle encryption provider directly, avoiding the need to configure JCE to use unrestricted
 * strength encryption. (Good idea, very poor execution.)
 */
class BouncyCastleEncryptionProvider( val key: BouncyCastleEncryptionKey) extends EncryptionProvider
{
  def name = "BouncyCastle"

  def decode( origName: String, size: Long, input: InputStream) =
    if( size > 0 ) new BouncyCastleInputStream( input, key.generateCipher( origName, Cipher.DECRYPT_MODE))
    else input

  def encode( parentPath: Iterable[ String ], file: SomerFile) =
    if( file.size > 0 ) new BouncyCastleEncryptResult( key, file.name, encryptName( file.name))
    else new NullEncryptResult( encryptName( file.name))

  def encodeFile( name: String) = new BouncyCastleEncryptResult( key, name, encryptName( name))

  def encodeDir( name: String) = encodeFile( name)

  /**
   * Encrypts the filename and then base-32 encodes it for storage on the FileSystem.
   */
  private def encryptName( origName: String): String =
  {
    val cipher = key.generateCipher( origName, Cipher.ENCRYPT_MODE)
    val origNameBytes = origName.getBytes
    val output = new Array[ Byte ]( cipher.getOutputSize( origNameBytes.length))
    val firstWriteCount = cipher.processBytes( origNameBytes, 0, origNameBytes.length, output, 0)
    val secondWriteCount = cipher.doFinal( output, firstWriteCount)
    val finalOutput = ArrayUtils.subarray( output, 0, firstWriteCount + secondWriteCount)
    Base32.encode( finalOutput)
  }
}

class BouncyCastleEncryptionProviderPersister extends Persister
{
  def canRead( context: PersisterContext, element: Element): Boolean = checkClass(
    element, classOf[ BouncyCastleEncryptionProvider ])

  def read[ T ]( context: PersisterContext, element: Element): T =
  {
    val key: BouncyCastleEncryptionKey = new BouncyCastleEncryptionKeyPersister( ).read(
      context, Persister.getChildElement( element, "key"))
    new BouncyCastleEncryptionProvider( key).asInstanceOf[ T ]
  }

  def write( context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false)
  {
    obj match
    {
      case provider: BouncyCastleEncryptionProvider =>
        writeClass( obj, element, dynamic)
        val key = Persister.createElement( "key", parent = element)
        new BouncyCastleEncryptionKeyPersister( ).write( context, provider.key, key)
      case _ => throw new IllegalArgumentException( "cannot write: " + obj.getClass.getName)
    }
  }
}

class BouncyCastleEncryptResult( val key: BouncyCastleEncryptionKey, val origName: String, newName: String)
    extends AbstractEncodeResult( true, newName) with EncryptResult
{
  def encode( output: OutputStream) =
    new BouncyCastleOutputStream( output, key.generateCipher( origName, Cipher.ENCRYPT_MODE))
}