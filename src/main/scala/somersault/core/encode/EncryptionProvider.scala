package somersault.core.encode

import net.jcip.annotations.Immutable

/**
 * Wraps InputStreams and OutputStreams with a cipher/encryption algorithm.
 *
 * Encryption is all or none. Either the entire FileSystem is encrypted or not. This allows FileSystem implementations
 * to use more advanced encryption techniques if desired, and also avoids the complexity of trying to encrypt the
 * parent directories for some files but not for other files. (I don't even know how you do that...)
 */
@Immutable
trait EncryptionProvider extends EncodingProvider[EncryptResult] {

  /**
   * The key used by the EncryptionProvider.
   */
  def key: EncryptionKey
}

@Immutable
class EncryptionProviderMismatchException private(message: String) extends Exception(message) {

  def this() = this("stored encryption does not match expected provided encryption")
}

trait EncryptResult extends EncodeResult
