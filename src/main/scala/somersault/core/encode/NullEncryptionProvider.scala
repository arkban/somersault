package somersault.core.encode

import java.io.{OutputStream, InputStream}
import somersault.core.filesystems.SomerFile
import net.jcip.annotations.Immutable
import nu.xom.Element
import somersault.util.{HashInfo, Hash}
import somersault.util.persist.{PersisterContext, Persister}

/**
 * Performs no encryption or decryption.
 */
@Immutable
class NullEncryptionProvider extends EncryptionProvider {

  val name = "Null (None)"

  val key = NullEncryptionProvider.key

  def encode(parentPath: Iterable[String], file: SomerFile) = new NullEncryptResult(file.name)

  def encodeFile(name: String) = new NullEncryptResult(name)

  def encodeDir(name: String) = new NullEncryptResult(name)

  def decode(newName: String, size: Long, input: InputStream) = input
}

object NullEncryptionProvider {

  val key = new EncryptionKey {
    def name = "Null (None)"

    def hashCodeStrong(hashInfo: HashInfo) = new Hash(hashInfo, new Array[Byte](0))
  }
}

class NullEncryptionProviderPersister extends Persister {

  def canRead(context: PersisterContext, element: Element): Boolean = checkClass(
    element, classOf[NullEncryptionProvider])

  def read[T](context: PersisterContext, element: Element): T = new NullEncryptionProvider().asInstanceOf[T]

  def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false) {
    obj match {
    case provider: NullEncryptionProvider => writeClass(obj, element, dynamic)
    case _ => throw new IllegalArgumentException("cannot write: " + obj.getClass.getName)
    }
  }
}

/**
 * Returns the InputStream unchanged. This class may be used by other EncryptionProviders when they decide not to
 * encode a file or directory.
 */
@Immutable
class NullEncryptResult(newName: String) extends AbstractEncodeResult(false, newName) with EncryptResult {

  def encode(output: OutputStream) = output
}

