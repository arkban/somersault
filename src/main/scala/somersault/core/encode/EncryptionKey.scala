package somersault.core.encode

import somersault.util.persist.Persister
import somersault.util.{HashInfo, Hash}

/**
 * Encapsulates everything needed to generate a Cipher. This will generate a Cipher on demand with the provided salt.
 *
 * This contains no information that can't be kept public while running Somersault.
 */
trait EncryptionKey
{
  def name: String

  /**
   * Creates a SHA-1 hash of the data in this instance.
   */
  def hashCodeStrong( hashInfo: HashInfo): Hash

  /**
   * Generates a salt value from the provided string. The string's data is not used directly, there is some randomness
   * involved which (hopefully) will make the salt more potent.
   */
  protected def generateSalt( saltLength: Int, value: String): Array[ Byte ] =
  {
    // maybe we should use an MD5 hash of the provided value instead of the Random to generate the bytes?
    // FNV hash - what's important is all values are used
    val seed = value.getBytes.foldLeft( -18652614)( ( x, y) => ( x * 16777619 ) | y)

    val random = new java.util.Random( seed)
    val result = new Array[ Byte ]( saltLength)
    random.nextBytes( result)
    result
  }
}

object EncryptionKey
{
  /**
   * Number of iterations that used in the key generation algorithm.
   */
  val keyGenIterations = 1024
}

/**
 * This will write the hashed user key, but can read an unhashed key and hash it as needed.
 */
trait EncryptionKeyPersister extends Persister
