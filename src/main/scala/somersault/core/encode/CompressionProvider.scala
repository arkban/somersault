package somersault.core.encode

import net.jcip.annotations.Immutable

/**
 * Wraps InputStreams and OutputStreams with a compression algorithm. (The filter should be used to avoid compressing
 * files tht are already compressed because is no benefit in re-compressing already compressed files -- sometimes they
 * end up being larger than they started!)
 */
@Immutable
trait CompressionProvider extends EncodingProvider[ CompressResult ]

object CompressionProvider
{
  def build( d: AnyRef) = null
}

@Immutable
class CompressionProviderMismatchException private( message: String, val expectedName: String, val actualName: String)
    extends Exception( message)
{
  def this( expectedName: String, actualName: String) = this (
    String.format( "%1$s does not match expected %2$s", actualName, expectedName), expectedName, actualName)
}

trait CompressResult extends EncodeResult
