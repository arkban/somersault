package somersault.core.encode

import org.bouncycastle.crypto.generators.{PKCS12ParametersGenerator, PKCS5S1ParametersGenerator}
import org.bouncycastle.crypto.paddings.{ZeroBytePadding, X923Padding, TBCPadding, ISO10126d2Padding, ISO7816d4Padding, PKCS7Padding, BlockCipherPadding, PaddedBufferedBlockCipher}
import org.bouncycastle.crypto.modes.{OFBBlockCipher, SICBlockCipher, OpenPGPCFBBlockCipher, PGPCFBBlockCipher, GOFBBlockCipher, CFBBlockCipher, CBCBlockCipher}
import org.bouncycastle.crypto.engines.{TwofishEngine, RijndaelEngine, RC564Engine, RC2Engine, DESedeEngine, DESEngine, BlowfishEngine, AESFastEngine}
import org.bouncycastle.crypto.{BlockCipher, PBEParametersGenerator}
import org.bouncycastle.crypto.digests.{SHA1Digest, MD5Digest}
import somersault.util.{Hash, HashInfo}
import java.io.{ByteArrayOutputStream, ByteArrayInputStream}
import javax.crypto.Cipher
import org.bouncycastle.crypto.params.ParametersWithIV
import org.apache.commons.lang.math.NumberUtils
import nu.xom.Element
import somersault.util.persist.{PersisterContext, Persister}

/**
 * This exposes the BouncyCastle encryption libraries using a very similar interface as used by JCE. As best as
 * possible, this follows the JCE naming conventions for algorithms. (But the JCE is kind of stupid in its naming
 * convention.)
 *
 * I make no claims to being a security expert. I know enough to be dangerous and enough to get something working. So
 * if you are security expert, please feel free to help correct any mistakes you may find.
 *
 * References:
 * - http://download.oracle.com/javase/1.5.0/docs/guide/security/jce/JCERefGuide.html
 */
class BouncyCastleEncryptionKey(
  val cipherSpec: String, val pbeSpec: String, val keyLength: Int, val saltLength: Int, val userKey: String)
  extends EncryptionKey {

  private def canEqual(other: Any): Boolean = other.isInstanceOf[BouncyCastleEncryptionKey]

  def name = String.format("Cipher: %1$s, PBE: %2$s", cipherSpec, pbeSpec)

  override def equals(other: Any): Boolean =
    other match {
      case that: BouncyCastleEncryptionKey =>
        this.canEqual(that) &&
          cipherSpec == that.cipherSpec &&
          pbeSpec == that.pbeSpec &&
          keyLength == keyLength &&
          saltLength == saltLength &&
          userKey == that.userKey
      case _ => false
    }

  override def hashCode: Int =
    (
      41 *
        (
          41 *
            (41 * (41 * cipherSpec.hashCode + pbeSpec.hashCode) + keyLength.hashCode) + saltLength.hashCode)
        + userKey.hashCode)

  /**
   * Creates a SHA-1 hash of the data in this instance.
   */
  def hashCodeStrong(hashInfo: HashInfo): Hash = {
    val output = new ByteArrayOutputStream
    output.write(cipherSpec.getBytes)
    output.write(pbeSpec.getBytes)
    output.write(keyLength.toHexString.getBytes)
    output.write(saltLength.toHexString.getBytes)
    output.write(userKey.getBytes)
    Hash.create(hashInfo, new ByteArrayInputStream(output.toByteArray))
  }

  /**
   * This requires its own salt to allow a per-file salt, which greatly increases the complexity of decryption all
   * files. A good suggestion is the IndexFile.storedName. This is easily found from the index and prevents a
   * dictionary based attack across multiple files. The IndexFile.storedName is stored in the Index, which itself
   * should be encryption.
   */
  def generateCipher(salt: String, cipherMode: Int): PaddedBufferedBlockCipher = {
    val saltBytes = generateSalt(saltLength, salt)

    val pbeGenerator = BouncyCastleEncryptionKey.createPbeGenerator(pbeSpec)
    pbeGenerator.init(userKey.getBytes, saltBytes, EncryptionKey.keyGenIterations)

    val (cipherAlg, modeAlg, paddingAlg) = splitCipherSpec()

    val params = new ParametersWithIV(
      pbeGenerator.generateDerivedParameters(keyLength), saltBytes)
    val cipher = BouncyCastleEncryptionKey.createCipherEngine(cipherAlg)
    val cipherWithMode = BouncyCastleEncryptionKey.createModeCipher(modeAlg, cipher)

    val cipherWithPadding = new PaddedBufferedBlockCipher(
      cipherWithMode, BouncyCastleEncryptionKey.createPadding(paddingAlg))

    cipherWithPadding.init(if (cipherMode == Cipher.ENCRYPT_MODE) true else false, params)
    cipherWithPadding
  }

  /**
   * Splits the Cipher Spec String into Algorithm/Mode/Padding.
   */
  private def splitCipherSpec(): (String, String, String) = {
    val result = cipherSpec.split("\\/")
    (result(0), result(1), result(2))
  }
}

object BouncyCastleEncryptionKey {

  def createStrongKey(userKey: String) = new BouncyCastleEncryptionKey(
    "AES/CBC/PKCS5Padding", "PBEWithSHAAndTwofish-CBC", 256, 16, userKey)

  def createWeakKey(userKey: String) = new BouncyCastleEncryptionKey(
    "DES/CBC/PKCS5Padding", "PBEWithMD5AndDES", 64, 8, userKey)

  /**
   * Creates an uninitialized BlockCipher
   */
  private def createCipherEngine(cipherAlg: String): BlockCipher =
    cipherAlg match {
      case "AES" => new AESFastEngine
      case "Blowfish" => new BlowfishEngine
      case "DES" => new DESEngine
      case "DESede" => new DESedeEngine
      case "RC2" => new RC2Engine
      case "RC5" => new RC564Engine
      case "Rijndael" => new RijndaelEngine
      case "Twofish" => new TwofishEngine
      case _ => throw new IllegalArgumentException("unrecognized Cipher Algorithm: " + cipherAlg)
    }

  /**
   * Creates a PBE algorithm based on the provided string
   */
  private def createPbeGenerator(pbeAlg: String): PBEParametersGenerator = {
    val digest = pbeAlg match {
      // PKCS v5 Scheme 1
      case "PBEWithMD5AndDES" => new MD5Digest
      case "PBEWithMD5AndRC2" => new MD5Digest
      case "PBEWithSHA1AndDES" => new SHA1Digest
      case "PBEWithSHA1AndRC2" => new SHA1Digest
      // PKCS v12
      case "PBEWithSHAAnd2-KeyTripleDES-CBC" => new SHA1Digest
      case "PBEWithSHAAnd3-KeyTripleDES-CBC" => new SHA1Digest
      case "PBEWithSHAAnd128BitRC2-CBC" => new SHA1Digest
      case "PBEWithSHAAnd40BitRC2-CBC" => new SHA1Digest
      case "PBEWithSHAAnd128BitRC4" => new SHA1Digest
      case "PBEWithSHAAnd40BitRC4" => new SHA1Digest
      case "PBEWithSHAAndTwofish-CBC" => new SHA1Digest
      case "PBEWithSHAAndIDEA-CBC" => new SHA1Digest
      case _ => throw new IllegalArgumentException("unrecognized PBE Algorithm: " + pbeAlg)
    }

    pbeAlg match {
      // PKCS v5 Scheme 1
      case "PBEWithMD5AndDES" => new PKCS5S1ParametersGenerator(digest)
      case "PBEWithMD5AndRC2" => new PKCS5S1ParametersGenerator(digest)
      case "PBEWithSHA1AndDES" => new PKCS5S1ParametersGenerator(digest)
      case "PBEWithSHA1AndRC2" => new PKCS5S1ParametersGenerator(digest)
      // PKCS v12
      case "PBEWithSHAAnd2-KeyTripleDES-CBC" => new PKCS12ParametersGenerator(digest)
      case "PBEWithSHAAnd3-KeyTripleDES-CBC" => new PKCS12ParametersGenerator(digest)
      case "PBEWithSHAAnd128BitRC2-CBC" => new PKCS12ParametersGenerator(digest)
      case "PBEWithSHAAnd40BitRC2-CBC" => new PKCS12ParametersGenerator(digest)
      case "PBEWithSHAAnd128BitRC4" => new PKCS12ParametersGenerator(digest)
      case "PBEWithSHAAnd40BitRC4" => new PKCS12ParametersGenerator(digest)
      case "PBEWithSHAAndTwofish-CBC" => new PKCS12ParametersGenerator(digest)
      case "PBEWithSHAAndIDEA-CBC" => new PKCS12ParametersGenerator(digest)
      case _ => throw new IllegalArgumentException("unrecognized PBE Algorithm: " + pbeAlg)
    }
  }

  private[encode] def createPadding(paddingAlg: String): BlockCipherPadding =
    paddingAlg match {
      case "PKCS5Padding" => new PKCS7Padding // i couldn't find a PKCS5Padding class
      case "PKCS7Padding" => new PKCS7Padding
      case "ISO7816d4Padding" => new ISO7816d4Padding
      case "ISO10126Padding" => new ISO10126d2Padding
      case "TBCPadding" => new TBCPadding
      case "X923Padding" => new X923Padding
      case "ZeroBytePadding" => new ZeroBytePadding
      case "NoPadding" => new ZeroBytePadding
      case _ => throw new IllegalArgumentException("unrecognized Padding Algorithm: " + paddingAlg)
    }

  private[encode] def createModeCipher(modeAlg: String, cipher: BlockCipher): BlockCipher =
    modeAlg match {
      case "CBC" => new CBCBlockCipher(cipher)
      case "CFB" => new CFBBlockCipher(cipher, cipher.getBlockSize * 8)
      case "GOFB" => new GOFBBlockCipher(cipher)
      case "OFB" => new OFBBlockCipher(cipher, cipher.getBlockSize)
      case "PGPCFB" => new PGPCFBBlockCipher(cipher, false)
      case "OpenPGPCFB" => new OpenPGPCFBBlockCipher(cipher)
      case "SIC" => new SICBlockCipher(cipher)
    }
}

/**
 * This will write the hashed user key, but can read an unhashed key and hash it as needed.
 */
class BouncyCastleEncryptionKeyPersister extends Persister {

  def canRead(context: PersisterContext, element: Element): Boolean = checkClass(
    element, classOf[BouncyCastleEncryptionKey])

  def read[T](context: PersisterContext, element: Element): T = {
    val cipherAlg = Persister.getChildElement(element, "cipherSpec").getValue
    val keyAlg = Persister.getChildElement(element, "pbeSpec").getValue
    val keyLength = NumberUtils.toInt(Persister.getChildElement(element, "hashedKeyLength").getValue)
    val saltLength = NumberUtils.toInt(Persister.getChildElement(element, "saltLength").getValue)
    val userKey = Persister.getChildElement(element, "userKey").getValue

    new BouncyCastleEncryptionKey(cipherAlg, keyAlg, keyLength, saltLength, userKey).asInstanceOf[T]
  }

  def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false) {
    obj match {
      case encKey: BouncyCastleEncryptionKey =>
        writeClass(obj, element, dynamic)
        Persister.createElement(
          name = "cipherSpec",
          value = encKey.cipherSpec,
          parent = element,
          comment = "Encrpytion Algorithm, used to encrypt file data, format is: cipher-algorithm/block-mode/padding-scheme")
        Persister.createElement(
          name = "pbeSpec",
          value = encKey.pbeSpec,
          parent = element,
          comment = "Password Based Encryption Algorithm, used to derive a cryptographic pasword from the user provided key")
        Persister.createElement(
          name = "hashedKeyLength",
          value = encKey.keyLength.toString,
          parent = element,
          comment = "Length of the hashed user key, in bytes. This depends on cipher algorithm")
        Persister.createElement(
          name = "saltLength",
          value = encKey.saltLength.toString,
          parent = element,
          comment = "Length of the salt to be used, in bytes. This depends on cipher algorithm")
        Persister.createElement(
          name = "userKey",
          value = encKey.userKey,
          parent = element,
          comment = "user provided key used to encrypt file data")
      case _ => throw new IllegalArgumentException("cannot write: " + obj.getClass.getName)
    }
  }
}

