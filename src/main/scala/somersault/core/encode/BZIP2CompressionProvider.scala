package somersault.core.encode

import java.io.{OutputStream, InputStream}
import nu.xom.Element
import org.apache.tools.bzip2.{CBZip2OutputStream, CBZip2InputStream}
import somersault.core.filesystems.SomerFile
import somersault.core.filters.{ExtendedFilters, SomerFilter}
import somersault.util.persist.{PersisterContext, Persister}

class BZIP2CompressionProvider(val filter: SomerFilter, val extension: String = ".bz2") extends CompressionProvider {

  def this() = this(ExtendedFilters.standardCompressionFilter)

  def name = "BZIP2"

  def encode(parentPath: Iterable[String], file: SomerFile) =
  // CBZip2OutputStream throws a divide by zero error if the file is empty
    if (filter.accept(parentPath, file) && file.size > 0) new BZIP2CompressResult(file.name + extension)
    else new NullCompressResult(file.name)

  def encodeFile(name: String) = new BZIP2CompressResult(name + extension)

  def encodeDir(name: String) = new BZIP2CompressResult(name)

  def decode(newName: String, size: Long, input: InputStream) =
  // CBZip2InputStream throws a divide by zero error if the file is empty
    if (size > 0) new CBZip2InputStream(input) else input
}

class BZIP2CompressResult(newName: String) extends AbstractEncodeResult(true, newName) with CompressResult {

  def encode(output: OutputStream) = new CBZip2OutputStream(output)
}

class BZIP2CompressionProviderPersister extends Persister {

  def canRead(context: PersisterContext, element: Element): Boolean = checkClass(
    element, classOf[BZIP2CompressionProvider])

  def read[T](context: PersisterContext, element: Element): T = {
    val filter = Persister.getChildElement(element, "filter")
    val ext = element.getAttributeValue("extension")
    new BZIP2CompressionProvider(context.container.read[SomerFilter](context, filter), ext).asInstanceOf[T]
  }

  def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false) {
    obj match {
      case provider: BZIP2CompressionProvider =>
        writeClass(obj, element, dynamic)
        Persister.createAttribute("extension", provider.extension, element)
        context.container.write(
          context, provider.filter, Persister.createElement("filter", parent = element), dynamic = true)
      case _ => throw new IllegalArgumentException("cannot write: " + obj.getClass.getName)
    }
  }
}
