package somersault.core.encode

import java.io.InputStream
import net.jcip.annotations.NotThreadSafe
import org.apache.commons.io.input.ProxyInputStream
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher

/**
 * @param cipher Assumes the Cipher has already been initialized.
 */
@NotThreadSafe
class BouncyCastleInputStream(proxy: InputStream, cipher: PaddedBufferedBlockCipher) extends ProxyInputStream(proxy) {

  /**
   * All data read from the cipher will be buffered here, since the cipher will provide data at different intervals
   * than the reader will want the data.
   */
  private var processedBuffer = new Array[Byte](0)

  /**
   * Temporary buffer used for reading data from the proxy stream. This exists as a field only for the performance
   * benefit of not reallocating this array on each read.
   */
  private var readBuffer = new Array[Byte](0)

  /**
   * Current index in the processedBuffer.
   */
  private var currIndex = 0

  /**
   * Current length of the data in the processedBuffer.
   */
  private var currLen = 0

  /**
   * Set to true when cipher.doFinal() is called, the last call that should be made to the cipher object.
   */
  private var doFinalCalled = false

  override def read(bts: Array[Byte], off: Int, len: Int): Int = {
    if (currIndex == currLen && doFinalCalled) return -1

    // get data from proxy and process if no data in the processedBuffer
    if (currIndex == currLen) {
      resetBuffers(len)
      val result = proxy.read(readBuffer, 0, len)
      processReadBuffer(result)
    }

    // pull from processed buffer
    val minLen = java.lang.Math.min(len, currLen - currIndex)
    Array.copy(processedBuffer, currIndex, bts, off, minLen)
    currIndex += minLen
    minLen
  }

  override def read(bts: Array[Byte]) = read(bts, 0, bts.length)

  override def read(): Int = {
    if (currIndex == currLen && doFinalCalled) return -1

    // get data from proxy and process if no data in the processedBuffer
    if (currIndex == currLen) {
      resetBuffers(1)

      var readResult = 0
      // loop until we get some data in our buffer. (the read() method must block until it returns data)
      while (readResult != -1 && currLen == 0) {
        readResult = proxy.read() // blocks
        if (readResult != -1) {
          readBuffer(0) = readResult.asInstanceOf[Byte]
          processReadBuffer(1)
        }
        else {
          processReadBuffer(-1)
        }
      }
    }

    // pull from processed buffer
    if (currIndex == currLen) {
      -1
    }
    else {
      val result = processedBuffer(currIndex)
      currIndex += 1
      result & 0xFF
    }
  }

  override def reset() {
    cipher.reset()
    super.reset()
  }

  /**
   * Resets all buffers based on the provided length, resizing our buffers if necessary.
   */
  private def resetBuffers(len: Int) {
    readBuffer = increaseBuffer(readBuffer, len)
    processedBuffer = increaseBuffer(processedBuffer, len)
    currIndex = 0
    currLen = 0
  }

  /**
   * Process data in the readBuffer, and moves it to the processedBuffer. This will update currLen with the amount of
   * data in the processedBuffer.
   *
   * @param bytesRead The number of bytes read from the proxy stream. Supply -1 if the end of data was read, which
   *                  will still likely add data to the processedBuffer.
   */
  private def processReadBuffer(bytesRead: Int) {
    if (bytesRead == -1) {
      doFinalCalled = true
      currLen = cipher.doFinal(processedBuffer, 0)
    }
    else {
      currLen = cipher.processBytes(readBuffer, 0, bytesRead, processedBuffer, 0)
    }
  }

  /**
   * Increases the size of the buffer as needed. Returns either a new buffer or the provided buffer.
   */
  private def increaseBuffer(buffer: Array[Byte], len: Int): Array[Byte] = {
    // doubled because I found a few instances where cipher.doFinal() wanted exactly twice the outputSize. no idea why
    val newLen = cipher.getOutputSize(len) * 2
    if (buffer.length < newLen) new Array[Byte](newLen)
    else buffer
  }
}