package somersault.core.encode

import java.io.{OutputStream, InputStream}
import somersault.core.filesystems.SomerFile
import net.jcip.annotations.Immutable

/**
 * Wraps {@link InputStream}s and {@link OutputStream}s from the {@link FileSystem#read()} and
 * {@link FileSystem#write()} methods with some encoding.
 *
 * The {@link SomerFile} to be encoded is provided to decide whether to encode or which algorithm to use for encoding.
 */
@Immutable
trait EncodingProvider[ T <: EncodeResult ]
{
  /**
   * A user-friendly name for this {@link EncodingProvider}. This should allow a human user to identify a
   * {@link EncodingProvider} implementation (from a report or GUI).F
   */
  def name: String

  /**
   * Returns an EncodeResult which will allow encoding the provided file. It is up to the implementation to
   * determine which encoding method is used and which files are encoded. (To avoid encoding a file, this should
   * return an instance of NullEncodeResult.)
   *
   * @param file The file to be encoded. This, with the path, may be used with a filter to determine if encoding should
   * occur. Also the file's name may be used to initialize the encoding, for example the origName might be used as the
   * salt for encryption.
   */
  def encode( parentPath: Iterable[ String ], file: SomerFile): T

  /**
   * Returns an EncodeResult which will allow encoding the provided file name. This should always return an
   * EncodeResult whose encode flag is true (if this implementation does any encoding).
   *
   * This method is intended for files that the FileSystem wishes to manually or always encode, such as the index file
   * or other system files.
   *
   * @param name The original name of the file to be decoded. This may be used to correctly initialize encoding,
   * for example the origName might be used as the salt for encryption.
   */
  def encodeFile( name: String): T

  /**
   * Returns an EncodeResult which will allow encoding the provided directory name. This should always return an
   * EncodeResult whose encode flag is true (if this implementation does any encoding).
   *
   * This method is intended for directories that the FileSystem wishes to manually or always encode.
   *
   * @param name The original name of the file to be decoded. This may be used to correctly initialize encoding,
   * for example the origName might be used as the salt for encryption.
   */
  def encodeDir( name: String): T

  /**
   * Creates a decoding stream for the provided InputStream. This method differs from encode() because the choice to
   * decode should be completely up to the caller. This allows one to encode a file with some criteria, change that
   * criteria (which would exclude that file from future encodings), and still allow decoding the file as desired.
   *
   * @param origName The original name of the file to be decoded. This may be used to correctly initialize decoding,
   * for example the origName might be used as the salt for encryption.
   */
  def decode( origName: String, size: Long, input: InputStream): InputStream
}

/**
 * This class is created in request to encode some artifact, and contains everything one needs to determine if encoding
 * will be used and to actually perform the encoding. (This is a result of EncodingProvider.encode().)
 */
@Immutable
trait EncodeResult
{
  /**
   * True if the result will be encoded, false if the result will decoded.
   */
  def encoded: Boolean

  /**
   * The new name of the artifact. This name should be different ONLY if encoded is true, but may be the same if no
   * name change is desired.
   */
  def newName: String

  /**
   * Wraps the provided OutputStream for encoding, and returns the wrapped stream.
   */
  def encode( output: OutputStream): OutputStream
}

@Immutable
abstract class AbstractEncodeResult( val encoded: Boolean, val newName: String) extends EncodeResult
