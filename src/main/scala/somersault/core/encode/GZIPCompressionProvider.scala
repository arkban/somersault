package somersault.core.encode

import java.io.{OutputStream, InputStream}
import java.util.zip.{GZIPOutputStream, GZIPInputStream}
import net.jcip.annotations.Immutable
import nu.xom.Element
import somersault.core.filesystems.SomerFile
import somersault.core.filters.{ExtendedFilters, SomerFilter}
import somersault.util.persist.{PersisterContext, Persister}

@Immutable
class GZIPCompressionProvider(val filter: SomerFilter, val extension: String = ".gz") extends CompressionProvider {

  def this() = this(ExtendedFilters.standardCompressionFilter)

  def name = "GZIP"

  def encode(parentPath: Iterable[String], file: SomerFile) =
    if (filter.accept(parentPath, file) && file.size > 0) new GZIPCompressResult(file.name + extension)
    else new NullCompressResult(file.name)

  def encodeFile(name: String) = new GZIPCompressResult(name + extension)

  def encodeDir(name: String) = new GZIPCompressResult(name)

  def decode(newName: String, size: Long, input: InputStream) =
    if (size > 0) new GZIPInputStream(input) else input
}

@Immutable
class GZIPCompressResult(newName: String) extends AbstractEncodeResult(true, newName) with CompressResult {

  def encode(output: OutputStream) = new GZIPOutputStream(output)
}

class GZIPCompressionProviderPersister extends Persister {

  def canRead(context: PersisterContext, element: Element): Boolean = checkClass(
    element, classOf[GZIPCompressionProvider])

  def read[T](context: PersisterContext, element: Element): T = {
    val filter = Persister.getChildElement(element, "filter")
    val ext = element.getAttributeValue("extension")
    new GZIPCompressionProvider(context.container.read[SomerFilter](context, filter), ext).asInstanceOf[T]
  }

  def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false) {
    obj match {
      case provider: GZIPCompressionProvider =>
        writeClass(obj, element, dynamic)
        Persister.createAttribute("extension", provider.extension, element)
        context.container.write(
          context, provider.filter, Persister.createElement("filter", parent = element), dynamic = true)
      case _ => throw new IllegalArgumentException("cannot write: " + obj.getClass.getName)
    }
  }
}
