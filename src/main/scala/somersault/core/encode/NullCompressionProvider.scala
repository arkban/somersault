package somersault.core.encode

import java.io.{OutputStream, InputStream}
import somersault.core.filesystems.SomerFile
import net.jcip.annotations.Immutable
import nu.xom.Element
import somersault.util.persist.{PersisterContext, Persister}

/**
 * Performs no compression or decompression.
 */
@Immutable
class NullCompressionProvider extends CompressionProvider
{
  val name = "Null (None)"

  def encode( parentPath: Iterable[ String ], file: SomerFile) = new NullCompressResult( file.name)

  def encodeFile( name: String) = new NullCompressResult( name)

  def encodeDir( name: String) = new NullCompressResult( name)

  def decode( newName: String, size: Long, input: InputStream) = input
}

/**
 * Returns the InputStream unchanged. This class may be used by other CompressionProviders when they decide not to
 * encode a file or directory.
 */
@Immutable
class NullCompressResult( newName: String) extends AbstractEncodeResult( false, newName) with CompressResult
{
  def encode( output: OutputStream) = output
}

class NullCompressionProviderPersister extends Persister
{
  def canRead( context: PersisterContext, element: Element): Boolean = checkClass(
    element, classOf[ NullCompressionProvider ])

  def read[ T ]( context: PersisterContext, element: Element): T = new NullCompressionProvider( ).asInstanceOf[ T ]

  def write( context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false)
  {
    obj match
    {
      case provider: NullCompressionProvider => writeClass( obj, element, dynamic)
      case _ => throw new IllegalArgumentException( "cannot write: " + obj.getClass.getName)
    }
  }
}
