package somersault.core.encode

import java.io.OutputStream
import net.jcip.annotations.NotThreadSafe
import org.apache.commons.io.output.ProxyOutputStream
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher

@NotThreadSafe
class BouncyCastleOutputStream(proxy: OutputStream, cipher: PaddedBufferedBlockCipher)
  extends ProxyOutputStream(proxy) {

  /**
   * Buffer used for reading from the proxy stream. Allocated once and re-used for performance.
   */
  private var buffer = new Array[Byte](cipher.getBlockSize)

  override def write(bts: Array[Byte], off: Int, len: Int) {
    increaseBuffer(len)
    val processed = cipher.processBytes(bts, off, len, buffer, 0)
    if (processed > 0)
      proxy.write(buffer, 0, processed)
  }

  override def write(bts: Array[Byte]) { write(bts, 0, bts.length) }

  override def write(idx: Int) {
    increaseBuffer(1)
    val processed = cipher.processByte(idx.asInstanceOf[Byte], buffer, 0)
    if (processed > 0)
      proxy.write(buffer, 0, processed)
  }

  override def close() {
    increaseBuffer(cipher.getBlockSize * 2)
    val result = cipher.doFinal(buffer, 0)
    if (result > 0)
      proxy.write(buffer, 0, result)
  }

  /**
   * Increases the size of the buffer as needed
   */
  private def increaseBuffer(len: Int) {
    val newLen = cipher.getOutputSize(len)
    if (buffer.length < newLen)
      buffer = new Array[Byte](newLen)
  }
}
