package somersault.core.exceptions

import somersault.core.actions.SomerAction
import somersault.util._

/**
 * Exception classes for ActionRunners. These exceptions all have a common base class so they can be separated from
 * other actions.
 *
 * Note that not all FileSystems must throw these Exceptions. Rather these are all the Exceptions that are understood
 * by the ActionRunners. The ActionRunners can recognize and work to recover from some of these exceptions.
 *
 * @param msg A short message. The parentPath and Action name will be appended to this message.
 * @param parentPath Describes the parent path of the SomerAction. The path starts at index 0 and descends to N.
 *                   Items at the root should have Nil as the pasrent path.
 */

class SomerActionRunnerException(
  val msg: String, val action: SomerAction, val parentPath: List[String], ex: Throwable)
  extends Exception(
    msg + ": " + FilenameUtils2.dirSeparator + FilenameUtils2.concat(parentPath ::: action.name :: Nil), ex) {

  def this(msg: String, action: SomerAction, parentPath: List[String]) = this(msg, action, parentPath, null)

  /**
   * Returns the full path of the SomerArtifact described by the parentPath and SomerAction.
   */
  def path = parentPath ::: action.name :: Nil
}

class UnknownActionRunnerException(msg: String, action: SomerAction, parentPath: List[String], ex: Throwable)
  extends SomerActionRunnerException(msg, action, parentPath, ex) {

  def this(msg: String, action: SomerAction, parentPath: List[String]) = this(msg, action, parentPath, null)

  def this(action: SomerAction, parentPath: List[String], ex: Throwable) = this("Unknown error", action, parentPath, ex)

  def this(action: SomerAction, ex: SomerFileSystemException) =
    this(ex.msg, action, if (ex.path.size == 0) Nil else ex.path.take(ex.path.size - 1), ex)
}

class SourceDoesNotExistActionRunnerException(action: SomerAction, parentPath: List[String])
  extends SomerActionRunnerException("Source path does not exist", action, parentPath)

class DestDoesNotExistActionRunnerException(action: SomerAction, parentPath: List[String])
  extends SomerActionRunnerException("Destination path does not exist", action, parentPath)

class DirExistsWhereFileExpectedActionRunnerException(action: SomerAction, parentPath: List[String])
  extends SomerActionRunnerException("Directory exists where file was expected", action, parentPath)

class FileExistsWhereDirExpectedActionRunnerException(action: SomerAction, parentPath: List[String])
  extends SomerActionRunnerException("File exists where directory was expected", action, parentPath)

class SourceOpenActionRunnerException(action: SomerAction, parentPath: List[String], ex: Throwable)
  extends SomerActionRunnerException("Cannot open source file", action, parentPath, ex)

class SourceReadActionRunnerException(action: SomerAction, parentPath: List[String], ex: Throwable)
  extends SomerActionRunnerException("Error reading from source file", action, parentPath, ex)

class DestOpenActionRunnerException(action: SomerAction, parentPath: List[String], ex: Throwable)
  extends SomerActionRunnerException("Cannot open destination file", action, parentPath, ex)

class DestWriteActionRunnerException(action: SomerAction, parentPath: List[String], ex: Throwable)
  extends SomerActionRunnerException("Error writing to destination file", action, parentPath, ex)
