package somersault.core.exceptions

import somersault.util.FilenameUtils2

/**
 * Exception classes for ActionBuilders. These exceptions all have a common base class so they can be separated from
 * other actions.
 *
 * @param msg A short message. The path will be appended to this message.
 * @param path Describes the path of the SomerAction. The path starts at index 0 and descends to N. Items at the root
 *             should have Nil as the parent path.
 */

abstract class SomerActionBuilderException(val msg: String, val path: List[String], ex: Throwable)
  extends Exception(msg + ": " + FilenameUtils2.dirSeparator + FilenameUtils2.concat(path), ex) {

  def this(msg: String, path: List[String]) = this(msg, path, null)
}

class UnknownActionBuilderException(msg: String, path: List[String], ex: Throwable)
  extends SomerActionBuilderException(msg, path, ex) {

  def this(msg: String, path: List[String]) = this(msg, path, null)

  def this(path: List[String], ex: Throwable) = this("Unknown error", path, ex)
}

class DirExistsWhereFileExpectedActionBuilderException(path: List[String])
  extends SomerActionBuilderException("Directory exists where file was expected", path)

class FileExistsWhereDirExpectedActionBuilderException(path: List[String])
  extends SomerActionBuilderException("File exists where directory was expected", path)
