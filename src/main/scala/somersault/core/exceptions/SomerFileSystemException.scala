package somersault.core.exceptions

import somersault.util.FilenameUtils2

/**
 * The base class for Exceptions to be thrown by FileSystem implementations. These exceptions are expected to be thrown
 * when something goes wrong inside a FileSystem.
 *
 * @param msg A short message. The path will be appended to this message.
 * @param path Describes the path of the SomerArtifact. The path starts at index 0 and descends to N.
 *             Items at the root should have only their name as the path.
 */

abstract class SomerFileSystemException(val msg: String, val path: List[String], ex: Throwable)
  extends Exception(msg + ": " + FilenameUtils2.dirSeparator + FilenameUtils2.concat(path), ex) {

  def this(msg: String, path: List[String]) = this(msg, path, null)
}

class UnknownException(msg: String, path: List[String], ex: Throwable)
  extends SomerFileSystemException(msg, path, ex) {

  def this(msg: String, path: List[String]) = this(msg, path, null)
}

/**
 *
 * @param path This path may not be complete, but it should at least contain the name of the artifact in question.
 */
class PathDoesNotExistException(path: List[String], ex: Throwable)
  extends SomerFileSystemException("Path does not exist", path, ex) {

  def this(path: List[String]) = this(path, null)
}

class AlreadyExistsException(path: List[String], ex: Throwable)
  extends SomerFileSystemException("Directory or file already exists", path, ex) {

  def this(path: List[String]) = this(path, null)
}

class CannotCreateDirException(path: List[String], ex: Throwable)
  extends SomerFileSystemException("Cannot write to file", path, ex) {

  def this(path: List[String]) = this(path, null)
}

class CannotWriteFileException(path: List[String], ex: Throwable)
  extends SomerFileSystemException("Cannot write to file", path, ex) {

  def this(path: List[String]) = this(path, null)
}

class CannotReadFileException(path: List[String], ex: Throwable)
  extends SomerFileSystemException("Cannot read from file", path, ex) {

  def this(path: List[String]) = this(path, null)
}
