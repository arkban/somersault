package somersault.util

import java.util._

/**
 * Adapter wraps multiple Comparators. This executes each comparator, returning the first non-zero result. If all
 * comparators return zero, this returns zero.
 */
class MultiComparator[T](val comparators: Iterable[Comparator[T]]) extends Comparator[T] {

  def this(comparators: Comparator[T]*) = this(comparators)

  def compare(p1: T, p2: T): Int = {
    comparators.foreach(
      c => {
        val result = c.compare(p1, p2)
        if (result != 0)
          return result
      })
    0
  }
}