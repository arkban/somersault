package somersault.util

import java.io.{FileFilter, File}
import javax.sql.DataSource
import org.apache.commons.io.FileUtils
import org.apache.commons.lang.IllegalClassException
import org.h2.jdbcx.JdbcConnectionPool

/**
 * A base class for all DataAccessObjects that use an H2 database.
 *
 * Currently this implementation creates a new database on construction and deletes it when the JVM exists. This will
 * most likely need to be modified to open an existing database if specified.
 */

trait H2DataAccessObject extends DataAccessObject {

  /**
   * Creates a database at the specified file, and creates the schema using createSchema(). Only if the database does
   * not previously exist will the schema be created.
   *
   * This will call initialize() before returning the DataSource.
   *
   * @param file The file that will be used for the index. Note that H2 index files always have a ".h2.db" extension,
   *             so this will be added if necessary.
   * @param deleteIfExists If true, this will delete any existing database, otherwise will create a new one.
   */
  protected def createDataSource(file: File, deleteIfExists: Boolean): DataSource = {
    // H2 indexes add the databaseFileExt whether it exists already or not. further it makes other files with
    // different extensions. so this is getting that file path without that extension
    val rootFilePath =
      if (file.getName.toLowerCase.endsWith(H2DataAccessObject.databaseFileExt))
        new File(
          file.getParentFile, file.getName.substring(
            0, file.getName.length - H2DataAccessObject.databaseFileExt.length))
      else
        file

    // H2 uses multiple files, all with the same prefix
    val parent = file.getParentFile
    val dbFiles = parent.listFiles(
      new FileFilter() {def accept(f: File) = f.getName.startsWith(rootFilePath.getName)})

    val exists = !dbFiles.isEmpty

    // if a file exists, move it and delete it. (we move and delete because delete may not be instantaneous on all
    // operating systems
    if (deleteIfExists && exists) {
      dbFiles.foreach(
        f => {
          try {
            FileUtils.forceDelete(f)
          }
          catch {
            case ex: Exception =>
              val newFilePath = new File(f + "." + FilenameUtils2.generateUniqueStamp() + ".delete")
              f.renameTo(newFilePath)
              FileUtils.forceDeleteOnExit(newFilePath)
          }
        })
    }

    // the TRACE_LEVEL_FILE=4 enables logging via SLF4J
    val ds = JdbcConnectionPool.create("jdbc:h2:file:" + rootFilePath + ";TRACE_LEVEL_FILE=4", "sa", "sa")

    if (!exists)
      createSchema(ds)

    initialize(ds)

    ds
  }

  /**
   * Disconnects and re-connects to the database. This forces a flush of all data to the database.
   *
   * @param dataSource The original dataSource
   * @param file The data file needed to re-open the dataSource.
   * @return The newly opened dataSource.
   */
  protected def flush(dataSource: DataSource, file: File): DataSource = {
    dataSource match {
      case jcp: JdbcConnectionPool =>
        jcp.dispose()
        createDataSource(file, deleteIfExists = false)
      case _ => throw new IllegalClassException(classOf[JdbcConnectionPool], dataSource)
    }
  }

  /**
   * Closes all unused pool connections.
   */
  protected def dispose(dataSource: DataSource) {
    dataSource match {
      case jcp: JdbcConnectionPool => jcp.dispose()
      case _ => throw new IllegalClassException(classOf[JdbcConnectionPool], dataSource)
    }
  }
}

object H2DataAccessObject {

  val databaseFileExt = ".h2.db"
}
