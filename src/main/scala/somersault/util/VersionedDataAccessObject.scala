package somersault.util

import javax.sql.DataSource

/**
 * A DAO whose schema is versioned. The schemaVersion is stored in metadata (using MetaDataAccessObject), and can be
 * retrieved and verified. The intent of this to help with identifying and upgrading between different versions of the
 * schema.
 */
trait VersionedDataAccessObject extends MetaDataAccessObject {

  private val keyMajor: String = "VER_MAJOR"
  private val keyMinor: String = "VER_MINOR"
  private val keyRelease: String = "VER_RELEASE"

  /**
   * The expected version of the DAO.
   */
  def schemaVersion: Version

  /**
   * Writes the schemaVersion to the DataSource.
   */
  override def createSchema(dataSource: DataSource) {
    super.createSchema(dataSource)

    writeVersion(dataSource, schemaVersion)
  }

  /**
   * Reads the version currently stored in the schema.
   */
  def readVersion(dataSource: DataSource): Version =
    new Version(
      Integer.parseInt(get(dataSource, keyMajor)),
      Integer.parseInt(get(dataSource, keyMinor)),
      Integer.parseInt(get(dataSource, keyRelease)))

  /**
   * Writes the provided version to the database
   */
  def writeVersion(dataSource: DataSource, version: Version) {
    set(dataSource, keyMajor, Integer.toString(version.major))
    set(dataSource, keyMinor, Integer.toString(version.minor))
    set(dataSource, keyRelease, Integer.toString(version.release))
  }
}