package somersault.util

import javax.sql.DataSource
import org.springframework.dao.IncorrectResultSizeDataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate

/**
 * A DAO for storing constant or rarely changing metadata in a data source.
 *
 * This creates a single METADATA table that is a generic key, value store. This allows multiple types of metadata to
 * be stored without conflict and without lots of extra tables. Because a generic key, value table is slow, this table
 * should only be used for slow changing and rarely read data. (If your metadata does not fit this description, it is
 * probably faster and easier to use a custom table.)
 */

trait MetaDataAccessObject extends DataAccessObject {

  private val tableMetaData = "METADATA"
  private val colKey = "KEY"
  private val colValue = "VALUE"

  private val keyAppName = "APP_NAME"
  private val valueAppName = "somersault"

  /**
   * Creates and populates the metadata table, inserting some basic information.
   */
  override def createSchema(dataSource: DataSource) {
    super.createSchema(dataSource)

    val jdbc = new SimpleJdbcTemplate(dataSource)

    val statement = (
      "CREATE TABLE " + tableMetaData + " ( "
        + colKey + " VARCHAR(64) PRIMARY KEY, "
        + colValue + " VARCHAR(64) )")

    jdbc.getJdbcOperations.update(statement)

    // set basic info
    jdbc.update(
      "INSERT INTO " + tableMetaData + " ( " + colKey + " ," + colValue + " ) VALUES ( ?, ? )",
      keyAppName,
      valueAppName)
  }

  /**
   * Checks basic metadata stored in the table.
   */
  override def initialize(dataSource: DataSource) {
    if (get(dataSource, keyAppName) != valueAppName)
      throw new IllegalArgumentException(
        "expected app name \"" + valueAppName + "\" but found \"" + get(dataSource, keyAppName) + "\"")
  }

  /**
   * Returns true if the key exists.
   */
  def exists(dataSource: DataSource, key: String): Boolean = {
    val sql = ("SELECT COUNT(" + colKey + ") FROM " + tableMetaData + " WHERE " + colKey + " = ?")
    val jdbc = new JdbcTemplate(dataSource)
    jdbc.queryForInt(sql, key) match {
      case 0 => false
      case 1 => true
      case _ => throw new IncorrectResultSizeDataAccessException(1)
    }
  }

  /**
   * Gets the value for the provided key.
   *
   * @throws DataAccessException If key does not exist
   */
  def get(dataSource: DataSource, key: String): String = {
    val sql = ("SELECT " + colValue + " FROM " + tableMetaData + " WHERE " + colKey + " = ?")
    val jdbc = new JdbcTemplate(dataSource)
    jdbc.queryForObject[String](sql, classOf[String], key)
  }

  /**
   * Sets a value for the provided key.
   */
  def set(dataSource: DataSource, key: String, value: String) = {
    val sql = ("MERGE INTO " + tableMetaData + " ( " + colKey + ", " + colValue + " ) VALUES ( ?, ? )")
    val jdbc = new SimpleJdbcTemplate(dataSource)
    jdbc.update(sql, key, value)
  }
}