package somersault.util.persist

import java.io.File

/**
 * Contains all the information needed to read or write
 */
class PersisterContext( val container: PersisterContainer, inputFile: File)
{
  /**
   * The absolute path to the current file that is being read from or written to for this persisting effort. This is
   * useful for {@link Persister}s that may need to access others files; they can use this to read or write other
   * files relative to this file.
   *
   * It is not technically necessary to be an absolute path, but it simplifies the uses if this is absolute.
   */
  val filePath = inputFile.getAbsoluteFile
}