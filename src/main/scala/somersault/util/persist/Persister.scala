package somersault.util.persist

import java.io._
import nu.xom.{Builder, Document, Elements, Attribute, Comment, ParentNode, Serializer, Element}
import somersault.core.{ScenarioDataPersister, ScenarioData}

/**
 * Writes and reads a single class. This is encapsulated in one place to make it easy to build lots of these objects
 * and orchestrate them together for more complete functionality.
 *
 * Somersault performs manual serialization and de-serialization to simplify reading old versions of XML documents.
 * Persisters are designed to be combined together in a hierarchy, with Persisters using other Persisters
 * for each distinct part of the XML document. When a change is needed, a new Persister will be built to handle
 * the change to that part of the XML document. But the old Persisters and hierarchy may still be maintained to
 * allow old XML documents to be read.
 *
 * The orchestration of these hierarchies will check a well-know never-changing version identifier. The Persister
 * object will examine an XML document, check the version, and build the appropriate Persisters.
 *
 * The question remains why can this not be done with a simpler XML serialization technique, like one provided by JAXB,
 * XStream, or Simple? Those techniques (AFAIK) skip the intermediate DOM step and write data directly into the objects.
 * For this to work with versioning, the entire hierarchy of objects must be duplicated for each version. This leads to
 * a lot of classes, and a lot of testing to ensure that something didn't break in the process. (My arguments are based
 * on my professional experience, where I have used and maintain code that uses direct XML serialization.)
 */
trait Persister {

  /**
   * Returns true if this class can be used to construct an object from the provided element. This can only be used
   * on Elements previously written with dynamic set to true in the call to write(). If dynamic was set to false,
   * this method is free to return false or throw an Exception.
   */
  def canRead(context: PersisterContext, element: Element): Boolean

  /**
   * Creates an object from the provided Element. The T is to eliminate and simplify casting in many places. This
   * method is only safe to call if a) you have confirmed that this is the correct instance for the Element you are
   * providing or b) canRead() returns true for the same Element.
   */
  def read[T](context: PersisterContext, element: Element): T

  /**
   * Writes a single object as ac child of the provided element. Attributes a can be added to the provided, but the
   * Element's name must NOT change.
   *
   * @param dynamic If this is true additional information will be written to allow dynamically determining the type,
   *                via the canRead() method.
   */
  def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false)

  /**
   * Returns true if the provided element contains the class name written by writeClass().
   */
  protected def checkClass[T](element: Element, expectedClass: Class[T]): Boolean = {
    val classAttribute = element.getAttribute("class")
    classAttribute != null && classAttribute.getValue == expectedClass.getName
  }

  /**
   * Writes the class name to the provide element. This will be do nothing if dynamic is false.
   *
   * This method works with the default implementation of canRead(). If that default implementation is not used then
   * this method may not be useful.
   */
  protected def writeClass(obj: Object, element: Element, dynamic: Boolean = false) =
    if (dynamic) Persister.createAttribute("class", obj.getClass.getName, element)
}

object Persister {

  def write(rootElementName: String, writeFunc: Element => Unit, out: OutputStream) {
    val root = createElement(rootElementName)
    writeInfo(root)
    writeFunc(root)
    createSerializer(out).write(new Document(root))
  }

  def write(rootElementName: String, writeFunc: Element => Unit, filePath: File) {
    val output = new FileOutputStream(filePath, false)
    try {
      write(rootElementName, writeFunc, output)
    }
    finally {
      output.close()
    }
  }

  /**
   * Loads the {@link ScenarioData} from the provided file.
   */
  def readScenarioData(filePath: File): ScenarioData = {
    val context = new PersisterContext(PersisterSuite.create(), filePath)
    def fn(element: Element): ScenarioData = new ScenarioDataPersister().read(context, element)
    read(filePath, fn)
  }

  /**
   * Loads an object, using a provided function. This will change the working directory to the path of the file (if
   * the filePath has a parent path).
   */
  def read[T](filePath: File, func: Element => T): T = {
    if (!filePath.exists)
      throw new FileNotFoundException(filePath.getAbsolutePath)

    val input = new FileInputStream(filePath)
    try {
      val builder = new Builder
      val document = builder.build(input)
      func(document.getRootElement)
    }
    finally {
      input.close()
    }
  }

  private def writeInfo(parent: Element) = {
    createAttribute("appName", "somersault", parent)
    createAttribute("version", "0.1", parent)
  }

  private def createSerializer(out: OutputStream) = {
    val serializer = new Serializer(out)
    serializer.setIndent(4)
    serializer.setMaxLength(79)
    serializer
  }

  /**
   * Creates an element with the (optional) specified text, under the (optional) specified parent, with the (optional)
   * provided comment. (Comments can ONLY be added if there is a parent specified).
   */
  def createElement(name: String, value: String = null, parent: ParentNode = null, comment: String = null) = {
    val elem = new Element(name)

    if (value != null)
      elem.appendChild(value)

    if (parent != null) {
      // put comment before node (but only if parent)
      if (comment != null)
        parent.appendChild(new Comment(comment))
      parent.appendChild(elem)
    }

    elem
  }

  /**
   * Creates an attribute with the specified text, under the specified parent
   */
  def createAttribute(name: String, value: String, parent: Element) = {
    val attribute = new Attribute(name, value.toString)
    parent.addAttribute(attribute)
    attribute
  }

  implicit def elementsToIterable(elements: Elements): Iterable[Element] =
    (0 until elements.size).map(index => elements.get(index))

  /**
   * Find a child element with the provided name. This will throw an exception if no element with the name exists.
   */
  def getChildElement(parent: Element, name: String): Element =
    elementsToIterable(parent.getChildElements).find(_.getLocalName == name) match {
      case Some(e: Element) => e
      case None => throw new IllegalArgumentException("cannot find \"" + name + "\" element")
    }

  /**
   * Find a child element with the provided name.
   */
  def findChildElement(parent: Element, name: String): Option[Element] =
    elementsToIterable(parent.getChildElements).find(_.getLocalName == name)
}
