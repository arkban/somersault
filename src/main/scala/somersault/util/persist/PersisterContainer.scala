package somersault.util.persist

import net.jcip.annotations.ThreadSafe
import nu.xom.Element
import scala.collection.mutable

/**
 * A container class for multiple {@link Persister}s. This class allows registering {@link Persister}s by class name
 * to simplify constructing objects. The implementation of the {@link Persister} trait is to keep the familiar
 * methods used to read and write persistable objects.
 *
 * This class is design to be shared with other classes (including other {@link Persister}s) to help build complex
 * graphs of objects.
 */
@ThreadSafe
class PersisterContainer extends Persister {

  /**
   * Map from class name (from class.getName()) to Persister.
   */
  private val helpers = new mutable.HashMap[String, Persister]

  /**
   * Adds a Persister registered with the provided class name (from class.getName()). The class name used as a key
   * should be the concrete classes; using base classes could cause issues.
   */
  def add(className: String, pw: Persister) { helpers += (className -> pw) }

  def add[T](clazz: Class[T], pw: Persister) { add(clazz.getName, pw) }

  def add(pwc: PersisterContainer) { pwc.helpers.foreach(t => add(t._1, t._2)) }

  override def canRead(context: PersisterContext, element: Element) =
    helpers.values.find(_.canRead(context, element)).isDefined

  def read[T](context: PersisterContext, element: Element) =
    helpers.values.find(_.canRead(context, element)) match {
      case Some(p: Persister) => p.read[T](context, element)
      case _ => throw new IllegalArgumentException("cannot read: " + element.toXML)
    }

  def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false) {
    helpers.get(obj.getClass.getName) match {
      case Some(p: Persister) => p.write(context, obj, element, dynamic)
      case None => throw new IllegalArgumentException("no Persister registered for: " + obj.getClass.getName)
    }
  }
}
