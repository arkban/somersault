package somersault.util.persist

import somersault.core.comparators._
import somersault.core.encode._
import somersault.core.filesystems._
import somersault.core.filesystems.amazonS3._
import somersault.core.filesystems.local._
import somersault.core.filters._
import somersault.core.{ScenarioDataPersister, ScenarioData}
import somersault.util.{VersionPersister, Version, HashInfo, HashInfoPersister}

/**
 * Returns {@link PersisterContainer}s for different versions of Somersault.
 */
object PersisterSuite {

  def create(): PersisterContainer = {
    val pwc = new PersisterContainer
    pwc.add(classOf[ScenarioData], new ScenarioDataPersister)
    // encoders
    pwc.add(classOf[GZIPCompressionProvider], new GZIPCompressionProviderPersister)
    pwc.add(classOf[BZIP2CompressionProvider], new BZIP2CompressionProviderPersister)
    pwc.add(classOf[BouncyCastleEncryptionProvider], new BouncyCastleEncryptionProviderPersister)
    pwc.add(classOf[NullCompressionProvider], new NullCompressionProviderPersister)
    pwc.add(classOf[NullEncryptionProvider], new NullEncryptionProviderPersister)
    // filesystems
    pwc.add(classOf[AmazonS3FileSystemData], new AmazonS3FileSystemDataPersister)
    pwc.add(classOf[AmazonS3FileSystem.Credentials], new AmazonS3FileSystem.CredentialsPersister)
    pwc.add(classOf[LocalDirectFileSystemData], new LocalDirectFileSystemDataPersister)
    pwc.add(classOf[LocalIndexedFileSystemData], new LocalIndexedFileSystemDataPersister)
    // comparators
    pwc.add(classOf[BooleanComparator], new BooleanComparatorPersister)
    pwc.add(classOf[HashComparator], new HashComparatorPersister)
    pwc.add(classOf[ModifyDateComparator], new ModifyDateComparatorPersister)
    pwc.add(classOf[MultiSomerFileComparator], new MultiSomerFileComparatorPersister)
    pwc.add(classOf[SizeComparator], new SizeComparatorPersister)
    pwc.add(classOf[SharedComparator], new SharedComparatorPersister)
    // filters
    pwc.add(classOf[AndFilter], new AndFilterPersister)
    pwc.add(classOf[BooleanFilter], new BooleanFilterPersister)
    pwc.add(classOf[DepthFilter], new DepthFilterPersister)
    pwc.add(classOf[DirectoryFilter], new DirectoryFilterPersister)
    pwc.add(classOf[FileAttributesFilter], new FileAttributesFilterPersister)
    pwc.add(classOf[FileFilter], new FileFilterPersister)
    pwc.add(classOf[NameFilter], new NameFilterPersister)
    pwc.add(classOf[NotFilter], new NotFilterPersister)
    pwc.add(classOf[OrFilter], new OrFilterPersister)
    pwc.add(classOf[PathFilter], new PathFilterPersister)
    pwc.add(classOf[SharedFilter], new SharedFilterPersister)
    pwc.add(classOf[SizeFilter], new SizeFilterPersister)
    // util
    pwc.add(classOf[HashInfo], new HashInfoPersister)
    pwc.add(classOf[Version], new VersionPersister)
    pwc.add(classOf[PeriodicFileSystemEventControllerData], new PeriodicFileSystemEventControllerDataPersister)

    pwc
  }
}
