package somersault.util.persist

import nu.xom.Element
import java.io.File

/**
 * A helper trait for writing persistables that put their contents in external files. They are called "shared" because
 * writing the contents to external files allows them to shared and allows reducing duplication between configuration
 * files.
 */
trait SharedPersister
{
  /**
   * Reads the {@link File} written by {@link #writeFilePath()}. The returned path may be absolute or relative.
   */
  protected def readRawFilePath( element: Element) = new File( Persister.getChildElement( element, "path").getValue)

  /**
   * Reads an object from the the provided {@link File}.
   *
   * @param rawFilePath This should be the raw file path, as expected by {@link #readRawFilePath} or
   * {@link #writeRawFilePath}
   */
  protected def readSharedObject[ T ]( context: PersisterContext, rawFilePath: File): T =
  {
    val newFilePath =
      if( rawFilePath.isAbsolute ) rawFilePath
      else new File( context.filePath.getParent, rawFilePath.getPath)

    // new context based on the changed path
    val newContext = new PersisterContext( context.container, newFilePath)

    // read from external file
    def fn( root: Element): T = newContext.container.read( newContext, root).asInstanceOf[ T ]
    Persister.read( newFilePath, fn).asInstanceOf[ T ]
  }

  /**
   * Writes the provided {@link File} to the provided {@link Element}. This will be the path of the shared object.
   * This may be absolute or relative.
   */
  protected def writeRawFilePath( element: Element, file: File)
  {
    Persister.createElement( "path", file.getPath, element, "path to external file")
  }

  /**
   * Writes the provided object to the provided {@link File}.
   *
   * @param rawFilePath This should be the raw file path, as expected by {@link #readRawFilePath} or
   * {@link #writeRawFilePath}
   */
  protected def writeSharedObject( context: PersisterContext, rawFilePath: File, obj: Object)
  {
    // construct the complete file path to write to
    val newFilePath =
      if( rawFilePath.isAbsolute ) rawFilePath
      else new File( context.filePath.getParentFile, rawFilePath.getPath)

    // new context based on the changed path
    val newContext = new PersisterContext( context.container, newFilePath)

    // write to external shared file
    newFilePath.getParentFile.mkdirs
    val fn = ( element: Element) => newContext.container.write( newContext, obj, element, true)
    Persister.write( "shared", fn, newFilePath)
  }
}
