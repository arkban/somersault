package somersault.util

import java.io.File
import java.lang.String
import java.util.Date
import logahawk.formatters.{StandardMessageFormatter, ArgumentFormatter, FormatterSigFormatter, SignatureFormatter}
import logahawk.listeners.Listener
import logahawk.{SimpleLogger, Utils, Severity}
import org.apache.commons.io.FilenameUtils
import org.apache.commons.lang.StringEscapeUtils
import org.apache.log4j.{DailyRollingFileAppender, Appender, PatternLayout, Level}

/**
 * Standard logger for Somersault.
 */
object Logger extends logahawk.SimpleLogger {

  LoggerUtil.addStandardFormatters(this)

  /**
   * Creates the standard Listener for Somersault, which logs using Log4j. This also setups up the root Log4j
   * appender to be used by the various libraries that use commons-logging.
   */
  def createStandardListener(): Listener = {
    val log4jConfig = new File(FilenameUtils.concat("config", "somersault.log4j.xml"))
    val listener = if (log4jConfig.exists) {
      org.apache.log4j.xml.DOMConfigurator.configure(log4jConfig.getAbsolutePath)
      new Log4jListener(org.apache.log4j.Logger.getLogger(LoggerUtil.somerLoggerName))
    }
    else {
      val filename = "logs/somersault.log"
      val datePattern = "'.'yyyy-MM-dd'.log'"

      val rootAppender = new DailyRollingFileAppender(LoggerUtil.rootLog4jLayout, filename, datePattern)
      rootAppender.setThreshold(Level.INFO)
      LoggerUtil.createLog4jLogger(None, rootAppender, additive = true)

      val somerAppender = new DailyRollingFileAppender(LoggerUtil.somerLog4jLayout, filename, datePattern)
      somerAppender.setThreshold(Level.DEBUG)
      val log4j = LoggerUtil.createLog4jLogger(Some(LoggerUtil.somerLoggerName), somerAppender, additive = false)

      new Log4jListener(log4j)
    }
    listener
  }

  /**
   * Escapes any % characters.
   */
  def escape(s: String): String = StringEscapeUtils.escapeJava(s).replaceAll("%", "%%")
}

object LoggerUtil {

  val rootLog4jLayout = new PatternLayout("%d{yyyy MM HH'T'hh:mm:ss.SSS'Z'} %-5p: %m%n")

  val somerLog4jLayout = new PatternLayout("%m%n")

  val somerLoggerName = "somersault"

  def addStandardFormatters(logger: SimpleLogger) {
    val sigFormatters = new java.util.ArrayList[SignatureFormatter]
    sigFormatters.add(new FormatterSigFormatter)
    Utils.addStandardFormatters(logger, sigFormatters, new java.util.ArrayList[ArgumentFormatter])
  }

  /**
   * Sets up a Log4j logger with the provided appender, and returns it. The returned logger does not need to be used,
   * which is useful when setting up Loggers for libraries that use commons-logging.
   */
  def createLog4jLogger(name: Option[String], appender: Appender, additive: Boolean): org.apache.log4j.Logger = {
    val logger = name match {
      case Some(s) => org.apache.log4j.Logger.getLogger(s)
      case None => org.apache.log4j.Logger.getRootLogger
    }
    logger.setAdditivity(additive)
    logger.removeAllAppenders()
    logger.addAppender(appender)
    logger
  }
}

class Log4jListener(logger: org.apache.log4j.Logger) extends Listener {

  private val msgFormatter = new StandardMessageFormatter

  def log(severity: Severity, epoch: Date, message: String) {
    val level = convert(severity)

    if (logger.isEnabledFor(level))
      logger.log(level, msgFormatter.format(severity, epoch, message))
  }

  private def convert(severity: Severity): Level =
    severity match {
      case Severity.DEBUG => Level.DEBUG
      case Severity.INFO => Level.INFO
      case Severity.ALERT => Level.INFO
      case Severity.WARN => Level.WARN
      case Severity.ERROR => Level.ERROR
      case Severity.FATAL => Level.FATAL
      case Severity.PANIC => Level.FATAL
      case _ => Level.INFO
    }
}
