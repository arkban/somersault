package somersault.util

import java.io.{FilterOutputStream, OutputStream}
import java.util.EventListener
import net.jcip.annotations.ThreadSafe
import scala.collection.mutable

/**
 * An OutputStream that will inform other classes about various events. This wraps an existing OutputStream and forwards
 * all data sent to this class to the wrapped class without changes, but may also call various listeners if any are
 * registered.
 *
 * @param length Total length of the data to be read (if known).
 */

@ThreadSafe
class ObservableOutputStream(output: OutputStream, length: Option[Long]) extends FilterOutputStream(output) {

  private val listeners = new mutable.ListBuffer[ObservableOutputStreamListener]

  def addListener(l: ObservableOutputStreamListener) {
    listeners += l
    l.init(this, length)
  }

  def removeListener(l: ObservableOutputStreamListener) { listeners -= l }

  override def write(b: Int) {
    // intentionally calling output to use the most efficient write() method possible (see FilterOutputStream docs)
    output.write(b)
    listeners.foreach(_.write(this, Array(b.asInstanceOf[Byte]), 0, 1))
  }

  override def write(b: Array[Byte], off: Int, len: Int) {
    // intentionally calling output to use the most efficient write() method possible (see FilterOutputStream docs)
    output.write(b, off, len)
    listeners.foreach(_.write(this, b, off, len))
  }

  override def write(b: Array[Byte]) {
    // intentionally calling output to use the most efficient write() method possible (see FilterOutputStream docs)
    output.write(b)
    listeners.foreach(_.write(this, b, 0, b.length))
  }

  override def close() {
    super.close()
    listeners.foreach(_.close(this))
  }
}

trait ObservableOutputStreamListener extends EventListener {

  /**
   * Called when ObservableOutputStream is first created.
   */
  def init(output: ObservableOutputStream, length: Option[Long]) {}

  /**
   * All write() calls on the observed stream will be passed through this method.
   */
  def write(output: ObservableOutputStream, b: Array[Byte], off: Int, len: Int) {}

  /**
   * Called after the stream is closed.
   */
  def close(output: ObservableOutputStream) {}
}

class NullObservableOutputStreamListener extends ObservableOutputStreamListener

class MultiObservableOutputStreamListener
  extends AbstractMultiObserver[ObservableOutputStreamListener] with ObservableOutputStreamListener {

  override def init(output: ObservableOutputStream, length: Option[Long]) {
    observers.foreach(_.init(output, length))
  }

  override def write(output: ObservableOutputStream, b: Array[Byte], off: Int, len: Int) {
    observers.foreach(_.write(output, b, off, len))
  }

  override def close(output: ObservableOutputStream) { observers.foreach(_.close(output)) }
}
