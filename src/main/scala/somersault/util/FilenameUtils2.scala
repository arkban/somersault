package somersault.util

import java.io.File
import java.util.regex.Pattern
import org.apache.commons.io.{FileCleaningTracker, FileDeleteStrategy, FileUtils, FilenameUtils}
import org.apache.commons.lang.StringUtils
import org.joda.time.DateTime

/**
 * Additional utility methods for File names, based on and in the spirit of Apache Commons IO FilenameUtils.
 */
object FilenameUtils2 {

  private val fileCleaningTracker = new FileCleaningTracker

  /**
   * Directory separator used in concat().
   */
  val dirSeparator = "/"

  val uniqueStamp = generateUniqueStamp()

  lazy val tempPath = createTempPath()

  /**
   * Concatenates multiple paths together at once. The returned path does not have a "/" prefix.
   */
  def concat(pathToAdd: Iterable[String]): String = {
    val sb = new StringBuilder()

    pathToAdd.filter(_ != null).foreach(
      p => {
        val prefix: Int = FilenameUtils.getPrefixLength(p)
        if (prefix < 0)
          throw new IllegalArgumentException("invalid path: " + p)

        if (prefix > 0)
          sb.delete(0, sb.length)
        else if (sb.length > 0 && !sb.endsWith(dirSeparator))
          sb.append(dirSeparator)

        sb.append(p)
      })

    sb.toString()
  }

  /**
   * Concatenates multiple paths together at once. The returned path does not have a "/" prefix.
   */
  def concat(pathToAdd: String*): String = concat(pathToAdd.toList)

  /**
   * Creates a random file object in the parent directory with the provided prefix and extension. This will ensure that
   * the filename created does not exist in the parent directory by inserting an incrementing identifier between the
   * prefix and extension (if necessary). Note this does NOT try and create the file.
   */
  def createTempFile(prefix: String, ext: String, dir: File): File = {
    if (prefix == null)
      throw new IllegalArgumentException("prefix cannot be null")

    val finalExt = if (ext.startsWith(".")) ext else "." + ext

    var index = 0
    var result = new File(dir, String.format("%1$s%2$s", prefix, finalExt))
    while (result.exists()) {
      result = new File(dir, "%1$s_%2$d%3$s".format(prefix, index, finalExt))
      index += 1
    }
    result
  }

  /**
   * Creates a "unique" string that can be appended to file or directory names to make them unique. The primary usage
   * is to make file/directory names unique before deleting. (Delete in Java is often flaky and may not occur
   * immediately.)
   *
   * This uses a timestamp and a random number to ease differentiating two usages that occur close in time (less than
   * one second).
   */
  def generateUniqueStamp(): String = {
    // create new unit test directory, using a date for when we ran the test and a random number to make it easier
    // to identify the test directory when walking the file system
    val ts = new DateTime().toString("HHmmss")
    val ran = StringUtils.leftPad(Integer.toHexString(new java.util.Random().nextInt(0xFFFF)), 4, '0')
    ts + "." + ran
  }

  /**
   * Creates and returns a temporary path name for Somersault. The path will be under the system temporary directory,
   * under a "somersault" sub-directory, under a unique sub-directory whose name contains a timestamp and a random
   * number. The path will be created by this method, and set to be deleted when the JVM stops.
   */
  private def createTempPath(): File = {
    val tempParentPath = new File(FilenameUtils.concat(System.getProperty("java.io.tmpdir"), "somersault"))

    // delete older unit tests directories
    if (tempParentPath.exists) {
      val origList = tempParentPath.list.toList
      val keepCount = if (origList.size > 3) origList.size - 3 else 0
      val deleteList = origList.sortWith(_.compareTo(_) < 0).take(keepCount)
      // delete all but the newest 3 unit test directories
      deleteList.foreach(
        path => {
          try {
            FileUtils.forceDelete(new File(path))
          }
          catch {
            case _: Exception =>
              val deleteTempPath = new File(path + "." + generateUniqueStamp() + ".delete")
              tempParentPath.renameTo(deleteTempPath)
              fileCleaningTracker.track(deleteTempPath, this, FileDeleteStrategy.FORCE)
          }
        })
    }

    val tempRunPath = new File(FilenameUtils.concat(tempParentPath.getAbsolutePath, uniqueStamp))
    FileUtils.forceMkdir(tempRunPath)
    tempRunPath
  }

  /**
   * Get the relative path from one file to another, specifying the directory separator.
   * If one of the provided resources does not exist, it is assumed to be a file unless it ends with '/' or '\'.
   *
   * Ported from http://stackoverflow.com/questions/204784/how-to-construct-a-relative-path-in-java-from-two-absolute-paths-or-urls
   *
   * @param targetPath targetPath is calculated to this file
   * @param basePath basePath is calculated from this file
   * @param pathSeparator directory separator. The platform default is not assumed so that we can test Unix behaviour
   *                      when running on Windows (for example)
   * @return
   */
  def getRelativePath(targetPath: String, basePath: String, pathSeparator: String): String = {
    // Normalize the paths
    var normalizedTargetPath = FilenameUtils.normalizeNoEndSeparator(targetPath)
    var normalizedBasePath = FilenameUtils.normalizeNoEndSeparator(basePath)

    // Undo the changes to the separators made by normalization
    if (pathSeparator.equals("/")) {
      normalizedTargetPath = FilenameUtils.separatorsToUnix(normalizedTargetPath)
      normalizedBasePath = FilenameUtils.separatorsToUnix(normalizedBasePath)
    }
    else if (pathSeparator.equals("\\")) {
      normalizedTargetPath = FilenameUtils.separatorsToWindows(normalizedTargetPath)
      normalizedBasePath = FilenameUtils.separatorsToWindows(normalizedBasePath)
    }
    else {
      throw new IllegalArgumentException("Unrecognised dir separator '" + pathSeparator + "'")
    }

    val base = normalizedBasePath.split(Pattern.quote(pathSeparator))
    val target = normalizedTargetPath.split(Pattern.quote(pathSeparator))

    // First get all the common elements. Store them as a string,
    // and also count how many of them there are.
    val common = new StringBuffer()

    var commonIndex = 0
    while (commonIndex < target.length &&
      commonIndex < base.length &&
      target(commonIndex).equals(base(commonIndex))) {
      common.append(target(commonIndex) + pathSeparator)
      commonIndex += 1
    }

    if (commonIndex == 0) {
      // No single common path element. This most likely indicates differing drive letters, like C: and D:.
      // These paths cannot be relative-ized.
      throw new PathResolutionException(
        "No common path element found for '" + normalizedTargetPath + "' and '" + normalizedBasePath + "'")
    }

    // The number of directories we have to backtrack depends on whether the base is a file or a dir
    // For example, the relative path from
    //
    // /foo/bar/baz/gg/ff to /foo/bar/baz
    //
    // ".." if ff is a file
    // "../.." if ff is a directory
    //
    // The following is a heuristic to figure out if the base refers to a file or dir. It's not perfect, because
    // the resource referred to by this path may not actually exist, but it's the best I can do
    val baseIsFile: Boolean = {
      val baseResource = new File(normalizedBasePath)

      if (baseResource.exists()) baseResource.isFile
      else if (basePath.endsWith(pathSeparator)) false
      else true
    }

    val relative = new StringBuffer()

    if (base.length != commonIndex) {
      val numDirsUp = if (baseIsFile) base.length - commonIndex - 1 else base.length - commonIndex

      (0 until numDirsUp).foreach(_ => relative.append(".." + pathSeparator))
    }
    relative.append(normalizedTargetPath.substring(common.length()))
    relative.toString
  }

  class PathResolutionException(msg: String) extends RuntimeException(msg)

}