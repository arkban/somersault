package somersault.util

import java.util._

/**
 * Negates the result of another comparator.
 */
class NegatingComparator[T](comparator: Comparator[T]) extends Comparator[T] {

  def compare(p1: T, p2: T) = -comparator.compare(p1, p2)
}