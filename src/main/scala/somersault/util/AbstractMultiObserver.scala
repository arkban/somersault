package somersault.util

import java.util.concurrent.CopyOnWriteArrayList
import net.jcip.annotations.ThreadSafe
import scala.collection.convert.Wrappers

/**
 * Base class for Observer implementations that broadcast to other Observers
 */

@ThreadSafe
abstract class AbstractMultiObserver[T] {

  protected val observers = Wrappers.JListWrapper(new CopyOnWriteArrayList[T])

  def add(obs: T) = observers += obs

  def remove(obs: T) = observers -= obs

  def clear() { observers.clear() }
}