package somersault.util

import java.io.{FilterInputStream, InputStream}
import java.util.EventListener
import net.jcip.annotations.ThreadSafe
import scala.collection.mutable

/**
 * An InputStream that will inform other classes about various events. This wraps an existing InputStream and forwards
 * all data sent to this class to the wrapped class without changes, but may also call various listeners if any are
 * registered.
 *
 * @param length Total length of the data to be read (if known).
 */

@ThreadSafe
class ObservableInputStream(input: InputStream, length: Option[Long]) extends FilterInputStream(input) {

  private val listeners = new mutable.ListBuffer[ObservableInputStreamListener]

  def addListener(l: ObservableInputStreamListener) {
    listeners += l
    l.init(this, length)
  }

  def removeListener(l: ObservableInputStreamListener) { listeners -= l }

  override def read(): Int = {
    // intentionally calling output to use the most efficient write() method possible (see FilterOutputStream docs)
    val result = input.read()
    if (result != -1)
      listeners.foreach(_.read(this, Array(result.asInstanceOf[Byte]), 0, 1, result))
    result
  }

  override def read(b: Array[Byte], off: Int, len: Int): Int = {
    // intentionally calling output to use the most efficient write() method possible (see FilterOutputStream docs)
    val result = input.read(b, off, len)
    listeners.foreach(_.read(this, b, off, len, result))
    result
  }

  override def read(b: Array[Byte]): Int = {
    // intentionally calling output to use the most efficient write() method possible (see FilterOutputStream docs)
    val result = input.read(b)
    listeners.foreach(_.read(this, b, 0, b.length, result))
    result
  }

  override def close() {
    super.close()

    listeners.foreach(_.close(this))
  }
}

trait ObservableInputStreamListener extends EventListener {

  /**
   * Called when ObservableInputStream is first created.
   */
  def init(input: ObservableInputStream, length: Option[Long]) {}

  /**
   * All read calls on the observed stream will be passed through this method.
   *
   * @param readResult The return value of the read() call to the underlying stream.
   */
  def read(input: ObservableInputStream, b: Array[Byte], off: Int, len: Int, readResult: Int) {}

  /**
   * Called after the stream is closed.
   */
  def close(input: ObservableInputStream) {}
}

class NullObservableInputStreamListener extends ObservableInputStreamListener

class MultiObservableInputStreamListener
  extends AbstractMultiObserver[ObservableInputStreamListener] with ObservableInputStreamListener {

  override def init(input: ObservableInputStream, length: Option[Long]) {
    observers.foreach(_.init(input, length))
  }

  override def read(input: ObservableInputStream, b: Array[Byte], off: Int, len: Int, readResult: Int) {
    observers.foreach(_.read(input, b, off, len, readResult))
  }

  override def close(input: ObservableInputStream) { observers.foreach(_.close(input)) }
}
