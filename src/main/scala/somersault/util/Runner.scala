package somersault.util

/**
 * Helper class to convert a delegate into a {@link Runnable}. Useful for inter-operating with regular old Java code.
 */
class Runner( op: () => Unit) extends Runnable
{
  def run( )
  {
    op( )
  }
}