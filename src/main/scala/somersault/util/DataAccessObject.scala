package somersault.util

import javax.sql.DataSource

/**
 * A basic data access object, designed to create some commonality between all DAO implementaitons.
 */

trait DataAccessObject
{
  /**
   * Creates the initial schema for the database. This should be called after class initialization and the construction
   * of the DataSource.
   *
   * This should be called only for new databases, or be written to handle existing databases. This will be called
   * automatically be createDataSource() if this is a new database.
   *
   * This is not called from this class because other initialization may need to occur.
   */
  def createSchema( dataSource: DataSource ) =
    {}

  /**
   * Called to initialize the database after the DataSource has been created. This will be called after createSchema(),
   * regardless of whether the database is new or not.
   */
  def initialize( dataSource: DataSource ) =
    {}
}