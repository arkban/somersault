package somersault.util

import java.io.InputStream
import java.security.{DigestInputStream, MessageDigest}
import java.util.Arrays
import org.apache.commons.io.output.NullOutputStream
import org.apache.commons.lang.StringUtils
import somersault.core.{StreamPump, StreamPumpData}

/**
 * A handy wrapper class for a hash or message digest, with useful utility methods.
 */
class Hash(val info: HashInfo, private val data: Array[Byte]) {

  def this(md: MessageDigest) = this(new HashInfo(md.getAlgorithm, md.getDigestLength), md.digest)

  private def canEqual(other: Any): Boolean = other.isInstanceOf[Hash]

  override def equals(other: Any): Boolean =
    other match {
      case that: Hash => this.canEqual(that) && info == that.info && Arrays.equals(data, that.data)
      case _ => false
    }

  override def hashCode: Int = Arrays.hashCode(data)

  override def toString: String = toString(hashInfoPrefix = true)

  /**
   * Returns a copy of the bytes stored in this instance.
   */
  def toBytes: Array[Byte] = Arrays.copyOf(data, data.length)

  def toString(hashInfoPrefix: Boolean): String =
    if (hashInfoPrefix) String.format("Hash(name=%1$s, value=%2$s)", info.name, Hash.toHex(data))
    else Hash.toHex(data)
}

object Hash {

  /**
   * Creates a hash for the provided HashInfo and InputStream.
   */
  def create(hashInfo: HashInfo, input: InputStream): Hash = {
    val digestIn = new DigestInputStream(input, hashInfo.createDigest())

    new StreamPump().pump(digestIn, new NullOutputStream, new StreamPumpData(true, None))

    new Hash(hashInfo, digestIn.getMessageDigest.digest)
  }

  /**
   * Converts the provided byte array into a hexadecimal string, with each entry separated by a space. This method was
   * designed to be used on byte arrays before comparison, because its easy to compare strings and if there is a
   * comparison failure the resultant error is easy to read.
   */
  def toHex(value: Array[Byte]): String =
    value.map(b => StringUtils.leftPad(Integer.toHexString(b + 128), 2, '0')).mkString(" ")
}

class HashMismatchException private(message: String, val expected: Hash, val actual: Hash)
  extends Exception(message) {

  def this(expected: Hash, actual: Hash) = this(
    String.format("%1$s does not match expected %2$s", actual, expected), expected, actual)
}
