package somersault.util

/**
 * Executes the provided Function, and repeats the operation if an exception is thrown up to the number of configured
 * times. If all attempts throw an exception, the final exception is thrown as an inner exception. This class is
 * intended to help execute logic that can occasionally fail but generally does not.
 */

class Repeater[+R](func: () => R, count: Int) {

  def repeat(): R = {
    var attempt = 0
    while (attempt < count) {
      try {
        attempt += 1
        return func()
      }
      catch {
        case ex: Exception =>
          if (attempt >= count)
            throw new RepeaterException(attempt, ex)
      }
    }

    throw new Exception("Repeater failed but not via an exception")
  }
}

class RepeaterException(val attempts: Int, ex: Exception)
  extends Exception("Failed after " + attempts + " attempts", ex)