package somersault.util

import java.security.MessageDigest
import nu.xom.Element
import org.apache.commons.lang.math.NumberUtils
import persist.{PersisterContext, Persister}

/**
 * Information about a hash algorithm.
 *
 * @param name Name of the hash algorithm, that can be used by MessageDigest.getInstance().
 * @param size Size of the hash, in bytes.
 */
class HashInfo(val name: String, val size: Int) {

  def this(md: MessageDigest) = this(md.getAlgorithm, md.getDigestLength)

  def this(p: AnyRef) = this("name", 0)

  private def canEqual(other: Any): Boolean = other.isInstanceOf[HashInfo]

  override def equals(other: Any): Boolean =
    other match {
      case that: HashInfo => this.canEqual(that) && name == that.name && size == that.size
      case _ => false
    }

  override def hashCode: Int = 41 * name.hashCode + size

  override def toString: String = "HashInfo(name=%1$s, size=%2$d)".format(name, size)

  /**
   * Creates a MessageDigest. This method should not throw exceptions (such as NoSuchAlgorithmException) because the
   * algorithm name has been already been verified.
   */
  def createDigest(): MessageDigest = MessageDigest.getInstance(name)
}

object HashInfo {

  val md5 = new HashInfo("MD5", 16)

  val sha1 = new HashInfo("SHA1", 20)
}

class HashInfoMismatchException private(message: String, val expected: HashInfo, val actual: HashInfo)
  extends Exception(message) {

  def this(expected: HashInfo, actual: HashInfo) = this(
    String.format("%1$s does not match expected %2$s", actual, expected), expected, actual)
}

class HashInfoPersister extends Persister {

  def canRead(context: PersisterContext, element: Element): Boolean = checkClass(element, classOf[HashInfo])

  def read[T](context: PersisterContext, element: Element): T = {
    val name = element.getAttributeValue("name")
    val size = NumberUtils.toInt(element.getAttributeValue("size"))
    new HashInfo(name, size).asInstanceOf[T]
  }

  def write(context: PersisterContext, obj: Object, element: Element, dynamic: Boolean = false) {
    obj match {
      case hashInfo: HashInfo =>
        writeClass(obj, element, dynamic)
        Persister.createAttribute("name", hashInfo.name, element)
        Persister.createAttribute("size", hashInfo.size.toString, element)
      case _ => throw new IllegalArgumentException("cannot write: " + obj.getClass.getName)
    }
  }
}

