package somersault.util

import nu.xom.Element
import org.apache.commons.lang.math.NumberUtils
import persist.{PersisterContext, Persister}

/**
 * Utility class to hold versioning information.
 *
 * @param major Should change when major functionality or architecture changes.
 * @param minor Should change when minor functionality is added or removed, or between significant events.
 * @param release Should change for each released schemaVersion of the software. Changes should be minor, mostly bug fixes
 * and tweaks.
 */
class Version( val major: Int, val minor: Int, val release: Int)
{
  private def canEqual( other: Any): Boolean = other.isInstanceOf[ Version ]

  override def equals( other: Any): Boolean =
    other match
    {
      case that: Version =>
        ( this.canEqual( that)
            && major == that.major
            && minor == that.minor
            && release == that.release )
      case _ => false
    }

  override def hashCode: Int = 41 * ( 41 * major + minor ) + release

  override def toString: String = String.format(
    "%d.%d.%d", Predef.int2Integer( major), Predef.int2Integer( minor), Predef.int2Integer( release))
}

object Version
{
  /**
   * The current version of Somersault.
   */
  val current = new Version( 0, 3, 0)
}

class VersionMismatchException private( message: String, expected: Version, actual: Version)
    extends Exception( message)
{
  def this( expected: Version, actual: Version) = this (
    String.format( "Version %s does not match expected version %s", actual, expected), expected, actual)
}

class VersionPersister extends Persister
{
  def canRead( context: PersisterContext, element: Element) = checkClass( element, classOf[ Version ])

  def read[ T ]( context: PersisterContext, element: Element) =
  {
    val major = NumberUtils.toInt( element.getAttributeValue( "major"))
    val minor = NumberUtils.toInt( element.getAttributeValue( "minor"))
    val release = NumberUtils.toInt( element.getAttributeValue( "release"))
    new Version( major, minor, release).asInstanceOf[ T ]
  }

  def write( context: PersisterContext, obj: Object, element: Element, dynamic: Boolean)
  {
    obj match
    {
      case v: Version =>
        writeClass( obj, element, dynamic)
        Persister.createAttribute( "major", v.major.toString, element)
        Persister.createAttribute( "minor", v.minor.toString, element)
        Persister.createAttribute( "release", v.release.toString, element)
      case _ => throw new IllegalArgumentException( "cannot persist: " + obj.getClass.getName)
    }
  }
}